/*
* Name    : appTaskCreation 
* Company : ET Marlabs
* Purpose : This class is used to assign Task to the users HL
* Author  : Subas
*/
trigger appTaskCreation on Task (after update,before update, before delete) {
    String objectType ='';
    
    if(Trigger.IsAfter && Trigger.IsUpdate && !TaskFlow_Helper.TASK_TRIGGER_RUNNING){
        for(Task t : Trigger.new){
            if(t.whatID<>null){
                Id applicationId = t.whatID;
                objectType = String.ValueOf(applicationId.getsobjecttype());
                if(objectType == 'genesis__Applications__c' & t.Status =='Completed'){
                    //System.debug('@@@@'+applicationId+'!!!'+t.Subject);
                    System.debug('@@@@'+t.Subject);
                    TaskFlow_Helper.createTaskApp(applicationId,t.Subject,t);
                    Retry_Task_call.recallTask(t);
                    Retry_Task_call.ValidateTask(t);
                }
            }
        } 
    }
    // upload EC validation on before update status to completed
    if(Trigger.IsBefore && Trigger.IsUpdate && !TaskFlow_Helper.TASK_TRIGGER_RUNNING){
        for(Task t : Trigger.new){
            if(t.whatID<>null){
                
                Id applicationId = t.whatID;
                
                objectType = String.ValueOf(applicationId.getsobjecttype());
                
                if(objectType == 'genesis__Applications__c' & t.Status =='Completed'){
                
                    Retry_Task_call.ValidateTask(t);
                    
                }
            }
        } 
    }


    if( Trigger.IsBefore && Trigger.isDelete && !TaskFlow_Helper.TASK_TRIGGER_RUNNING ){
        for(Task t : Trigger.old){
                if( t.NonDeletable__c){
                    TaskFlow_Helper.preventTaskDeletion(t);
                }
            }
        }
    }