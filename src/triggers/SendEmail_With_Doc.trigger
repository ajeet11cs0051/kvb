trigger SendEmail_With_Doc on Attachment (after insert) {
    String objectType ='';
    for(Attachment attach : Trigger.new){
        Id applicationId = attach.ParentId;
        objectType = String.ValueOf(applicationId.getsobjecttype());
        if(objectType == 'genesis__Applications__c'){
            SendEmailHandler.SendEmail(attach.ParentId,attach); //Call for Email
        }
    }
}