trigger ApplicationTrigger on genesis__Applications__c (after update, before update, after insert, before insert) {

    if(Utility.runApplicationTrigger()){
        //Handler singleton object
        ApplicationTriggerHandler appTriggerHandlerObj = ApplicationTriggerHandler.getInstance();
        
        // After Update 
        if(Trigger.isUpdate && Trigger.isAfter &&  ApplicationTriggerHandler.IsFirstRun ){
            appTriggerHandlerObj.AfterUpdateCls(Trigger.new,Trigger.oldMap,Trigger.old,Trigger.newMap);
        }
        
         // After Insert
        if(Trigger.isInsert && Trigger.isAfter  && ApplicationTriggerHandler.IsFirstRun){
            appTriggerHandlerObj.AfterInsertCls(Trigger.new);       
        }
        
         // Before Update
        if(Trigger.isUpdate && Trigger.isBefore  && ApplicationTriggerHandler.IsFirstRun){
             appTriggerHandlerObj.BeforeUpdateCls(Trigger.new,Trigger.newMap,Trigger.oldMap);
        }
        
         // Before Insert 
        if(Trigger.isBefore && Trigger.isInsert && ApplicationTriggerHandler.IsFirstRun){
             appTriggerHandlerObj.BeofreInsertCls(Trigger.new);
        }
    }
    
}