trigger DocumentAPplicantCreation on Digio_Document_ID__c (before insert) {
    
    
      if(Trigger.IsInsert & Trigger.Isbefore){
        EsignApplicantsMapping.ApplicantCreating(Trigger.new);
    }

}