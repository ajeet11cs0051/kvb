trigger DocCategoryJunctionTrigger on genesis__AppDocCatAttachmentJunction__c (before insert,before update,after insert) {
    if(Trigger.IsInsert & Trigger.Isbefore){
        UpdateDocName_Handler.getDocName(Trigger.new);
    }
    if(Trigger.IsInsert & Trigger.IsAfter){
        //Delete other document and documentCategoryJunctionObject against document category after document insert
        DocumentFetch.deleteDocAndDocumentCategory(Trigger.newMap);
    }
}