@isTest
public class Test_ApplicationService {
    public static genesis__Applications__c genApp;
    public static Account acc;
    
    @isTest
    public static void init (){
      
        genApp = TestUtility.intialSetUp('Digital Home Loan – 777', true);
        acc = new Account(name = 'Testname');
    }
    @isTest
    public static void methodCreateApplication(){
        
        try{
            init();
         KVB_Company_Details__c k =KVB_Company_Details__c.getOrgDefaults();
        ApplicationService.createApplication(acc.Id, 'Home Loan','123','Venu');
       
        }
        catch(exception e){
            
        }
    }
    @isTest
    public static void methodGetApplication(){
      
        init();
        genApp.genesis__Account__c = acc.Id;
        UPDATE genApp;
        ApplicationService.getApplication(genApp.Id);
        
    }
}