@RestResource(urlMapping='/CreatePerfios')

global class Ws_PerfiosAccNumbers {
    
     global class Response extends WS_Response{
        
      
        public List<Perfios__c> Bank_AccountNumbers;
   
    }
 
    Public cls_PerfDetails PfDetails;
    
    public class cls_PerfDetails{
        Public string ClientID;
        public List<BankWrapper> Account_Numbers;
        public string BankName;
        public string ApplicationId;
    }
    public class BankWrapper{
        Public string BankAccNumber;
         public string BankName;
    }
    
    @HttpPost
    global static Response CreateBankaccounts(){
        
        RestRequest req      = Restcontext.Request;
        Response res         = new Response();    
        
        if(req == null || req.requestBody == null){
            res.status          = Constants.WS_ERROR_STATUS;
            res.errorMessage    = Constants.WS_REQ_BODY_IS_NULL;
            res.statusCode      = Constants.WS_ERROR_CODE;
            return res;            
        }else{
            
            try{
                 //Application Id
                 //string AppID = req.params.get('AppID');
                 //genesis__Applications__c apps = queryService.getApp(AppID);

                 String jsonData  = req.requestBody.toString(); 
                 system.debug('jsonData:::'+jsonData);
                
                cls_PerfDetails Cperfios=(cls_PerfDetails)Json.deserialize(jsonData, cls_PerfDetails.class);
                system.debug('Cperfios'+Cperfios);
                List<Perfios__c> pf=new List<Perfios__c>();
                AppAccountNMIJunction__c accAppNMIJunction = new AppAccountNMIJunction__c();
                for(BankWrapper reqobj :Cperfios.Account_Numbers){
                    Perfios__c p=new Perfios__c();
                    if(!Utility.ISStringBlankorNull(reqobj.BankAccNumber))
                    p.Bank_Account_Number__c =reqobj.BankAccNumber;
                    if(!Utility.ISStringBlankorNull(Cperfios.ClientID))
                        p.Applicant_Name__c=Cperfios.ClientID;
                     if(!Utility.ISStringBlankorNull(reqobj.BankName))
                        p.Bank_Name__c=reqobj.BankName;
                     if(!Utility.ISStringBlankorNull(Cperfios.ApplicationId))
                        {p.Application__c = Cperfios.ApplicationId;
                        p.Application_id__c = Cperfios.ApplicationId;}
                     //if(!Utility.ISStringBlankorNull(apps.id))
                        //p.Application_id__c = apps.id;      
                    pf.add(p);
                }
                    System.debug('pf:::'+pf);
                    if(pf.size() > 0){
                        insert pf;
                        accAppNMIJunction = queryService.getNMIJunction(Cperfios.ClientID,Cperfios.ApplicationId);
                        //if(accAppNMIJunction == null){
                            //accAppNMIJunction.Account__c = Cperfios.ClientID;
                            //accAppNMIJunction.Application__c = Cperfios.ApplicationId;
                            //insert accAppNMIJunction;
                        //}
                    }
                res.Bank_AccountNumbers=pf;
                System.debug('res:::'+res);
             return res;
            }
            catch(exception e){
                system.debug('Error  :'+e.getStackTraceString()+'Line number  :'+e.getLineNumber());
            }
        }
        return res;
    }
}