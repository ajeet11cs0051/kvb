@RestResource(urlMapping='/GetPerfiosDetails')
global class Ws_PerfiosGetDetaisl {
    
    global class DetailResponse extends WS_Response{
      
        public perfiosDetails PDetails;
        public DetailResponse (){
            PDetails    = New perfiosDetails();
        }
    }
    public class perfiosDetails {
        public string Income;
        public List<BankAccNumber> BankAccountNumbers; 
        public string shouldUploadITR; 
        //public string isITRRequired;
        public string AdditionalIncome;
        public string ApplicationID;
        
    }
    public class BankAccNumber{
        
        public string BankName;
        public string ShouldUploadBS;
        public string ShouldRetryBS;
        public string BankAccountNumber;
    }
    @httpGet
    global static DetailResponse getDetails(){
        
        RestRequest req = Restcontext.Request;
        RestResponse restRes = Restcontext.response;
        try{ 
            DetailResponse res=new DetailResponse();
            string AccID=req.params.get('AccID');
            //Application ID
            string AppID = req.params.get('AppID');
            system.debug('#####'+AppID+'@@@@@@@@'+AccID);
            Decimal TotalAvgBalance6Month =0 ;
            Decimal TotalChequeBounce = 0;
            Decimal SelfEmpNMI = 0;
            
            perfiosCalculations.CalculateBS(AccID,AppID);
            
            account acc =[select id,ITR_Uploaded__c,Income_Computation_From__c,Computed_NMI__c,NMI_as_per_26AS__c,NMI_as_per_BS__c,NMI_as_per_ITR__c,Total_ChequeECS_bounces__c,Average_Balances6_months__c,Employment_Type__c,(select id,SelfEmpNMI__c,Failure_Reason__c,Bank_Name__c,AvgBalance_Of_6_Months__c,Cheque_Bounces__c,Bank_Account_Number__c,Upload_Bank_Statement__c from Perfios__r) from account where id=:AccID];
            

            List<Perfios__c> ListofPerfios = [Select SelfEmpNMI__c,Failure_Reason__c,Bank_Name__c,AvgBalance_Of_6_Months__c,Cheque_Bounces__c,Bank_Account_Number__c,Upload_Bank_Statement__c,Applicant_Name__c,Application__c from Perfios__c where Applicant_Name__c=:AccID AND Application__c=:AppID ];
            
            system.debug('##### '+acc.Perfios__r);
            genesis__Applications__c apps = queryService.getApp(AppID);
            
            AppAccountNMIJunction__c accAppNMIJunction = queryService.getNMIJunction(AccID,AppID);
            system.debug('##### '+accAppNMIJunction);
            // Comented as per discussion with Nirali & Vivek dated: 2018_06_01
            /*if(acc.Employment_Type__c ==Constants.EmpType_Salaried){
                 res.PDetails.isITRRequired='false';
            }
            else{
                  res.PDetails.isITRRequired='true';

            }*/
            res.PDetails.shouldUploadITR=string.valueOf(acc.ITR_Uploaded__c);
            res.PDetails.ApplicationID = string.valueOf(apps.id);
            res.PDetails.BankAccountNumbers= new list<BankAccNumber>();
            for(Perfios__c p:ListofPerfios){
                 system.debug('#####'+p);
                BankAccNumber b=new BankAccNumber();
                b.ShouldUploadBS=string.valueOf(p.Upload_Bank_Statement__c);
                b.ShouldRetryBS=string.valueOf(p.Failure_Reason__c);
                b.BankName=string.valueOf(p.Bank_Name__c);
                b.BankAccountNumber=string.valueOf(p.Bank_Account_Number__c);
                res.PDetails.BankAccountNumbers.add(b);
                if(!Utility.ISStringBlankorNull(String.valueOf(p.AvgBalance_Of_6_Months__c)))
                TotalAvgBalance6Month = TotalAvgBalance6Month + p.AvgBalance_Of_6_Months__c;
                if(!Utility.ISStringBlankorNull(String.valueOf(p.Cheque_Bounces__c)))
                TotalChequeBounce =TotalChequeBounce + p.Cheque_Bounces__c;
                if(!Utility.ISStringBlankorNull(String.valueOf(p.SelfEmpNMI__c)))
                SelfEmpNMI = SelfEmpNMI + p.SelfEmpNMI__c;
            }
            
            
            if(acc.Employment_Type__c <>Constants.EmpType_Salaried && acc.Employment_Type__c <> null){
                if(Utility.ISStringBlankorNull(String.valueOf(acc.NMI_as_per_ITR__c)))
                  {   SelfEmpNMI = 0;
                    }else{SelfEmpNMI = Math.min(SelfEmpNMI, (acc.NMI_as_per_ITR__c * 1.2));}
                acc.NMI_as_per_BS__c = SelfEmpNMI;
                accAppNMIJunction.NMI_as_per_BS__c = SelfEmpNMI;
                update accAppNMIJunction;
            }
            acc.Average_Balances6_months__c = TotalAvgBalance6Month;
            acc.Total_ChequeECS_bounces__c = TotalChequeBounce;

            
            if(!Utility.ISStringBlankorNull(String.valueOf(acc.NMI_as_per_ITR__c)) && !Utility.ISStringBlankorNull(String.valueOf(acc.Computed_NMI__c)) && (acc.Computed_NMI__c == (acc.NMI_as_per_ITR__c).SetScale(2) )){
                acc.Income_Computation_From__c = 'ITR';
                }else if(!Utility.ISStringBlankorNull(String.valueOf(acc.NMI_as_per_BS__c)) && !Utility.ISStringBlankorNull(String.valueOf(acc.Computed_NMI__c)) && (acc.Computed_NMI__c == (acc.NMI_as_per_BS__c).SetScale(2))){
                 acc.Income_Computation_From__c = 'Bank Statement';
                    }else if(!Utility.ISStringBlankorNull(String.valueOf(acc.NMI_as_per_26AS__c))){
                        acc.Income_Computation_From__c = 'Form 26 AS';
                        }
            
            
            update acc;

            
            system.system.debug('##### Accs -->'+ acc + 'AccNMI Junction -->' +accAppNMIJunction);
            accAppNMIJunction = queryService.getNMIJunction(AccID,AppID);
            res.PDetails.Income=string.valueOf(accAppNMIJunction.Computed_NMI__c);
            return res;
        }
        catch(QueryException e){
            DetailResponse res = new DetailResponse();
            system.debug('Error'+e.getStackTraceString()+'Line Number'+e.getLineNumber());
            return res;
        }
       
    }
}