/* 
* Name     : ApplicationTriggerHelper
* Purpose  : Helper class for Application trigger handler
* Company  : ET Marlabs
* Author   : Dushyant
*/
public class ApplicationTriggerHelper {
    //Get SME renewal record type id be name
    static String recordtypeIdSME = SOQL_Util.getRecordTypeId('SME_Renewal');
    
    ///CBS_ApiCalls
    public static void CBS_ApiCall(map<Id,genesis__Applications__c> newMap,map<Id,genesis__Applications__c> oldMap){
        List<Id> updatedAppIdList = new List<Id>();
        for(genesis__Applications__c app : newMap.values()){
            if(app.Application_Stage__c != oldMap.get(app.Id).Application_Stage__c && (app.Application_Stage__c == 'Sanction complete' || app.Application_Stage__c == 'Sanction complete- Final Sanction')){
                updatedAppIdList.add(app.Id);
            }
        }
        List<genesis__Applications__c> updatedAppList = new List<genesis__Applications__c>();
        updatedAppList = [SELECT Id,(SELECT Id,Is_New__c,genesis__Party_Account_Name__r.CBS_Customer_ID__c,genesis__Party_Account_Name__c,
                                     genesis__Party_Account_Name__r.Branch_Code__c,genesis__Party_Account_Name__r.Customer_IC__c,
                                     genesis__Party_Account_Name__r.Pan_Number__c,genesis__Party_Account_Name__r.Voter_Id__c,
                                     genesis__Party_Account_Name__r.Passport_Number__pc,genesis__Party_Account_Name__r.Driving_License_ID__c,
                                     genesis__Party_Account_Name__r.National_Identification_Code__c,genesis__Party_Account_Name__r.Other_identification_number__c,
                                     genesis__Party_Account_Name__r.CreatedDate,genesis__Party_Account_Name__r.Customer_ID_Creation_Date__c,
                                     genesis__Party_Account_Name__r.Telephone_Bill__c,genesis__Party_Account_Name__r.Ration_Card__c,
                                     genesis__Party_Account_Name__r.Employment_Letter__c,genesis__Party_Account_Name__r.Same_Present_Address_Permanent_Address__c,
                                     genesis__Party_Account_Name__r.Bank_Statement__c,genesis__Party_Account_Name__r.Annual_TurnoverIncome__c,genesis__Party_Account_Name__r.Aadhaar_Number__pc FROM genesis__Application_Parties__r WHERE Active__c = true) FROM genesis__Applications__c WHERE Id IN :updatedAppIdList];
        if(!updatedAppIdList.isEmpty()){
            SME_CBS_Handler.eKYC_Updation(updatedAppIdList);
            
            SME_CBS_Handler.limitUpdation_ChargeColllection(updatedAppIdList);
        }
    }
    
    //Send SMS and task creation for SME customer
    public static void sendSMSandtaskCreation(map<Id,genesis__Applications__c> appNewMap,map<Id,genesis__Applications__c> appOldMap){
        Set<String> applicationIds  = new Set<String>();
        List<genesis__Applications__c> updateAppList = new List<genesis__Applications__c>();
        List<Account> accountsToupdate    = new List<Account>();
        for(genesis__Applications__c app : appNewMap.values()){
            if(String.ValueOf(app.RecordTypeId) == recordtypeIdSME){
                if(appOldMap.get(app.id).Application_Stage__c != appNewMap.get(app.id).Application_Stage__c){
                    applicationIds.add(app.Id);                
                    updateAppList.add(app);
                } 
                if(appOldMap.get(app.id).Industry_type_Application__c!= appNewMap.get(app.id).Industry_type_Application__c){
                    accountsToupdate.add(new Account(id=app.genesis__Account__c,Industry_Type__c=app.Industry_type_Application__c));
                }
            }  
        }
        // Send SMS 
        if(applicationIds.size() > 0) SME_SendSMSService.sendSMSApp_SME(applicationIds); 
        
        //task creation
        if(updateAppList.size()>0) AssignTask.taskAssignment(updateAppList);
        
        if(accountsToupdate.size()>0) {
            AccountTriggerHandler.isAccountTrigger = true;
            update accountsToupdate;
        }
    }
    
    // Added by Raushan.
    // For Update Application Sub Stage critical Change. 
    public static void updateAppSubStage(map<Id,genesis__Applications__c> appNewMap,map<Id,genesis__Applications__c> appOldMap){
        //List<genesis__Applications__c> updateAppList = new List<genesis__Applications__c>();
         //for Current user
         Id userId  =   UserInfo.getUserId();
         User userObj   =   [select id,Designation__c from User where id =:userId]; 
        for(genesis__Applications__c app : appNewMap.values()){
            if(String.ValueOf(app.RecordTypeId) == recordtypeIdSME){
                if(app.Application_Stage__c ==  'Application review-Critical changes'){
                    
                    if(userObj.Designation__c=='CLPC Head' && app.Financials_upload_check_done__c==true){
                        app.Application_Stage__c = 'Perfios upload pending';
                        app.Sub_Stage__c = '';
                        app.Application_Status__c='';
                        app.CLPC_review_done__c = true;                        
                    }
                    else if(userObj.Designation__c=='CLPC Head' && app.Financials_upload_check_done__c==false){
                        app.Application_Stage__c = 'Customer follow up- Critical changes';
                        app.Sub_Stage__c = 'Re-upload financials ';
                        app.Application_Status__c='';
                        app.CLPC_review_done__c = true;                        
                    }
                    /*else if(app.Constitution_change_done__c==true && app.Financials_upload_check_done__c==true && app.Key_person_change_done__c==true && app.Section_20_deone_change__c==true){
                        app.Sub_Stage__c = '';
                    }else{
                        if(app.Constitution_change_done__c==false && app.Financials_upload_check_done__c==true && app.Key_person_change_done__c==true && app.Section_20_deone_change__c==true){
                            app.Sub_Stage__c = 'Constitution change decline';
                            app.Critical_Change_done__c = true;
                        }
                        else if(app.Constitution_change_done__c==true && app.Financials_upload_check_done__c==false && app.Key_person_change_done__c==true && app.Section_20_deone_change__c==true){
                            app.Sub_Stage__c = 'Re-upload financials';
                            app.Critical_Change_done__c = true;
                        }
                        else if(app.Constitution_change_done__c==true && app.Financials_upload_check_done__c==true && app.Key_person_change_done__c==false && app.Section_20_deone_change__c==true){
                            app.Sub_Stage__c = 'Key person change decline';
                            app.Critical_Change_done__c = true;
                        }
                        else if(app.Constitution_change_done__c==true && app.Financials_upload_check_done__c==true && app.Key_person_change_done__c==true && app.Section_20_deone_change__c==false){
                            app.Sub_Stage__c = 'Section 20 change decline';
                            app.Critical_Change_done__c = true;
                        }
                        else {
                            app.Sub_Stage__c = 'More than 1 critical change decline';
                            app.Critical_Change_done__c = true;
                        }
                    }  */     
                }
               
            }  
        }
    } 
    //Reset sign info on change of application stage to 'Limit extended'
    public static void resetSignInfo(map<Id,genesis__Applications__c> newMap,map<Id,genesis__Applications__c> oldMap){
        for(genesis__Applications__c app : newMap.values()){
            if(app.Application_Stage__c != oldMap.get(app.Id).Application_Stage__c && app.Application_Stage__c == 'Limit extended'){
                app.Sanction_ESign_Id__c = '';
                app.Sanction_Class2_Check__c = false;
                app.Sanction_ESign_Check__c = false;
                app.Sanction_Doc_Generation_Check__c = false;
                app.All_Party_Signed__c = false;
                app.Document_Page_Count__c = 0;
                app.Sanction_Letter_Name__c             ='';
            }
        }
    }
    //Delete Financial Data
    public static void deleteFinancialData(map<Id,genesis__Applications__c> newMap,map<Id,genesis__Applications__c> oldMap){
        List<genesis__Applications__c> appList = new List<genesis__Applications__c>();
        
        for(genesis__Applications__c app : newMap.values()){
            if(app.recordType.DeveloperName == Constants.SME_APP_RECORD_TYPE && app.Application_Stage__c != oldMap.get(app.Id).Application_Stage__c && ((app.Application_Stage__c == 'Customer follow up- Critical changes'  && app.Sub_Stage__c != oldMap.get(app.Id).Sub_Stage__c && app.Sub_Stage__c == 'Re-upload financials') || app.Application_Stage__c == 'Perfios error')){
                appList.add(app);
            }
        }
        if(!appList.isEmpty()){
            deleteFinancialDataHelper(appList);
        }
        
    }
    public static void deleteFinancialDataHelper(List<genesis__Applications__c> appList){
        List<Id> attIdList = new List<Id>();
        List<Id> appIdList = new List<Id>();
        List<genesis__AppDocCatAttachmentJunction__c> docJuncCatList = new List<genesis__AppDocCatAttachmentJunction__c>();
        List<String> docCatNameList = new List<String>{'Audited-balance sheet document last FY','Trading account document last FY','P&L document last FY','Form 3CA/3CB/3CD'};
            if(!appList.isEmpty()){
                for(genesis__Applications__c app : appList){
                    appIdList.add(app.id);
                }
            }
        if(!appIdList.isEmpty()){
            docJuncCatList = [SELECT Id,genesis__AttachmentId__c FROM genesis__AppDocCatAttachmentJunction__c WHERE genesis__Application_Document_Category__r.genesis__Application__c IN :appIdList AND genesis__Application_Document_Category__r.Name IN :docCatNameList];
        }
        if(!docJuncCatList.isEmpty()){
            for(genesis__AppDocCatAttachmentJunction__c juncCat : docJuncCatList){
                attIdList.add(juncCat.genesis__AttachmentId__c);
            }
        }
        if(!attIdList.isEmpty()){
            DELETE [SELECT Id FROM Attachment WHERE Id IN :attIdList];
        }
        if(!docJuncCatList.isEmpty()){
            DELETE docJuncCatList;
        }
    }
    public static void clearPerfiosTxId(map<Id,genesis__Applications__c> newMap,map<Id,genesis__Applications__c> oldMap){
        List<Id> accIdList = new List<Id>();
        List<Perfios__c> perfList = new List<Perfios__c>();
        //Perfios__c perf = 
        for(genesis__Applications__c app : newMap.values()){
            if(app.recordType.DeveloperName == Constants.SME_APP_RECORD_TYPE && app.Application_Stage__c != oldMap.get(app.Id).Application_Stage__c && app.Application_Stage__c == 'Perfios error'){
                if(app.genesis__Account__c != null) accIdList.add(app.genesis__Account__c);
            }
        }
        if(!accIdList.isEmpty())
            perfList = [SELECT Id,Transaction_Id__c FROM Perfios__c WHERE Applicant_Name__c IN :accIdList];
        if(!perfList.isEmpty()){
            for(Perfios__c perf : perfList){
                perf.Transaction_Id__c = '';
            }
            UPDATE perfList;
        }
    }
    
    //Date capture for TAT claculation(SME)
    public static void tatCapture(map<Id,genesis__Applications__c> newMap,map<Id,genesis__Applications__c> oldMap){
         for(genesis__Applications__c app : newMap.values()){
            if(app.recordType.DeveloperName == Constants.SME_APP_RECORD_TYPE && app.Application_Stage__c != oldMap.get(app.Id).Application_Stage__c){
                //Capture Initiated date
                if(app.Application_Stage__c == 'Application filling initiated' || app.Application_Stage__c == 'Application Filling - Final' || app.Application_Stage__c == 'Interim Sanction'){
                    if(app.Application_UI_Stage__c != oldMap.get(app.Id).Application_UI_Stage__c && app.Application_UI_Stage__c == 'CompanyDetailsView'){
                        app.Application_initiated__c = Date.today();
                    }
                }
                //Capture Submitted date
                if(app.Application_UI_Stage__c != oldMap.get(app.Id).Application_UI_Stage__c && app.Application_UI_Stage__c == 'Submission'){
                    app.Application_submitted__c = Date.today();
                }
            }
        }
    }
}