/*
 * Description : SME Ehancement Request API
 * Author       : Amritesh
 */
@RestResource(urlMapping = '/getApplicationDetails')
global class WS_FetchSMEInfo {

    //Response structure
    global class Response extends WS_Response {
        Customer360view customerFullView;
        public Response() {
            customerFullView = new Customer360view();
        }
    }
    public class ReqWrapObj {
        public String CUSTOMER_ID;
        public String LOS_APPLICATION_ID;
        public String APP_TYPE;
        public String TYPE;
        public WS_FetchSMEInfo_Helper.CustomerDetail CustomerDetail;
    }

    @HttpPost
    global static Response getApplicationDetail() {
        RestRequest req = Restcontext.Request;
        Response res = new Response();
        if (req == null || req.requestBody == null) {
            return getWSResponse(res, Constants.WS_ERROR_STATUS, null, Constants.WS_ERROR_CODE, Constants.WS_REQ_BODY_IS_NULL);
        } else {
            try {
                String jsonData = req.requestBody.toString();
                ReqWrapObj reqObj = (ReqWrapObj) Json.deserialize(jsonData, ReqWrapObj.class);
                System.debug('reqdatac:::' + reqobj);
                if (reqObj.Customer_ID != null || reqObj.Customer_ID != '') {
                    string appClone = '';
                    string appETBId = '';
                    string appId = '';
                    string appFetchId = '';

                    if (reqObj.TYPE != null && reqObj.TYPE != '') {

                        if (reqObj.TYPE == 'EAE') {
                            genesis__Applications__c appForInActive = new genesis__Applications__c();

                            if (reqObj.LOS_Application_ID == null || reqObj.LOS_Application_ID == '') {

                                if (reqObj.APP_TYPE != null && reqObj.APP_TYPE != '') {
                                    //Clone Renewal Application for specified APP_TYPE
                                    try {
                                        genesis__Applications__c appLicationObject = new genesis__Applications__c();
                                        System.debug('Inside APP TYPE');
                                        genesis__Applications__c appObject = SOQL_Util.getExistingApplication(reqObj.Customer_ID, reqObj.APP_TYPE);
                                        appId = (String) LandingPageHelper.landingPageRes(reqObj.Customer_ID, true);
                                        If(appObject != null) {
                                            appId = appObject.Id;

                                        } else {

                                            List < genesis__Applications__c > listAppObjectEAE = SOQL_Util.getApplicationDueDate(reqObj.Customer_ID);

                                            If(listAppObjectEAE != null && !listAppObjectEAE.isEmpty()) {

                                                for (genesis__Applications__c appObjectCheck: listAppObjectEAE) {
                                                       System.debug('RecordType Name@@@'+appObjectCheck.recordType.developerName);    
                                                    If(appObjectCheck.recordType.developerName == Constants.SME_NEW_LOAN_RECORD_TYPE) {
                                                        return getWSResponse(res, Constants.WS_ERROR_STATUS, null, Constants.WS_ERROR_CODE, 'Already New Loan Application Created.');

                                                    } else If(appObjectCheck.recordType.developerName == Constants.SME_APP_RECORD_TYPE) {
                                                        appLicationObject.Id = appObjectCheck.Id;
                                                        appLicationObject.Renewal_Due_Date__c = appObjectCheck.Renewal_Due_Date__c;
                                                    }
                                                }
                                                If(appLicationObject.Id != null) {
                                                    
                                                    appClone = ApplicationClone.cloneApplication(appId, reqObj.APP_TYPE);
                                                    
                                                    If(appClone != null && appClone != '') {
                                                        
                                                        If(appLicationObject.Renewal_Due_Date__c != null) {
                                                           
                                                            If(appLicationObject.Renewal_Due_Date__c < (System.today().addDays(90))) {
                                                                appForInActive.id = appClone;
                                                                appForInActive.Application_to_close__c = true;
                                                            }
                                                        }
                                                    }
                                                }
                                                If(appForInActive.id != null) {
                                                    update appForInActive;
                                                }
                                            }
                                        }
                                        If((appId !=null && appId!='') && (appClone ==null || appClone =='')){
                                            
                                            appFetchId = appId;
                                        } else if (appClone != null && appClone != '') {
                                            appFetchId = appClone;
                                        }
                                    } catch (Exception e) {
                                        system.debug('Exception:::::' + e.getMessage() + '!!!' + e.getLineNumber());
                                        return getWSResponse(res, Constants.WS_ERROR_STATUS, null, Constants.WS_ERROR_CODE, 'Application Creation Failed.');
                                    }
                                } else {
                                    return getWSResponse(res, Constants.WS_ERROR_STATUS, null, Constants.WS_ERROR_CODE, 'APP_TYPE is required.');
                                }
                            } else {
                                appFetchId = reqObj.LOS_Application_ID;
                            }
                            if (appFetchId != null && appFetchId != '') {
                                try {
                                    res.customerFullView = Handle_FetchSMEReq_Response.getApplicationInfo(reqObj.Customer_ID, appFetchId);
                                } catch (Exception e) {
                                    system.debug('Exception::' + e.getMessage() + e.getLineNumber());
                                    return getWSResponse(res, Constants.WS_ERROR_STATUS, null, Constants.WS_ERROR_CODE, 'Exception' + e.getMessage() + e.getLineNumber());
                                }
                            }
                        }
                        if (reqObj.TYPE == 'ETB-WC') {
                            //Clone Renewal Application for specified APP_TYPE
                            try {
                                genesis__Applications__c appForInActive = new genesis__Applications__c();
                                genesis__Applications__c appForBundle = new genesis__Applications__c();
                                Map < String, genesis__Applications__c > mapOfApp = new Map < String, genesis__Applications__c > ();
                                Id devRecordTypeId = Schema.SObjectType.genesis__Applications__c.getRecordTypeInfosByName().get(Constants.SME_NEW_LOAN_RECORD_TYPE_LABEL).getRecordTypeId();
                                appETBId = (String) LandingPageHelper.landingPageRes(reqObj.Customer_ID, true);
                                If(appETBId != null && appETBId != '') {
                                    appClone = ApplicationClone.cloneApplication(appETBId, Constants.SME_APP_RECORD_TYPE);
                                    List < genesis__Applications__c > listAppObject = SOQL_Util.getApplicationDueDate(reqObj.Customer_ID);
                                    If(listAppObject != null && !listAppObject.isEmpty()) {
                                        for (genesis__Applications__c appObject: listAppObject) {
                                            If(appObject != null && appObject.Renewal_Due_Date__c != null && appObject.recordType.developerName == Constants.SME_APP_RECORD_TYPE_ENHANCEMENT) {
                                                mapOfApp.put(Constants.SME_APP_RECORD_TYPE_ENHANCEMENT, appObject);
                                            } else If(appObject != null && appObject.Renewal_Due_Date__c != null && appObject.recordType.developerName == Constants.SME_APP_RECORD_TYPE) {
                                                mapOfApp.put(Constants.SME_APP_RECORD_TYPE, appObject);
                                            }
                                        }
                                    }
                                    If(appClone != null && appClone != '') {
                                        appForInActive.id = appClone;
                                        appForInActive.RecordTypeId = devRecordTypeId;
                                        appForInActive.Type__c   = 'ETB-WC';
                                        appForInActive.Application_Stage__c = 'New loans - Application created';
                                        If(mapOfApp != null && mapOfApp.containsKey(Constants.SME_APP_RECORD_TYPE_ENHANCEMENT)) {
                                            appForBundle = mapOfApp.get(Constants.SME_APP_RECORD_TYPE_ENHANCEMENT);
                                        } else If(mapOfApp != null && mapOfApp.containsKey(Constants.SME_APP_RECORD_TYPE)) {
                                            appForBundle = mapOfApp.get(Constants.SME_APP_RECORD_TYPE);
                                        }
                                        If(appForBundle.Renewal_Due_Date__c.Month() < (System.today().Month() + 3)) {
                                            appForInActive.Application_to_close__c = true;
                                            appForInActive.Parent_Application__c = appForBundle.Id;
                                        }
                                        If(appForInActive != null) {
                                            update appForInActive;
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                system.debug('Exception:::::' + e.getMessage() + '!!!' + e.getLineNumber());
                                return getWSResponse(res, Constants.WS_ERROR_STATUS, null, Constants.WS_ERROR_CODE, 'Application Creation Failed.');
                            }
                            if (appClone != null && appClone != '') {
                                try {
                                    res.customerFullView = SMELoan_Helper.getCustomer(appClone, reqObj.Customer_ID);
                                } catch (Exception e) {
                                    system.debug('Exception::' + e.getMessage() + e.getLineNumber());
                                    return getWSResponse(res, Constants.WS_ERROR_STATUS, null, Constants.WS_ERROR_CODE, 'Exception' + e.getMessage() + e.getLineNumber());
                                }
                            }
                        }
                        if (reqObj.TYPE == 'ETB-CA') {
                            System.debug('Request Structure'+reqObj);
                            System.debug('CustomerDetails'+reqObj.CustomerDetail);
                            WS_FetchSMEInfo_Helper.CustomerDetail cusObj = (WS_FetchSMEInfo_Helper.CustomerDetail) reqObj.CustomerDetail;
                            System.debug('cusObj'+cusObj);
                            Savepoint sp = Database.setSavepoint();
                            System.debug('cusObj' + cusObj);
                            String accountId     = '';
                            String applicationId = '';
                            
                            try {
                                If(cusObj != null) {

                                    accountId = WS_FetchSMEInfo_Helper.updateCustomerDetails(cusObj,reqObj.TYPE);
              
                                    If(accountId != null && accountId != '') {

                                        applicationId = WS_FetchSMEInfo_Helper.createApplication(accountId,reqObj.TYPE);
                    
                                    }
                                    If(cusObj.Key_Person_Details !=null && accountId != null && accountId != '' && applicationId != null && applicationId != ''){
                                        WS_FetchSMEInfo_Helper.createParties(accountId,applicationId,cusObj.Key_Person_Details);
                                    }
                                    
                                }

                            } catch (Exception e) {
                                Database.rollback(sp);
                                system.debug('Exception:::::' + e.getMessage() + '!!!' + e.getLineNumber());
                                return getWSResponse(res, Constants.WS_ERROR_STATUS, null, Constants.WS_ERROR_CODE, 'Customer Details Creation Failed.');
                            }
                            If(accountId != null && accountId != '' && applicationId != null && applicationId != '') {
                                try {
                                        res.customerFullView = SMELoan_Helper.getCustomer(applicationId, accountId);
                                    } catch (Exception e) {
                                    system.debug('Exception::' + e.getMessage() + e.getLineNumber());
                                    return getWSResponse(res, Constants.WS_ERROR_STATUS, null, Constants.WS_ERROR_CODE, 'Exception' + e.getMessage() + e.getLineNumber());
                                }
                            }
                        }
                    } else {
                        return getWSResponse(res, Constants.WS_ERROR_STATUS, null, Constants.WS_ERROR_CODE, 'TYPE is required.');
                    }
                }
                // NTB APPLICATION
                if (reqObj.Customer_ID == null || reqObj.Customer_ID == '' && reqObj.TYPE != 'NTB') {
                    return getWSResponse(res, Constants.WS_ERROR_STATUS, null, Constants.WS_ERROR_CODE, 'Customer_ID is required.');
                }

                if (reqObj.Customer_ID == null || reqObj.Customer_ID == '' && reqObj.TYPE == 'NTB') {
                    
                    WS_FetchSMEInfo_Helper.CustomerDetail cusObj = (WS_FetchSMEInfo_Helper.CustomerDetail) reqObj.CustomerDetail;
                    Savepoint sp = Database.setSavepoint();
                    System.debug('cusObj' + cusObj);
                    try {
                        If(cusObj != null) {
                            String accountId = '';
                            String appId = '';
                            Account accountObject = SOQL_Util.getAccountDetails(cusObj.PAN, cusObj.GST);
                            If(accountObject != null) {
                                return getWSResponse(res, Constants.WS_ERROR_STATUS, null, Constants.WS_ERROR_CODE, 'Account already exist with provided Details.');
                            } else {
                                   
                                    accountId = WS_FetchSMEInfo_Helper.updateCustomerDetails(cusObj,reqObj.TYPE);
                                
                            }
                            If(accountId != null && accountId != '') {
                                
                                appId = WS_FetchSMEInfo_Helper.createApplication(accountId,reqObj.TYPE);

                                If(accountId != null && accountId != '' && appId != null && appId != '') {
                                    try {
                                        res.customerFullView = SMELoan_Helper.getCustomer(appId, accountId);
                                    } catch (Exception e) {
                                        system.debug('Exception::' + e.getMessage() + e.getLineNumber());
                                        return getWSResponse(res, Constants.WS_ERROR_STATUS, null, Constants.WS_ERROR_CODE, 'Exception' + e.getMessage() + e.getLineNumber());
                                    }
                                }
                            }
                        }

                    } catch (Exception ex) {
                        Database.rollback(sp);
                        return getWSResponse(res, Constants.WS_ERROR_STATUS, null, Constants.WS_ERROR_CODE, 'Customer creation failed'+ex.getMessage() + ex.getLineNumber());
                    }
                } else if (reqObj.Customer_ID == null || reqObj.Customer_ID == '' && reqObj.TYPE == '') {
                    return getWSResponse(res, Constants.WS_ERROR_STATUS, null, Constants.WS_ERROR_CODE, 'Customer_ID and NTB is Required');

                }
            } catch (Exception ex) {
                system.debug('Exception::' + ex.getMessage() + ex.getMessage() + ex.getLineNumber());
                return getWSResponse(res, Constants.WS_ERROR_STATUS, null, Constants.WS_ERROR_CODE, 'Exception' + ex.getMessage() + ex.getLineNumber());
            }


            return res;
        }
    }
    static Response getWSResponse(Response res, string status, string succMsg, string statusCode, string errMsg) {
        res.status = status;
        res.successMessage = succMsg;
        res.statusCode = statusCode;
        res.errorMessage = errMsg;
        return res;
        }

    }