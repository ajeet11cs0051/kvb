@isTest
public class Test_ApplicationEligibiltyHandler {
    public static genesis__Applications__c genApp;
    //public static Account acc;
    public static genesis__Application_Parties__c genAppParty;
    
    @isTest
    public static void init(){
        genApp = TestUtility.intialSetUp('Home Loan', true);
        genApp.Sub_Stage__c = 'Property Information Captured';//Constants.Sub_Stage_PropInfoCaptured;
        update genApp;
        Account acct = new Account(firstName='Venu',LastName='Gopal');
        insert acct;
        System.assertEquals(genApp.Sub_Stage__c , Constants.Sub_Stage_PropInfoCaptured);
        System.debug('>>>>>>'+genApp.Sub_Stage__c);
        System.debug('>>>>>>'+Constants.Sub_Stage_PropInfoCaptured);
       
    }
    @isTest
    public static void methodCheckEMI(){
        init();
        List<genesis__Applications__c> genAppList = new List<genesis__Applications__c>();
        genAppList.add(genApp);
        System.debug('~~genAppList'+genAppList);
        Map<Id,genesis__Applications__c> mapGenApp = new Map<Id,genesis__Applications__c>();
        mapGenApp.put(genApp.Id, genApp);
        System.debug('~~mapGenApp'+mapGenApp);
        ApplicationEligibiltyHandler.CheckEMI(genAppList, mapGenApp);
    }
    /*@isTest
    public static void methodGetEmi_LTV_ROIValues(){
        init();
        List<genesis__Applications__c> genAppList = new List<genesis__Applications__c>();
        genAppList.add(genApp);
        
        
        
        
        ApplicationEligibiltyHandler.getEmi_LTV_ROIValues(genAppList);
    }*/
    
}