/*
*   Name    : Digioe_Docs_Service
*   purpose : Call Digioe service to get document signed and 
*             document back in SF system
*  Author   : Amritesh
*/  
public class Digioe_Docs_Service {
    public static String attachmentId;
    Public static string getSanctionDocforSigning(string applicationId){        
        String signedDoc = Digioe_Sign_Service.getSanctionDoc(applicationId);
        return signedDoc;
    }
    
    /*
    * Method to request for Class-2 Sign for a document
    *
    */
    public static String class2Signer(DocSignerRequest req, integer pageCount, String documentType){
        
        try{
            
            Map<String,String> headerMap = new Map<String,String>();
            headerMap.put('Content-Type','application/json');
            HTTPResponse response = new HTTPResponse();
            
            Class2ReqWrap reqWrp    = new Class2ReqWrap();
            reqWrp.exe_tenantId     = 'cuecent_tenant';
            reqWrp.owner_tenantId    = 'cuecent_tenant';
            reqWrp.serviceName       = 'TestJsonStr';
            reqWrp.inputVariables    = new cls_inputVariables();
            reqWrp.inputVariables.in_msg = '';
            reqWrp.inputVariables.inputMap = new DocSignerRequest();
            reqWrp.inputVariables.inputMap    = req;
            reqWrp.inputVariables.inputMap.sign_coordinates = new Map<Integer,List<Object>>();
            if(documentType == Constants.SANCTION_TEMPLATE_SME || documentType == Constants.SME_SANCTION_Letter123){
                cls_1 con = new cls_1(414.79,553.29,523.54,626.44);
                reqWrp.inputVariables.inputMap.sign_coordinates.put(pageCount-9, new List<Object>{con});
            }
             if(documentType == Constants.SME_SANCTION_Letter1 ){
                cls_1 con = new cls_1(414.79,553.29,523.54,626.44);
                reqWrp.inputVariables.inputMap.sign_coordinates.put(pageCount-4, new List<Object>{con});
            }
             if(documentType == Constants.SME_SANCTION_Letter12){
                cls_1 con = new cls_1(414.79,553.29,523.54,626.44);
                reqWrp.inputVariables.inputMap.sign_coordinates.put(pageCount-5, new List<Object>{con});
            }
             if(documentType == Constants.SME_SANCTION_Letter13){
                cls_1 con = new cls_1(414.79,553.29,523.54,626.44);
                reqWrp.inputVariables.inputMap.sign_coordinates.put(pageCount-8, new List<Object>{con});
            }
            if(documentType == Constants.PROV_SANCTION_TEMPLATE_SME){ 
                cls_1 con = new cls_1(375.18,569.89,521.21,644.52);
                reqWrp.inputVariables.inputMap.sign_coordinates.put(pageCount-4, new List<Object>{con});
            }
             if(documentType == Constants.PRE_Approval_Sanction_HL){
                cls_1 con = new cls_1(310.51,635.62,447.55,670.3);
                reqWrp.inputVariables.inputMap.sign_coordinates.put(pageCount, new List<Object>{con});
            }
             if(documentType == Constants.Acknowledgement_For_Sanction){
                 system.debug('Test'+documentType);
                cls_1 con = new cls_1(307.67,629.3,446.56,664.46);
                reqWrp.inputVariables.inputMap.sign_coordinates.put(pageCount-3, new List<Object>{con});
            }
             if(documentType == Constants.A23_HL){
                 system.debug('Test'+documentType);
                cls_1 con = new cls_1(371.13,466.46,512.08,502.14);
                reqWrp.inputVariables.inputMap.sign_coordinates.put(pageCount-1, new List<Object>{con});
            }
            if(documentType == Constants.A46_HL){
                 system.debug('Test'+documentType);
                cls_1 con = new cls_1(361.1,547.62,504.05,583.3);
                reqWrp.inputVariables.inputMap.sign_coordinates.put(pageCount, new List<Object>{con});
            }
             //Add Cord for class 2 signer for PL.....
            if(documentType == Constants.PRE_APPROVE_SANCTION_PL){
                system.debug('Test'+documentType);
                cls_1 con = new cls_1(305.31,178.63,442.35,213.32);
                reqWrp.inputVariables.inputMap.sign_coordinates.put(pageCount-3, new List<Object>{con});
            }
            
            system.debug('documentType'+documentType+'PageCount'+pageCount);         
            if(reqWrp == null) throw new CustomException('Invalid Request');
            string reqString = JSON.serialize(reqWrp);
            system.debug('reqString::'+reqString);
            response = HttpUtility.sendHTTPRequest(Utility.getEndpoint('DocSigner'), 'POST',null ,reqString ,headerMap,label.CA_CERTIFICATE);
            
            if(response.getStatusCode() == 200 || response.getStatusCode() == 201){
                String signedDoc = response.getBody();  
                system.debug('signedDoc::'+signedDoc);            
                DocSignerResponse appReqObj = (DocSignerResponse)Json.deserialize(signedDoc, DocSignerResponse.class);
                if(appReqObj == null || appReqObj.outMap == null || appReqObj.outMap.signed_file_content == null){
                    //throw new CustomException('Status Code-'+appReqObj.code+' Message-'+appReqObj.message);
                    throw new CustomException('Class-2 Sign Failed.Invalid Response Received.');
                }
                system.debug('jsonData::'+appReqObj.outMap.signed_file_content);
                return appReqObj.outMap.signed_file_content;                
            }else{
                throw new CustomException('Status Code-'+response.getStatusCode()+' Status-'+response.getStatus());
            }
            
        }catch(Exception e){            
            system.debug('Exception in DocSigner '+e.getMessage()+' Stack '+e.getStackTraceString());
            throw new CustomException('Class-2/3 Failed-'+e.getMessage());
        }
    }
    
    public class Class2ReqWrap{
        public String exe_tenantId; //cuecent_tenant
        public String owner_tenantId;   //cuecent_tenant
        public String serviceName;  //TestJsonStr
        public cls_inputVariables inputVariables;
    }
    
    public class cls_inputVariables {
        public String in_msg;   //
        public DocSignerRequest inputMap;
    }
    
    public class DocSignerRequest{
        public String key_store_name = KVB_Company_Details__c.getInstance().Key_Store_Name__c;//Constants.KEYSTORENAME;
        public String name;
        public String reason;
        public String location; //city/town/village
        public String file_content; //base64 string
        public String display_on_page = 'custom'; //options avaliable first/last/all/custom
        public Map<Integer,List<Object>> sign_coordinates;
        
        
        public DocSignerRequest(){}
        public DocSignerRequest(String name,String reason,String location,String file_content){
            this.name = name;
            this.reason = reason;
            this.location = location;
            this.file_content = file_content;
        }
    }
        
    public class cls_1 {
        public decimal llx; //315
        public decimal lly; //20
        public decimal urx; //455
        public decimal ury; //60
        public cls_1(){}
        public cls_1(decimal llx, decimal lly, decimal urx, decimal ury){
            this.llx = llx;
            this.lly = lly;
            this.urx = urx;
            this.ury = ury;
        }
    }
          
    public class DocSignerResponse{
        public cls_outMap outMap;   
        //public String signed_file_content;                   
        // public String code;
        // public String message;
    }
    public class cls_outMap {
        public String signed_file_content;  //JVBERi0xLYKJSVFT0YK
    } 
    
    public class DocGenResponse{
        public cls_outMapDoc outMap;
    }
    public  class cls_outMapDoc {
        public cls_Data Data;
    }
    public class cls_Data {
        public String outfile;  //JiUlRU9G
        public String totalpages;
    } 

    /*
    * Method to generate the document from DIGIO
    *
    */
    public static DocGenResponse getEDocs(String appId,String documentName,String templateName){
        try{
            
            //String returnString = null;
            DocGenResponse appReqObj    = new DocGenResponse();
            String req                  = '';
            
            HTTPResponse response = new HTTPResponse(); 
            if(documentName == Constants.SANCTION_TEMPLATE_SME || documentName == Constants.PROV_SANCTION_TEMPLATE_SME
              || documentName == Constants.SME_SANCTION_Letter123 || documentName == Constants.SME_SANCTION_Letter12
              || documentName == Constants.SME_SANCTION_Letter1 || documentName == Constants.SME_SANCTION_Letter13)                
                req = JSON.serialize(DigioTemplateService.getSMESanctionLetter(appId,documentName,templateName));            
            if(documentName == Constants.PRE_Approval_Sanction_HL)
                req=JSON.serialize(HL_DigioTemplateService.getHLSanctionLetter(appId,templateName));
            if(documentName == Constants.Execution_Certificate_C11_HL)
                req=JSON.serialize(HL_DigioTemplateService.getHLExecutionCertiC11(appId,templateName));
            if(documentName == Constants.Acknowledgement_For_Sanction)
                req=JSON.serialize(HL_DigioTemplateService.getHLAcknowledgeSanctionC1(appId,templateName));
             // PL Sanction letter call
            if(documentName == Constants.PRE_APPROVE_SANCTION_PL){
                req=JSON.serialize(HL_DigioTemplateService.getPLSanctionLetter(appId,templateName));  
                system.debug('req:: PL-------> '+req);    
                }
                
            system.debug('req::'+req);            
            if(req != null && req != 'null'){                 
                Map<String,String> headerMap = new Map<String,String>();              
                headerMap.put('Content-Type','application/json');                   
                response = HttpUtility.sendHTTPRequest(Utility.getEndpoint('DocGeneration'), 'POST', null,req,headerMap,label.CA_CERTIFICATE);
                
                if(response.getStatusCode() == 200 || response.getStatusCode() == 201){
                    String responseString = response.getBody();  
                    system.debug('responseString'+responseString);
                    appReqObj = (DocGenResponse)Json.deserialize(responseString, DocGenResponse.class);                    
                    if(appReqObj == null || appReqObj.outMap == null || appReqObj.outMap.Data == null
                       || appReqObj.outMap.Data.outfile == null){
                           throw new CustomException('Doc Generation Failed.');
                       }
                    system.debug('response::'+appReqObj.outMap.Data);                    
                    return appReqObj;
                }else{
                    throw new CustomException('Status Code-'+response.getStatusCode()+' Status-'+response.getStatus());
                }
            }else{
                
                throw new CustomException('Null template generated');
            }   
            return appReqObj;
            
        }catch(Exception e){
            
            system.debug('Exception in eDoc generation '+e.getMessage()+' Stack '+e.getStackTraceString()+'Line Number'+e.getLineNumber());
            throw new CustomException('DIGIO document generation failed-'+e.getMessage());
        }  
    }

    /*
     * Method to save the document as attachement
     * Developer : Numaan
    */
    public static void upsertDoc(String appId,String fileName,String content){

        List<Attachment> attList = new List<Attachment>();
        Attachment att = new Attachment();
        attList = [Select Id,Body,ParentId,Name from Attachment where ParentId=:appId AND Name=:fileName limit 1];
        if(!attList.isEmpty()){
            att = attList[0];
            att.Body = EncodingUtil.base64Decode(content);
            update att;
        }else{

            att.Name = fileName;
            att.ParentId = appId;
            att.Body = EncodingUtil.base64Decode(content);
            insert att;
        }
    }


    /*
     * Method provides the attachment body for a given name and parent Id
     * Developer : Numaan
    */
    public static Blob getAttachmentBody(String pId,String name){
        Attachment att = [Select Id,Body,ParentId,Name from Attachment where ParentId=:pId AND Name=:name limit 1];
        attachmentId = att.Id;
        return att.Body;
    }
    
    /*
     * Method makes call to DIGIO and returns the downloaded document
     * Developer : Dushyant
    */
    public static String downloadDocument(String docId){
        Attachment att = new Attachment();
        DownloadDocoment.DOC_Request eDownReq = new DownloadDocoment.DOC_Request();
        eDownReq.exe_tenantId           = 'cuecent_tenant';
        eDownReq.owner_tenantId         = 'cuecent_tenant'; 
        eDownReq.inputVariables         = new DownloadDocoment.cls_inputVariables();
        eDownReq.inputVariables.in_msg  = '';
        eDownReq.inputVariables.documentId  = docId;
        
        string reqStr = JSON.serialize(eDownReq);
        System.debug(reqStr);
        //Callout to download document
        HTTPResponse response           = new HTTPResponse();
        Map<String,String> headerMap    = new Map<String,String>();             
        headerMap.put('Content-Type','application/json');
        System.debug(Utility.getEndpoint('DocumentDownload'));
        response = HttpUtility.sendHTTPRequest(Utility.getEndpoint('DocumentDownload'), 'POST', null,reqStr,headerMap,label.CA_CERTIFICATE);
        //System.debug(response.getStatusCode());
        System.debug(response.getBody());
        
        
        if(response.getStatusCode() == 200 || response.getStatusCode() == 201){
            String jsonString = response.getBody();
            system.debug('jsonData::'+jsonString);
            downRes obj = (downRes)JSON.deserialize(jsonString,downRes.class);
            System.debug(obj);
            //System.debug(obj.outMap.Base64Str);
            try{
                att = [SELECT Id,ParentId,Name FROM Attachment WHERE Description =: docId];
                upsertDoc(att.ParentId,att.Name,obj.outMap.Base64Str);
            }catch(Exception e){
                system.debug('No Doc found::'+e.getMessage());
            }
            
            RETURN obj.outMap.Base64Str;
        }else{
            system.debug('**Status code-'+response.getStatusCode()+' **Status-'+response.getStatus()); 
            return null;
        }
    }
    public class downRes{
        public outMapClass outMap;
    }
    public class outMapClass {
        public String RESDate;
        public String SuccessMessage;
        public String RESTime;
        public String ErrorCode;
        public String Base64Str;
    }
}