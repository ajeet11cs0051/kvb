/**
 * Created by ET-MARLABS on 08-06-2018.
 */

public with sharing class YTDAnalysisHelper {

    public static Integer currentYear                                   = Utility.getCurrentYear();
    public static String cyString                                       = String.valueOf(currentYear);
    public static String currFiscalYear                                 = (currentYear-1)+'-'+Integer.valueOf(cyString.subString(cyString.length()-2,cyString.length()));
    public static String nthFiscalYear                                  = (currentYear-2)+'-'+Integer.valueOf(String.valueOf(currentYear-1).subString(String.valueOf(currentYear-1).length()-2,String.valueOf(currentYear-1).length()));
    public static String n_1_FiscalYear                                 = (currentYear-3)+'-'+Integer.valueOf(String.valueOf(currentYear-2).subString(String.valueOf(currentYear-1).length()-2,String.valueOf(currentYear-1).length()));
    public static String n_2_FiscalYear                                 = (currentYear-4)+'-'+Integer.valueOf(String.valueOf(currentYear-3).subString(String.valueOf(currentYear-1).length()-2,String.valueOf(currentYear-1).length()));
    public static String nextEstFiscalYear                              = currentYear+'-'+Integer.valueOf(String.valueOf(currentYear+1).subString(String.valueOf(currentYear+1).length()-2,String.valueOf(currentYear+1).length()));
    public static String nextProjFiscalYear                             = (currentYear+1)+'-'+Integer.valueOf(String.valueOf(currentYear+2).subString(String.valueOf(currentYear+2).length()-2,String.valueOf(currentYear+2).length()));

    public static String getEstimateSalesAnalysis(String losCustId,String appId){
        try{
            M68_Balance_Sheet_Analysis__c m68DataEstimated              = new M68_Balance_Sheet_Analysis__c();
            M68_Balance_Sheet_Analysis__c m68DataProjected              = new M68_Balance_Sheet_Analysis__c();
            M68_Balance_Sheet_Analysis__c m68YTDRecord                  = new M68_Balance_Sheet_Analysis__c();
            List<M68_Balance_Sheet_Analysis__c> m68DataList             = new List<M68_Balance_Sheet_Analysis__c>();
            List<String> fyList                                         = new List<String>{nthFiscalYear,n_1_FiscalYear,n_2_FiscalYear,currFiscalYear,nextProjFiscalYear,nextEstFiscalYear};
            m68DataList                                                 = SOQL_Util.getM68Data(losCustId,fyList);
            Decimal salesForN_1,salesForN_2,purchaseForN_1,purchaseForN_2;
            Integer flag=0;
            M68_Balance_Sheet_Analysis__c nthYearData                   = new M68_Balance_Sheet_Analysis__c();
            M68_Balance_Sheet_Analysis__c n_1_thYearData                = new M68_Balance_Sheet_Analysis__c();
            M68_Balance_Sheet_Analysis__c n_2_thYearData                = new M68_Balance_Sheet_Analysis__c();

            if(m68DataList != null && !m68DataList.isEmpty()) {
                for (M68_Balance_Sheet_Analysis__c m68Data : m68DataList) {

                    if(m68Data.Fiscal_Year__c == currFiscalYear && m68Data.Financial_type__c == 'Actual') {

                        salesForN_1                                     = m68Data.Net_sales__c;
                        purchaseForN_1                                  = m68Data.Purchases__c;
                        nthYearData                                     =m68Data;
                        flag++;

                    }else if (m68Data.Fiscal_Year__c == nthFiscalYear && m68Data.Financial_type__c == 'Actual') {
                        //newly added by souvik on 07.06.2018
                        n_1_thYearData                                  = m68Data;
                        flag++;
                    }
                    else if (m68Data.Fiscal_Year__c == n_1_FiscalYear && m68Data.Financial_type__c == 'Actual') {

                        salesForN_2                                     = m68Data.Net_sales__c;
                        purchaseForN_2                                  = m68Data.Purchases__c;
                        n_2_thYearData                                  = m68Data;
                        flag++;
                    }else if (m68Data.Fiscal_Year__c == nextEstFiscalYear && m68Data.Financial_type__c == 'Estimated') {
                        m68DataEstimated                                = m68Data;
                    }else if (m68Data.Fiscal_Year__c == nextProjFiscalYear && m68Data.Financial_type__c == 'Projected') {
                        m68DataProjected                                = m68Data;
                    }else if (m68Data.Fiscal_Year__c == nextEstFiscalYear && m68Data.Financial_type__c == 'YTD') {
                        m68YTDRecord                                    = m68Data;
                    }

                }
            }
            if(flag == 3){
                if(m68DataEstimated !=null){

                    If(m68DataEstimated.Net_sales__c !=null){
                        Boolean estimatedSalesAnalysisFlag      = YTDAnalysisHelper.getSalesAnalysisFlag(salesForN_1,salesForN_2,m68DataEstimated.Net_sales__c,'EST');

                        If(estimatedSalesAnalysisFlag !=null)
                            YTDAnalysisController.ytdAnalysisMap.put('Estimate Sales Analysis',estimatedSalesAnalysisFlag);
                    }

                    If(m68DataEstimated.Purchases__c !=null){

                        Boolean estimatedpurchaseAnalysisFlag   = YTDAnalysisHelper.getPurchaseAnalysisFlag(purchaseForN_1,purchaseForN_2,m68DataEstimated.Purchases__c,'EST');

                        If(estimatedpurchaseAnalysisFlag !=null)
                            YTDAnalysisController.ytdAnalysisMap.put('Estimate Purchase Analysis',estimatedpurchaseAnalysisFlag);
                    }

                }
                if(m68DataProjected !=null){
                    If(m68DataProjected.Net_sales__c !=null){
                        Boolean ProjectedSalesAnalysisFlag      = YTDAnalysisHelper.getSalesAnalysisFlag(salesForN_1,salesForN_2,m68DataProjected.Net_sales__c,'PROJ');
                        If(ProjectedSalesAnalysisFlag !=null)
                            YTDAnalysisController.ytdAnalysisMap.put('Projected Sales Analysis',ProjectedSalesAnalysisFlag);
                    }
                    If(m68DataProjected.Purchases__c !=null){

                        Boolean ProjectedpurchaseAnalysisFlag   = YTDAnalysisHelper.getPurchaseAnalysisFlag(purchaseForN_1,purchaseForN_2,m68DataProjected.Purchases__c,'PROJ');
                        If(ProjectedpurchaseAnalysisFlag !=null)
                            YTDAnalysisController.ytdAnalysisMap.put('Projected Purchase Analysis',ProjectedpurchaseAnalysisFlag);
                    }
                }
                If(nthYearData !=null && n_1_thYearData !=null && n_2_thYearData !=null && m68DataEstimated.Sundry_Creditors__c!=null && m68DataEstimated.Purchases__c !=null && m68DataEstimated.Sundry_Debtors__c !=null && m68DataEstimated.Net_sales__c !=null){

                    Boolean ECVA = getDeltaFlag(calculateVelocity(nthYearData,'CREDITOR'),calculateVelocity(n_1_thYearData,'CREDITOR'),calculateVelocity(n_2_thYearData,'CREDITOR'),m68DataEstimated.Sundry_Creditors__c/m68DataEstimated.Purchases__c);
                    Boolean EDVA = getDeltaFlag(calculateVelocity(nthYearData,'DEBITOR'),calculateVelocity(n_1_thYearData,'DEBITOR'),calculateVelocity(n_2_thYearData,'DEBITOR'),m68DataEstimated.Sundry_Debtors__c/m68DataEstimated.Net_sales__c);
                    Boolean PCVA = getDeltaFlag(calculateVelocity(nthYearData,'CREDITOR'),calculateVelocity(n_1_thYearData,'CREDITOR'),calculateVelocity(n_2_thYearData,'CREDITOR'),m68DataProjected.Sundry_Creditors__c/m68DataProjected.Purchases__c);
                    Boolean PDVA = getDeltaFlag(calculateVelocity(nthYearData,'DEBITOR'),calculateVelocity(n_1_thYearData,'DEBITOR'),calculateVelocity(n_2_thYearData,'DEBITOR'),m68DataProjected.Sundry_Debtors__c/m68DataProjected.Net_sales__c);
                    If(ECVA !=null)
                        YTDAnalysisController.ytdAnalysisMap.put('Estimate Creditors Velocity Analysis',ECVA);
                    If(EDVA !=null)
                        YTDAnalysisController.ytdAnalysisMap.put('Estimate Debtors Velocity Analysis',EDVA);
                    If(PCVA !=null)
                        YTDAnalysisController.ytdAnalysisMap.put('Projected Creditors Velocity Analysis',PCVA);
                    If(PDVA !=null)
                        YTDAnalysisController.ytdAnalysisMap.put('Projected Debtors Velocity Analysis',PDVA);

                }
                if(m68YTDRecord != null && m68DataEstimated != null){
                    Boolean ytdSalesFlag= YTDAnalysisHelper.getYTDAnalysis(m68YTDRecord,'SALES',m68DataEstimated.Net_sales__c);
                    If(ytdSalesFlag !=null)
                        YTDAnalysisController.ytdAnalysisMap.put('YTD Sales Analysis',ytdSalesFlag);
                    Boolean ytdPurchaseFlag= YTDAnalysisHelper.getYTDAnalysis(m68YTDRecord,'PURCHASE',m68DataEstimated.Purchases__c);
                    If(ytdPurchaseFlag !=null)
                        YTDAnalysisController.ytdAnalysisMap.put('YTD Purchase Analysis',ytdPurchaseFlag);
                }
                return null;
            }else{
                return 'Last three year m68 data is missing ';
            }


        }catch(Exception e){
            system.debug('Exeption In::'+e.getCause()+'--------'+e.getLineNumber());
            return null;
        }
    }
    //Get flag of creditors and Debtors.
    public static List<Credit_Underwriting__c> getCreditorDebitorsFlag(String losCustId){
        Boolean debtorAnalysis = getDebitorsAnalysis(losCustId);
        If(debtorAnalysis !=null)
            YTDAnalysisController.creditorDebtorsAnalysisMap.put('Debtors Analysis',debtorAnalysis);
        Boolean creditorsAnalysis = getCreditorsAnalysis(losCustId);
        If(creditorsAnalysis !=null)
            YTDAnalysisController.creditorDebtorsAnalysisMap.put('Creditors Analysis',creditorsAnalysis);

        String appraisal = 'Debtors/Creditors Analysis';
        If(YTDAnalysisController.creditorDebtorsAnalysisMap !=null){
            List<Credit_Underwriting__c> listOfCredit   =   new List<Credit_Underwriting__c>();
            for(String str    : YTDAnalysisController.creditorDebtorsAnalysisMap.keySet()){
                If(str !=null && str !=''){
                    Credit_Underwriting__c creditUnderObj   = new Credit_Underwriting__c();
                    creditUnderObj.Appraisal__c             = appraisal;
                    creditUnderObj.Variable_type__c         = str;
                    creditUnderObj.Flags_Value__c           = String.valueOf(YTDAnalysisController.creditorDebtorsAnalysisMap.get(str));
                    creditUnderObj.Threshold_breached__c    = true;
                    listOfCredit.add(creditUnderObj);
                }
            }
            If(!listOfCredit.isEmpty()){
                return listOfCredit;
            }
        }
        return new List<Credit_Underwriting__c>();
    }
    //Creditors Analysis
    public static Boolean getCreditorsAnalysis(String losCustId){
        try{
            String financial_Type   ='Actual';
            Double sumOfFiveAmount = 0;
            Double concentration   = 0;
            Boolean flag=false;
            Set<String> setName                                = new Set<String>();
            List<String> listOfFiscalYear                      = new List<String>{currFiscalYear,nextEstFiscalYear};
            List<Sundry_Creditors__c>   listSundryCreditors    = [select id,Age__c,Amount_To_Be_Paid__c,Fiscal_Year__c,Name_Of_Trade_Creditors__c from Sundry_Creditors__c  where Account__c =: losCustId AND Fiscal_Year__c IN :listOfFiscalYear];
            M68_Balance_Sheet_Analysis__c   objOfM68           = [select id,Sundry_Creditors__c,Fiscal_Year__c,Financial_type__c from M68_Balance_Sheet_Analysis__c where Account__c =:losCustId AND Fiscal_Year__c =:currFiscalYear AND Financial_type__c =:financial_Type];

            If(listSundryCreditors !=null){
                for(Sundry_Creditors__c  sundryCreditorsObj        : listSundryCreditors){
                    If(sundryCreditorsObj !=null && sundryCreditorsObj.Amount_To_Be_Paid__c !=null && sundryCreditorsObj.Name_Of_Trade_Creditors__c !=null && sundryCreditorsObj.Fiscal_Year__c ==currFiscalYear){
                        setName.add(sundryCreditorsObj.Name_Of_Trade_Creditors__c);
                        sumOfFiveAmount += sundryCreditorsObj.Amount_To_Be_Paid__c;
                    }
                }
            }
            If(sumOfFiveAmount >0 && objOfM68 !=null && objOfM68.Sundry_Creditors__c !=null && objOfM68.Sundry_Creditors__c > 0){
                concentration = (sumOfFiveAmount/objOfM68.Sundry_Creditors__c)*100;
                //Double temp = (sumOfFiveAmount*50)/100;
                If(concentration > 0){
                    //return concentration>50?true:false;
                    flag = concentration>50?true:false;
                }
            }

            If(listSundryCreditors !=null){
                for(Sundry_Creditors__c  sundryCreditorsObj        : listSundryCreditors){
                    If(sundryCreditorsObj !=null && sundryCreditorsObj.Name_Of_Trade_Creditors__c !=null && sundryCreditorsObj.Fiscal_Year__c ==nextEstFiscalYear){
                        If(setName.contains(sundryCreditorsObj.Name_Of_Trade_Creditors__c)){
                            //return true;
                            flag = true;
                        }
                    }
                }
            }   
            //return false;
            return flag == true?true:flag;
        }catch(Exception ex){
            system.debug('Exeption In::'+ex.getCause()+'--------'+ex.getLineNumber()+ex.getMessage());
            return null;
        }
    }
    //Debitors Analysis
    public static Boolean getDebitorsAnalysis(String losCustId){
        try{
            String financial_Type   ='Actual';
            Double sumOfFiveAmount = 0;
            Double concentration   = 0;
            Boolean flag=false;
            Set<String> setName                                = new Set<String>();
            List<String> listOfFiscalYear                      = new List<String>{currFiscalYear,nextEstFiscalYear};
            System.debug('listOfFiscalYear----> ' + listOfFiscalYear);
            List<Debtors__c>    listDebitors                   = [select id,Age__c,Amount_Collected__c,Fiscal_Year__c,Customer_Name__c from Debtors__c  where Account__c =: losCustId AND Fiscal_Year__c IN :listOfFiscalYear];
            System.debug('listDebitors---> ' + listDebitors);
            M68_Balance_Sheet_Analysis__c   objOfM68           = [select id,Sundry_Debtors__c,Fiscal_Year__c,Financial_type__c from M68_Balance_Sheet_Analysis__c where Account__c =:losCustId AND Fiscal_Year__c =:currFiscalYear AND Financial_type__c =:financial_Type];
            System.debug('objOfM68---> ' + objOfM68);
            If(listDebitors !=null){
                System.debug('---Inside If---');
                System.debug('listDebitors---> ' + listDebitors);
                for(Debtors__c  debtorsObj                     : listDebitors){
                    If(debtorsObj !=null && debtorsObj.Amount_Collected__c !=null && debtorsObj.Customer_Name__c !=null && debtorsObj.Fiscal_Year__c ==currFiscalYear){
                        setName.add(debtorsObj.Customer_Name__c);
                        System.debug('setName---> ' + setName);
                        sumOfFiveAmount += debtorsObj.Amount_Collected__c;
                    }
                }
                System.debug('sumOfFiveAmount---> ' + sumOfFiveAmount);
            }
            If(sumOfFiveAmount >0 && objOfM68.Sundry_Debtors__c !=null && objOfM68.Sundry_Debtors__c > 0){
                concentration = (sumOfFiveAmount/objOfM68.Sundry_Debtors__c)*100;
                System.debug('concentration---> ' + concentration);
                If(concentration > 0){
                   // return concentration>50? true:false;
                   flag = concentration>50? true:false;
                }
            }   

            If(listDebitors !=null){
                for(Debtors__c  debtorsObj     : listDebitors){
                    If(debtorsObj !=null && debtorsObj.Customer_Name__c !=null && debtorsObj.Fiscal_Year__c ==nextEstFiscalYear){
                        System.debug('setName---> ' + setName);
                        If(setName.contains(debtorsObj.Customer_Name__c)){
                            System.debug('setName---> ' + setName);
                           // return true;
                           flag = true;
                        }
                    }
                }
            }
            //return false;
            System.debug('flag---> ' + flag);
            return flag == true?true:flag;
        }catch(Exception ex){
            system.debug('Exeption In::'+ex.getCause()+'--------'+ex.getLineNumber()+ex.getMessage());
            return null;
        }
    }

    public static Boolean getSalesAnalysisFlag(Decimal salesfor_n_year, Decimal salesfor_n_2_year,Decimal currYearSalesEstimate,String estOrProj){
        try{

            Decimal cagrValue = compoundAnnualGrowthRate(salesfor_n_year,salesfor_n_2_year);
            Decimal idealEstimate = calculateIdealEstimate(salesfor_n_year,cagrValue);
            Decimal allowedValue = calculateAllowedValues(idealEstimate,'SALES',estOrProj);

            return currYearSalesEstimate > allowedValue ? TRUE : FALSE;
        }catch(Exception e){
            system.debug('Exeption In::'+e.getCause()+'--------'+e.getLineNumber()+e.getMessage());
            return null;
        }
    }

    public static Boolean getPurchaseAnalysisFlag(Decimal salesfor_n_year, Decimal salesfor_n_2_year,Decimal currYearPurchasesEstimate,String estOrProj){
        try{

            Decimal cagrValue = compoundAnnualGrowthRate(salesfor_n_year,salesfor_n_2_year);
            Decimal idealEstimate = calculateIdealEstimate(salesfor_n_year,cagrValue);
            Decimal allowedValue = calculateAllowedValues(idealEstimate,'PURCHASE',estOrProj);

            return currYearPurchasesEstimate > allowedValue ? TRUE : FALSE;

        }catch(Exception e){
            system.debug('Exeption In::'+e.getCause()+'--------'+e.getLineNumber()+e.getMessage());
            return null;
        }
    }

    private static Decimal compoundAnnualGrowthRate(Decimal salesfor_n_year, Decimal salesfor_n_2_year){
        // CAGR = (Sales (for Year n)/Sales ( for Year n-2) ^0.5)-1

        try{
            If(salesfor_n_year !=null && salesfor_n_2_year !=null && salesfor_n_2_year !=0){
                return math.pow(Double.valueOf(salesfor_n_year/salesfor_n_2_year),Double.valueOf(0.5))-1;

            }else{
                return null;
            }

        }catch(Exception e){
            system.debug('Exeption In::'+e.getCause()+'--------'+e.getLineNumber()+e.getMessage());
            return null;
        }
    }

    private static Decimal calculateIdealEstimate(Decimal salesfor_n_year,Decimal cagr){
        //Sales (Year n) *(1+CAGR)
        try{
            If(salesfor_n_year !=null && cagr !=null){
                return salesfor_n_year * (1+ cagr);
            }else{
                return null;
            }
        }catch(Exception e){
            system.debug('Exeption In::'+e.getCause()+'--------'+e.getLineNumber());
            return null;
        }
    }

    private static Decimal calculateAllowedValues(Decimal idealEstimate, String isSalesorPurchase,String estOrProj){
        // Allowed value = Ideal Estimated Value *0.8214+81.6942 ->For Sales
        // Allowed value =  Ideal Estimated Value *0.8849+71.0160 --> for purchase
        try{
            if(idealEstimate !=null){
                if(isSalesorPurchase == 'SALES'){
                    if(estOrProj == 'EST')
                        return ((idealEstimate * 0.8214)+81.6942);
                    else if(estOrProj == 'PROJ')
                        return ((idealEstimate * 0.8220)+92.8884);
                    else
                            return null;
                }else if(isSalesorPurchase == 'PURCHASE'){
                    if(estOrProj == 'EST')
                        return ((idealEstimate * 0.8849)+71.0160);
                    else if(estOrProj == 'PROJ')
                        return ((idealEstimate * 0.7895)+93.9755);
                    else
                            return null;
                }else{
                    return null;
                }

            }else{
                return null;
            }
        }catch(Exception e){
            system.debug('Exeption In::'+e.getCause()+'--------'+e.getLineNumber()+e.getMessage());
            return null;
        }
    }

    public static Decimal calculateVelocity(M68_Balance_Sheet_Analysis__c m68Data ,String credOrDeb){

        try{
            if (m68Data != null) {
                if(credOrDeb =='CREDITOR' && m68Data.Sundry_Creditors__c != null && m68Data.Purchases__c  != null){
                    return m68Data.Sundry_Creditors__c / m68Data.Purchases__c ;
                }else if (credOrDeb =='DEBITOR' && m68Data.Sundry_Debtors__c != null && m68Data.Net_sales__c  != null) {
                    return m68Data.Sundry_Debtors__c / m68Data.Net_sales__c ;
                }else{
                    return null;
                }
            }else{
                return null;
            }
        }catch(Exception e){
            system.debug('Exeption In:::::'+e.getCause()+'--------'+e.getLineNumber()+e.getMessage());
            return  null;
        }
    }

    public static Boolean getDeltaFlag(Decimal cred_velocity_n,Decimal cred_velocity_n_1,Decimal cred_velocity_n_2,Decimal est_proj_cred_deb){
        try{
            //Decimal delta_1,delta_2,delta_3;

            Decimal delta_1;
            Decimal delta_2;
            Decimal delta_3;

            if(cred_velocity_n_1 != null && cred_velocity_n_2 != null)
                delta_1 = cred_velocity_n_1 - cred_velocity_n_2;
            if(cred_velocity_n != null && cred_velocity_n_1 != null)
                delta_2 = cred_velocity_n - cred_velocity_n_1;
            if(est_proj_cred_deb != null && cred_velocity_n_1 != null)
                delta_3 = est_proj_cred_deb - cred_velocity_n;
            if(delta_3 != null && delta_3 > 0)
                return true;
            else if((delta_1 != null && delta_2 != null && delta_3 != null) && delta_1 > 0 && delta_2 >0 && delta_3 < 0)
                return true;
            else
                    return false;
        }catch(Exception e){
            system.debug('Exeption In::::'+e.getCause()+'--------'+e.getLineNumber()+e.getMessage());
            return null;
        }
    }

    public static boolean getYTDAnalysis(M68_Balance_Sheet_Analysis__c ytdm68Data, String purchaseOrSales,Decimal estimatedSalesOrPurchase){
        try{

            Decimal annualizedYTD;

            if(ytdm68Data != null && ytdm68Data.Net_sales__c != null && ytdm68Data.Month__c != null){
                if(purchaseOrSales == 'SALES')
                    annualizedYTD = ((ytdm68Data.Net_sales__c)*12)/Utility.convertYearToDateToNumber(ytdm68Data.Month__c);
                else if(purchaseOrSales == 'PURCHASE')
                    annualizedYTD = ((ytdm68Data.Purchases__c)*12)/Utility.convertYearToDateToNumber(ytdm68Data.Month__c);
            }
            if(annualizedYTD != null && estimatedSalesOrPurchase != null){
                return estimatedSalesOrPurchase < 0.9 * annualizedYTD ? TRUE : estimatedSalesOrPurchase > 1.1 * annualizedYTD ? TRUE:FALSE;
            }
            else {
                return null;
            }
        }catch(Exception e){
            system.debug('Exeption In::'+e.getCause()+'--------'+e.getLineNumber()+e.getMessage());
            return null;
        }
    }

    /**
     * Method to check if new limit aggregate of all facilities under a application is <= 50 L
     *
     * @param appId
     *
     * @Boolean flag
     */
    public static boolean isExceedingAdhocLessThanFifty(String appId){
        List<Facility__c> allfacilities                         = new List<Facility__c>();
        Decimal aggregateNewLimit                               = 0;

        try {

            allfacilities                                       = SanctionMatrixControllerHelper_EA.getAllFacilities(appId);
            aggregateNewLimit                                   = SanctionMatrixControllerHelper_EA.getNewLimitAggregate(allfacilities);

            return aggregateNewLimit <= 5000000 ? true : false;
        }catch (Exception e) {
            system.debug('Exeption In::'+e.getCause()+'--------'+e.getLineNumber()+e.getMessage());
            return null;
        }
    }

}