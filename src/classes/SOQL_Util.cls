/*
* Name     : SOQL_Util
* Company  : ET Marlabs
* Purpose  : This class is used as an utility for all SOQL Queries
* Author   : Amritesh
*/
public class SOQL_Util {

    public static List<genesis__Applications__c> getApplicationList(string branchCode){
        String queryString      = 'Select Id,Name,RecordType.DeveloperName,CustomerID__c,genesis__Account__c,genesis__Account__r.Name,Loan_Account_Number__c,Renewal_Due_Date__c,genesis__Loan_Amount__c,Pre_Renewal_Checklist__c,Application_Status__c,Application_UI_Stage__c,Application_Stage__c, (select id,Existing_Limit__c,Balance_Outstandings__c from Facilities__r)from genesis__Applications__c';
        queryString             += ' where genesis__CL_Product__c =:productId AND Branch_Code__c =:branchCode AND RecordTypeId =:rtypeID AND Active__c = true';
        return database.query(queryString);
        // Renewal_Due__c = true --Condition needs to be added
    }
    public static List<genesis__Applications__c> getApplicationList(string branchCode, string rtypeID, string productId){
        String queryString      = 'Select Id,Name,RecordType.DeveloperName,CustomerID__c,genesis__Account__c,genesis__Account__r.Name,Loan_Account_Number__c,Renewal_Due_Date__c,genesis__Loan_Amount__c,Pre_Renewal_Checklist__c,Application_Status__c,Application_UI_Stage__c,Application_Stage__c, (select id,Existing_Limit__c,Balance_Outstandings__c from Facilities__r)from genesis__Applications__c';
        queryString             += ' where genesis__CL_Product__c =:productId AND Branch_Code__c =:branchCode AND RecordTypeId =:rtypeID AND Active__c = true';
        return database.query(queryString);
        // Renewal_Due__c = true --Condition needs to be added
    }

    public static List<genesis__Applications__c> getApplicationList(List<string> appIds){
        String queryString      = 'Select Id,Name,RecordType.DeveloperName,CustomerID__c,genesis__Account__c,genesis__Account__r.Name,genesis__Account__r.CBS_Customer_ID__c,Loan_Account_Number__c,Renewal_Due_Date__c,genesis__Loan_Amount__c,Pre_Renewal_Checklist__c,Application_Status__c,Application_UI_Stage__c,Application_Stage__c, (select id,Existing_Limit__c,Balance_Outstandings__c from Facilities__r where Active__c = true)from genesis__Applications__c';
        queryString             += ' where Id IN:appIds AND  Active__c = true';
        return database.query(queryString);
        // Renewal_Due__c = true --Condition needs to be added
    }


    public static List<genesis__Applications__c> getCustApplicationList(string CustId, string rtypeID, string productId){
        String queryString      = 'Select Id,Name,RecordType.DeveloperName,CustomerID__c,genesis__Account__c,genesis__Account__r.Name,Loan_Account_Number__c,Renewal_Due_Date__c,Pre_Renewal_Checklist__c,Application_Status__c,Application_UI_Stage__c,Application_Stage__c,(select id,Existing_Limit__c,Balance_Outstandings__c from Facilities__r) from genesis__Applications__c';
        queryString             += ' where Renewal_Due__c = true AND genesis__CL_Product__c =:productId AND CustomerID__c =:CustId AND RecordTypeId =:rtypeID';
        return database.query(queryString);
    }

    public static List<genesis__Applications__c> getApplicationListForFetchCustomerList(List<string> appIds){
        String queryString      = 'Select Id,Name,Renewal_Due_Date__c,RecordType.DeveloperName,CustomerID__c,genesis__Account__c,genesis__Account__r.Name,genesis__Account__r.CBS_Customer_ID__c,Loan_Account_Number__c,genesis__Loan_Amount__c,Pre_Renewal_Checklist__c,Application_Status__c,Application_UI_Stage__c,Application_Stage__c, (select id,Existing_Limit__c,CL_Product__r.Id,CL_Product__r.Product_Code__c,Account_Number__c,Balance_Outstandings__c,Limit_Amount__c,CL_Product__c,Product_Name__c from Facilities__r where Active__c = true)from genesis__Applications__c';
        queryString             += ' where Id IN:appIds AND  Active__c = true';
        return database.query(queryString);
        // Renewal_Due__c = true --Condition needs to be added
    }

    public static List<Account> getAllCustomerByBranchCode(string branchCode) {
        return [SELECT Id,Name,CBS_Customer_ID__c,(SELECT Id,Renewal_Due_Date__c,Application_Stage__c,Application_UI_Stage__c from genesis__Applications_account__r) from Account where Branch_Code__c=:branchCode And RecordType.DeveloperName=:Constants.BUSINESS_ACCOUNT];
    }

    public static string getProductID(String productCode){
        return [Select id from clcommon__CL_Product__c where Product_Code__c =:productCode LIMIT 1].Id;
    }

    public static string getRecordTypeId(String rtypeName){
        return [Select id from RecordType where DeveloperName =:rtypeName].Id;
    }
    public static string getRecordTypeIdBySobject(String rtypeName,String obj){
        return [Select id,SobjectType from RecordType where DeveloperName =:rtypeName And SobjectType=:obj].Id;
    }

    public static string getCustomerRecordType(string customerId){
        return [Select Id,recordtypeId,recordtype.DeveloperName from Account where CBS_Customer_ID__c =:customerId limit 1].recordtype.DeveloperName;
    }

    public static Account getAccountAudit(string accountId){
        return [Select Id,CBS_Customer_ID__c,Branch_Code__c from Account where Id=:accountId limit 1];
    }

    // Get Application Facilities info
    public static List<Facility__c> getFacilities(string appId){
        if(appId != null && appId != '')
            return [Select Id,Existing_Spread__c,Recommended_Limit__c,CL_Product__c,CL_Product__r.clcommon__Product_Name__c,Limit_Amount__c,Existing_Limit__c,Existing_Rate__c,CL_Product__r.Product_Code__c,
                    Account_Number__c,Application__c,Total_Loan_Amount__c,Balance_Outstandings__c,New_Limit_Amount__c,Over_Due__c,Product_Name__c,Requested_Amount__c,Limit_Period_In_Days__c,
            (SELECT Id FROM Facility_Security__r)
            from Facility__c where Application__c=:appId AND RecordType.DeveloperName ='Parent'];
        else
                return null;
    }
    public static List<Facility__c> getFacilityList(List<String> appIdList){
        return [Select Id,Existing_Spread__c,Application__r.Sanction_Date__c,Application__r.OD_limit_creation_updation__c,Limit_Amount__c,Processing_Charge__c,Variance__c,Application__r.Next_Renewal_Date__c,Application__r.Cibil_Charges__c,Limit_start_date__c,Limit_end_date__c,Due_Date__c,Existing_Limit__c,Final_Spread__c,Recommended_Rate__c,Recommended_Limit__c,Application__r.Sanction_Authority__c,Application__r.Renewal_Due_Date__c,Application__r.genesis__Account__r.Branch_Code__c,Application__c,Application__r.Name,Recommended_Processing_Charge__c,CL_Product__c,CL_Product__r.clcommon__Product_Name__c,Existing_Rate__c,
                Account_Number__c,Total_Loan_Amount__c,Balance_Outstandings__c,New_Limit_Amount__c,Over_Due__c,CL_Product__r.Product_Code__c,Product_Name__c,Requested_Amount__c,Limit_Period_In_Days__c,
        (SELECT Id FROM Facility_Security__r)
        from Facility__c where Application__c IN :appIdList AND RecordType.DeveloperName ='Parent' AND Type__c = 'Funded'];
    }
    public static List<Facility__c> getLoanFacilityList(Set<Id> accIdList){
        return [Select Id,Due_Date__c,Recommended_Limit__c,Recommended_Rate__c,Limit_Amount__c,Application__r.Renewal_Due_Date__c,Application__r.genesis__Account__r.Branch_Code__c,Application__c,Application__r.Name,Recommended_Processing_Charge__c,CL_Product__c,CL_Product__r.clcommon__Product_Name__c,Existing_Rate__c,
                Account_Number__c,Total_Loan_Amount__c,Balance_Outstandings__c,New_Limit_Amount__c,Over_Due__c,CL_Product__r.Product_Code__c,Existing_Limit__c,Product_Name__c,Requested_Amount__c,Limit_Period_In_Days__c
        from Facility__c where Application__r.genesis__Account__c IN :accIdList AND RecordType.DeveloperName ='Parent' AND Application__r.RecordType.DeveloperName = 'Term_Loan'];
    }

    public static List<Customer_Feedback__c> getAllCustomerFeedback(String appId){
        List<Customer_Feedback__c> custFeedbackList = new List<Customer_Feedback__c>();
        If(appId != null){
            custFeedbackList = [SELECT Id,Name,Application__c,Question__c,Answer__c,Status__c FROM Customer_Feedback__c
            WHERE Application__c =: appId];
        }
        return custFeedbackList;

    }

    public static Map<Id,genesis__Applications__c> getApplicationDetail(string appId, string customerId){
        String custRecordType   = getCustomerRecordType(customerId);
        Map<Id,genesis__Applications__c> appln = new Map<Id,genesis__Applications__c>();
        if(custRecordType == Constants.BUSINESS_ACCOUNT){
            appln   = new Map<Id,genesis__Applications__c>([select id,Approved_Loan_Amount__c,Total_Networth__c,Total_Outstanding_Liabilities__c,Original_Amount_Investments__c,Overall_Exposure__c,Type__c,Application_to_close__c,Parent_Application__c,Enhancement_amount__c,Enhancement_reason__c,Name,Application_UI_Stage__c,Loan_Account_Number__c,New_Person__c,Deleted_Person__c,Recommended_Final_Rate_of_Interest__c,Sundry_Creditor_Atual__c,Sundry_Creditor_Estimated__c,Sundry_Creditor_Projected__c,
                    Debitor_Atual__c,Debitor_Estimated__c,Debitor_Projected__c,genesis__Account__c,genesis__Account__r.Name,genesis__Account__r.Contact_Person_Name__c,
                    Request_Reason__c,EAE_Renewal_Date_in_days__c,Reason_for_Application__c,genesis__Account__r.Contact_Person_Designation__c,genesis__Account__r.Previous_Constitution__c,genesis__Account__r.Line_of_Activity__c,genesis__Account__r.CIN_Number__c,Application_Status__c,Cash_Flow_Method__c,
                    genesis__Account__r.Pan_Number__c,genesis__Account__r.IE_Code__c,genesis__Loan_Amount__c,genesis__Account__r.Designation__pc,CustomerID__c,genesis__Account__r.Group_Name__c,genesis__Account__r.Reasons_for_exemption_from_GST__c,genesis__Account__r.Annual_TurnoverIncome__c,
                    genesis__Account__r.Aadhaar_Number__pc,genesis__Account__r.CBS_Customer_ID__c,genesis__Account__r.Education_Qualification__pc,genesis__Account__r.PersonBirthdate,Application_Stage__c,Sanction_ESign_Id__c,RecordType.DeveloperName,genesis__Account__r.Principal_nature_of_business__c,genesis__Account__r.Cash_Budget__c,
                    genesis__Account__r.PersonMobilePhone,genesis__Account__r.PersonEmail,genesis__Account__r.BillingStreet,genesis__Account__r.Number_of_Shares_Held__c,RecordTypeId,
                    genesis__Account__r.BillingCity,genesis__Account__r.Limit_Assesment_Closing_Balance__c,genesis__Account__r.BillingPostalCode,genesis__Account__r.BillingState,genesis__Account__r.BillingCountry,genesis__Account__r.Latest_Date_Of_Reconstitution__c,
                    genesis__Account__r.ShippingStreet,genesis__Account__r.ShippingCity,genesis__Account__r.ShippingPostalCode,genesis__Account__r.Amount_of_Shares__pc,
                    genesis__Account__r.Balance_Outstandings__c,genesis__Account__r.Name_of_Facility__c,genesis__Account__r.Related__c,genesis__Account__r.ShippingState,genesis__Account__r.ShippingCountry,genesis__Account__r.Percentage_of_holding__pc,genesis__Interest_Rate__c,
                    genesis__Margin__c,genesis__Account__r.Group__c,genesis__Account__r.Name__c,genesis__Account__r.Constitution__c,genesis__Account__r.Present_interest__c,genesis__Account__r.Phone,
                    genesis__Account__r.Over_Due__c,genesis__Account__r.Industry_Type__c,genesis__Account__r.Firm_Company_was_Reconstituted__c,genesis__Account__r.GST_Number__c,genesis__Account__r.Total_Loan_Amount__c,
            (Select id,genesis__Collateral__r.clcommon__City__c,genesis__Collateral__r.clcommon__Collateral_Category__r.Name,genesis__Collateral__r.clcommon__Collateral_Name__c,genesis__Collateral__r.clcommon__Collateral_Type__r.Name,genesis__Collateral__r.clcommon__Collateral_Type__c,
                    genesis__Collateral__r.clcommon__Collateral_Category__c,Facility__c,Account__c,Application__c,genesis__Collateral__r.id,genesis__Collateral__r.clcommon__Value__c,genesis__Collateral__r.Ownership_Type__c,genesis__Collateral__r.clcommon__Address__c,
                    genesis__Collateral__r.clcommon__Value_Date__c,genesis__Collateral__r.Total_Asset_Value__c,genesis__Collateral__r.Nature_of_Property__c,genesis__Collateral__r.SurveyKhasraKhataPatta_No__c,genesis__Collateral__r.MORT_TYPE__c,genesis__Collateral__r.Collateral_Category__c,genesis__Collateral__r.Door__c,
                    genesis__Collateral__r.Percentage_of_Ownership__c,genesis__Collateral__r.Landmark__c,genesis__Collateral__r.TalukaTehsil__c,genesis__Collateral__r.Area_Square_Feet__c,genesis__Collateral__r.Street_NameNo__c,genesis__Collateral__r.State__c,genesis__Collateral__r.Country__c,genesis__Collateral__r.clcommon__Postal_Code__c,Security_Type__c from Application_Collateral__r),
            (Select id,genesis__Application__c,genesis__Party_Account_Name__r.FirstName,genesis__Party_Account_Name__r.LastName,genesis__Party_Account_Name__r.Name,
                    genesis__Party_Account_Name__r.Passport_Number__pc, genesis__Party_Account_Name__r.Gender__pc,genesis__Party_Account_Name__r.Nationality__pc,
                    genesis__Party_Account_Name__r.Father_Name__pc ,genesis__Party_Account_Name__r.Marital_Status__pc,genesis__Party_Account_Name__r.Designation__pc,
                    genesis__Party_Account_Name__r.Caste__pc,genesis__Party_Account_Name__r.DIN_Number__pc,genesis__Party_Account_Name__r.Pan_Number__c,
                    genesis__Party_Account_Name__r.Location_Type__pc,genesis__Party_Account_Name__r.Built_up_Area__pc,genesis__Party_Account_Name__r.Number_of_Storeys__pc,
                    genesis__Party_Account_Name__r.Joint_holder_CUTSTOMER_NAME__c,genesis__Party_Account_Name__r.Value_Of_Land_Building__pc,
                    genesis__Party_Account_Name__r.Land_and_building__pc,genesis__Party_Account_Name__r.Residential_Status__pc,genesis__Party_Account_Name__r.Spouse_Name__pc,
                    genesis__Party_Account_Name__r.Already_Charged__pc,genesis__Party_Account_Name__r.Type_of_Land__pc,genesis__Party_Account_Name__r.Type_of_Ownership__pc,
                    genesis__Party_Account_Name__r.Aadhaar_Number__pc,genesis__Party_Account_Name__r.CBS_Customer_ID__c,genesis__Party_Account_Name__r.Area_of_Land__pc,
                    genesis__Party_Account_Name__r.Education_Qualification__pc,genesis__Party_Account_Name__r.Net_Worth__pc,genesis__Party_Account_Name__r.Value_Of_Land__pc,
                    genesis__Party_Account_Name__r.PersonMailingStreet,genesis__Party_Account_Name__r.PersonMailingCity,genesis__Party_Account_Name__r.Name_Of_Director_Related_To__c,
                    genesis__Party_Account_Name__r.PersonMailingState,genesis__Party_Account_Name__r.PersonMailingPostalCode,genesis__Party_Account_Name__r.Relationship__c,
                    genesis__Party_Account_Name__r.Previous_Contact_Number__c,genesis__Party_Account_Name__r.Previous_Contact_Person__c,genesis__Party_Account_Name__r.Bank_name__c,
                    genesis__Party_Account_Name__r.Previous_GST_Number__c,genesis__Party_Account_Name__r.Previous_Whether_the_firm_was_reconstitu__c,genesis__Party_Account_Name__r.Is_KVB_Director__c,
                    genesis__Party_Account_Name__r.Previous_Address_of_Land__pc,genesis__Party_Account_Name__r.Previous_Education_Qualification__pc,
                    genesis__Party_Account_Name__r.Previous_Net_Worth__pc,genesis__Party_Account_Name__r.Previous_Type_of_Land__pc,genesis__Party_Account_Name__r.Previous_Value_of_Land__pc,
                    genesis__Party_Account_Name__r.PersonMailingCountry,genesis__Party_Account_Name__r.Guarantor_Networth__c,Guarantor__c,genesis__Party_Account_Name__r.Existing_Networth__c,
                    genesis__Party_Account_Name__r.Property_City__c,genesis__Party_Account_Name__r.Property_Country__c,genesis__Party_Account_Name__r.Property_State__c,
                    genesis__Party_Account_Name__r.Property_Street__c,genesis__Party_Account_Name__r.Property_Type__c,genesis__Party_Account_Name__r.Property_Value__c,genesis__Party_Account_Name__r.Propety_Pincode__c,
                    genesis__Party_Account_Name__r.PersonMobilePhone,genesis__Party_Account_Name__r.PersonEmail,genesis__Party_Account_Name__r.Is_Physically_Handicapped__c,genesis__Party_Account_Name__r.Is_Ex_Service_Man__c,
                    Key_Contact__c,genesis__Party_Account_Name__r.PersonBirthdate,genesis__Party_Account_Name__c,genesis__Party_Type__c,Active__c,Is_New__c,Critical_Change__c,Status__c,Product_Type__c from genesis__Application_Parties__r WHERE Active__c = true)
            from genesis__Applications__c where Id =:appId]);
        }

        return appln;
    }
    public static List<Group_Concern__c> getGroupConcern(String accID){
        return [select id,Group_1__c,Group_1__r.Name,Group_2__r.Limit__c,Group_2__r.Bank_name__c,Group_2__r.Pan_Number__c,Type__c,Limit_Amount__c,Bank_Name__c,Pan_card_Number__c,Group_2__c,Group_2__r.Name from Group_Concern__c where Group_1__c =:accID];
    }
    public static List<Property__c> getProperty(Id accId) {
        return [select id,Existing_Networth__c,Nature_of_Property__c,Property_City__c,Property_Country__c,Property_Pincode__c,Property_State__c,Property_Street__c,Property_Type__c,Property_value__c,Area__c,Asset_Type__c,Details_of_mortgage_if_any__c,Door_Number__c,Ownership_percentage__c,Ownership_Type__c,Survey_No__c,Taluk__c,Nearest_Landmark__c from Property__c where Account__c =:accId];
    }
    public static List<Debtors__c> getDebtors(string customerId){
        return [select id,Age__c,Customer_Name__c,Amount_Collected__c,Fiscal_Year__c from Debtors__c where Account__r.CBS_Customer_ID__c =:customerId];
    }
    public static List<Sundry_Creditors__c> getSundryCreditors(string customerId){
        return [select id,Age__c,Amount_To_Be_Paid__c,Name_Of_Trade_Creditors__c,Fiscal_Year__c from Sundry_Creditors__c where Account__r.CBS_Customer_ID__c =:customerId];
    }
    //Get M68C data filtere>d by customer id and financial year list(2015-2016,2016-2017...)
    public static List<M68_Balance_Sheet_Analysis__c> getM68Data(String accId,List<String> fyList){
        List<M68_Balance_Sheet_Analysis__c> m68DataList = new List<M68_Balance_Sheet_Analysis__c>();
        String query ='SELECT ';
        Map<String, Schema.SObjectField> accountFieldMap = Schema.getGlobalDescribe().get( 'M68_Balance_Sheet_Analysis__c' ).getDescribe().fields.getMap();
        for(Schema.SObjectField s : accountFieldMap.values()){
            query = query + s.getDescribe().getName()+',';
        }
        query   = query.removeEnd(',');
        query   = query + ' from M68_Balance_Sheet_Analysis__c WHERE Account__c =\''+accId+'\' AND Fiscal_Year__c IN :fyList';

        system.debug('===Start Query :::'+query);
        m68DataList = Database.query(query);
        RETURN m68DataList;
    }
    public static String getRandomExternalIdForCollateral(){

        String leftPaddedString = getRandomString();
        if(checkDupliacteCollateralByExternalId(leftPaddedString)){
            return 'FE-'+leftPaddedString;
        }else{
            return getRandomExternalIdForCollateral();
        }

    }

    public static String getRandomString(){
        Integer randomInteger = Crypto.getRandomInteger();
        String randomString=String.valueOf(math.abs(randomInteger));
        String leftPadding = '000000';
        String leftPaddedString = randomString.leftPad(20,leftPadding);
        return leftPaddedString;
    }
    public static Boolean checkDupliacteCollateralByExternalId(String externalId){
        List<clcommon__Collateral__c> allCollateral = [Select Id,External_ID__c from clcommon__Collateral__c];
        for(clcommon__Collateral__c collatrrec:allCollateral){
            if(collatrrec.External_ID__c == 'FE-'+externalId)
                return false;
        }
        return true;
    }
    public static List<genesis__Applications__c> getApplication(Set<Id> appIdList){
        Map<String, Schema.SObjectField> accountFieldMap = Schema.getGlobalDescribe().get( 'genesis__Applications__c' ).getDescribe().fields.getMap();
        String query = 'SELECT ';
        for(Schema.SObjectField s : accountFieldMap.values()){
            query = query + s.getDescribe().getName()+',';
        }
        query   = query.removeEnd(',');
        query   = query + ' from genesis__Applications__c WHERE Id IN :appIdList';

        system.debug('===Start Query :::'+query);
        RETURN Database.query(query);
    }

    public static User getRLPCUser(String appId){
        User rlpcUser   = new User();
        try{
            String appOwner = [Select OwnerId from genesis__Applications__c where id=:appId limit 1].OwnerId;
            String DivisVal = [Select Division from User where id=:appOwner limit 1].Division;
            rlpcUser        = [Select id from User where Designation__c='CLPC head' AND Division=:DivisVal limit 1];

        }catch(Exception e){}
        return rlpcUser;
    }
    public static genesis__Applications__c getExistingApplication(String CustId,String APP_TYPE){

        List<String> appStage = new List<String>{'Limit renewed/Application close','Application close - enhancement','Enhancement Application close','Exceeding Application close','Adhoc Application close'};
        List<genesis__Applications__c>    listAppObject   =   [select id,Application_Stage__c,genesis__Account__r.CBS_Customer_Id__c,RecordType.DeveloperName from genesis__Applications__c where genesis__Account__r.CBS_Customer_Id__c =: CustId AND RecordType.DeveloperName =: APP_TYPE AND Application_Stage__c NOT IN : appStage AND Active__c = true Order by  createddate LIMIT 1 ];
        if(! listAppObject.isEmpty()){
            return listAppObject[0];
        }else{
            return null;
        }

    }
    //using for update api delete functionality


    public static String getRecordTypeNameByRecordId(Id recId){

        String recordQuery = 'SELECT Id,RecordType.developerName from '+String.valueOf(recId.getSObjectType())+' WHERE Id=:recId '+' Limit 1';
        genesis__Applications__c record = new genesis__Applications__c();
        record = DataBase.Query(recordQuery);

        return record.RecordType.developerName;
    }
    public static List<genesis__Applications__c> getApplicationDueDate(String CustId){
        List<genesis__Applications__c>    listAppObject   =  [select id,Application_Stage__c,genesis__Account__r.CBS_Customer_Id__c,Renewal_Due_Date__c,RecordType.DeveloperName from genesis__Applications__c where genesis__Account__r.CBS_Customer_Id__c=: CustId AND Active__c = true];
        if(!listAppObject.isEmpty()){
            return listAppObject;
        }else{
            return null;
        }
    }
    public static Account getAccountDetails(String pancard,String gst){
        Boolean flag=false;

        String query='SELECT id,Name,Pan_Number__c,GST_Number__c from Account ';
        query+='WHERE ';
        if((pancard !=null && pancard !='') &&  (gst !=null && gst !=''))
            query+= 'Pan_Number__c =:pancard AND GST_Number__c =:gst ';
        else if((pancard !=null && pancard !=''))
            query+= 'Pan_Number__c =:pancard ';
        else if((gst !=null && gst !=''))
            query+= 'GST_Number__c =:gst ';
        else
                flag=true;

        query +='LIMIT 1';
        System.debug('Query'+query);
        if(!flag){
            List<Account> sobjList = Database.query(query);
            If(!sobjList.isEmpty()){
                return sobjList[0];
            }
        }
        return null;

    }
    public static List<clcommon__Collateral__c> getAppCollaterals(String appId){
        List<clcommon__Collateral__c> delQuery = [SELECT Id, genesis__Application__c, (Select Id, Account__r.Name from Ownerships__r) FROM clcommon__Collateral__c WHERE genesis__Application__c=:appId];
        System.debug('delQuery'+delQuery);
        return delQuery;
    }

    public static List<genesis__Application_Parties__c> getFirmDetails(String appleId){
        List<genesis__Application_Parties__c> businessParties = [SELECT Id, Applicant_Name__c, genesis__Application__c, Company__c, Key_Contact__c, genesis__Party_Account_Name__c, Party_Sub_Type__c, genesis__Party_Type__c, Product_Type__c, Status__c FROM genesis__Application_Parties__c WHERE Company__c = true];
        System.debug('businessParties'+businessParties);
        return businessParties;
    }
    public static Boolean getApplicationProbe42(String appId){
        If(appId !=null && appId !=''){
            genesis__Applications__c  appObj = [select id,is_Probe_42__c from genesis__Applications__c where id =:appId];
            return appObj.is_Probe_42__c;
        }
        return false;
    }
	public static genesis__Applications__c getApplicationId_By_Name(String AppName,String appId){
         System.debug('AppName@@@'+AppName);
        If((AppName !=null && AppName !='') || (appId !=null && appId !='')){
            System.debug('AppName@@@'+AppName);
            genesis__Applications__c appObject = [select id,Name,genesis__Account__r.CBS_Customer_ID__c,CreatedDate from genesis__Applications__c where Name =:AppName OR Id =:appId];
            return appObject;
        }
        return null;
    }
}