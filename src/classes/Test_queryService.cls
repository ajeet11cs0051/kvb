@isTest
public class Test_queryService {
    public static genesis__Applications__c genApp;
    
	@isTest
    public static void methodGetApplicants(){
        try{
        genApp = TestUtility.intialSetUp('Home Loan', true);
        Account acc = new Account(name='TestQuery');
        INSERT acc;
        
        genesis__Application_Parties__c genPar = new genesis__Application_Parties__c();
        genPar.genesis__Party_Account_Name__c = acc.Id;
        genPar.Key_Contact__c = acc.Id;
        genPar.genesis__Application__c = genApp.Id;
        INSERT genPar;
        //genesis__Application_Parties__c genpar = [SELECT id,Key_Contact__c,genesis__Party_Account_Name__c from genesis__Application_Parties__c  where genesis__Application__c=:genApp.Id];
        
        Test.startTest();
        queryService.getApplicants(genApp.Id);
        queryService.getApplication(genApp.Id);
        Test.stopTest();
        }catch(Exception e){}
    }
}