public class Form26asResponse{
	public cls_Form26ASInfo Form26ASInfo;
	public class cls_Form26ASInfo {
		public cls_PersonalDetails PersonalDetails;
		public cls_TDSDetails[] TDSDetails;
		public cls_NonTDSDetails[] NonTDSDetails;
	}
	public class cls_PersonalDetails {
		public String name;	//KRISHNA KUMAR TADIPARTI
		public String address;	//NO 491 RAM NAGAR, KVB STAFF QUARTERS, OFF SALEM BYEPASS ROAD, VENGAMEDU POST, KARUR, TAMILNADU, 639006
		public String dob;	//1967-08-15
		public String pan;	//ABTPT6730E
		public String panStatus;	//Active
	}
	public class cls_TDSDetails {
		public String fy;	//2016-17
		public String ay;	//2017-18
		public cls_TDSs TDSs;
	}
	public class cls_TDSs {
		public cls_TDS[] TDS;
	}
	public class cls_TDS {
		public String tds;	//8202
		public String section;	//192
		public String deductor;	//THE KARUR VYSYA BANK LTD CENTRAL OFFICE
		public String TDSDate;	//2016-04-30
		public String amount;	//105402
		public String tan;	//CHET04666E
	}
	public class cls_NonTDSDetails {
		public String fy;	//2016-17
		public String ay;	//2017-18
	}
	public static Form26asResponse parse(String json){
		return (Form26asResponse) System.JSON.deserialize(json, Form26asResponse.class);
	}
	}