@isTest
public class Test_APPDocCategoryCreation {
    public static genesis__Applications__c genApp;
    public static genesis__Document_Category__c genDocCat;
    public static Account acc;
    
    @isTest
    public static void init(){
        genApp = TestUtility.intialSetUp('Home Loan', true);
        genDocCat = TestUtility.createDocumentCat(Constants.FIReport);
        acc = new Account(name = 'Priti@');
    }
    @isTest
    public static void methodCreateDocCat(){
        init();
        
        APPDocCategoryCreation.CreateDocCat(genApp, acc, true);
        APPDocCategoryCreation.CreateDocCat(genApp, acc, false);
    }

}