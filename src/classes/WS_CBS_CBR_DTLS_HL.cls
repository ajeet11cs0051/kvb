/*
* Name    : WS_CBS_CBR_DTLS_HL
* Company : ET Marlabs
* Purpose : This class Will be used to CBS (CBR) 
* Author  : Subas
*/
public class WS_CBS_CBR_DTLS_HL {
    @future (Callout=true)
    public static void CBR_DTLSF(String AppID){
        CBR_DTLS(AppID);
    }
    
    public static void CBR_DTLS(String AppID){
        genesis__Applications__c application = new genesis__Applications__c();
        application = queryService.getApp(appID);
        
        CBR_DTLS_HELPER_HL cbs = new CBR_DTLS_HELPER_HL();
        cbs.inputVariables = new CBR_DTLS_HELPER_HL.cls_inputVariables();
        CBR_DTLS_HELPER_HL.cls_inputVariables cbsInput = new CBR_DTLS_HELPER_HL.cls_inputVariables();
        cbsInput.in_msg = new CBR_DTLS_HELPER_HL.cls_in_msg();
        cbs.inputVariables = cbsInput;
        cbs.exe_tenantId = 'cuecent_tenant';
        cbs.owner_tenantId = 'cuecent_tenant';
        cbsInput.in_msg.AccountNumber  = application.Loan_Account_Number__c;	//1219223000000526
        cbsInput.in_msg.CustomerID  = application.CustomerID__c;	//28957
        cbsInput.in_msg.MISType  = 'LN';//'CH';	//CH
        cbsInput.in_msg.GovtScheme  = '';	//null
        cbsInput.in_msg.Guarantee  = '';	//null
        cbsInput.in_msg.Others  = '';	//null
        cbsInput.in_msg.Refinance  = '';	//null
        cbsInput.in_msg.SanctionReference  = '';	//null
        cbsInput.in_msg.SanctionDate  =  application.Sanction_Date__c != null ? formatDate(application.Sanction_Date__c) : null;//'07102017';
        cbsInput.in_msg.DateoOfDPN  = application.Sanction_Date__c != null ? formatDate(application.Sanction_Date__c) : null; //formatDate(application.Created_Date__c); //'07102017';
        cbsInput.in_msg.DateOfRevivalLetter  = '';	//null
        cbsInput.in_msg.PSDateOfValuation_Inspection  = '';	//null
        cbsInput.in_msg.Investment_In_P_And_M  = '';	//null
        cbsInput.in_msg.CreditRiskRating  = '';	//null
        cbsInput.in_msg.RiskClass  = '';	//null
        cbsInput.in_msg.SME  = '';	//null
        cbsInput.in_msg.RiskWeight  = '';	//null
        cbsInput.in_msg.LoanDate  = application.Sanction_Date__c != null ? formatDate(application.Sanction_Date__c) : null;//formatDate(application.Created_Date__c);//'07102017';	//null create date
        cbsInput.in_msg.SanctionBy  = 1;	//0
        cbsInput.in_msg.PSValue_On_Date_Of_Inspection  = 0;	//0
        cbsInput.in_msg.CSValue_On_Date_Of_Inspection  = 0;	//0
        cbsInput.in_msg.SubsidyAmount  = 0;	//0
        cbsInput.in_msg.Margin  = 0;	//0
        cbsInput.in_msg.Code_No_Of_Manager_Sanction  = 0;	//0
        cbsInput.in_msg.RBIPurposeCode  = 0;	//0
        cbsInput.in_msg.ENCPortCode  = 0;	//0
        cbsInput.in_msg.SectorEngagedIn  = 0;	//0
        cbsInput.in_msg.Investment_In_Equipment_Service  = 0;	//0
        cbsInput.in_msg.Sales_Turnover_Rupees  = 0;	//0
        cbsInput.in_msg.Number_Of_Items_Jewels  = 0;	//0
        cbsInput.in_msg.Jewels_Gross_Weight_gms  = 0;	//0
        cbsInput.in_msg.Jewels_Net_Weight_gms  = 0;	//0
        cbsInput.in_msg.Category_Of_Borrowers  = 99;	//0
        cbsInput.in_msg.MakerID  = 'TELLER';	//TELLER
        cbsInput.in_msg.CheckerID  = 'SYSSUPER';	//SYSSUPER
        getCBR(JSON.serialize(cbs),AppID);
    }
    public static void getCBR(String cbrData,String AppID){
        system.debug('%%%%%'+cbrData);
        KVB_Endpoint_URLs__c kvb = KVB_Endpoint_URLs__c.getValues('CBS_CBR_DTLS_MNT_HL');
        Map<String,String> headerMap                            = new Map<String,String>();
        headerMap.put('Content-Type','application/json');
        HTTPResponse response                                   = new HTTPResponse();
        String endPoint                                         = kvb.Endpoint_URL__c;
        response = HttpUtility.sendHTTPRequest(endPoint, 'POST', null,cbrData,headerMap,label.CA_CERTIFICATE);   
        system.debug('#####'+response);
        system.debug('$$$$'+response.getBody());
        If(response.getStatusCode() == 200 || response.getStatusCode() == 201){
            String jsonString = response.getBody();            
            CBR_DTLS_HELPER_HL.CBR_DTLS_RESPONSE res = (CBR_DTLS_HELPER_HL.CBR_DTLS_RESPONSE)Json.deserialize(jsonString,CBR_DTLS_HELPER_HL.CBR_DTLS_RESPONSE.class);
            System.debug(res);
            System.debug(res.out_msg.ErrorMessage);
            if(res.out_msg.ErrorMessage == 'Success'){
                //WS_CBS_HOST_Disbursment_HL.Host_request(AppId);
                WS_CBS_MIS_Update_HL.Mis_Update(AppId);
            }else{
                CBS_API_Log__c log = new CBS_API_Log__c();    
                log.Application__c = AppId;
                log.API_Name__c = 'CBR_Update';
                log.Status__c = 'Failure';
                log.Success_Error_Message__c = res.out_msg.ErrorMessage;
                log.Sequence_No__c = '8';  
                insert log;
            }
        }        
    }
    public static String formatDate(Date d) {
        return String.ValueOf(d.day() + d.month() + d.year());
    }
}