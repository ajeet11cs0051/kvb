/*
* Name          : DocumentFetch
* Description   : Prepares the structure for document category fetch request
* Author        : Dushyant
*/
public class DocumentFetch {
    //get document category and create application document category if not there.
    public static List<genesis__Application_Document_Category__c> checkDocumentCategory(String appId,List<String> documentCategoryType){
        try{
            System.debug('documentCategoryType---> ' + documentCategoryType);
            Map<Id,genesis__Document_Category__c> docCatList;
            List<genesis__Application_Document_Category__c> appDocCategoryList          = new List<genesis__Application_Document_Category__c>();
            Set<string> documentSet                                                     = new set<string>();
            documentSet.addAll(documentCategoryType);
            docCatList = new Map<Id,genesis__Document_Category__c>([SELECT Id,genesis__Category_Name__c,Doc_Category_No__c,Document_Category__c FROM genesis__Document_Category__c WHERE Document_Category__c IN :documentCategoryType order by Doc_Category_No__c]);
            
            appDocCategoryList = [SELECT Id,Name,App_Doc_Category_No__c,Parent_Category_Name__c,genesis__Document_Category__c,genesis__Document_Category__r.Document_Category__c FROM genesis__Application_Document_Category__c WHERE genesis__Document_Category__r.Document_Category__c IN: documentCategoryType AND genesis__Application__c =: appId];
            
            set<string> docParentIds    = new set<string>();
            for(genesis__Application_Document_Category__c  ap : appDocCategoryList){
                docParentIds.add(ap.genesis__Document_Category__c);
            }
            List<genesis__Document_Category__c> doclistToAdd    = new List<genesis__Document_Category__c>();
            for(string dcatId : docCatList.keySet()){
                    if(!docParentIds.contains(dcatId)){
                        if(docCatList.containsKey(dcatId))
                            doclistToAdd.add(docCatList.get(dcatId));
                }
            }
            System.debug(documentSet+'--dfsdf-'+doclistToAdd);
            if(!doclistToAdd.isEmpty()){
                List<genesis__Application_Document_Category__c> newCategoryList = new List<genesis__Application_Document_Category__c>();
                 genesis__Application_Document_Category__c parentCatg = new genesis__Application_Document_Category__c(Name = 'SME_'+appId+'_Root' , genesis__Application__c = appId,External_ID__c='SME_'+appId+'_Root');
                  upsert parentCatg External_ID__c;
                    
                    for(genesis__Document_Category__c dcat : doclistToAdd){
                        newCategoryList.add(new genesis__Application_Document_Category__c(Name = dcat.genesis__Category_Name__c , genesis__Application__c = appId , genesis__Document_Category__c = dcat.Id, genesis__Parent_Application_Document_Category__c=parentCatg.id,App_Doc_Category_No__c=dcat.Doc_Category_No__c));
                    }
                    INSERT newCategoryList;
                    
                    return [SELECT Id,Name,App_Doc_Category_No__c,Parent_Category_Name__c,genesis__Document_Category__c,genesis__Document_Category__r.Document_Category__c FROM genesis__Application_Document_Category__c WHERE genesis__Document_Category__r.Document_Category__c IN: documentCategoryType AND genesis__Application__c =: appId];
            }
            
            RETURN appDocCategoryList;
        }
        catch(Exception e){
            System.debug(e.getMessage());
            System.debug(e.getLineNumber());
            RETURN null;
        }
    }
    public static List<genesis__Application_Document_Category__c> checkDocumentCategory2(String appId,List<String> documentCategoryType,List<String> missingFin){
        try{
            System.debug('documentCategoryType---> ' + documentCategoryType);
            List<genesis__Document_Category__c> docCatList                              = new List<genesis__Document_Category__c>();
            List<genesis__Application_Document_Category__c> appDocCategoryList          = new List<genesis__Application_Document_Category__c>();
            docCatList = [SELECT Id,genesis__Category_Name__c,Required_for_Missing_Financial__c,Doc_Category_No__c,Document_Category__c FROM genesis__Document_Category__c WHERE Document_Category__c IN :documentCategoryType order by Doc_Category_No__c];
            System.debug('docCatList---> ' + docCatList);
            appDocCategoryList = [SELECT Id,Name,App_Doc_Category_No__c,genesis__Document_Category__r.Document_Category__c FROM genesis__Application_Document_Category__c WHERE genesis__Document_Category__r.Document_Category__c IN: documentCategoryType AND genesis__Application__c =: appId];
            System.debug(appDocCategoryList.size()+'---'+appDocCategoryList);
            if(appDocCategoryList.isEmpty()){
                genesis__Application_Document_Category__c parentCatg = new genesis__Application_Document_Category__c(Name = 'SME_'+appId+'_Root' , genesis__Application__c = appId);
                insert parentCatg;
                System.debug('---'+parentCatg.Id);
                Decimal flag=10000;
                for(genesis__Document_Category__c dcat : docCatList){
                    flag++;
                    if(missingFin!= null && missingFin.size()>0){
                        //flag++;
                        for(String year:missingFin){
                            flag++;
                            if(dcat.Required_for_Missing_Financial__c)
                                appDocCategoryList.add(new genesis__Application_Document_Category__c(Name = dcat.genesis__Category_Name__c+' '+year, genesis__Application__c = appId , genesis__Document_Category__c = dcat.Id, genesis__Parent_Application_Document_Category__c=parentCatg.id,App_Doc_Category_No__c=flag+dcat.Doc_Category_No__c));

                        }
                    }
                    //if(! dcat.Required_for_Missing_Financial__c)
                    appDocCategoryList.add(new genesis__Application_Document_Category__c(Name = dcat.genesis__Category_Name__c+' '+YearDataConstant.currFiscalYear , genesis__Application__c = appId , genesis__Document_Category__c = dcat.Id, genesis__Parent_Application_Document_Category__c=parentCatg.id,App_Doc_Category_No__c=dcat.Doc_Category_No__c));


                }
                INSERT appDocCategoryList;
            }
            RETURN appDocCategoryList;
        }
        catch(Exception e){
            System.debug(e.getMessage());
            System.debug(e.getLineNumber());
            RETURN null;
        }
    }
    //Get financial details against an application
    public static List<genesis__AppDocCatAttachmentJunction__c> getFinanceDetails(String appId){
        System.debug('appId#####'+appId);
        List<genesis__AppDocCatAttachmentJunction__c> appDocCategoryList    = new List<genesis__AppDocCatAttachmentJunction__c>();
        appDocCategoryList = [SELECT Id,Name,genesis__Application_Document_Category__r.Name,genesis__AttachmentId__c,genesis__Application_Document_Category__r.App_Doc_Category_No__c FROM genesis__AppDocCatAttachmentJunction__c WHERE genesis__Application_Document_Category__r.genesis__Application__c =: appId];
        System.debug(appDocCategoryList.size()+'---'+appDocCategoryList);
        RETURN appDocCategoryList;
    }
    //Delete other document and documentCategoryJunctionObject against document category after document insert
    public static void deleteDocAndDocumentCategory(Map<Id,genesis__AppDocCatAttachmentJunction__c> docJuncMap){
        List<genesis__AppDocCatAttachmentJunction__c> docCatJuncToDelete   = new List<genesis__AppDocCatAttachmentJunction__c>();
        List<Attachment> attToDeleteList                   = new List<Attachment>();
        Set<Id> catIdSet                           = new Set<Id>();
        Set<Id> attIsSet                           = new Set<Id>();
        List<String> docCategoryToExclude                   = new List<String>{'Individual ITR','Constitution Supporting Document','Constitution Deed Document'};
        for(genesis__AppDocCatAttachmentJunction__c catJuncObj : docJuncMap.values()){
            System.debug(catJuncObj.genesis__Application_Document_Category__c);
            System.debug(catJuncObj.genesis__Application_Document_Category__r.genesis__Application__c);
            System.debug(catJuncObj.genesis__Application_Document_Category__r.genesis__Application__r.RecordType.DeveloperName);
            System.debug(Constants.SME_APP_RECORD_TYPE);
           // if(catJuncObj.genesis__Application_Document_Category__r.genesis__Application__r.RecordType.DeveloperName == Constants.SME_APP_RECORD_TYPE){
                catIdSet.add(catJuncObj.genesis__Application_Document_Category__c);
            //}
        }
        if(!catIdSet.isEmpty())
          docCatJuncToDelete = [SELECT Id,genesis__AttachmentId__c FROM genesis__AppDocCatAttachmentJunction__c WHERE genesis__Application_Document_Category__c IN :catIdSet AND Id != :docJuncMap.keySet() AND genesis__Application_Document_Category__r.Name NOT IN :docCategoryToExclude AND genesis__Application_Document_Category__r.genesis__Application__r.RecordType.DeveloperName =: Constants.SME_APP_RECORD_TYPE];
        if(!docCatJuncToDelete.isEmpty()){
            for(genesis__AppDocCatAttachmentJunction__c catJunc : docCatJuncToDelete){
                attIsSet.add(catJunc.genesis__AttachmentId__c);
            }
            if(!attIsSet.isEmpty()){
                 attToDeleteList = [SELECT Id FROM Attachment WHERE Id IN :attIsSet];
            }
        }
        if(!docCatJuncToDelete.isEmpty()){
            DELETE docCatJuncToDelete;
        }
        if(!attToDeleteList.isEmpty()){
            DELETE attToDeleteList;
        }
    }
    
    //Wrapper Classes
    public class DocCatRequest{
        public List<DocumentCategory> CATEGORY_LIST;
    }
    
    public class DocumentCategory{
        public String LABEL_ID;
        public String CATEGORY_NAME;
        public String CATEGORY_ID;
        //Constructor
        public DocumentCategory(String LABEL_ID,String catName, String catId){
            this.LABEL_ID  = LABEL_ID;
            this.CATEGORY_NAME = catName;
            this.CATEGORY_ID = catid;
        }
        //Constructor
        public DocumentCategory(){}
    }
    
    public class FinanceRequest{
        public List<FinanceSection> FINANCIAL_SECTION;
    }
    
    public class FinanceSection{
        public String FILE_NAME;
        public String FILE_URL;
        public String LOS_FINANCE_FILE_ID;
        //Constructor
        public FinanceSection(String fName, String fURL, String fId){
            this.FILE_NAME = fName;
            this.FILE_URL = fURL;
            this.LOS_FINANCE_FILE_ID = fId;
        }
        //Constructor
        public FinanceSection(){}
    }
}