@isTest
public class Test_Ws_IDV_CIBILResponse {
  public Static genesis__Applications__c genApp = TestUtility.intialSetUp('HomeLoan', true);
    //public static Account acc = new Account();
    
    
    
    
    @isTest
    public static void getIDVCreating(){

        Account acc=new Account();
        acc.FirstName = 'TestWs';
        acc.MiddleName = '_IDV_';
        acc.LastName = 'CIBILResponse';
        acc.Gender__pc = 'Male';
        acc.PersonBirthdate = system.today();
        acc.Aadhaar_Number__pc = '123343233444';
        acc.Pan_Number__c = 'EQAD2133RJ';
        acc.Verify_PAN_Number__c = 'Yes';
        acc.Is_PAN_card_available__c = true;
        acc.PersonMailingStreet='test@yahoo.com';
        acc.PersonMailingPostalCode='12345';
        acc.PersonMailingCity='SFO';
        acc.PersonEmail='test@yahoo.com';
        acc.PersonHomePhone='1234567';
        acc.PersonMobilePhone='12345678' ;
        //acc.= genApp.Id;
        insert acc;
        
        RestRequest req = new RestRequest(); 
    	RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/IDV_CIBILResponse'; 
        req.httpMethod  = 'POST';
        req.params.put('Appid',acc.id);
        req.params.put('TUCall','IDV');
        req.requestBody = Blob.valueOf(Test_IDVResponseTU_HL.testParse());
        //req.requestBody = Blob.valueOf(Test_CibilTUService.methodGetIDVDetails());
        
        RestContext.request = req;
        RestContext.response = res;
        
          IDVResponse reg= new IDVResponse();
        reg.Result = false;
        
        genesis__Application_Parties__c genPar = new genesis__Application_Parties__c();
        genPar.genesis__Party_Account_Name__c = acc.Id;
        genPar.genesis__Application__c = genApp.Id;
        genPar.Active__c = true;
        INSERT genPar;
        
        Test.startTest();
        Ws_IDV_CIBILResponse.IDVCreating();
        Test.stopTest();
    }
    
    @isTest
    public static void method2(){
        Account acc=new Account();
        acc.FirstName = 'TestWs';
        acc.MiddleName = '_IDV_';
        acc.LastName = 'CIBILResponse';
        acc.Gender__pc = 'Male';
        acc.PersonBirthdate = system.today();
        acc.Aadhaar_Number__pc = '123343233444';
        acc.Pan_Number__c = 'EQAD2133RJ';
        acc.Verify_PAN_Number__c = 'Yes';
        acc.Is_PAN_card_available__c = true;
        acc.PersonMailingStreet='test@yahoo.com';
        acc.PersonMailingPostalCode='12345';
        acc.PersonMailingCity='SFO';
        acc.PersonEmail='test@yahoo.com';
        acc.PersonHomePhone='1234567';
        acc.PersonMobilePhone='12345678' ;
        //acc.= genApp.Id;
        insert acc;
        
        RestRequest req1 = new RestRequest(); 
    	RestResponse res1 = new RestResponse();
        
        req1.requestURI = '/services/apexrest/IDV_CIBILResponse'; 
        req1.httpMethod  = 'POST';
        req1.params.put('Appid',acc.id);
        req1.params.put('TUCall','CIBIL');
        req1.requestBody = Blob.valueOf(Test_CibilTUService.methodGetCibilDetails());
        
        RestContext.request = req1;
        RestContext.response = res1;
        
        Test.startTest();
        Ws_IDV_CIBILResponse.IDVCreating();
        Test.stopTest();
    }
}