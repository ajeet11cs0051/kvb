/*
* @name         :   SanctionUnderwriting_Process
* @description  :   This class is used for sanction authority 
                    application review/approval task process and to check logged in 
                    user status for Underwriting dashboard
* @author       :   Amritesh
*/ 
public class SanctionUnderwriting_Process {
    
    public static string APP_CURRENT_SATGE;
    Public static final String COMMITTE_TASK_SUBJECT    = 'Application Approval Pending';
    Public static final String SANC_OFFICER_TASK_SUBJ   = 'Application Review Pending';
    public static final String ADDITIONAL_CHARGE_STAGE  = 'Additional Charge Required';
    Public static final String ADDITIONAL_CHARGE_TO_BE_CREATED      = 'Additional charge to be created';
    Public static final String DEVIATION_TASK_SUBJECT   = 'Application Deviation Approval Pending';
    Public static final String APPILICATION_REVIEW      = 'Application Review';
    Public static final String EME_BM_REVIEW            = 'EME BM Review';
    Public static final String SANCTION_APPROVAL        = 'Sanction Approval';
    Public static final String DEVIATION_APPROVAL       = 'Deviation Approval';
    public static final String REFERRED_BACK_TASK_SUBJ  = 'Referred back by previous Authority';
    public static final String REFERRED_BACK_TASK_TYPE  = 'Referred by previous Authority';
    public static final Set<String> COMMITEE_ROLES      =  new Set<String>{'DOCC','COCC'};
    Public static final String COMMITTEE_REVIEW_MSG     = 'Application sent for committee review';
    public static final Set<String> TASK_STAGES         = new Set<String>{APPILICATION_REVIEW,SANCTION_APPROVAL,DEVIATION_APPROVAL,EME_BM_REVIEW,'Pre-disbursement Approval'};

    public static string createTaskForNextAuthority(string applId, boolean deviatiated){
        
        List<Facility__c> facAuthority  =  getFacilityAuthority(applId);
        boolean isFinalAuthority        = false;
        integer fCount                  = facAuthority.size();
        integer nextAuhtCount           = 0;
        string role                     = '';
        User loggedUser = currentUserInfo();
        for(Facility__c f : facAuthority){
            if(f.role__C    == loggedUser.Designation__c){
                nextAuhtCount = integer.valueof(f.Higher_Authority_Order__c) + 1;
                break;
            }
        }
        System.debug('@@'+nextAuhtCount);
        if(nextAuhtCount <= facAuthority.size()){
            integer index = nextAuhtCount - 1;
            role    = facAuthority[index].Role__c;
            
            if(facAuthority[index].Highest_Authority__c){
                isFinalAuthority    = true;
            }
        }
        System.debug('@@'+nextAuhtCount);
        List<User> authUser  = new List<User>();
        if(role !=null && role !='' && isFinalAuthority && COMMITEE_ROLES.contains(role)){
            getCommitteMemebers(loggedUser.Division, applId, role); 
        }else{
            if(role != null && role != ''){                
                if(role.contains('CO') || role.contains('President'))
                    authUser = getUserRecord(role, null);
                else
                    authUser = getUserRecord(role, loggedUser.Division);
                if(authUser.size() > 0){
                    string taskSubj = '';
                    string taskType = '';
                    if(deviatiated){
                        taskSubj    = DEVIATION_TASK_SUBJECT;
                        taskType    = DEVIATION_APPROVAL;
                    }else{
                        taskSubj    = SANC_OFFICER_TASK_SUBJ;
                        taskType    = APPILICATION_REVIEW;
                    }
                    createTaskforOtherAuthority(getTaskObj(applId,authUser[0].Id,taskSubj,taskType));
                }
            }            
        }
        return role;
    }
    public static string createTaskForPreviousAuthority(string applId){
        try {
            System.debug('applId::'+applId);
            List<Facility__c> facAuthority  =  getFacilityAuthority(applId);
            List<Task> taskList = new List<Task>();
            //boolean isFinalAuthority        = false;
            integer fCount                  = facAuthority.size();
            integer prevAuhtCount           = 0;
            string role                     = '';
            User loggedUser = currentUserInfo();
            User prevAuth = new User();
            
            taskList = [SELECT Id,Subject,Designation__c,OwnerId FROM Task WHERE WhatId =: applId AND Designation__c =: role LIMIT 1];
            for(Facility__c f : facAuthority){
                if(f.role__C    == loggedUser.Designation__c){
                    prevAuhtCount = integer.valueof(f.Higher_Authority_Order__c) - 1;
                    break;
                }
            }
            if(facAuthority.size() > 0){
                integer index = prevAuhtCount - 1;
                role    = facAuthority[index].Role__c;
            }
            System.debug('role::'+role);
            List<User> authUser  = new List<User>();
            if(role != null && role != ''){
                if(!role.contains('BM')){
                    authUser = getUserRecord(role, null);
                    System.debug('authUser::'+authUser);
                    if(authUser.size() > 0){
                        String taskSubj    = REFERRED_BACK_TASK_SUBJ;
                        string taskType    = REFERRED_BACK_TASK_TYPE;

                        //createTaskforOtherAuthority(getTaskObj(applId,authUser[0].Id,taskSubj,taskType));
                        //createTaskforOtherAuthority(getTaskObj(applId,authUser[0].Id,taskSubj,taskType));
                        createTaskforOtherAuthority(getTaskObj(applId,taskList[0].OwnerId,taskSubj,taskType));
                    }
                }
            }
            if(applId != null)closeTaskForLoggedInUser(loggedUser, applId);
            return 'Task has been cretaed for previous authority';
        } catch (Exception e) {
            System.debug('@@@@@'+e.getMessage()+' AT '+e.getLineNumber()+' StackTrace '+ e.getStackTraceString());
            return e.getMessage()+' AT '+e.getLineNumber()+' StackTrace '+ e.getStackTraceString();
        }

        //return role;
    }

    // Get User record by Designation
    public static List<User> getUserRecord(String designation, string division){
        if(division != null)
            return [Select id,Designation__c from user where Designation__c =:designation AND Division =:division  limit 1];
        else
            return [Select id,Designation__c from user where Designation__c =:designation limit 1];        
    }
    
    // Get DOCC/COCC Committe Member details for Sanction Approval Task Creation
    public static void getCommitteMemebers(String division, string applId, string committeFor){        
        List<genesis__Committee_Member__c> committeMembers  = new List<genesis__Committee_Member__c>();
        string query    = 'Select id,genesis__Meeting_Schedule__c,Name,genesis__User__c,genesis__User__r.Name,genesis__Role__c from genesis__Committee_Member__c'+' ';
        query           += 'where genesis__Meeting_Schedule__r.Sanction_Committe_For__c =:committeFor ';
        if(committeFor == 'DOCC' && division != null){
            query   += 'AND genesis__Meeting_Schedule__r.Division__c =:division';
        }
        query   += ' AND genesis__Meeting_Schedule__r.genesis__Status__c =\'New\'';
        system.debug('query::'+query);
        committeMembers = Database.query(query);
        if(committeMembers.size() > 0){
            createTaskforCommitteMember(committeMembers,applId);
            genesis__Applications__c appl   = new genesis__Applications__c();
            appl.Id                         = applId;
            appl.Committe__c                = committeMembers[0].genesis__Meeting_Schedule__c;
            
            ApplicationTriggerHandler.IsFirstRun = false;
            update appl; // update committe info on Application
        }        
    }
    
    // Create Task for Indiviual User
    public static void createTaskforOtherAuthority(Task individualTask){
        System.debug('@@@'+individualTask);
        TaskFlow_Helper.TASK_TRIGGER_RUNNING    = true;        
        if(APP_CURRENT_SATGE != null && APP_CURRENT_SATGE != ''){
            individualTask.Application_Stage__c     = APP_CURRENT_SATGE;
        }
        insert individualTask;
        System.debug('@@@'+individualTask.Id);
    }
    
    // Create Task for each Committe Memeber
    public static void createTaskforCommitteMember(List<genesis__Committee_Member__c> committeMembers,String applnId){
        List<Task> taskToInsert = new List<Task>();
        for(genesis__Committee_Member__c cm : committeMembers){
            Task newTask                    = getTaskObj(applnId, cm.genesis__User__c,COMMITTE_TASK_SUBJECT,SANCTION_APPROVAL);
            newTask.Application_Stage__c    = APP_CURRENT_SATGE;
            taskToInsert.add(newTask);
        }
        TaskFlow_Helper.TASK_TRIGGER_RUNNING    = true;
        insert taskToInsert;
    }
    
    // Return task object
    public static Task getTaskObj(String parentId, string taskOwner, string taskSubject, string taskType){
        return new Task(WhatId=parentId,Subject=taskSubject,OwnerId=taskOwner,NonDeletable__c=true,ActivityDate=System.today()+2,Type=taskType);
    }
    
    public static UserInfoWrapper getCurrentUserStatus(String applnId, genesis__Applications__c appInfo, User loggedInUser ){
        UserInfoWrapper uInfo           = new UserInfoWrapper();
        List<Facility__c> facAuthority  = getFacilityAuthority(applnId);
        List<Task>  mytaskList          = getUserTaskList(applnId,loggedInUser.Id);
        List<Task>  otherUsertasks      = getOtherAuthTaskList(applnId,loggedInUser.Id);
        uInfo.isPreDisbersementEnabled  = isEveryPreDisbersementBMComplied(applnId);
        boolean committeTaskDone        = false;
                System.debug('#####'+mytaskList);
                System.debug('#####'+otherUsertasks);
        if(facAuthority.size() == 0) return uInfo;         
                
        if(appInfo.OwnerId  == loggedInUser.Id){
            uInfo.isOwner = true;
                    System.debug('##### inside if');       
            if(loggedInUser.Designation__c.contains('BR')){
                if(mytaskList.size() > 0 && otherUsertasks.size() == 0){
                    uInfo.isBranchRole = true;
                  uInfo.isEmeBMReviewer = true;
                    System.debug('##### inside if');
                }
            }
            
            uInfo.isReadOnly    = false;
            for(Facility__c f : facAuthority){
                System.debug('##### inside if');
                if(f.role__C    == loggedInUser.Designation__c){
                    System.debug('##### inside if');
                    if((mytaskList.size() > 0 && otherUsertasks.size() == 0) && f.Highest_Authority__c){
                        System.debug('##### inside if');
                        uInfo.isFinalAuhtority  = true; 
                        break;
                    }
                }
            }
        }else{
            boolean otherUser   = false;
            for(Facility__c f : facAuthority){
                System.debug('##### inside if');
                if(f.role__C    == loggedInUser.Designation__c){
                    System.debug('##### inside if');
                    if(f.Is_Referred_Back_Enabled__c){
                        System.debug('##### inside if');
                        uInfo.IsReferredBackEnabled =  true;
                    }
                    otherUser   = true;                    
                    if(f.Highest_Authority__c){
                        System.debug('##### inside if');
                        if(mytaskList.size() > 0){
                            System.debug('##### inside if');
                            uInfo.isFinalAuhtority  = true;
                        }                        
                        break;
                    }else{                        
                        if(f.Read_Only_User__c){
                            if(mytaskList.size() > 0){
                                uInfo.isReadOnly    = true;
                                if(f.Deviation_Approver__c){
                                   uInfo.isDeviationApprover = true;
                                }
                                break;
                            } 
                        }else{
                            uInfo.isPartOfHierarchy = true;
                        }
                    }
                }
            }
            if(!otherUser){                
                if(mytaskList.size()>0){
                    System.debug('#####');
                    uInfo.isSanctionApproval    = true;
                }
            }
        }
                System.debug('#####'+uInfo);
        return uInfo;
    }
    public static Boolean isEveryPreDisbersementBMComplied(String appId){


        List<Audit__c> preDisbursementComments                      = getpreDisbursementCommentsAuthorityComment(appId);
        Boolean isEveryPreDisbersementBMComplied                    = true;
        if(!preDisbursementComments.isEmpty()){

            for(Audit__c auditRec:preDisbursementComments){
                if(auditRec.Complied__c != null){
                    isEveryPreDisbersementBMComplied                =  auditRec.Complied__c == 'Yes' ? true : false;
                }else{
                    isEveryPreDisbersementBMComplied                =   false;
                }
            }
        }else{
            // what if there is no pre-disbersement comments
            // Still needs to be improvised
            isEveryPreDisbersementBMComplied                        = false;
        }
        return isEveryPreDisbersementBMComplied ;

    }
    public static List<Audit__c> getpreDisbursementCommentsAuthorityComment(string appId){
        return [select User__c,User__r.Name,Complied__c,Sanction_Authority_Comment__c,Application__c,Terms_and_Conditions__c, 
                RecordType.DeveloperName from Audit__c where (RecordType.DeveloperName =:Constants.PREDISBURSEMENT_T_C_RTYPE ) AND Application__c =:appId];
    }
    public static List<Task> getUserTaskList(string applnId,string taskOwnerId){
        return [Select id,OwnerId,Status,Application_Stage__c from Task 
                where WhatId =:applnId AND nondeletable__c =true 
                AND (OwnerID =:taskOwnerId AND Type IN:TASK_STAGES) AND Status != 'Completed'];
    }
    
    public static List<Task> getOtherAuthTaskList(string applnId,string taskOwnerId){
        return [Select id,OwnerId,Status,Application_Stage__c from Task 
                where WhatId =:applnId AND nondeletable__c =true 
                AND (OwnerID !=:taskOwnerId AND Type IN:TASK_STAGES) AND Status != 'Completed'];
    }
    
    public static List<Facility__c> getFacilityAuthority(string appId){
        List<Facility__c> facilityList          = new List<Facility__c>();
        List<Facility__c> authorityFacilities   = new List<Facility__c>();
        
        authorityFacilities                     = [Select id,(Select id,Facility__c,Facility__r.Application__c,Role__c,Deviation_Approver__c,
                                                    Highest_Authority__c,Is_Referred_Back_Enabled__c,Read_Only_User__c,Higher_Authority_Order__c from Facilities__r
                                                    where Facility__r.Application__c =:appId AND RecordType.DeveloperName ='Child' order by Higher_Authority_Order__c asc)
                                                    from Facility__c where Active__c = true AND Application__c =:appId AND RecordType.DeveloperName ='Parent' limit 1];
        
        if(authorityFacilities.size() > 0) facilityList = authorityFacilities[0].Facilities__r;
        return facilityList;
    }
    public static List<Facility__c> getFacilityAuthorityForAllfacility(string appId){

        List<Facility__c> authorityFacilities   = new List<Facility__c>();

        authorityFacilities                     = [Select id,(Select id,Facility__c,Is_Referred_Back_Enabled__c,Facility__r.Application__c,Role__c,Deviation_Approver__c,
                Highest_Authority__c,Read_Only_User__c,Higher_Authority_Order__c from Facilities__r
        where Facility__r.Application__c =:appId AND RecordType.DeveloperName ='Child' order by Higher_Authority_Order__c asc)
        from Facility__c where Active__c = true AND Application__c =:appId AND RecordType.DeveloperName ='Parent' limit 1];

        if(authorityFacilities.size() > 0)
        return authorityFacilities;
        else
        return  null;
    }

    public static Map<Id,Facility__c> getExistingAuthorityLevel(String appId){

        try {


            Map<Id, Facility__c> parentVsChildFacility = new Map<Id, Facility__c>();

            List<Facility__c> authorityFacilities = new List<Facility__c>();
            authorityFacilities = getFacilityAuthorityForAllfacility(appId);

            if (authorityFacilities.size() > 0) {

                for (Facility__c facilityRec : authorityFacilities) {
                    /*if(facilityRec.Highest_Authority__c)
                    idVsFacility.put(Integer.valueOf(facilityRec.Higher_Authority_Order__c), facilityRec);*/
                    for(Facility__c faclityChild:facilityRec.Facilities__r){

                        if(faclityChild.Highest_Authority__c)
                        parentVsChildFacility.put(facilityRec.Id, faclityChild);
                    }
                }
            }
            if (!parentVsChildFacility.isEmpty()) {
                return parentVsChildFacility;
            } else {
                return null;
            }
        }catch(Exception e){
            System.debug('Error: ' +e.getMessage() +' AT '+e.getLineNumber());
            return null;
        }

    }
    
     /**
   * Makes Task Status as completed
   * @param loggedInUser :Information of the logged in user 
   * @param applId       :Application to which the task has been assigned
   *                
   */
    public static void closeTaskForLoggedInUser(User loggedInUser, String applId){
        System.debug('@@aaaaa');
        System.debug('@@aaaaa'+loggedInUser.Id);
        System.debug('@@aaaaa'+applId);
        List<Task> taskList =  [SELECT Id,WhatId,WhoId,Status 
                                FROM Task 
                                WHERE OwnerId =:loggedInUser.Id
                                AND whatId =:applId 
                                AND Status != 'Completed' ];
        System.debug('@@'+taskList);
        if(!taskList.isEmpty()){
            System.debug('@@'+taskList);
            for(Task taskRecord : taskList){
                System.debug('@@'+taskRecord);
                taskRecord.Status = 'Completed';
            }
            UPDATE taskList;
        }
    }

    
    public static User currentUserInfo(){
        return [Select id,Designation__c,Division,Name from User where Id=:userInfo.getUserId()];
    }
    
    public static boolean checkMemberApproved(string appId, String committeId){

        boolean status          = false;
        List<String>  userIds   = getCommitteMemberIds(committeId);
        if(userIds.size() > 0){
            List<Task> memberTasks  = new List<Task>([Select id,Status,Approval_Status__c,NonDeletable__c
                                                      from Task 
                                                      where OwnerId IN :userIds AND WhatID =:appId 
                                                      AND NonDeletable__c =true AND Type =:SANCTION_APPROVAL]);
            if(memberTasks.size() > 0){
                integer count = 0;
                for(Task t : memberTasks){
                    if(t.Status == 'Completed' && t.Approval_Status__c == 'Approved'){
                        count++;
                    }
                }
                if(memberTasks.size() == count){
                    status  = true;
                }
            }
        }
        return status;
    }
    
    public static List<String> getCommitteMemberIds(String committeId){
        List<String> userIds                                = new List<String>();
        List<genesis__Committee_Member__c> committeMembers  = new List<genesis__Committee_Member__c>();
        committeMembers     = [Select id,genesis__Meeting_Schedule__c,Name,genesis__User__c,genesis__User__r.Name,genesis__Role__c 
                               from genesis__Committee_Member__c 
                               where genesis__Meeting_Schedule__c =:committeId];
        for(genesis__Committee_Member__c c : committeMembers){
            if(c.genesis__User__c != null) userIds.add(c.genesis__User__c);
        }
        return userIds;
    }
    
    public static String gethighestRole(Set<String> rolelist, List<Authority_Hierarchy_Level__mdt> authLevelMap){
        //List<Authority_Hierarchy_Level__mdt> authLevelMap = new List<Authority_Hierarchy_Level__mdt>();
        //authLevelMap                                      = SanctionMatrix_Helper.getAuthorityLevel();
        integer roleIndex                                   = 0;
        string highestRole                                  = '';
        for(string role : rolelist){
            for(Authority_Hierarchy_Level__mdt mdtRole : authLevelMap){
                if(mdtRole.Authority_Level__c != null && role == mdtRole.DeveloperName && mdtRole.Authority_Level__c > roleIndex){
                    roleIndex   = Integer.ValueOf(mdtRole.Authority_Level__c);
                    highestRole = role;
                }
            }
        }
        if(highestRole == Constants.PRESIDENT_COO_API)highestRole = Constants.PRESIDENT_COO_NAME;
        return highestRole;
    }
    
    public static List<String> getCheckListPendingReason(List<Renewal_Checklists__c> checkList){
        List<String> reasonList = new List<String>();
        for(Renewal_Checklists__c c : checkList){
            if(c.Checklist_Name__c == 'Have we collected the FEC document (s)?')
                reasonList.add('FEC not obtained');
            if(c.Checklist_Name__c == 'Is CERSAI registration details available?')
                reasonList.add('CERSAI not available');
            if(c.Checklist_Name__c == 'Form 8 Available?')
                reasonList.add('Form 8 not available');
            if(c.Checklist_Name__c == 'Are all Audit Remarks are Closed?')
                reasonList.add('Audit Remarks are open.');
            if(c.Checklist_Name__c == 'If up to date stock statement received?')
                reasonList.add('Stock statement not received.');
            if(c.Checklist_Name__c == 'If special sanctioning terms and conditions are complied by the borrower ?')
                reasonList.add('T&C not complied.');
            if(c.Checklist_Name__c == 'Is the latest unit visit report available?')
                reasonList.add('Visit report not available.');
            if(c.Checklist_Name__c == 'Have we obtained the latest EC?')
                reasonList.add('EC not obtained.');
            if(c.Checklist_Name__c == 'Do we have full insurance coverage?')
                reasonList.add('No full coverage insurance.');
            if(c.Checklist_Name__c == 'Is the last collateral valuation date within 3 years from the renewal date?')
                reasonList.add('Collateral valuation date not valid');
            if(c.Checklist_Name__c == 'Do we have last year audited financials statements?')
                reasonList.add('Last FY statements not available.');
        }
        return reasonList;
    }
    
    // Wrapper class
    public class UserInfoWrapper{
        public boolean isReadOnly                       {get;set;}
        public boolean isFinalAuhtority                 {get;set;}
        public boolean isSanctionApproval               {get;set;}
        public boolean isPartOfHierarchy                {get;set;}
        public boolean isBranchRole                     {get;set;}
        public boolean isApplnReadOnly                  {get;set;}
        public boolean isDeviationApprover              {get;set;}
        public boolean isEmeBMReviewer                  {get;set;}
        public boolean isPreDisbersementEnabled         {get;set;}
        public boolean isOwner                          {get;set;}
        public boolean IsReferredBackEnabled            {get;set;}


        
        public UserInfoWrapper(){
            this.isReadOnly                             = false;
            this.isFinalAuhtority                       = false;
            this.isSanctionApproval                     = false;
            this.isPartOfHierarchy                      = false;
            this.isBranchRole                           = false;
            this.isApplnReadOnly                        = false;
            this.isDeviationApprover                    = false;
            this.isEmeBMReviewer                        = false;
            this.isPreDisbersementEnabled               = false;
            this.isOwner                                = false;
            this.IsReferredBackEnabled                  = false;

        }
    }    
}