@IsTest
private class Test_Underwriting_CTRL{

    static testmethod void testUnderwriting(){
        
        Underwriting_CTRL ctrl    = new Underwriting_CTRL();
        Account testAcc    = new Account(Name='test account');
        insert testAcc;
        genesis__Applications__c app   = new genesis__Applications__c();
        app.genesis__Account__c        = testAcc.id;
        app.RecordTypeId                = Schema.SObjectType.genesis__Applications__c.getRecordTypeInfosByName().get('SME Renewal').getRecordTypeId();
        insert app;
        
        Underwriting_CTRL.saveWrapper  wrp    = new Underwriting_CTRL.saveWrapper();
        SanctionUnderwriting_Process.userInfoWrapper aWrp    = new SanctionUnderwriting_Process.userInfoWrapper();
        Underwriting_CTRL.saveUnderWrite(wrp);
        Underwriting_CTRL.otherAuthoritySubmit(app.Id,'');
        Underwriting_CTRL.sanctionApprovalSubmit(app.Id,'Approved',aWrp,null);
        Underwriting_CTRL.sanctionApprovalSubmit(app.Id,'Mark as Exit',aWrp,null);
    }
}