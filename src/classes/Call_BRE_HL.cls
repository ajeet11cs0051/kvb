/*
* Name    : Call_BRE_HL
* Company : ET Marlabs
* Purpose : This class is used to call BRE
* Author  : Subas
*/
public class Call_BRE_HL {
    public static void call_BRE(List<genesis__Applications__c> appList, Map<Id,genesis__Applications__c> oldList){
        Decimal calVal = 0.00;
        for(genesis__Applications__c app : appList){
            if(oldList.get(app.Id).sub_stage__c <> app.Sub_Stage__c && app.Sub_Stage__c =='Loan Requirement Captured' && (app.Record_Type_Name__c =='Home Loan' || app.Record_Type_Name__c =='LAP')){ 
                String s = genesis.ScorecardAPI.generateScorecard(app.Id);
                system.debug('*******'+s);                
            }else if(oldList.get(app.Id).sub_stage__c <> app.Sub_Stage__c && app.Sub_Stage__c =='Terms and Conditions Accepted' && app.Record_Type_Name__c =='Home Loan'){
                String s = genesis.ScorecardAPI.generateScorecard(app.Id);
                system.debug('***Re_run_BRE****'+s);             	   
            }
        }
    }
}