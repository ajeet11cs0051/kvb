public class Estamp_HelperStructure {
    
    public class Estamp {
        public String ReferenceID;
        public String TemplateID;
        public String ApplicationID;
        public StampPaperData StampPaperData;
        public FormData FormData;
    }
    
    public class FormData {
        Public String Disb_Ref_Day;
        Public String Disb_Ref_Month;
        Public String Disb_Ref_Year;
        Public String Loc_Ref_Branch;
        Public String Borrower_Details;
        Public String Loan_Product;
        Public String Seller_Name;
        Public String Advance_Amount;
        Public String Sale_Agreem_Date;
        Public String Nature_Property;
        Public String Survey_No;
        Public String Plot_Door_Bungalow_No;
        Public String Built_up_area;
        Public String Floor_No;
        Public String Ward_No;
        Public String Building_Society_Name_No;
        Public String Street_Name;
        Public String Village_Town;
        Public String Taluka_Tehsil;
        Public String District_Name;
        Public String State_Name;
        Public String PIN_CODE;
        Public String Doc_Gen_date;
        Public String Title_Holder1_Name1;
        Public String Title_Holder1_Add_line1;
        Public String Title_Holder1_Add_line2;
        Public String Title_Holder1_Add_line3;
        Public String Title_Holder2_Name1;
        Public String Title_Holder2_Add_line1;
        Public String Title_Holder2_Add_line2;
        Public String Title_Holder2_Add_line3;
        Public String Title_Holder3_Name1;
        Public String Title_Holder3_Add_line1;
        Public String Title_Holder3_Add_line2;
        Public String Title_Holder3_Add_line3;
        Public String Title_Holder4_Name1;
        Public String Title_Holder4_Add_line1;
        Public String Title_Holder4_Add_line2;
        Public String Title_Holder4_Add_line3;
        Public String Title_Holder5_Name1;
        Public String Title_Holder5_Add_line1;
        Public String Title_Holder5_Add_line2;
        Public String Title_Holder5_Add_line3;
		public String Title_Holder6_Name1;	//name6
		public String Title_Holder6_Add_line1;	//L1
		public String Title_Holder6_Add_line2;	//L2
		public String Title_Holder6_Add_line3;	//L3
		public String Title_Holder7_Name1;	//name7
		public String Title_Holder7_Add_line1;	//L1
		public String Title_Holder7_Add_line2;	//L2
		public String Title_Holder7_Add_line3;	//L3
		public String Title_Holder8_Name1;	//name8
		public String Title_Holder8_Add_line1;	//L1
		public String Title_Holder8_Add_line2;	//L2
		public String Title_Holder8_Add_line3;	//L3
		public String Title_Holder9_Name1;	//name9
		public String Title_Holder9_Add_line1;	//L1
		public String Title_Holder9_Add_line2;	//L2
		public String Title_Holder9_Add_line3;	//L3
		public String Title_Holder10_Name1;	//name10
		public String Title_Holder10_Add_line1;	//L1
		public String Title_Holder10_Add_line2;	//L2
		public String Title_Holder10_Add_line3;	//L3
		public String Title_Holder11_Name1;	//name11
		public String Title_Holder11_Add_line1;	//L1
		public String Title_Holder11_Add_line2;	//L2
		public String Title_Holder11_Add_line3;	//L3
		public String Title_Holder12_Name1;	//name12
		public String Title_Holder12_Add_line1;	//L1
		public String Title_Holder12_Add_line2;	//L2
		public String Title_Holder12_Add_line3;	//L3
		public String Title_Holder13_Name1;	//name13
		public String Title_Holder13_Add_line1;	//L1
		public String Title_Holder13_Add_line2;	//L2
		public String Title_Holder13_Add_line3;	//L3
		public String Title_Holder14_Name1;	//name14
		public String Title_Holder14_Add_line1;	//L1
		public String Title_Holder14_Add_line2;	//L2
		public String Title_Holder14_Add_line3;	//L3        
        Public String MOD_Branch;
        Public String Borrower_Names;
        Public String Title_Holder_details;
        Public String Mod_date;
        Public String Xerox_Doc_table;
        Public String Xerox_Reason1;
        Public String Xerox_Reason2;
        Public String Sanction_Ref_Number;
        Public String Sanction_date;
        Public String Ap_LT;
        Public String Guarantor_Details;
        Public String Int_ROI;
        Public String Loan_tenor;
        Public String Holiday_period;
        Public String Guarantor1_Names;
        Public String Guarantor2_Names;
        Public String Guarantor3_Names;
        Public String Guarantor4_Names;
        Public String Guarantor5_Names;
        Public String Guarantor6_Names;
        Public String Guarantor7_Names;
        Public String Guarantor8_Names;
        Public String Guarantor9_Names;
        Public String Guarantor10_Names;        
        Public String Auth_Sign_Bnk;
        Public String Ap_LT_inwords;
        Public String Loan_Purpose;
        Public String Int_Type;
        Public String ROI_MCLR;
        Public String MCLR_Rate;
        Public String Rating_Freq;
        Public String PreClosure_Charges;
        Public String DD_FI;
        Public String DD_LI;
        Public String Borrower1_Names;
        Public String Borrower2_Names;
        Public String Borrower3_Names;
        Public String Borrower4_Names;
        Public String Borrower5_Names;
        Public String Extent_Site_Uds; //new
        public cls_TableData[] TableData;
    }
    
    public class FirstPartyAddress {
        public String StreetAddress;
        public String Locality;
        public String City;
        public String State;
        public String Pincode;
        public String Country;
    }
    
    public class StampPaperData {
        public String UID;   //kBtb5aQ48k4x5Vlkp8dj2eOH1bql/zEjyzgyHFGCUGw=
        public String FirstParty;
        public FirstPartyAddress FirstPartyAddress;
        public String SecondParty;
        public FirstPartyAddress SecondPartyAddress;
        public String StampAmount;
        public String StampDutyPaidBy;
        public String DocumentCategory;
        public String StampState;
    }
    public class cls_TableData {
        public String sr_no;	//1
        public String Doc_no;	//doc_no
        public String Doc_dt;	//doc_dt
        public String doc_nature;	//doc_nature
        public String transferor;	//transferor
        public String transferee;	//transferee
    }    
    
    
    public static Estamp_HelperStructure parse(String json) {
        return (Estamp_HelperStructure) System.JSON.deserialize(json, Estamp_HelperStructure.class);
    }
}