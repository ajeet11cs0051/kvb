/*
* Description : Helper class of WS_FetchSMEInfo
* Author       : Raushan
*/ 
public class WS_FetchSMEInfo_Helper {
	
	public CustomerDetail CustomerDetail;
	
	public class CustomerDetail{
		public String Applicant_name;
		public String Constitution;
		public String Customer_Id;
		public String Line_of_activity;
		public String Industry_type;
		public String PAN;
		public String GST;
		public String MSME;
		public String Import_Export_Code;
		public Registered_Address Registered_Address;
		public Registered_Address Communication_Address;
		public List<cls_Key_Person_Details> Key_Person_Details;
		public String Contact_Name;
		public String Contact_Number;
		public String Group_Name;
		public String Name;
		public String CIN;
		public String Reasons_for_exemption_from_GST;
		public String Turnover;
		public String Principal_nature_of_business; 
	}
	public class cls_Key_Person_Details {
		public String Name;
		public String Party_Type;
		public String Customer_ID;
		public String Passport_No;
		public String Nationality;
		public String PAN;
		public String Aadhaar_Number;
		public String Date_Of_Birth;
		public String Phone;
		public String Email;
		public Registered_Address Registered_Address;
	}
	public class Registered_Address {
	    public String Address;
	    public String City;
	    public String State;
	    public String Country;
	    public String PostalCode;
	}

	public CustomerDetail parse(String json) {
	    return (CustomerDetail) System.JSON.deserialize(json, CustomerDetail.class);
	}
public static String updateCustomerDetails(CustomerDetail cusObj,String Type){

	If(cusObj !=null){
	    
	    Account accObject  = new Account();
	    String appCloneId	= '';
	    Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
	    If(!Utility.ISStringBlankorNull(cusObj.Applicant_name))
	        accObject.Name	= cusObj.Applicant_name;
	    If(!Utility.ISStringBlankorNull(cusObj.Constitution))
	        accObject.Constitution__c	= cusObj.Constitution;
	    If(!Utility.ISStringBlankorNull(cusObj.Customer_Id))
	        accObject.CBS_Customer_ID__c	= cusObj.Customer_Id;    
	    If(!Utility.ISStringBlankorNull(cusObj.Line_of_activity)){
	        accObject.Line_of_Activity__c	= cusObj.Line_of_activity;
	        If(cusObj.Line_of_activity == 'Service Provision'){
	            accObject.Cash_Budget__c = true;
	        }
	    }
	    accObject.RecordTypeId				=	devRecordTypeId;
	    If(!Utility.ISStringBlankorNull(cusObj.Industry_type))
	        accObject.Industry_Type__c	= cusObj.Industry_type;
	    If(!Utility.ISStringBlankorNull(cusObj.PAN))
	        accObject.Pan_Number__c	= cusObj.PAN;
	    If(!Utility.ISStringBlankorNull(cusObj.GST))
	        accObject.GST_Number__c	= cusObj.GST;
	    If(!Utility.ISStringBlankorNull(cusObj.Contact_Name))
	        accObject.Contact_Person_Name__c	= cusObj.Contact_Name;
	    If(!Utility.ISStringBlankorNull(cusObj.Contact_Number))
	        accObject.Cust_mobile_phone__c	= cusObj.Contact_Number;
	    If(!Utility.ISStringBlankorNull(cusObj.Group_Name))
	        accObject.Group_Name__c	= cusObj.Group_Name;
	    If(!Utility.ISStringBlankorNull(cusObj.CIN))
	        accObject.CIN_Number__c	= cusObj.CIN;
	    If(!Utility.ISStringBlankorNull(cusObj.Reasons_for_exemption_from_GST))
	        accObject.Reasons_for_exemption_from_GST__c	= cusObj.Reasons_for_exemption_from_GST;
	    If(!Utility.ISStringBlankorNull(cusObj.Turnover))
	        accObject.Annual_TurnoverIncome__c	= Decimal.valueOf(cusObj.Turnover);
	    If(!Utility.ISStringBlankorNull(cusObj.Principal_nature_of_business)){
	        accObject.Principal_nature_of_business__c	= cusObj.Principal_nature_of_business;	
	    }
	    If(cusObj.Registered_Address !=null){
	        WS_FetchSMEInfo_Helper.Registered_Address	registeredObj	=	cusObj.Registered_Address;
	        If(registeredObj !=null){
	            If(!Utility.ISStringBlankorNull(registeredObj.Address))
	                accObject.BillingStreet	= registeredObj.Address;
	            If(!Utility.ISStringBlankorNull(registeredObj.City))
	                accObject.BillingCity	= registeredObj.City;
	            If(!Utility.ISStringBlankorNull(registeredObj.State))
	                accObject.BillingState	= registeredObj.State;
	            If(!Utility.ISStringBlankorNull(registeredObj.Country))
	                accObject.BillingCountry	= registeredObj.Country;
	            If(!Utility.ISStringBlankorNull(registeredObj.PostalCode))
	                accObject.BillingPostalCode	= registeredObj.PostalCode;				
	        }	
	    }
	    If(cusObj.Communication_Address !=null){
	        WS_FetchSMEInfo_Helper.Registered_Address	communicationObj	=	cusObj.Communication_Address;
	        If(communicationObj !=null){
	            If(!Utility.ISStringBlankorNull(communicationObj.Address))
	                accObject.ShippingStreet	= communicationObj.Address;
	            If(!Utility.ISStringBlankorNull(communicationObj.City))
	                accObject.ShippingCity	= communicationObj.City;
	            If(!Utility.ISStringBlankorNull(communicationObj.State))
	                accObject.ShippingState	= communicationObj.State;
	            If(!Utility.ISStringBlankorNull(communicationObj.Country))
	                accObject.ShippingCountry	= communicationObj.Country;
	            If(!Utility.ISStringBlankorNull(communicationObj.PostalCode))
	                accObject.ShippingPostalCode	= communicationObj.PostalCode;				
	        }	
	    }
        If(accObject !=null){
            System.debug('accObject'+accObject);
            AccountTriggerHandler.isAccountTrigger = false;
            insert accObject;
            If(Type=='NTB'){
            	accObject.CBS_Customer_ID__c = accObject.id;
            	update accObject;
            }
        }
        If(accObject.CBS_Customer_ID__c !=null && accObject.CBS_Customer_ID__c !=''){
        	system.debug('accObject.CBS_Customer_ID__c'+accObject.CBS_Customer_ID__c);
        	return accObject.CBS_Customer_ID__c;
        }

	}
	return null;
 }	

 public static String createApplication(String Customer_ID,String Type){
	
	If(Customer_ID !=null && Customer_ID !=''){
        Id devRecordTypeId = Schema.SObjectType.genesis__Applications__c.getRecordTypeInfosByName().get(Constants.SME_NEW_LOAN_RECORD_TYPE_LABEL).getRecordTypeId();
        genesis__Applications__c newApplicationObj	=	new genesis__Applications__c();
        newApplicationObj.genesis__Account__r		=	new Account(CBS_Customer_ID__c=Customer_ID);
        System.debug('Customer_ID'+Customer_ID);
        newApplicationObj.RecordTypeId				=	devRecordTypeId;
        newApplicationObj.Type__c					=	Type;
        newApplicationObj.Active__c					=	true;
        newApplicationObj.Application_Stage__c 		= 	'New loans - Application created';
                
            If(newApplicationObj !=null){
            	System.debug('newApplicationObj'+newApplicationObj);
                insert newApplicationObj;
            	
            	return newApplicationObj.id;  
            }

    }
  return null;  
}
public static void createParties(String Customer_ID,String appId,List<cls_Key_Person_Details> listKeyPersonWrp){
	List<Account> listKeyPerson		=	new List<Account>();
	List<genesis__Application_Parties__c> listOfParties	=	new List<genesis__Application_Parties__c>();
	Map<String,String> mapOfString	=	new Map<String,String>();
	If(Customer_ID !=null && Customer_ID !='' && appId !=null && appId !=''){
		//PERSON_ACCOUNT
		Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
		If(listKeyPersonWrp !=null && ! listKeyPersonWrp.isEmpty()){
			for(cls_Key_Person_Details	Kpd  : 	listKeyPersonWrp){
				
				Account keyPersonObj	=	new Account();
				
				If(Kpd !=null){
					If(!Utility.ISStringBlankorNull(Kpd.Name))
	        			keyPersonObj.LastName	= Kpd.Name;
	        		If(!Utility.ISStringBlankorNull(Kpd.Customer_ID))
	        			keyPersonObj.CBS_Customer_ID__c	= Kpd.Customer_ID;
	        		If(!Utility.ISStringBlankorNull(Kpd.Passport_No))
	        			keyPersonObj.Passport_Number__pc		= Kpd.Passport_No;
	        		If(!Utility.ISStringBlankorNull(Kpd.Nationality))
	        			keyPersonObj.Nationality__pc	= Kpd.Nationality;
	        		If(!Utility.ISStringBlankorNull(Kpd.PAN))
	        			keyPersonObj.Pan_Number__c	= Kpd.PAN;
	        		If(!Utility.ISStringBlankorNull(Kpd.Aadhaar_Number)){

	        			keyPersonObj.Aadhaar_Number__pc	= Kpd.Aadhaar_Number;
	        			If(!Utility.ISStringBlankorNull(Kpd.Party_Type))
	        				mapOfString.put(Kpd.Aadhaar_Number,Kpd.Party_Type);
	        		}
	        		If(!Utility.ISStringBlankorNull(Kpd.Date_Of_Birth))
	        			keyPersonObj.PersonBirthdate	= Date.parse(Kpd.Date_Of_Birth);
	        		If(!Utility.ISStringBlankorNull(Kpd.Phone))
	        			keyPersonObj.PersonMobilePhone	= Kpd.Phone;
	        		If(!Utility.ISStringBlankorNull(Kpd.Email))
	        			keyPersonObj.PersonEmail		= Kpd.Email;
	        		If(Kpd.Registered_Address !=null){
	        		WS_FetchSMEInfo_Helper.Registered_Address	registeredObj	=	Kpd.Registered_Address;
	        		If(registeredObj !=null){
	            		If(!Utility.ISStringBlankorNull(registeredObj.Address))
	                		keyPersonObj.PersonMailingStreet	= registeredObj.Address;
	            		If(!Utility.ISStringBlankorNull(registeredObj.City))
	                		keyPersonObj.PersonMailingCity	= registeredObj.City;
	            		If(!Utility.ISStringBlankorNull(registeredObj.State))
	                		keyPersonObj.PersonMailingState	= registeredObj.State;
	            		If(!Utility.ISStringBlankorNull(registeredObj.Country))
	                		keyPersonObj.PersonMailingCountry	= registeredObj.Country;
	            		If(!Utility.ISStringBlankorNull(registeredObj.PostalCode))
	               			 keyPersonObj.PersonMailingPostalCode	= registeredObj.PostalCode;				
	        			}

	    			}
	    			keyPersonObj.RecordTypeId	=	devRecordTypeId;	
	    			listKeyPerson.add(keyPersonObj);
	        											
				}
			}

		}
		If(listKeyPerson.size() > 0){
			AccountTriggerHandler.isAccountTrigger = false;
			insert listKeyPerson;
		}
		If(listKeyPerson.size() > 0){
			for(Account acc  : listKeyPerson){
				genesis__Application_Parties__c	 newPartiesObj	=	new genesis__Application_Parties__c();
				newPartiesObj.genesis__Application__c	= 	(Id)appId;
	        	newPartiesObj.Key_Contact__r = new Account(CBS_Customer_ID__c=Customer_ID);
	        	newPartiesObj.genesis__Party_Account_Name__c = acc.id;
	        	newPartiesObj.Active__c	= true;
	        	newPartiesObj.Signatories__c = true;
	        	If(!Utility.ISStringBlankorNull(mapOfString.get(acc.Aadhaar_Number__pc)))
	        		newPartiesObj.genesis__Party_Type__c	= mapOfString.get(acc.Aadhaar_Number__pc);
	        	
	        	listOfParties.add(newPartiesObj);	
			}
		}
		If(listOfParties.size() > 0){
			insert listOfParties;
		}
	}

}

}