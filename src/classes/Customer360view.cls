/*
* Purpose   : WS Resposne Wrapper for Customer360 view
* Author    : Amritesh
*/ 
public class Customer360view{

    public String LOS_APPLICATION_NAME;
    public String LOS_BASE_DOMAIN_URL;
    public String LOS_APPLICATION_ID;
    public String LOS_CUSTOMER_SF_ID;
    public string LOS_CUSTOMER_ID;
    public string CUSTOMER_ID;
    public String REQUEST_DATE_TIME;    
    public String APPLICATION_UI_STAGE; 
    public String APPLICATION_STAGE;
    public String APP_SUB_STAGE;
    public String DIGIO_DOC_ID;
    public string APP_TYPE;
    public String APPLICATION_TO_CLOSE;
    public String PARENT_APPLICATION;
    public cls_COMPANY_DETAILS COMPANY_DETAILS;
    public cls_KEY_PERSON_DETAILS[] KEY_PERSON_DETAILS;
    public cls_SANCTION_SECTION SANCTION_SECTION;
    public cls_FINANCIAL_SECTION[] FINANCIAL_SECTION;
    public cls_LOANDETAIL loanInfo;
    public DocumentFetch.DocumentCategory ORDERS_ON_HAND;
    public List<DocumentFetch.DocumentCategory> MASTER_DOC_CAT_LIST;
    public DocumentFetch.DocumentCategory ENHANCEMENT_DOCUMENTS;
    public DocumentFetch.DocCatRequest DOCUMENT_CATEGORY_LIST;
    public List<cls_newFinacialDocuments> financial_documents;
    public cls_SUMMARY SUMMARY_VIEW;
    public cls_FINANCIAL_STATEMENT FINANCIALS;
    public CustEnhancementDetails CUST_ENHANCEMENT;
    // Added from sme_CustomerfetchWrapper
    public cls_NET_WORTH_STATEMENT NET_WORTH_STATEMENT;
    public List<cls_TEMPLATES> DOWNLOADABLE_TEMPLATES;
    public List<cls_MISSING_FIN> MISSING_FINACIAL_YEARS;
    public cls_APP_REQUEST NEW_REQUEST;
    public cls_DEBTORS_CREDITORS_INFO DEBTORS_CREDITORS_DETAILS;
    public List<cls_LIMIT_ASSESMENT> LIMIT_ASSESMENT;
    public cls_QUERY_FOLLOWUP CUSTOMER_QUERY;
    public string REJECT_REASON;
    public List<cls_Sanction_Document> SANCTION_DOCUMENT_LIST;
    //public DocumentFetch.FinanceSection FINANCIAL_SECTION; soql
    public class cls_COMPANY_DETAILS {
        public cls_COMPANY_PRIMARY_DETAILS COMPANY_PRIMARY_DETAILS;
        public cls_COMPANY_COMMUNICATION_DETAILS COMPANY_COMMUNICATION_DETAILS;
        public cls_COLLATERAL_DETAILS COLLATERAL_DETAILS;
        public cls_GROUP_CONCERN_DETAILS[] GROUP_CONCERN_DETAILS;
        public String CUSTOMER_ID;  
        public String LOAN_ACCOUNT_NO;  
        public String BORROWER_NAME;    
        public String CONTACT_PERSON_DESIGNATION;   
        public String LINE_OF_ACTIVITY; 
        public String CIN_NUMBER;   
        public String PAN_NUMBER;   
        public String IE_CODE_NUMBER;   
        public String RECONSTITUTED;    
        public String RELATED;
        public String REASONS_FOR_GST_NOT_APPLICABLE;
        public String TURNOVER;
        public String PRINCIPAL_NATURE_OF_BUSINESS;
        //public String NAME_OF_THE_FACILITY;   
        public String LOAN_AMOUNT;
        /*public String APPROVED_LOAN_AMOUNT;*/
        public String BALANCE_OUTSTANDING;  
        public String OVER_DUE; 
        public String OVERALL_EXPOSURE; 
        public cls_DOCUMENT_SECTION[] DOCUMENT_SECTION;
        public cls_CURRENT_LOAN_APPLICATION CURRENT_LOAN_APPLICATION;
        public string OPENING_BALANCE;
        // Different attributes from Sme_CustomerFetch
        public String CASH_FLOW_METHOD;   
        public String INDUSTRY_TYPE;
        public string CUSTOMER_NAME;
        public String GST_NUMBER;


    }

    public class cls_Signatory{

        public string LOS_Party_Id;
        public string LOS_Digio_Status;
        public string LOS_Digio_Status_Updation_Id;
    }

    public class cls_Sanction_Document{

        public string Document_Name;
        public List<cls_Signatory> Signers;
    }

    public class cls_newFinacialDocuments{
        public List<DocumentFetch.DocumentCategory> Category_list;
        //public DocumentFetch.DocCatRequest DOCUMENT_CATEGORY_LIST;
        public string year;
    }
    public class cls_QUERY_FOLLOWUP{
        public DocumentFetch.DocumentCategory QUERY_FOLLOW_CATEGOREY;
        public List<cls_CUSTOMER_QUERY> ALL_QUERIES;
    }
    public class cls_CUSTOMER_QUERY{
        public string LOS_ID;
        public string Question;
        public string Answer;
        public string Status;
    }
    
    public class cls_COMPANY_PRIMARY_DETAILS {
        public String NAME_OF_THE_GROUP;    
        public String NAME; 
        public String INDUSTRY_TYPE;    
        public cls_CONSTITUTION_DETAILS CONSTITUTION_DETAILS;   
        public String GST_NUMBER;   
    }
    
    public class cls_COMPANY_COMMUNICATION_DETAILS {
        public cls_ADDRESS REGISTERED_OFFICE_ADDRESS;
        public cls_ADDRESS CORPORATE_OFFICE_ADDRESS;
        public String CONTACT_PERSON_NAME;
        public String CONTACT_NUMBER;
    }
    public class cls_CONSTITUTION_DETAILS {
        public String CONSTITUTION;
        public String RECONSTITUTION_DATE;
        /*Sme_customer Fetch wrapper does'nt have following attributes*/
        public cls_FINANCIAL_SECTION[] CONSTITUTION_EXISTING_DOC;   
        public DocumentFetch.DocCatRequest CONSTITUTION_DOCUMENT_CATEGORY;
    }
    public class cls_COLLATERAL_DETAILS {
        public cls_PRIMARY_SECURITIES[] PRIMARY_SECURITIES;
        public cls_COLLATERAL_SECURITIES[] COLLATERAL_SECURITIES;
    }
    
    public class cls_PRIMARY_SECURITIES {
        public string LOS_FACILITY_SECURITY_ID;
        public string LOS_PRIMARY_SECURITY_ID;
        public String SECYRITY_TYPE;    
        public String SECURITY_ADDRESS; 
        public String SECURITY_OWNERSHIP_TYPE;  
        public String SECURITY_VALUATION_DATE;  
        public String SECURITY_VALUE;   
        public String MORTGAGE_DETAILS;
        public String NATURE_OF_PROPERTY;
        public String PERCENTAGE_OWNERSHIP;
        public String SURVEY_NO;
        public String ASSET_TYPE;
        public String DOOR_NO;
        public String NEAREST_LANDMARK;
        public String PROPERTY_AREA;
        public cls_ADDRESS ADDRESS;
    }
    
    public class cls_COLLATERAL_SECURITIES {
        public string LOS_FACILITY_SECURITY_ID;
        public string LOS_COLLATERAL_SECURITY_ID;
        public String SECYRITY_TYPE;    
        public String SECURITY_ADDRESS; 
        public String SECURITY_OWNERSHIP_TYPE;  
        public String SECURITY_VALUATION_DATE;  
        public String SECURITY_VALUE;
        public String MORTGAGE_DETAILS;
        public String NATURE_OF_PROPERTY;
        public String PERCENTAGE_OWNERSHIP;
        public String SURVEY_NO;
        public String ASSET_TYPE;
        public String DOOR_NO;
        public String NEAREST_LANDMARK;
        public String PROPERTY_AREA;
        //public String PERCENTAGE_OWNERSHIP;
        public cls_ADDRESS ADDRESS;
    }
    
    public class cls_DOCUMENT_SECTION {
        public String FILE_TYPE;    
        public String FILE_NAME;    
        public String FILE_URL;
        public String LOS_SANC_FILE_ID;
    }
    
    public class cls_CURRENT_LOAN_APPLICATION {
        public cls_PRIMARY_SECURITIES[] PRIMARY_SECURITIES;
        public cls_COLLATERAL_SECURITIES[] COLLATERAL_SECURITIES;
        public String PRESENT_RATE_OF_INTEREST; 
    }
    
    public class cls_KEY_PERSON_DETAILS {
        public cls_PRIMARY_DETAILS PRIMARY_DETAILS;
        public cls_COMMUNICATION_DETAILS COMMUNICATION_DETAILS;
        public cls_SECTION_CHANGES SECTION_CHANGES;
        public cls_NETWORTH_DETAILS[] NETWORTH_DETAILS;
        public string LOS_PARTY_TYPE_ID;
        public string DIGIO_STATUS;        
        public String CBS_CUSTOMER_ID;  
        public String PASSPORT_NUMBER;  
        public String SEX;  
        public String NATIONALITY;  
        public String FATHER_NAME;  
        public String MARITAL_STATUS;   
        public String SPOUSE_NAME;  
        public String CASTE;    
        public String RESIDENTIAL_STATUS;   
        public String RESIDENCE_TYPE;   
        public String DIN_NUMBER;   
        public String LAND_AND_BUILDING;    
        public String LOCATION_TYPE;    
        public String CITY; 
        public String STATE;    
        public String PINCODE;  
        public String BUILT_UP_AREA;    
        public String NUMBER_OF_STOREYS;    
        public String VALUE_OF_LAND_AND_BUILDING;   
        public String ALREADY_CHARGED;  
        public String JOINT_HOLDERS;    
        public String TYPE_OF_LAND; 
        public String TYPE_OF_OWNERSHIP;    
        public String AREA_OF_LAND; 
        public String VALUE_OF_LAND;    
        public String PARTY_TYPE;
        public String PRODUCT_TYPE;
        public String GUARANTOR;    //YES/NO
        public string DELETEKEYPERSON; //YES/NO
        public string IS_PHYSTCALLY_HANDICAPPED; //YES/NO
        public string IS_EXSERVICE_MAN; //YES/NO
        public string IS_ACTIVE;//YES/NO
        public string ITR_DOCCATEGORY_ID;
    }
    
    public class cls_PRIMARY_DETAILS {
        public string LOS_PERSON_ACCOUNT_ID;
        public String FIRST_NAME;
        public String LAST_NAME;
        public String DESIGNATION;  
        public String DOB;  
        public String EDUCATIONAL_QUALIFICATION;    
        public String PAN_NUMBER;   
        public String AADHAAR_NUMBER;   
    }
    public class cls_COMMUNICATION_DETAILS {
        public String MOBILE_NUMBER;    
        public String EMAIL_ADDRESS;    
        public cls_ADDRESS PARMANENT_ADDRESS;   
        public String ADDRESS;  
    }
    public class cls_SECTION_CHANGES {
        public string FIRST_NAME;
        public string LAST_NAME;
        public String NAME;
        public String RELATIONSHIP;
        public String BANK_NAME;
        public String IS_KVB_DIRECTOR;
    }
    public class cls_NETWORTH_DETAILS {
        /*public string Existing_Networth;
        public string PROPERTY_TYPE;
        public string PROPERTY_VALUE;
        public string LOS_PROPERTY_ID;
        public cls_ADDRESS ADDRESS;*/
        //attributes copied from sme_customer_fetch wrapper
        public string LOS_PROPERTY_ID;
        public string NATURE_OF_PROPERTY;
        public string PROPERTY_VALUE;
        public string OWNERSHIP_TYPE;
        public string SURVEY_NO;
        public string MORTGAGE_DETAILS;
        public string PERCENTAGE_OWNERSHIP; 
        public string ASSET_TYPE;
        public cls_ADDRESS ADDRESS;
        public string DOOR_NO;
        public string NEAREST_LANDMARK;
        public string PROPERTY_AREA;
        public string EXISTING_NETWORTH; 
        public string PROPERTY_TYPE;
        public List<cls_TITLE_HOLDER> TILE_HOLDERS ;
    }
    public class cls_TITLE_HOLDER{
        public string LOS_TITLE_HOLDER_ID;
        public string LOS_TITLE_HOLDER_NAME;

    }
    public class cls_ADDRESS{
        public string STREET;
        public string CITY;
        public string STATE;
        public string COUNTRY;
        public string PIN_CODE;
        public string TALUK;

    }   
    public class cls_SANCTION_SECTION {
        public String RECOMMENDED_FINAL_RATE_OF_INTEREST;   
    }
    public class cls_FINANCIAL_SECTION {
        public Decimal LABEL_ID;    
        public String FILE_NAME;    
        public String FILE_URL; 
        public String LOS_FINANCE_FILE_ID;
        public cls_FINANCIAL_SECTION(String fName, String fId,Decimal labelId){
            this.FILE_NAME = fName;
            this.FILE_URL = Constants.SITE_BASE_URL+fId; 
            this.LOS_FINANCE_FILE_ID = fId;
            this.LABEL_ID = labelId;
        }
        public cls_FINANCIAL_SECTION(){}
    }
    public class cls_GROUP_CONCERN_DETAILS {
        public String PAN_NUMBER;
        public String GROUP_CONCERN_ID;
        public String FIRM_RELATED_NAME;
        public String BANK_NAME;
        public String LIMIT_AMOUNT;
        public String TYPE;
    }
     
    public class cls_LOANDETAIL {
        public List<cls_FACILITIES> WORKING_CAPITAL; 
        public STOCK_MODEL STOCK; 
        public List<cls_FACILITIES> MY_LOAN; 
    }
    
    public class cls_FACILITIES {
        public string NAME_OF_THE_FACILITY;
        public string ACCOUNT_NUMBER;
        public string TOTAL_LOAN_AMT;
        public string BALANCE_OUTSTANDING;
        public string LIMIT_AMT;
        public string OVER_DUE;
        public string INTEREST_RATE;
        public string PRODUCT_CODE;
        public string APPROVED_LIMIT;
        // Attribute added from sme_fetch wrapper
        public string FACILITY_ID;
        public string NEW_LIMIT_AMT;
    }
    
    public class cls_SUMMARY {
        public cls_CONSTITUTION CONSTITUTION_CHANGED;
        public cls_KEYPERSON NEW_KEYPERSON;
        public cls_KEYPERSON DELETE_KEYPERSON;
        public List<cls_SECTION_CHANGES> SEC_20_CHANGES;
        public cls_REQUEST_SUMMARY REQUEST_SUMMARY;
        
    }
    public class cls_REQUEST_SUMMARY {
        public string REASON_FOR_APPLICATION;
        public string LIMIT_PERIOD;
        public cls_FACILITIES[] FACILITY_INFO;
    }
    public class cls_CONSTITUTION {
        public string PREVIOUS_VALUE;
        public string NEW_VALUE;
    }
    
    public class cls_KEYPERSON {
        public string NAMES;    
    }
    public static Customer360view parse(String json){
        return (Customer360view) System.JSON.deserialize(json, Customer360view.class);
    }
    //Financial statements
    public class cls_FINANCIAL_STATEMENT{
        public cls_FINANCIALS SALES;
        public cls_FINANCIALS PURCHASE;
        public cls_FINANCIALS DEBITOR_AEP  ;
        public cls_FINANCIALS SUNDARY_CREDITOR_AEP  ;
    }
    public class cls_FINANCIALS{
        public cls_FY ACTUAL_PREV_FY;
        public cls_FY ACTUAL_CURRENT_FY;
        public cls_FY YTD_CURRENT_FY;
        public cls_FY EST_NEXT_FY;
        public cls_FY PROJ_NEXT2_FY;
    }
    public class cls_FY{
        public String FY_LABEL;
        public String FY_TOTAL;
        public String MONTH;
        public String LOS_ID;
    }
    //Customer enhancement details
    public class CustEnhancementDetails{
        public String AMOUNT;
        public String REASON;
    }
    
    public class STOCK_MODEL {
        public List<STOCK_TEMPLATE> templates;
        public List<cls_STOCK_STATEMENT> STOCK_STATEMENT;      
    }
    
    public class STOCK_TEMPLATE{
        public string TEMPLATE_LABEL;
        public string TEMPLATE_LINK;
    }
    
    public class cls_STOCK_STATEMENT{
        public string year;
        public string month;
        public string status;
        public STOCK_BELOW_LIMIT belowLimit;
        public STOCK_ABOVE_LIMIT aboveLimit;
    }
    public class STOCK_BELOW_LIMIT {      
        public string OPENING_STOCK;
        public string PURCHASE;
        public string SALES;
        public string SUNDRY_DEBT;
        public string SUNDRY_CREDIT;
        public string LOS_RECORD_ID;
        
        public STOCK_BELOW_LIMIT(){           
            this.OPENING_STOCK          = '';
            this.PURCHASE               = '';
            this.SALES                  = '';
            this.SUNDRY_DEBT            = '';
            this.SUNDRY_CREDIT          = '';
            this.LOS_RECORD_ID          = '';
        } 
    }
    public class STOCK_ABOVE_LIMIT {
        public cls_STATUS STOCK_STATUS;
        public cls_STATUS DEBTORS_STATUS;
        public cls_STATUS CREDITORS_STATUS;
    }
    public class cls_TEMPLATES {
        public string TEMPLATE_LABEL;
        public string TEMPLATE_LINK;
    }
    public class cls_APP_REQUEST {
        public string REQ_REASON;
        public string REASON_FOR_APPLICATION;
        public string LIMIT_PERIOD;
        public List<cls_NETWORTH_DETAILS> COLLATERAL_DETAILS;
        public cls_FACILITIES[] FACILITY_INFO;
    }
    public class cls_DEBTORS_CREDITORS_INFO {
        public List<cls_DEBTORS_CREDITORS> DEBOTORS_AS_FINACIAL_END;
        public List<cls_DEBTORS_CREDITORS> DEBOTORS_AS_CURRENT_DATE;
        public List<cls_DEBTORS_CREDITORS> CREDITORS_AS_FINACIAL_END;
        public List<cls_DEBTORS_CREDITORS> CREDITORS_AS_CURRENT_DATE;
    }
    public class cls_LIMIT_ASSESMENT {
        public string YEAR;
        public List<cls_LIMIT_ASSESMENT_INFO> monthwiseInfo;
    }
    public class cls_DEBTORS_CREDITORS {
        public string LOS_RECORD_ID;
        public string NAME;
        public String AGE;
        public string AMOUNT;        
    }
    public class cls_LIMIT_ASSESMENT_INFO{
        public string MONTH; 
        public String Advances_from_customers;
        public String Cash_Sales_Receipts;
        public String Credit_Purchases;
        public String Other_payments_if_any;
        public String Other_receipts_if_any;
        public String Payment_of_creditors;
        public String Payment_of_Expenses;
        public String Purchases;
        public String Realization_of_debtors;
        public String Salary_and_Wages;
    }
    public class cls_STATUS{
        public string status; // COMPLETED, PENDING
    }
    public class cls_MISSING_FIN{
        public string year;
        public string status;
        public string datasource;
    }
    public class cls_NET_WORTH_STATEMENT{
        public String TOTAL_OUTSTANDING_LIABALITIES;
        public String TOTAL_NETWORTH;
        public String ORIGINAL_AMOUNT_INVESTMENTS;
        public DocumentFetch.DocumentCategory TOL_AND_TNW_DOC_CATEGORY;
    }
}