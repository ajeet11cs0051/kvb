/* 
* Name      : perfiosCalculations
* Purpose   : This class is used for Calculating the Bank Statement ITR values.
* Company   : ET Marlabs
* Author    : Venu
*/
public class perfiosCalculations {
    public static void CalculateBS(string AccID, string AppID){
        list<Perfios__c> pF=[select id,Regular_Credits__c,Application__c,(select id,Year__c,Month_Name__c, Month_no__c from Monthly_Detials_Perfios__r) from Perfios__c where Applicant_Name__c=:AccID  and Application__c=:AppID and active__c=true];
        genesis__Applications__c application = [Select Id,Branch_Code__c,Additional_Income_Approval__c,Record_Type_Name__c,Sub_Stage__c From genesis__Applications__c where Id =:appID];
        map<string,string> MonthYe=new map<string,string>();
        AppAccountNMIJunction__c accAppNMIJunction = new AppAccountNMIJunction__c();
        set<integer> Ye=new set<integer>();
        set<string> PFMIds=new set<string>();
        decimal RegCredit = 0;
        system.debug(pf.size());
        for(Perfios__c Pios:pF){
            RegCredit =+ Pios.Regular_Credits__c;
            PFMIds.add(Pios.id);
            for(Monthly_Detials_Perfios__c p:Pios.Monthly_Detials_Perfios__r){
                ye.add(integer.valueOf(p.Year__c));
            }
        }

        list<integer> yea= new list<integer>();
        integer s;
        integer s1;
        yea.addAll(ye);
        system.debug(ye);
        system.debug(yea);
        if(yea.size()>0){
            yea.sort();
            s=yea[(yea.size()-1)];
            if(yea.size()>=2){
                s1=yea[(yea.size()-2)];
            }
        }
        Boolean SalaryCont = false;
        decimal I1,I2,I3,I4,I5,I6=0;
        decimal SR1=0; Decimal SR2=0; Decimal SR3 =0;
        decimal NMIasBS = 0;
        Decimal EMILastMonth =0;
        if(pF.size()>0){
            for(Perfios__c Pios:pF){
                list<Monthly_Detials_Perfios__c> mf=[select id,Total_Salary__c,Total_EMI_issue__c from Monthly_Detials_Perfios__c where perfios__c =:Pios.id order by Transaction_Date__c desc limit 6];
                if(mf.size() > 0){
                    system.debug('mf'+mf);
                    system.debug('mf'+mf.size());
                    system.debug('mf perfios size'+pf.size());
                    I1 = mf[0].Total_Salary__c;
                    I2 = mf[1].Total_Salary__c;
                    I3 = mf[2].Total_Salary__c;
                    I4 = mf[3].Total_Salary__c;
                    I5 = mf[4].Total_Salary__c;
                    I6 = mf[5].Total_Salary__c;

                    system.debug('Salary values'+I1+','+I2+','+I3+','+I4+','+I5+','+I6);
                    if(I1 !=0 && I2!=0 && I3!=0 && I4!=0 && I5!=0 && I6!=0){
                        if (application <> null && application.Record_Type_Name__c == Constants.LAPLOAN) {
                            Decimal LapImax = 1.3 * (Math.min(Math.min(Math.min(Math.min(Math.min(I5, I6), I4), I3), I2), I1));
                            Decimal EstimatedInc1 = (Math.min(I1, LapImax) + Math.min(I2, LapImax) + Math.min(I3, LapImax) + Math.min(I4, LapImax) + Math.min(I5, LapImax) + Math.min(I6, LapImax)) / 6;
                            NMIasBS = EstimatedInc1;
                        }else{
                            Decimal Imax=1.3*(Math.min(Math.min(I1, I2 ), I3 ));
                            decimal EstimatedSInc = (Math.min(I1, Imax ) + Math.min(I2, Imax ) + Math.min(I3, Imax ))/3;
                            SalaryCont =true;
                            system.debug('Regular_Credits__c'+RegCredit);
                            EMILastMonth+=mf[0].Total_EMI_issue__c;
                            NMIasBS+=EstimatedSInc;//+RegCredit; chnages to add regular credit in last update
                            SR1+=I1;
                            SR2+=I2;
                            SR3+=I3;
                        }

                    } else{
                        SalaryCont =false;
                    // NMIasBS+=NMIasBS;
                }//else{
                //  NMIasBS = RegCredit;
                //}
                        }

            }
            account acc=new account();
            if(Utility.ISStringBlankorNull(String.valueOf(RegCredit)))
            {
                RegCredit = 0;
            }
            acc.NMI_as_per_BS__c=NMIasBS+RegCredit;
            acc.id=AccID;

            acc.EMI_of_Last_Month__c=EMILastMonth;
            acc.Salary_Continous_Flag__c=SalaryCont;
            acc.Salary_Credited_1__c=SR1;
            acc.Salary_Credited_2__c=SR2;
            acc.Salary_Credited_3__c=SR3;

            acc.Annual_Other_Income_Recurring_Credits_IT__c=RegCredit;
            accAppNMIJunction = queryService.getNMIJunction(AccID,AppID);
            accAppNMIJunction.NMI_as_per_BS__c = NMIasBS+RegCredit;

            try{
                update acc;
                update accAppNMIJunction;
            }catch(exception e){
                system.debug('Error'+e.getStackTraceString()+'Line Number'+e.getLineNumber() +'ErrorMsg : '+e.getMessage());
            }
        }
    }
}