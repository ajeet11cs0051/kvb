public with sharing class PL_Approve_Sanction_DigioDocreq {
		
	public String exe_tenantId= 'cuecent_tenant';   //cuecent_tenant
    public String owner_tenantId= 'cuecent_tenant'; //cuecent_tenant
    public String serviceName= 'TestJsonStr';   //TestJsonStr
    public cls_inputVariables inputVariables;
    public class cls_inputVariables {        
        public String in_msg= '';   //
        public String unique_key= '';   //
        public cls_inputmap inputmap;
    }
    
    public class cls_inputmap {
        public string Borrower1_Add_line1='';
        public string Application_date   ='';
        public string Loc_Ref_Branch     ='';
        public string Borrower1_Name1    ='';
        public string Processing_charges ='';
        public string Date_Time_Ver      ='';
        public string Application_number ='';
        public string Date_Time_Adm      ='';
        public string sanction_date      ='';
        public string Ap_LT              ='';
        public string Borrower1_Add_line3='';
        public string Borrower1_Add_line2='';
        public string Int_ROI            ='';
        public string Loan_product       ='';
        public string Financial_Yr       ='';
        public string Loan_tenor         ='';
        public string Branch_code        ='';
        public string Doc_gen_date       ='';
        public string Auth_Sign_Bnk     ='';

        //additional Tags for PL Approval sanction letter
        public string Conditions_LOS = '';
        public string Borrower1_Pan = '';
        public string Penal_Interest ='';
        public string Borrower1_Email ='';
        public string PreClosure_Charges ='';
        public string Borrower1_Aadhaar = '';
        public string EMI_amount ='';
        public string Borrower1_Phone = '';
        public string Non_refundable_charges = '';
        
    }
    
}