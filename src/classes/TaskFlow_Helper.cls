/*
* Name    : TaskFlow_Helper
* Company : ET Marlabs
* Purpose : This class is used to assign Task to the users
* Author  : Subas
*/
public class TaskFlow_Helper {   
    public static boolean TASK_TRIGGER_RUNNING = false;
    public static void createTask(List<genesis__Applications__c> newAppList, List<genesis__Applications__c> oldAppList){
        try{
            System.debug('** inside create task');
           
            // Record type id of LAP Loan
            Id AppLapId = Schema.SObjectType.genesis__Applications__c.getRecordTypeInfosByName().get(Constants.LAPLOAN).getRecordTypeId();
            System.debug('AppLapId'+AppLapId);
            
            for(genesis__Applications__c newApp :newAppList){
                
                
                for(genesis__Applications__c oldApp :oldAppList){
                   
                if((newApp.Record_Type_Name__c ==Constants.LAPLOAN && oldApp.Record_Type_Name__c ==Constants.LAPLOAN) ||(newApp.Record_Type_Name__c ==Constants.HOMELOAN && oldApp.Record_Type_Name__c ==Constants.HOMELOAN)){ 
                    if(newApp.Record_Type_Name__c == Constants.HOMELOAN){
                    if(newApp.Sub_Stage__c =='Terms and Conditions Accepted' && oldApp.Sub_Stage__c <> 'Terms and Conditions Accepted' && newApp.Charges_Processed__c == oldApp.Charges_Processed__c){
                        //FI TASK   
                        system.debug('**newApp.Project_Code__c**'+newApp.Project_Code__c+'**oldApp.Sub_Stage__c**'+oldApp.Sub_Stage__c);                 
                        String usrId = getUserList(newApp.Branch_Code__c,'RLPC processing officer');
                        if(usrId ==null || usrId ==''){
                            
                          
                            usrId = getUserList(newApp.Branch_Code__c,'DO processing officer');
                        }                        
                        system.debug('^^^usrId^^^^'+usrId);
                        if(usrId != null){
                            creatTaskAll(newApp.Id, usrId, 'Verify current address', 3);                            
                        }
                    }
                    if(newApp.Sub_Stage__c =='Terms and Conditions Accepted' && oldApp.Sub_Stage__c<>'Terms and Conditions Accepted'){ // && (newApp.Project_Code__c ==null) && (newApp.Loan_Purpose__c != 'Property not Identified')&& newApp.Charges_Processed__c == oldApp.Charges_Processed__c){
                        //M121                    
                        String usrId = getUserList(newApp.Branch_Code__c,'Branch manager');
                        system.debug('^^M121^^^usrId^^^^'+usrId);
                        if(usrId != null){
                            creatTaskAll(newApp.Id, usrId, 'Verify property and upload valuation and legal reports', 5);
                        }
                    }
                    else if(newApp.Sub_Stage__c =='Terms and Conditions Accepted' && newApp.Flags_Raised__c ==True && oldApp.Flags_Raised__c ==False && newApp.Charges_Processed__c == oldApp.Charges_Processed__c){
                        //Flag CLEARNCE
                        String usrId = getUserList(newApp.Branch_Code__c,'DO Processing Officer');
                        if(usrId != null){
                            creatTaskAll(newApp.Id, usrId, 'Verify application flags', 1);
                        }
                    }
                   
                    //Task for Disbursement
                    else if((newApp.Sub_Stage__c =='Loan Sanctioned Non-STP' || newApp.Sub_Stage__c =='Loan Sanctioned STP' || newApp.Sub_Stage__c =='Loan Sanctioned-Committee') && (newApp.Sub_Stage__c <> oldApp.Sub_Stage__c)&& newApp.Charges_Processed__c == oldApp.Charges_Processed__c){
                        String usrId = getBMList(newApp.Branch_Code__c,'Branch manager');
                        if(usrId !=null || usrId !=''){
                            creatTaskAll(newApp.Id, usrId, 'Approval for disbursement', 60);    
                        }
                    }
                    //Task for MOD
                    else if(newApp.Sub_Stage__c =='Loan Account Opened' && oldApp.Sub_Stage__c<>'Loan Account Opened' && newApp.Charges_Processed__c == oldApp.Charges_Processed__c && oldApp.Loan_Purpose__c !='Construction on Own Land' && oldApp.Loan_Purpose__c != 'Repair or Renovation of House' && oldApp.Execute_Tripartite_Agreement__c !=null && oldApp.Execute_Tripartite_Agreement__c!='Yes') {
                        String usrId = getBMList(newApp.Branch_Code__c,'Branch manager');
                        if(usrId !=null || usrId !=''){
                            creatTaskAll(newApp.Id, usrId, 'Initiate MOD', 14); 
                        }
                    }                    
                    if(newApp.Sub_Stage__c =='MOD Documents Esigned' && oldApp.Sub_Stage__c<>'MOD Documents Esigned'){
                        String usrId = getBMList(newApp.Branch_Code__c, 'Branch Manager');
                        System.debug('userId:::'+usrid);
                        if(usrId !=null || usrId !=''){
                            creatTaskAll(newApp.Id, usrId, 'Upload latest EC', 10); 
                            
                            creatTaskAll(newApp.Id, usrId, 'Upload Property Insurance', 10);
                             }                        
                    }
                    if(newApp.Sub_Stage__c =='Terms and Conditions Accepted' && oldApp.Sub_Stage__c<>'Terms and Conditions Accepted' && newApp.Is_Take_Over__c == oldApp.Is_Take_Over__c && oldApp.Is_Take_Over__c == True){
                        String usrId = getBMList(newApp.Branch_Code__c,'Branch manager');
                        if(usrId !=null || usrId !=''){
                            creatTaskAll(newApp.Id, usrId, 'Take-over Validation', 5); 
                        }                        
                    }
                }
                    //  This block will run both Home Loan and LAP Loan for additional income and assess income
                    //Additonal Income Assessment task create only if additional agriculture income is claimed
                    if(newApp.Sub_Stage__c =='Terms and Conditions Accepted' && oldApp.Sub_Stage__c<>'Terms and Conditions Accepted' && (newApp.Record_Type_Name__c == Constants.HOMELOAN ) ){
                        integer AgrriCount = 0;
                        integer AllhavePerfios = 0;  
                   System.debug('** inside additional income');
                      System.debug('** inside additional income-Lap');  
                        List <Account> accList = queryService.ApplicantList(newApp.id);
                            for(Account acc : accList){
                                if(acc.Perfios_Captured__c == True){
                                    AllhavePerfios++;
                                }

                                if(acc.Agriculture1__c != null){
                                         AgrriCount++;
                                }
                            }
                        system.debug('AllhavePerfios'+AllhavePerfios+'**AgrriCount**'+AgrriCount);
                            if(AgrriCount > 0 && accList.size() == AllhavePerfios){
                                String usrId = getUserList(newApp.Branch_Code__c,'RLPC processing officer');
                             if(usrId ==null || usrId ==''){
                                usrId = getUserList(newApp.Branch_Code__c,'DO Processing Officer');
                            }
                            if(usrId !=null || usrId !=''){
                                creatTaskAll(newApp.Id, usrId, 'Additonal Income Assessment', 5); 
                            }
                            }else if(accList.size() != AllhavePerfios){
                                String usrId = getUserList(newApp.Branch_Code__c,'RLPC processing officer');
                                if(usrId ==null || usrId ==''){
                                    usrId = getUserList(newApp.Branch_Code__c,'DO Processing Officer');
                                }
                                if(usrId !=null || usrId !=''){
                                creatTaskAll(newApp.Id, usrId, 'Assess income', 1);   
                                }
                            }                        
                    }  
                  }
             // Personal Loan Task Creation starts here -------------------------------------------------------
                    else if(newApp.Record_Type_Name__c == Constants.PERSONALLOAN && oldApp.Record_Type_Name__c == Constants.PERSONALLOAN){

                           
                          if(newApp.Sub_Stage__c =='Terms and Conditions Accepted' && oldApp.Sub_Stage__c <> 'Terms and Conditions Accepted' && newApp.Perfios_Captured__c == False){
                                //Assess Income TASK   
                                system.debug('**oldApp.Sub_Stage__c**'+oldApp.Sub_Stage__c);                 
                                String usrId = getUserList(newApp.Branch_Code__c,'RLPC processing officer');
                                if(usrId ==null || usrId ==''){
                                  
                                    usrId = getUserList(newApp.Branch_Code__c,'DO processing officer');
                                }                        
                                system.debug('^^^usrId^^^^'+usrId);
                                if(usrId != null){
                                    creatTaskAll(newApp.Id, usrId, 'Assess Income', 1);                            
                                }
                            }
                            if(newApp.Sub_Stage__c =='Terms and Conditions Accepted' && oldApp.Sub_Stage__c <> 'Terms and Conditions Accepted'  && oldApp.Additional_Income_Approval__c == True && newApp.Perfios_Captured__c == True){
                                //Assess Additional income TASK   
                                system.debug('**oldApp.Sub_Stage__c**'+oldApp.Sub_Stage__c);                 
                                String usrId = getUserList(newApp.Branch_Code__c,'RLPC processing officer');
                                if(usrId ==null || usrId ==''){
                                  
                                    usrId = getUserList(newApp.Branch_Code__c,'DO processing officer');
                                }                        
                                system.debug('^^^usrId^^^^'+usrId);
                                if(usrId != null){
                                    creatTaskAll(newApp.Id, usrId, 'Assess Additional income', 1);                            
                                }
                            }

                           
                            if(oldApp.Sub_Stage__c == 'Terms and Conditions Accepted' && (newApp.FI_Status__c == 'Fraud' || newApp.FI_Status__c == 'Negative' ||newApp.FI_Status__c == 'Refer') ){
                                //Validate FI rejection TASK   
                                system.debug('**oldApp.Sub_Stage__c**'+oldApp.Sub_Stage__c);                 
                                String usrId = getUserList(newApp.Branch_Code__c,'RLPC Head');
                                 if(usrId ==null || usrId ==''){
                                        System.debug('#####');
                                        usrId = getUserList(newApp.Branch_Code__c,'DOO');
                                        if(usrId ==null || usrId ==''){
                                           usrId = getUserList(newApp.Branch_Code__c,'DO Head');
                                        }  
                                }        
                                system.debug('^^^usrId^^^^'+usrId);
                                if(usrId != null){
                                    creatTaskAll(newApp.Id, usrId, 'Verify FI rejection', 2);                            
                                }
                            }
                          
                            if( (newApp.Sub_Stage__c =='Loan Sanctioned Non-STP' && oldApp.Sub_Stage__c <> 'Loan Sanctioned Non-STP') || (newApp.Sub_Stage__c =='Loan Sanctioned STP' && oldApp.Sub_Stage__c <> 'Loan Sanctioned STP') && newApp.Docgen_Fail__c == TRUE){
                                  
                                system.debug('**oldApp.Sub_Stage__c**'+oldApp.Sub_Stage__c);                 
                                String usrId = getUserList(newApp.Branch_Code__c,'Branch manager');
                                system.debug('^^^usrId^^^^'+usrId);
                                if(usrId != null){
                                    creatTaskAll(newApp.Id, usrId, 'Failed document generation', 1);                            
                                }
                            }
                            if(newApp.Sub_Stage__c =='Disbursement Documents Esigned' && oldApp.Sub_Stage__c <> 'Disbursement Documents Esigned' && newApp.genesis__Account__r.Are_you_An_Existing_Customer__c == False){
                                  
                                system.debug('**oldApp.Sub_Stage__c**'+oldApp.Sub_Stage__c);                 
                                String usrId = getUserList(newApp.Branch_Code__c,'Branch manager');
                                system.debug('^^^usrId^^^^'+usrId);
                                if(usrId != null){
                                    creatTaskAll(newApp.Id, usrId, 'Manual disbursement', 1);                            
                                }
                            }
                            if(newApp.Sub_Stage__c =='Disbursement Documents Esigned' && oldApp.Sub_Stage__c <> 'Disbursement Documents Esigned' ){
                                
                                CBS_API_Log__c CBSFail = [Select id, Name,Sequence_No__c, CreatedDate, API_Name__c,Application__c,Status__c from CBS_API_Log__c where Sequence_No__c =: Constants.PL_DISBURSMNTFAIL_SEQ_NO AND  Application__c=: newApp.id order by CreatedDate desc limit 1];
                                
                                system.debug('**oldApp.Sub_Stage__c**'+oldApp.Sub_Stage__c);                 
                                String usrId = getUserList(newApp.Branch_Code__c,'Branch manager');
                                                    
                                system.debug('^^^usrId^^^^'+usrId);
                                if(usrId != null && CBSFail.Status__c == 'Failure'){
                                    creatTaskAll(newApp.Id, usrId, 'Failed disbursement', 1);                            
                                }
                            }
                            if(newApp.Sub_Stage__c =='Loan Account Opened' && oldApp.Sub_Stage__c <> 'Loan Account Opened' ){
                                  
                                system.debug('**oldApp.Sub_Stage__c**'+oldApp.Sub_Stage__c);                 
                                String usrId = getUserList(newApp.Branch_Code__c,'Branch manager');
                                system.debug('^^^usrId^^^^'+usrId);
                                if(usrId != null){
                                    creatTaskAll(newApp.Id, usrId, 'Update credit life insurance', 30);                            
                                }
                            }
                        
                    }  // PL Task creation ENDs here------------------------------------------------------  
                    
                    /// Lap Task start here
                     if(newApp.Record_Type_Name__c ==Constants.LAPLOAN && oldApp.Record_Type_Name__c ==Constants.LAPLOAN ){
                        System.debug('inside lap');
                          
                        //Income Assessment
                        if(newApp.Sub_Stage__c =='Terms and Conditions Accepted' && oldApp.Sub_Stage__c <> 'Terms and Conditions Accepted' && newApp.Perfios_Captured__c == False && newApp.Additional_Income__c==True){
                                 
                                system.debug('** inside Income Assessment oldApp.Sub_Stage__c**'+oldApp.Sub_Stage__c);                 
                                String usrId = getUserList(newApp.Branch_Code__c,'RLPC processing officer');
                                if(usrId ==null || usrId ==''){
                                  
                                    usrId = getUserList(newApp.Branch_Code__c,'DO processing officer');
                                }                        
                                system.debug('^^^usrId^^^^'+usrId);
                                if(usrId != null){
                                    creatTaskAll(newApp.Id, usrId, 'Income Assessment', 1);                            
                                }
                            }
                            
                        // Additional income assessment
                        integer AgrrCount = 0;
                        integer AllhavePerf = 0;  

                        List <Account> accList = queryService.ApplicantList(newApp.id);
                            for(Account acc : accList){
                                if(acc.Perfios_Captured__c == True){
                                    AllhavePerf++;
                                }

                                if(acc.Agriculture1__c != null){
                                         AgrrCount++;
                                }
                            }
                        system.debug('AllhavePerfios'+AllhavePerf+'**AgrriCount**'+AgrrCount);
                        
                        
                        if(newApp.Sub_Stage__c =='Terms and Conditions Accepted' && oldApp.Sub_Stage__c <> 'Terms and Conditions Accepted' && newApp.Additional_Income_Approval__c == True && oldApp.Additional_Income_Approval__c == False && newApp.Perfios_Captured__c == True ){
                              
                            if(AgrrCount> 0){
                                system.debug('**oldApp.Sub_Stage__c**'+oldApp.Sub_Stage__c);                 
                                String usrId = getUserList(newApp.Branch_Code__c,'RLPC processing officer');
                                if(usrId ==null || usrId ==''){
                                  
                                    usrId = getUserList(newApp.Branch_Code__c,'DO processing officer');
                                }                        
                                system.debug('^^^usrId^^^^'+usrId);
                                if(usrId != null){
                                    creatTaskAll(newApp.Id, usrId, 'Additional income assessment', 1);                            
                                }
                            }
                            }

                        // Verify property and purpose of loan
                        
                        if(newApp.Sub_Stage__c =='Terms and conditions accepted' && oldApp.Sub_Stage__c <> 'Terms and conditions accepted'){
                                  
                            System.debug('Verify property and purpose of loan');
                                system.debug('**oldApp.Sub_Stage__c**'+oldApp.Sub_Stage__c);                 
                                String usrId = getUserList(newApp.Branch_Code__c,'Branch manager');
                                                        
                                system.debug('^^^usrId^^^^'+usrId);
                                if(usrId != null){
                                    creatTaskAll(newApp.Id, usrId, 'Verify property and purpose of loan', 5);                            
                                }
                            }
                        // Verify FI rejection
                         if(newApp.Sub_Stage__c =='Terms and Conditions Accepted' && oldApp.Sub_Stage__c <> 'Terms and Conditions Accepted' && (newApp.FI_Status__c=='Fraud' || newApp.FI_Status__c=='Negative' || newApp.FI_Status__c=='Refer')){                                                                                 
                                String usrId = getUserList(newApp.Branch_Code__c,'RLPC Head');
                                 if(usrId ==null || usrId ==''){
                                        System.debug('#####');
                                        usrId = getUserList(newApp.Branch_Code__c,'DOO');
                                        if(usrId ==null || usrId ==''){
                                           usrId = getUserList(newApp.Branch_Code__c,'DO Head');
                                        }  
                                }                          
                                system.debug('^^^usrId^^^^'+usrId);
                                if(usrId != null){
                                    creatTaskAll(newApp.Id, usrId, 'Verify FI rejection', 2);                            
                                }
                            }
                        
                            Boolean bureau_flag=false;
                   
                        
                         List<genesis__Application_Parties__c> partylist =associtedBuisness(newApp.Id );
                         System.debug('partylist'+partylist);
                         for(genesis__Application_Parties__c pp: partylist){
                             System.debug('**ppp'+pp.genesis__Party_Account_Name__r.Jocata_List_Match__c);
                             if(pp.genesis__Party_Account_Name__r.Jocata_List_Match__c=='Yes' || Integer.valueOf(pp.genesis__Party_Account_Name__r.Bureau_Score__c)>=7){
                                  bureau_flag=true;
                                break;
                             }
                         }
                         
                         
                        //Approve associated business entity
                        if(newApp.Sub_Stage__c =='Terms and conditions accepted' && oldApp.Sub_Stage__c <> 'Terms and conditions accepted' && bureau_flag==true ){
                                System.debug('Approve associated business entity');  
                                
                                system.debug('**oldApp.Sub_Stage__c**'+oldApp.Sub_Stage__c);                 
                                String usrId =getUserList(newApp.Branch_Code__c,'DO Head');
                                                        
                                system.debug('^^^usrId^^^^'+usrId);
                                if(usrId != null){
                                    creatTaskAll(newApp.Id, usrId, 'Approve associated business entity', 1);                            
                                }
                            }
                        //Initiate MOD TASK 
                        if(newApp.Sub_Stage__c =='Loan Sanctioned Non-STP' && oldApp.Sub_Stage__c <> 'Loan Sanctioned Non-STP' && newApp.Sanction_Date__c<>NULL ){
                                //Initiate MOD TASK   
                                System.debug('Initiate MOD TASK ');
                                Date todaydate = system.today();
                                Date sanctiondate = newApp.Sanction_Date__c;
                                Integer numberDays = todaydate.daysBetween(sanctiondate);
                                System.debug('numberDays'+numberDays);
                                  integer totalday =numberDays+90;
                                 
                                system.debug('**oldApp.Sub_Stage__c**'+oldApp.Sub_Stage__c);                 
                                String usrId = getUserList(newApp.Branch_Code__c,'Branch manager');
                                                        
                                system.debug('^^^usrId^^^^'+usrId);
                                if(usrId != null){
                                    creatTaskAll(newApp.Id, usrId, 'Initiate MOD', totalday);                            
                                }
                            }
                        
                         //Approve disbursement
                        if(newApp.MODValueCheckbox__c==True){
                                 System.debug('*Approve disbursement');
                                Date todaydate = system.today();
                                Date sanctiondate = newApp.Sanction_Date__c;
                                Integer numberDays = todaydate.daysBetween(sanctiondate);
                                System.debug('numberDays'+numberDays);
                                  integer totalday =numberDays+90;
                                system.debug('**oldApp.Sub_Stage__c**'+oldApp.Sub_Stage__c);                 
                                String usrId = getUserList(newApp.Branch_Code__c,'Branch manager');
                                                        
                                system.debug('^^^usrId^^^^'+usrId);
                                if(usrId != null){
                                    creatTaskAll(newApp.Id, usrId, 'Approve disbursement', totalday);                            
                                }
                            }
                    }
                    ///LAP end here
                    else{
                        System.debug('at last');
                    }
                   
                }
            }
        }catch(Exception e){
            System.debug('+++exception'+e);
            System.debug('**line number'+e.getLineNumber());
        }
        
    }
    //method will be called from Task Trigger
    public static void createTaskApp(String appId, String taskSubject, Task tsk){
        try{
            System.debug('@@@@ inside createTaskApp '+appId);
            genesis__Applications__c application = [Select Id,Branch_Code__c,Additional_Income_Approval__c,Record_Type_Name__c,Sub_Stage__c,FI_Status__c From genesis__Applications__c where Id =:appId];
            system.debug('!!!!!'+application);
            if(application<>null && (application.Record_Type_Name__c == Constants.HOMELOAN ||application.Record_Type_Name__c == Constants.LAPLOAN || application.Record_Type_Name__c == Constants.PERSONALLOAN)){
                //Post FE trigger
                if((taskSubject =='Assess income' || taskSubject =='Additonal Income' || taskSubject == 'Assess Additional income' || taskSubject == 'Income Assessment') && (application.Record_Type_Name__c == Constants.HOMELOAN || application.Record_Type_Name__c == Constants.PERSONALLOAN)){
                    String usrId = getUserList(application.Branch_Code__c,'RLPC Head');
                    System.debug('#####');
                    if(usrId ==null || usrId ==''){
                        System.debug('#####');
                          usrId = getUserList(application.Branch_Code__c,'DOO');
						if(usrId ==null || usrId ==''){
					    usrId = getUserList(application.Branch_Code__c,'DO Head');
						}
                        
                    }                      
                    if(usrId !=null || usrId !=''){
                        System.debug('#####');
                        creatTaskAll(application.Id, usrId, 'Approve income', 1); 
                    }
                }
                //Post M121
                else if(taskSubject =='Enter property details' && application.Record_Type_Name__c == Constants.HOMELOAN  ){
                    String usrId = getUserList(application.Branch_Code__c,'DO Legal Clearance Officer');
                    System.debug('useriddsdsds:::'+usrId);
                    if(usrId !=null || usrId !=''){
                        creatTaskAll(application.Id, usrId, 'Legal clearance', 1);      
                    }
                }
                //Post Legal Clearance
                else if(taskSubject =='Verify property and upload valuation and legal reports' && application.Record_Type_Name__c == Constants.HOMELOAN ){
                    String usrId = getUserList(application.Branch_Code__c,'RLPC processing officer');
                    if(usrId ==null || usrId ==''){
                        usrId = getUserList(application.Branch_Code__c,'DO Processing Officer');    
                    }
                    creatTaskAll(application.Id, usrId, 'Enter property details', 1);             
                }
            }
            if(application<>null && application.Record_Type_Name__c == Constants.PERSONALLOAN || application.Record_Type_Name__c == Constants.LAPLOAN){
                 System.debug('*inside fi block');
                if(taskSubject == 'Verify FI rejection' && tsk.Owner_Designation__c == 'RLPC Head' && application.Sub_Stage__c <> 'FI Rejected' && (application.FI_Status__c == 'Negative' || application.FI_Status__c == 'Fraud') ){
                                //Approve FI TASK   
                    System.debug('**aprove fi');
                    system.debug('**Sub_Stage__c**'+application.Sub_Stage__c);                 
                    String usrId = getUserList(application.Branch_Code__c,'DO Head');
                    system.debug('^^^usrId^^^^'+usrId);
                    if(usrId != null){
                        creatTaskAll(application.Id, usrId, 'Approve FI', 1);                            
                    }
                }

             }
            
            /// For  Lap task
             if(application<>null && application.Record_Type_Name__c == Constants.LAPLOAN){
                 System.debug('**inside create task lap');
                 //Verify property and purpose of loan
                   if(taskSubject =='Verify property and purpose of loan'){
                    system.debug('inside property and purpose');
                    String usrId = getUserList(application.Branch_Code__c,'RLPC processing officer');
                    System.debug('**usrId'+usrId);
                    if(usrId ==null || usrId ==''){
                        usrId = getUserList(application.Branch_Code__c,'DO Processing Officer');    
                    }
                    creatTaskAll(application.Id, usrId, 'Property data entry', 1);             
                }
                
				if(taskSubject =='Property data entry'){
                    System.debug('inside Property data entry');
                    String usrId = getUserList(application.Branch_Code__c,'DO Legal Clearance Officer');
                    if(usrId ==null || usrId ==''){
                        usrId = getUserList(application.Branch_Code__c,'DO Processing Officer');    
                    }
                    creatTaskAll(application.Id, usrId, 'Legal clearance', 1); 					
				}
                 
                     if(taskSubject == 'Additional income assessment' || taskSubject == 'Income Assessment'){
                    String usrId = getUserList(application.Branch_Code__c,'RLPC Head');
                    System.debug('##### inside approve income');
                    if(usrId ==null || usrId ==''){
                        
					    usrId = getUserList(application.Branch_Code__c,'DO Head');
						}
                        
                                         
                    if(usrId !=null || usrId !=''){
                        System.debug('#####');
                        creatTaskAll(application.Id, usrId, 'Approve income', 1); 
                    }
                } 

                 
             }
            // Lap end here
            
        }catch(Exception e){
            
            System.debug('+++exception'+e);
            System.debug('**line number'+e.getLineNumber());
        }
    }
    public static void creatTaskAll(String appId,String usrId, String sub, Integer days){
        List<Task> tskList = new List <Task>();
        tskList = [Select Subject,WhatId From Task Where WhatId =: appId AND Subject =: sub];
        system.debug('**tskList**'+tskList);
        if(tskList.size()==0){
            Task tsk = new Task();
            tsk.WhatId = appId;
            tsk.OwnerId = usrId;
            tsk.ActivityDate = system.today() + days;
            tsk.Subject = sub;
            tsk.Status = 'Not Started';
            tsk.Priority = 'High';
            System.debug('tsk:::'+tsk);
            try{
                Insert tsk;
            }catch(Exception e){
                System.debug('e.msg:::'+e.getCause());
                System.debug('e.msg:::'+e.getLineNumber());
                System.debug('e.msg:::'+e.getLineNumber());
                System.debug('e.msg:::'+e.getStackTraceString());
            }
            System.debug('tsk:::'+tsk);
        }
    }
    public static String getUserList(String branchCode, String Designation){
        system.debug('***branchCode**'+branchCode+'***Designation****'+Designation);
        Branch_Master__c brMaster = [Select Id, CODCCBRN__c,Linked_RLPC__c, Division_Code__c From Branch_Master__c Where CODCCBRN__c=:branchCode limit 1];
        System.debug('brMaster:::'+brMaster);
        List <User> usrList = new List <User>();
        if(Designation =='Branch manager'){
            usrList = [Select ID From User where Office_Code__c =:brMaster.CODCCBRN__c And Role_Name__c=:Designation AND IsActive =:True];
        }else if(Designation.contains('RLPC') && brMaster.Linked_RLPC__c != null){
            usrList = [Select ID From User where Office_Code__c =:brMaster.Linked_RLPC__c And Role_Name__c=:Designation AND IsActive =:True];
        }
        else{
            usrList = [Select ID From User where Office_Code__c =:brMaster.Division_Code__c And Role_Name__c=:Designation AND IsActive =:True];        
        }
        system.debug('####'+usrList);
        String UsrId = AssignTask.userAssign(usrList);
        if(UsrId == null){
            if(usrList.size()>0){
                UsrId = usrList[0].Id;
            }            
        } 
        return UsrId;
    }
    public static String getBMList(String branchCode, String Designation){
        System.debug('branchCode:::'+branchCode+':::Designation:::'+Designation);
        List <User> usrList = [Select ID From User where Office_Code__c =:branchCode And Role_Name__c=:Designation AND IsActive =:True];
        String UsrId = AssignTask.userAssign(usrList);
        System.debug('usrid:::'+UsrId);
        return UsrId;
    }

    public static void preventTaskDeletion(Task taskRec){
        taskRec.addError('<div style="color:red"> This task can not be deleted, Please contact the administrator. </div>',false);
    }
    public static  List<genesis__Application_Parties__c>  associtedBuisness(String appid){
        
     List<genesis__Application_Parties__c> partylist=[SELECT Active__c,Company__c,genesis__Application__c,genesis__Party_Account_Name__r.name,genesis__Party_Account_Name__r.Jocata_List_Match__c,genesis__Party_Account_Name__r.Bureau_Score__c FROM genesis__Application_Parties__c WHERE genesis__Application__c =:appid AND Active__c = true AND Company__c = true];
            
        return partylist;
    }
}