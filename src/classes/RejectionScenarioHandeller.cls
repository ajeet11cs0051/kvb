/*
* Name      : RejectionScenarioHandeller
* Compnay   : ET Marlabs
* Purpose   : For LAP Rejection Scenario basesd on Collateral 
* Author    : Niladri
*/
public class RejectionScenarioHandeller {
    
    private static RejectionScenarioHandeller instance;
    //Singleton class Method.
    public static RejectionScenarioHandeller getInstance(){
        if(instance==null){
            instance=new RejectionScenarioHandeller();
        }
        return instance;
    }
    public static Boolean IsFirstRun = true;
    //Rejection scenario for LAP
    public static void RejectioMethod(Map<id,genesis__Applications__c> newMapApp,Map<id,genesis__Applications__c> oldMapApp){
        try{
            List<genesis__Applications__c> applist=new List<genesis__Applications__c>();
            for(genesis__Applications__c app :newMapApp.values()){
                if(oldMapApp.get(app.id).Sub_Stage__c != newMapApp.get(app.id).Sub_Stage__c && app.Sub_Stage__c=='Property information captured' && app.Record_Type_Name__c =='LAP' ){
                    
                    System.debug('Stage'+app.Sub_Stage__c);
                    List<clcommon__Collateral__c> collatarallist=[select EM_Charge_Types__c,No_of_tenants__c,Vacant_Land_area__c from  clcommon__Collateral__c where genesis__Application__c =:app.Id];
                    
                    System.debug('*collatarallist'+collatarallist);
                    for(clcommon__Collateral__c colvalue : collatarallist){
                        System.debug('*colvalue'+colvalue);
                        if(colvalue.EM_Charge_Types__c !='Fresh') {
                            System.debug('inside charge type fresh');
                            
                            app.Reject_Scenario__c = 'Charge can only be Fresh EM for digital applications';
                            app.Sub_Stage__c       = 'Work information captured';
                            //   applist.add(app);
                            break;
                        }
                        
                        else if((colvalue.No_of_tenants__c != null ? Integer.valueOf(colvalue.No_of_tenants__c) : 0) >10){
                            System.debug('inside if for no of tenants');
                            app.Reject_Scenario__c = 'No. of tenants cannot be >10 for digital applications';
                            app.Sub_Stage__c       = 'Work information captured';
                            //   applist.add(app);
                            break;
                        }
                        else if(colvalue.Vacant_Land_area__c > 10000){
                            app.Reject_Scenario__c  = 'Vacant land cannot be >10000 sq ft for digital applications';
                            app.Sub_Stage__c        = 'Work information captured';
                            //    applist.add(app);
                            break;
                        }   
                        else if(colvalue.clcommon__City__c != getBranchCity(app.Branch_Code__c,false)){
                            app.Reject_Scenario__c  = 'Property and KVB branch need to be in the same city for digital LAP';
                            app.Sub_Stage__c        = 'Work information captured';
                            //    applist.add(app);
                            break;
                        }
                        
                        else if(colvalue.EM_Charge_Types__c =='Fresh' && (colvalue.No_of_tenants__c != null ? Integer.valueOf(colvalue.No_of_tenants__c) : 0) <10 && colvalue.Vacant_Land_area__c < 10000 && (colvalue.clcommon__City__c == getBranchCity(app.Branch_Code__c,false))){
                            if(!String.isBlank(app.Reject_Scenario__c)){
                                app.Reject_Scenario__c = '';
                            }                            
                        }                        
                    }
                }
                if(oldMapApp.get(app.id).Sub_Stage__c == 'Application Created' && app.Sub_Stage__c=='Work Information Captured' && app.Record_Type_Name__c =='LAP' ){
                    List<genesis__Application_Parties__c> PTList = new List<genesis__Application_Parties__c>();
                    PTList = [Select Id From genesis__Application_Parties__c Where genesis__Application__c =: app.Id AND Active__c = true AND Company__c = false];
                    if(PTList.size() == 0){
                        app.Reject_Scenario__c  = 'Application should have at least one Co-applicant or Guarantor';
                        app.Sub_Stage__c        = 'Application Created';   
                    }else{
                        app.Reject_Scenario__c  = '';                       
                    }
                }
            }
            System.debug('*applistsssssssssssssss'+applist);
            //  update applist;
        }
        catch(Exception e){
            System.debug('exception'+e);
        }
        
    }
    
    
    //Rejection scenario for Personal Loan
    
    public static void RejectioMethodPersonalLoan(Map<id,genesis__Applications__c> newMapApp,Map<id,genesis__Applications__c> oldMapApp){
        genesis__applications__c appListUp = new genesis__applications__c();
        

        Map<Id,Id> accIdbyAppId = new Map<Id,Id>();
        Map<Id,Account> accbyAppId = new Map<Id,Account>();
        for(genesis__Applications__c appRec:newMapApp.values()){
            accIdbyAppId.put(appRec.genesis__Account__c,appRec.Id);
        }
        for(Account accRec:[SELECT Id,Name,Employment_Type__c,Annual_Other_Income_Recurring_Credits_IT__c,Age__c,Net_Monthly_Income__c,Employment_SubType__c from Account where Id IN:accIdbyAppId.keySet()]){
            accbyAppId.put(accIdbyAppId.get(accRec.Id),accRec);
        }
        
        try{
            List<genesis__Applications__c> applist=new List<genesis__Applications__c>();
            List<genesis__Applications__c> Newapplist = [ select id, Sub_Stage__c,PMax__c,Record_Type_Name__c,Total_Cheque_Bounces__c,Nmax__c,Sanction_Message__c,genesis__Account__r.Perfios_Captured__c, Perfios_Captured__c, Additional_Income__c, genesis__Account__r.Same_Present_Address_Permanent_Address__c,genesis__account__r.Current_Address_As_Permanent_Address__c, genesis__account__r.Are_you_An_Existing_Customer__c from genesis__applications__c where id IN : newMapApp.keySet()];
            System.debug('application list after pmax changed ==== '+ Newapplist);
            if(Newapplist.size() > 0 && RejectionScenarioHandeller.IsFirstRun){
                IsFirstRun = false;
                for(genesis__Applications__c app :Newapplist){ //newMapApp.values()){
            
                    Account acc = accbyAppId.get(app.Id);
                    System.debug('application after pmax changed ==== '+ app.PMax__c);
                    //if(oldMapApp.get(app.id).Sub_Stage__c!= newMapApp.get(app.id).Sub_Stage__c && app.Sub_Stage__c=='Work Information Captured' && app.Record_Type_Name__c == Constants.PERSONALLOAN ){
                    if(app.Sub_Stage__c=='Work Information Captured' && app.Record_Type_Name__c == Constants.PERSONALLOAN ){
                        //Personal Loan 
                        system.debug('app.Sub_Stage__c:::'+app.Sub_Stage__c);
                        if(app.PMax__c < 100000) {
                            appListUp.Reject_Scenario__c='Eligibility below minimum threshold of INR 1 Lakh';
                            appListUp.Sub_Stage__c='Application Created';
                            appListUp.Id = app.id;
                           // newMapApp.get(app.id).put()
                            system.debug('##### '+ appListUp);
                            applist.add(appListUp);
                            break;
                        }
                        if(app.Nmax__c<12){
                            appListUp.Reject_Scenario__c='Minimum Tenure ineligible';
                            appListUp.Sub_Stage__c='Application Created';
                            appListUp.error_message_pl__c ='Customer not eligible for loan based on age criterion';
                            appListUp.Id = app.id;
                            applist.add(appListUp);
                            break; 
                        }
                        if(app.Total_Cheque_Bounces__c> 1){
                            appListUp.Reject_Scenario__c='Cheque Returns';
                            appListUp.Sub_Stage__c='Application Created';
                            appListUp.Id = app.id;
                            applist.add(appListUp);
                            break; 
                        } 
                        if(app.PMax__c > 100000 && app.Nmax__c >12 && app.Total_Cheque_Bounces__c < 1 ){
                            appListUp.Reject_Scenario__c=''; 
                            appListUp.Id = app.id;
                            applist.add(appListUp);
                            break; 
                        }
                    }
                     if(app.Sub_Stage__c=='Terms and Conditions Accepted' && app.Record_Type_Name__c == Constants.PERSONALLOAN && app.genesis__Account__r.Perfios_Captured__c == true && app.Additional_Income__c == false && app.genesis__Account__r.Same_Present_Address_Permanent_Address__c == true && app.genesis__Account__r.Are_you_An_Existing_Customer__c == true){

                            appListUp.Sub_Stage__c = Constants.PL_LOAN_STP;
                            appListUp.Id = app.id;
                            applist.add(appListUp);
                            system.debug('##### PL_LOAN_STP'+ applist);
                            break;

                    }
                    system.debug('app.Sub_Stage__c:::'+app.Sub_Stage__c);
                    //capture income information related by Ashish (Start Here)
                    if(app.Record_Type_Name__c == Constants.PERSONALLOAN){
                        system.debug('app.Record_Type_Name__c:::'+app.Record_Type_Name__c);
                        if(acc.Employment_Type__c == Constants.EmpType_Salaried){
                            System.debug('@@@1111');
                            System.debug('acc.Age__c:::'+acc.Age__c);
                            if(acc.Age__c < 23){
                                System.debug('acc.Age__c:::'+acc.Age__c);
                                appListUp.Application_Stage__c = Constants.APP_REJECTED_STAGE;
                                appListUp.Sub_Stage__c = 'Age Ineligible';
                                appListUp.error_message_pl__c = 'Applicant age is less than 23 years and is not eligible for a loan';
                                appListUp.Id = app.id;
                                applist.add(appListUp);
                                break;
                            }
                            Decimal REcurringIncome = 0;
                            if(acc.Annual_Other_Income_Recurring_Credits_IT__c != null){
                                REcurringIncome =  acc.Annual_Other_Income_Recurring_Credits_IT__c;
                            }
                            
                            if((acc.Employment_SubType__c == 'Govt.' || acc.Employment_SubType__c == 'PSU' || acc.Employment_SubType__c == 'Public Ltd.') && ((acc.Net_Monthly_Income__c - REcurringIncome) < 15000)){
                                appListUp.Reject_Scenario__c = 'NMI does not meet minimum NMI criterion';
                                appListUp.Sub_Stage__c = 'Application Created';
                                appListUp.error_message_pl__c = 'Minimum income criterion not met, loan cannot be processed';
                                appListUp.Id = app.id;
                                applist.add(appListUp);
                                break;
                            }
                            if((acc.Employment_SubType__c != 'Govt.' && acc.Employment_SubType__c != 'PSU' && acc.Employment_SubType__c != 'Public Ltd.') && ( (acc.Net_Monthly_Income__c - REcurringIncome) < 25000)){
                                appListUp.Reject_Scenario__c = 'NMI does not meet minimum NMI criterion';
                                appListUp.Sub_Stage__c = 'Application Created';
                                appListUp.error_message_pl__c = 'Minimum income criterion not met, loan cannot be process';
                                appListUp.Id = app.id;
                                applist.add(appListUp);
                                break;
                            }
                            if( ((acc.Employment_SubType__c == 'Govt.' || acc.Employment_SubType__c == 'PSU' || acc.Employment_SubType__c == 'Public Ltd.') && (acc.Net_Monthly_Income__c > 15000)) ||  ((acc.Employment_SubType__c != 'Govt.' && acc.Employment_SubType__c != 'PSU' && acc.Employment_SubType__c != 'Public Ltd.') && ((acc.Net_Monthly_Income__c - REcurringIncome) > 25000))) {
                                appListUp.Reject_Scenario__c = '';
                                appListUp.error_message_pl__c = '';
                                appListUp.Id = app.id;
                                applist.add(appListUp);
                                break;
                            }
                        }else
                            if(acc.Age__c < 25){
                                system.debug('acc.age:::'+acc.Age__c);
                                appListUp.Application_Stage__c = Constants.APP_REJECTED_STAGE;
                                appListUp.Sub_Stage__c = 'Age Ineligible';
                                appListUp.error_message_pl__c = 'Applicant age is less than 25 years and is not eligible for a loan';
                                appListUp.Id = app.id;
                                applist.add(appListUp);
                                break;
                            }else{
                                appListUp.Id = app.id;
                                appListUp.error_message_pl__c = '';
                                applist.add(appListUp);
                                break;

                            }
                        
                    }
                    //capture income information related by Ashish (Ends Here)
                }
                update applist;

            }
            }catch(Exception e){
            
                System.debug('exception'+e.getMessage()+'At'+e.getLineNumber());
        }
        
    }
    public static String getBranchCity(String branchCode, boolean check){
        List <Branch_Master__c> branchL = [Select Id,CODCCBRN__c,State__c,Retail_Category__c from Branch_Master__c where CODCCBRN__c =:branchCode Limit 1];
        if(branchL.size()>0){
            if(check == true){
                return branchL[0].Retail_Category__c;
            }else if(check == false){
                return branchL[0].CODCCBRN__c;   
            }
        }
        return null;
    }
}