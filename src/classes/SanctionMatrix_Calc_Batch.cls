/*
 * @name        : SanctionMatrix_Calc_Batch
 * @description : Batch class for application sanction authority calculation
 * @author      : Amritesh
 */ 
public class SanctionMatrix_Calc_Batch implements Database.Batchable<sObject>{
    
     private String applnId;

    public SanctionMatrix_Calc_Batch(String appId) {
        applnId = appId;
    }
    
    public static set<string> APP_STAGES    = new set<string>{'Identified for renewal'};
    public Database.QueryLocator start(Database.BatchableContext BC){
        
        string query    = 'Select id,Name,Application__c,Application__r.ownerId,Application__r.Application_Stage__c,Limit_Amount__c,Existing_Limit__c,Type__c,Application__r.genesis__Account__c,CL_Product__c'+', (select id,genesis__Collateral__c from Facility_Security__r) ';
        if(applnId != null){
             query           += ' from Facility__c where Active__c = true AND Application__c =:applnId'; // Added only for testing.Remove after testing.
        }else{
            query         += ' from Facility__c where Active__c = true AND Application__c !=null AND Application__r.Application_Stage__c IN:APP_STAGES';
        }
               
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<Facility__c> records) {
        try{
            Set<Id> appIdSet = new Set<Id>();
            //Delete existing authority
            for(Facility__c fac : records){
                appIdSet.add(fac.Application__c);
            }
            List<Facility__c> childFacilityList = new List<Facility__c>();
            if(!appIdSet.iseMpty()){
                childFacilityList = [SELECT Id FROM Facility__c WHERE facility__r.Application__c IN : appIdSet AND RecordType.DeveloperName = 'Child'];
            }
            if(!childFacilityList.isEmpty()){
                DELETE childFacilityList;
            }
            //Create authority
            SanctionMatrix_Calculation  sObj    = new SanctionMatrix_Calculation();            
            sObj.calculateAuthority(records,false,null,null);
        }catch(Exception e){}
    }
    
    public void finish(Database.BatchableContext BC) {
    }
}