@isTest
public class Test_TaskNotificationBatch {
    @isTest
    public static void method1(){
        List<Task> tasks = new List<Task>{};
            date myDate = date.newInstance(2017, 11, 21);
            String userId = UserInfo.getUserId();
            

            for(Integer i = 0; i < 200; i++) 
        {
            
            Task t = new Task();
            t.OwnerId = userId;
            t.Subject = 'Hello World'+i;
            t.Status = 'Open';
            t.Priority = 'Normal';
            t.ActivityDate=myDate;
            tasks.add(t);
        }
        insert tasks;
        
        Test.startTest();
        TaskNotificationBatch ob=new TaskNotificationBatch();
         DataBase.executeBatch(ob);

        Test.stopTest();
    }
}