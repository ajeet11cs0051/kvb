/*
* Name    : WS_MIS_Update_HL
* Company : ET Marlabs
* Purpose : This class CBS API to update MIS
* Author  : Subas
*/
public class WS_CBS_MIS_Update_HL {
    //@future (Callout=true)
    public static void Mis_Update(String appID){
        genesis__Applications__c application = new genesis__Applications__c();
        application = queryService.getApp(appID);
		CBS_MIS_DATA__c mis = CBS_MIS_DATA__c.getOrgDefaults();
        
        MIS_Update_Handler cbs = new MIS_Update_Handler();
        cbs.InputVariables = new MIS_Update_Handler.cls_inputVariables();
        MIS_Update_Handler.cls_inputVariables cbsInput = new MIS_Update_Handler.cls_inputVariables();
        cbsInput.in_msg = new MIS_Update_Handler.cls_in_msg();
        cbs.InputVariables = cbsInput;
        cbs.exe_tenantId = 'cuecent_tenant';
        cbs.owner_tenantId = 'cuecent_tenant';
        cbsInput.in_msg.Mnemonic = application.Loan_Account_Number__c;	 
        cbsInput.in_msg.Security = mis.Security__c;
        if(application.Record_Type_Name__c == Constants.PERSONALLOAN)
            cbsInput.in_msg.Security = '';

        cbsInput.in_msg.NoOfDependents ='';	 
        cbsInput.in_msg.PrioritySectorCode = mis.Priority_Sector_Code__c;
        if(application.Record_Type_Name__c == Constants.PERSONALLOAN)
        	 cbsInput.in_msg.PrioritySectorCode = '999';

        cbsInput.in_msg.SpouseIncome ='99';	 
        cbsInput.in_msg.Occupation = mis.Occupation__c;	 
        cbsInput.in_msg.MonthlyHouseHoldIncome ='99';	 
        cbsInput.in_msg.RBIPurpose = mis.RBI_Purpose__c;	 
        cbsInput.in_msg.AssetOwnership = '';	 
        cbsInput.in_msg.Nostro = mis.Nostro__c;	 
        cbsInput.in_msg.ResidenceDetails ='99';	 
        cbsInput.in_msg.SelectedSecurityCode = mis.Selected_Security_Code__c;	 
        cbsInput.in_msg.CreditCardDetails ='999';	 
        cbsInput.in_msg.BSRActivityCode = mis.BSR_Activity_Code__c;	 
         if(application.Record_Type_Name__c == Constants.PERSONALLOAN)
            cbsInput.in_msg.BSRActivityCode = '3';

        cbsInput.in_msg.BankingActivities ='99';	 
        cbsInput.in_msg.CategoryOfBorrowers = mis.Category_Of_Borrowers__c;	 
        cbsInput.in_msg.InvestmentPreference ='99';	 
        cbsInput.in_msg.ExportDocType = mis.Export_Doc_Type__c;	 
        cbsInput.in_msg.LoanDetails ='99';	 
        cbsInput.in_msg.ExportStatusHolder = mis.Export_Status_Holder__c;	 
        cbsInput.in_msg.PresentLoanRequirements ='99';	 
        cbsInput.in_msg.MakerID = mis.MakerID__c;	 
        cbsInput.in_msg.CheckerID = mis.CheckerID__c;	 
        getMIS(JSON.serialize(cbs),AppId);
    }
    public static void getMIS(String misData, String AppId){
        system.debug('####'+misData);
        KVB_Endpoint_URLs__c kvb = KVB_Endpoint_URLs__c.getValues('CBS_MIS_Update');
        Map<String,String> headerMap                            = new Map<String,String>();
        headerMap.put('Content-Type','application/json');
        HTTPResponse response                                   = new HTTPResponse();
        String endPoint                                         = kvb.Endpoint_URL__c;
        response = HttpUtility.sendHTTPRequest(endPoint, 'POST', null,misData,headerMap,label.CA_CERTIFICATE);   
        system.debug('#####'+response);
        system.debug('$$$$'+response.getBody());
        If(response.getStatusCode() == 200 || response.getStatusCode() == 201){
            String jsonString = response.getBody();            
            MIS_Update_Handler.MIS_Update_Response res = (MIS_Update_Handler.MIS_Update_Response)Json.deserialize(jsonString,MIS_Update_Handler.MIS_Update_Response.class);
            System.debug(res);
            System.debug(res.out_msg.StatusDecs);
            System.debug(res.out_msg.ErrorMessage);
            if(res.out_msg.ErrorMessage == null || res.out_msg.ErrorMessage ==''){
                CBS_API_Log__c log = new CBS_API_Log__c();    
                log.Application__c = AppId;
                log.API_Name__c = 'MIS_Update';
                log.Status__c = 'Failure';
                log.Success_Error_Message__c = res.out_msg.ErrorMessage;
                log.Sequence_No__c = '10';  
                insert log;                
            }else if(res.out_msg.ErrorMessage == 'Success'){
                WS_CBS_UploadImage.cbs_photo_upload(AppId);
            }
        }
    }
}