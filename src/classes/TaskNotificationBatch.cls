/*
* Name          : TaskNotificationBatch
* Description   : Send notification to all task owner if due date passed
* Author        : Dushyant
*/
global class TaskNotificationBatch implements Database.Batchable<sObject>{
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'SELECT Id,ActivityDate,WhatId,OwnerId,Owner.Email,Manager_email__c,Manager_name__c,Subject,Owner_name__c,Status FROM Task WHERE ActivityDate > TODAY';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Task> taskList){
        Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage>();
        String body;
        String taskSubject = 'Task escalation!';
        String imageLogo = SME_Email_Handler.getDocumentLogoUrl();
        Map<String,String> emailIdBodyMap = new Map<String,String>();
        
        for(Task tsk : taskList){
            if(tsk.Manager_email__c != null){
                body = 'Dear '+tsk.Manager_name__c+',<br><br>As of this email, Task :'+tsk.Subject+' assigned to '+tsk.Owner_name__c+' is now overdue since '+Date.valueOf(tsk.ActivityDate).format()+' with the status: '+tsk.Status+'. Please look into the same and ensure completion of the task as soon as possible. <br><br>Thanks.<br><br><img src="'+imageLogo+'"/>';
                emailIdBodyMap.put(tsk.Manager_email__c,body);
            }
        }
        
        if(emailIdBodyMap.size() > 0){
            for(Task tsk : taskList){
                if(tsk.Manager_email__c != null){
                    if(emailIdBodyMap.containsKey(tsk.Manager_email__c)){
                        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                        message.toAddresses = new List<String>{tsk.Manager_email__c};
                        message.setSubject(taskSubject);
                        message.setReplyTo('support@kvb.com');
                        message.setSenderDisplayName('KVB TEAM');
                        message.setBccSender(false);
                        message.setHtmlBody(emailIdBodyMap.get(tsk.Manager_email__c));
                        messages.add(message);
                        
                    } 
                } 
            }
        }
        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
        
        if(results.size() > 0 ){
            if (results[0].success) {
                System.debug('The email was sent successfully.');
            } else {
                System.debug('The email failed to send: ' + results[0].errors[0].message);
            }
        }
    }
    
    global void finish(Database.BatchableContext BC){}
}