/*
* Name    : WS_CBS_Status_Update 
* Company : ET Marlabs
* Purpose : This class is used to update CBS API details
* Author  : Subas
*/

@RestResource(urlMapping='/updateCBSstatus')
global class WS_CBS_Status_Update {
    
    global class Response extends WS_Response{
        
        public CBS_API_Log__c application;
        public Response(){
            application = new CBS_API_Log__c();            
        }
    }
    
    public cbsStatusReq cbsStatus;
    public class cbsStatusReq{
        public String ApplicationId;
        public String ApiName;
        public List<acctReqWrapper> CbsStatus;
    }
    public class acctReqWrapper {
        
        public String ApplicantId;        
        public String SequenceNo;
        public String Status;
        public string succErrCode;
        public string message;
        public string CustomerID;
    }
    @HttpPost
    global static Response getMandateFields(){
        RestRequest req      = Restcontext.Request;
        Response res         = new Response();    
        
        if(req == null || req.requestBody == null){
            res.status          = Constants.WS_ERROR_STATUS;
            res.errorMessage    = Constants.WS_REQ_BODY_IS_NULL;
            res.statusCode      = Constants.WS_ERROR_CODE;
            return res;            
        }else{
            List<CBS_API_Log__c> rencustomers = new List<CBS_API_Log__c>();
            try{                
                String jsonData  = req.requestBody.toString();     
                system.debug('#####'+jsonData);
                cbsStatusReq rq  = (cbsStatusReq)Json.deserialize(jsonData, cbsStatusReq.class);                   
                system.debug('****rq****'+rq);
                List<Account> accList = new List<Account>();
                for(acctReqWrapper reqObj : rq.CbsStatus){
                    CBS_API_Log__c app =new CBS_API_Log__c();
                    if(!String.isBlank(rq.ApplicationID)){
                        app.Application__c = rq.ApplicationID;
                    }
                    if(!String.isBlank(rq.ApiName)){
                        app.API_Name__c = rq.ApiName;
                    }
                    if(!String.isBlank(reqObj.ApplicantId)){
                        app.Applicant__c = reqObj.ApplicantId;
                    }
                    if(rq.ApiName == 'Customer_ID_Creation_HL'){
                        app.Sequence_No__c = '3';
                    }else If(rq.ApiName == 'EKYCUpdation_HL'){
                        app.Sequence_No__c = '2';
                    }else if(rq.ApiName == 'Loan_Collateral_Creation_HL'){
                        app.Sequence_No__c = '6';
                    }else if(rq.ApiName == 'Term_Loan_Linkage_Collateral_HL'){
                        app.Sequence_No__c = '7';
                    }else if(rq.ApiName == 'FCR_CUST_SIGN_PHOTO_ADD_HL'){
                        app.Sequence_No__c = '11';
                    }
                    
                    if(!String.isBlank(reqObj.Status)){
                        app.Status__c = reqObj.Status;
                    }
                    if(!String.isBlank(reqObj.succErrCode)){
                        app.Success_Error_Code__c = reqObj.succErrCode;
                    }
                    if(!String.isBlank(reqObj.message)){
                        app.Success_Error_Message__c = reqObj.message;
                    }
                    if(!String.isBlank(reqObj.CustomerID)){
                        app.Success_Error_Message__c = reqObj.CustomerID;
                    }
                    rencustomers.add(app);
                    if(!String.isBlank(reqObj.CustomerID) & !String.isBlank(reqObj.ApplicantId)){
                        Account ac = new Account();
                        ac.Id = reqObj.ApplicantId;
                        ac.CBS_Customer_ID__c = reqObj.CustomerID;
                        accList.add(ac);
                    }                    
                }
                
                //if(rencustomers.size()>0){
                    insert rencustomers;
                //}
                if(accList.size()>0){
                    update accList;
                }
                
                //res.application  = rq.ApplicationID;
                res.status      = Constants.WS_SUCCESS_STATUS;
                res.statusCode  = Constants.WS_SUCCESS_CODE;
            }catch(Exception e){
                res.status          = Constants.WS_ERROR_STATUS;
                res.errorMessage   = e.getMessage();
                res.statusCode  = Constants.WS_ERROR_CODE;
                system.debug('Exception::'+e.getMessage());
                return res;
            }          
            
            
            return res;
        }        
    }
    
}