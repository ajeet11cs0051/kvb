/*
* Name          : Ws_PerfiosUpdate
* Description   : Update Perfios data using bank account numbers
* Author        : Venu Gopal
*/
@RestResource(urlMapping='/UpdatePerfios')

global class Ws_PerfiosUpdate {
    global class Response extends WS_Response{
        
        
    }
    
    Public cls_PerfDetails PfDetails;
    
    public class cls_PerfDetails{
        Public string ClientID;
        Public string RequestID;
        Public string Txn_ID;
        Public string Status;
        Public string BSuploadType;
        public string ApplicationID;
        public string Url;
        public string BankName;
        public string Method;
        public string RequestID2;
        public List<BankWrapper> Account_Numbers;
    }
    public class BankWrapper{
        Public string BankAccNumber; 
    }
    
    @HttpPost
    global static Response CreateBankaccounts(){
        
        RestRequest req      = Restcontext.Request;
        Response res         = new Response();    
        
        if(req == null || req.requestBody == null){
            res.status          = Constants.WS_ERROR_STATUS;
            res.errorMessage    = Constants.WS_REQ_BODY_IS_NULL;
            res.statusCode      = Constants.WS_ERROR_CODE;
            return res;            
        }else{
            
            try{
                
                String jsonData  = req.requestBody.toString(); 
                system.debug('jsonData:::'+jsonData);
                
                cls_PerfDetails Cperfios=(cls_PerfDetails)Json.deserialize(jsonData, cls_PerfDetails.class);
                system.debug('Cperfios'+Cperfios);
                if(Cperfios.Method==Constants.PFITR && !Utility.ISStringBlankorNull(Cperfios.RequestID2) && !Utility.ISStringBlankorNull(Cperfios.ClientID) && !Utility.ISStringBlankorNull(Cperfios.ApplicationID)){
                    string ITRReport=HL_ITRCallingAPI.callform26AS(Cperfios.RequestID2,system.label.ITRMethod);
                    string Form26Report=HL_ITRCallingAPI.callform26AS(Cperfios.RequestID2,system.label.Form26AsReport);
                    
                    system.debug('ITRReport'+ITRReport);
                    if(!Utility.ISStringBlankorNull(ITRReport))
                        {
                            ITRResponseHL Itr=ITRResponseHL.parse(ITRReport);
                            HL_Perfios_CallingAPI.ParseITRResponse(Itr,Cperfios.ClientID,Cperfios.ApplicationID);
                        }
                    
                    if(!Utility.ISStringBlankorNull(Form26Report))
                      {
                        Form26asResponse F26=(Form26asResponse)Json.deserialize(Form26Report,Form26asResponse.class);
                        HL_Perfios_CallingAPI.ParseForm26AS(F26,Cperfios.ClientID,Cperfios.ApplicationID);
                       }

                    ITR_Response__c iT=new ITR_Response__c();
                    if(!Utility.ISStringBlankorNull(Cperfios.RequestID2))
                        iT.Request_ID__c=Cperfios.RequestID2;
                        iT.Account__c=Cperfios.ClientID;
                        iT.Application__c = Cperfios.ApplicationID;
                    insert iT;
                }
                else{
                    set<string> BAnumbers=new set<string>();
                    set<string> BAnumbersSMS = new set<string>();

                    for(BankWrapper reqobj :Cperfios.Account_Numbers){
                        if(!Utility.ISStringBlankorNull(reqobj.BankAccNumber) && !Utility.ISStringBlankorNull(Cperfios.ApplicationID))
                            BAnumbers.add(reqobj.BankAccNumber);
                            
                            BAnumbersSMS.add('XXXXXXXXXXXX'+String.valueOf(reqobj.BankAccNumber).right(4));
                            system.debug('##### XXXXXXXXXXXX'+BAnumbersSMS);
                    }
                    if(BAnumbers.size()>0){
                        List<Perfios__c> UPerfios =new List<Perfios__c>();
                        List<Perfios__c>  pf =[select id,Send_URL__c,Active__c, Bank_Name__c,Bank_Account_Number__c,All_Bank_Accounts__c,Application__c from Perfios__c where Bank_Account_Number__c IN:BAnumbers and   Applicant_Name__c=:Cperfios.ClientID and Active__c=true and Application__c =:Cperfios.ApplicationID ];
                        system.debug('pf'+pf);
                        string BankAccNumbers;
                        //  if(pf.size())
                        if(!Utility.ISStringBlankorNull(Cperfios.Url) && !Utility.ISStringBlankorNull(Cperfios.ClientID)){
                            Account Acc=[select id,name,Full_Name__c,PersonEmail,PersonMobilePhone from account where id=:Cperfios.ClientID limit 1];
                            string Msg='Dear '+acc.Full_Name__c+', to complete your KVB Retail Loan application, please submit your bank statement as proof of income. Go to --> '+Cperfios.Url+' to login to Net banking/upload bank statement for the '+Cperfios.BankName+' Account number '+BAnumbersSMS+'.';
                            SMS_Services.sendSMSCall(Acc.PersonMobilePhone,Msg);
                            EmailMessages.sendEmail(Acc.PersonEmail,Msg);
                            for(Perfios__c p:pf){
                                p.Send_URL__c=Cperfios.Url;
                                p.url__c=true;
                                UPerfios.add(p);
                            }
                            
                            update UPerfios;
                        }
                        else if(!Utility.ISStringBlankorNull(Cperfios.Txn_ID)){
                            for(Perfios__c p:pf){
                                
                                p.Active__c =false;
                                
                                if(!Utility.ISStringBlankorNull(Cperfios.Txn_ID))
                                    p.Transaction_Id__c= Cperfios.Txn_ID;
                                if(!Utility.ISStringBlankorNull(Cperfios.RequestID))
                                    p.Request_Id__c=Cperfios.RequestID;
                                if(!Utility.ISStringBlankorNull(Cperfios.BSuploadType))
                                    p.BS_UploadType__c=Cperfios.BSuploadType;
                                
                                UPerfios.add(p);
                            }
                            pf[0].active__c=true;
                            if(UPerfios.size()>0 ){
                                
                                
                                // Parsing perfios response
                                
                                if(Cperfios.Status=='Completed'){
                                    PerfiosReportResponseHL PGReport=   HL_Perfios_CallingAPI.callReport(Cperfios.Txn_ID);
                                    system.debug('PGReport @'+PGReport);
                                    HL_Perfios_CallingAPI.parseReportResponse(PGReport,pf[0].id,Cperfios.ApplicationID);
                                    
                                }
                                else{
                                    PerfiosStatusReportHL PFstatus=   HL_Perfios_CallingAPI.callStatus(Cperfios.Txn_ID);
                                    
                                    system.debug('PFstatus @'+PFstatus);
                                    
                                    
                                    HL_Perfios_CallingAPI.StatusReport Srepo=  HL_Perfios_CallingAPI.parseStatusResponse(PFstatus);
                                    Perfios_Error__c Perr=new Perfios_Error__c();
                                    Perr.Transaction_Id__c =Srepo.TxnID;
                                    Perr.Error_Message__c =Srepo.ErrorMsg;
                                    Perr.Perfios__c=pf[0].id;
                                    insert Perr;
                                    
                                }
                                update UPerfios;
                                update pf[0];  
                            }
                        }
                    }
                }
                return res;
            }
            catch(exception e){
                system.debug('Error  :'+e.getStackTraceString()+'Line number  :'+e.getLineNumber()+'ErrorMsg : '+e.getMessage());
            }
        }
        return null;
    }
}