/*
* Name    : WS_CreateApplicant 
* Company : ET Marlabs
* Purpose : This class is used to Create applicant from App
* Author  : Venugopal N
*/
@RestResource(urlMapping='/CreateApplicant')

global  class WS_CreateApplicant {
    
    global class Response extends WS_Response{
        public Account ApplicantDetails;
        public genesis__Applications__c ApplicationDetails;
        // public string errorCode;
        public genesis__Application_Parties__c PartyDetails; 
        CibilRequestTU_HL IdvRequest;
        public string TUCall;
        public boolean DOCStructure;
    }
    
    public class applicantRequest{
        public Co_applicant applicants;
        
        
        public applicantRequest(){
            applicants =new Co_applicant();
        }
        
    }
    
    public class Co_applicant{
        public string ApplicantionID;
        public string AccountID;
        public string Fname;
        public string Lname;
        public string Mname;
        public string POstreet;
        public string POcity;
        public string POstate;
        public string POcountry;  
        public string POpostalcode;
        public string PMstreet;   
        public string PMcity;
        public string PMstate;
        public string PMcountry;  
        public string PMpostalcode;
        public string Gender;
        public string Bname;   
        public Date Pbirthdate;
        public string Nationality;
        public string NationalIdentificationCode;
        public string CustType;
        public string MariStatus;
        public string Occupation;
        public string PersonEmail;
        public string PersonMobile;
        public string CBScustID;
        public string AdhaarNumber;
        public string PanNumber;
        public string FatherName;
        public string HusbName;
        public string CBSaccountNumber1;
        Public String RelationToBorrower;
        public string PartyType;
        public boolean CretePrimaryAcc;
        public string Customer_IC;
        public string Phone;
        public string Fax_no;
        public string Title;
        public string Caste; 
        public string Religion; 
        public boolean IsExistingCustomer;
        public string CustIDCreatedDate;    
        public string CBSaccountNumber2;
        public string CBSaccountNumber3;
        public string CBSaccountNumber4;
        public string CBSaccountNumber5;
        public string FullName;
        public boolean isAadharAvailable;
        public boolean isPanAvailabe;
        Public String Sourced_by;
        Public String Sourced_by_office;
        Public String BranchCode;
        Public String BranchName;
        Public String LoanType; //Pass type of Loan(Null for HL)
    }
    
    
    @HttpPost
    global static Response AccCreating(){
        RestRequest req      = Restcontext.Request;
        RestResponse restRes = Restcontext.response;
        Response res         = new Response();
        
        
        if(req.requestBody == null){
            res.status          = Constants.WS_ERROR_STATUS;
            res.errorMessage    = Constants.WS_REQ_BODY_IS_NULL;
            res.statusCode      = Constants.WS_ERROR_CODE;
            return res;            
        }
        else{
            try{
                Decimal ApplicantAge = 0.00;
                String jsonData = req.requestBody.toString();
                System.debug('Json Data======>'+jsonData);
                
                Co_applicant applicantRequestobj=(Co_applicant)Json.deserialize(jsonData, Co_applicant.class);
                system.debug('applicantRequestobj'+applicantRequestobj);
                system.debug('applicantRequestobj'+applicantRequestobj.AccountID);
                if(!Utility.ISStringBlankorNull(applicantRequestobj.Fname) && !Utility.ISStringBlankorNull(applicantRequestobj.Gender ) && !Utility.ISStringBlankorNull(string.valueof(applicantRequestobj.Pbirthdate)) && !Utility.ISStringBlankorNull(applicantRequestobj.AdhaarNumber) && !Utility.ISStringBlankorNull(applicantRequestobj.PanNumber) && !Utility.ISStringBlankorNull(applicantRequestobj.PersonMobile)&& !Utility.ISStringBlankorNull(applicantRequestobj.PMcity ) && !Utility.ISStringBlankorNull(applicantRequestobj.PMstreet ) && !Utility.ISStringBlankorNull(applicantRequestobj.PMpostalcode ) && !Utility.ISStringBlankorNull(applicantRequestobj.PMstate )){
                system.debug('applicantRequestobj'+applicantRequestobj.AccountID);
                ApplicantService.borrowerDetail basicAcc=new ApplicantService.borrowerDetail();
                basicAcc.Fname=applicantRequestobj.Fname;
                basicAcc.Lname=applicantRequestobj.Lname;
                basicAcc.Mname=applicantRequestobj.Mname;
                basicAcc.POstreet =applicantRequestobj.POstreet;
                basicAcc.POcity  =applicantRequestobj.POcity;
                basicAcc.POstate =applicantRequestobj.POstate;
                basicAcc.POcountry =applicantRequestobj.POcountry;
                basicAcc.POpostalcode =applicantRequestobj.POpostalcode;
                basicAcc.PMstreet =applicantRequestobj.PMstreet;
                basicAcc.PMcity =applicantRequestobj.PMcity;
                basicAcc.PMstate=applicantRequestobj.PMstate;
                basicAcc.PMcountry =applicantRequestobj.PMcountry;
                basicAcc.PMpostalcode =applicantRequestobj.PMpostalcode;
                basicAcc.Gender =applicantRequestobj.Gender;
                basicAcc.Bname =applicantRequestobj.Bname;
                basicAcc.Pbirthdate =applicantRequestobj.Pbirthdate;
                basicAcc.Nationality =applicantRequestobj.Nationality;
                basicAcc.NationalIdentificationCode =applicantRequestobj.NationalIdentificationCode;
                basicAcc.CustType =applicantRequestobj.CustType;
                basicAcc.MariStatus=applicantRequestobj.MariStatus;
                basicAcc.PersonEmail=applicantRequestobj.PersonEmail;
                basicAcc.PersonMobile =applicantRequestobj.PersonMobile;
                basicAcc.CBScustID =applicantRequestobj.CBScustID;
                basicAcc.AdhaarNumber =applicantRequestobj.AdhaarNumber;
                basicAcc.PanNumber =applicantRequestobj.PanNumber;
                basicAcc.FatherName =applicantRequestobj.FatherName;
                basicAcc.HusbName=applicantRequestobj.HusbName;
                basicAcc.CBSaccountNumber1=applicantRequestobj.CBSaccountNumber1;
                basicAcc.Occupation = applicantRequestobj.Occupation;
                basicAcc.CustIDCreatedDate =applicantRequestobj.CustIDCreatedDate;
                basicAcc.Customer_IC =applicantRequestobj.Customer_IC;
                basicAcc.Phone =applicantRequestobj.Phone;
                basicAcc.CBSaccountNumber2 = applicantRequestobj.CBSaccountNumber2;
                basicAcc.CBSaccountNumber3 =applicantRequestobj.CBSaccountNumber3;
                basicAcc.CBSaccountNumber4 =applicantRequestobj.CBSaccountNumber4;
                basicAcc.CBSaccountNumber5 =applicantRequestobj.CBSaccountNumber5;
                basicAcc.Fax_no=applicantRequestobj.Fax_no;
                basicAcc.Title=applicantRequestobj.Title;
                basicAcc.Caste = applicantRequestobj.Caste; 
                basicAcc.Religion=applicantRequestobj.Religion;
                basicAcc.IsExistingCustomer = applicantRequestobj.IsExistingCustomer; 
                basicAcc.FullName = applicantRequestobj.FullName;
                basicAcc.isAadharAvailable = applicantRequestobj.isAadharAvailable;
                basicAcc.isPanAvailabe = applicantRequestobj.isPanAvailabe;
                account acc=  ApplicantService.CreateAccount(basicAcc);
                CibilRequestTU_HL Creq=new  CibilRequestTU_HL ();
                if(basicAcc.Pbirthdate<>null){                    
                    ApplicantAge = (basicAcc.Pbirthdate.daysBetween(system.today())/365.2425);
                }
                if(ApplicantAge <23 && applicantRequestobj.CretePrimaryAcc == true){
                    res.status          = 'ERROR';
                    res.statusCode  = string.valueOf(genesis.Constants.HTTP_INTERNAL_ERROR);
                    res.errorMessage  = 'Primary Applicant Age should be more than 23';
                    
                    return res;
                }
                else if(ApplicantAge <18 && applicantRequestobj.CretePrimaryAcc == false){
                    res.status          = 'ERROR';
                    res.statusCode  = string.valueOf(genesis.Constants.HTTP_INTERNAL_ERROR);
                    res.errorMessage  = 'Co Applicant Age should be more than 18';
                    
                    return res;
                }
                else if(ApplicantAge >80){
                    res.status          = 'ERROR';
                    res.statusCode  = string.valueOf(genesis.Constants.HTTP_INTERNAL_ERROR);
                    res.errorMessage  = 'Applicant age should be less than 80';
                    
                    return res;                    
                }
                else if(applicantRequestobj.CretePrimaryAcc == true){
                    if(applicantRequestobj.CBScustID!=null && !Utility.ISStringBlankorNull(applicantRequestobj.CBScustID)){
                    upsert acc CBS_Customer_ID__c; 
                    }
                    else{
                      insert acc;  
                    }
                  // insert acc;
                    account ac=[select id,name,FirstName,LastName,MiddleName,Gender__pc,PersonBirthdate,Aadhaar_Number__pc,Pan_Number__c,PersonMobilePhone,
                                PersonMailingStreet,PersonMailingCity,PersonMailingPostalCode,PersonMailingState,Full_Name__c from account where id=:acc.id limit 1];
                    system.debug('aaaaa'+ac.name);
                    Creq=  WS_CibilServiceTU_HL.prepareRequest(ac,Constants.IDVCall) ;   
                    
                    
                    /* system.debug('Creq'+Creq);
CibilResponseTU_HL CibilRes= WS_CibilServiceTU_HL.callCibil(Creq); 
system.debug('CibilRes'+CibilRes);
CibilTUService.getcibildetails(CibilRes);
*/                  String RType = '';
                    if(applicantRequestobj.LoanType == null){
                        RType = Constants.HOMELOAN;    
                    }else if(applicantRequestobj.LoanType == Constants.LAPLOAN){
                        RType = Constants.LAPLOAN;
                    }else if(applicantRequestobj.LoanType == 'PL'){
                        RType = Constants.PERSONALLOAN;
                    }                                     
                    genesis__Applications__c App = ApplicationService.createApplication2(Acc.id,RType,applicantRequestobj.Sourced_by,applicantRequestobj.Sourced_by_office,applicantRequestobj.BranchCode,applicantRequestobj.BranchName,applicantRequestobj.LoanType); 
                    res.DOCStructure  = APPDocCategoryCreation.CreateDocCat(App,acc,applicantRequestobj.CretePrimaryAcc);
                    // Account ac=[select id,firstname,lastname,middlename,Gender__pc,PersonBirthdate from account where id=:acc.id];
                    genesis__Applications__c Applicatoin=[select id,name from genesis__Applications__c where ID=:App.id];
                    system.debug('Done');
                    res.ApplicationDetails=Applicatoin;
                    res.ApplicantDetails=acc; 
                    res.IdvRequest=Creq;
                    res.TUcall=Constants.IDVCall;
                    return res;
                }
                //creating co-applicant
                else if(applicantRequestobj.AccountID != null && ((applicantRequestobj.AccountID.length() == 15 || applicantRequestobj.AccountID.length() == 18)) && applicantRequestobj.CretePrimaryAcc == false){ 
                    genesis__Applications__c App= Applicationservice.getApplication(applicantRequestobj.AccountID);
                    ValidateApp(App);        
                    Boolean dupeCheck;
                    Boolean dupeMobCheck;
                    List <Account> accountList = queryService.accList(App.Id);
                    List<String> aadharList = new List<String>();
                    List<String> mobileList = new List<String>();
                    if(accountList.size()>0){
                        for(Account a:accountList){
                            aadharList.add(a.Aadhaar_Number__pc);
                        }
                        dupeCheck = aadharList.contains(basicAcc.AdhaarNumber);
                    }
                    if(accountList.size()>0){
                        for(Account a:accountList){
                            mobileList.add(a.PersonMobilePhone);
                        } 
                        dupeMobCheck = mobileList.contains(basicAcc.PersonMobile);
                    }
                    if(dupeCheck ==True){
                        res.status          = 'ERROR';
                        res.statusCode  = string.valueOf(genesis.Constants.HTTP_INTERNAL_ERROR);
                        res.errorMessage  = 'Duplicate Aadhar Card found for the same Application';
                        
                        return res;                        
                    }else if(dupeMobCheck == True){
                        res.status          = 'ERROR';
                        res.statusCode  = string.valueOf(genesis.Constants.HTTP_INTERNAL_ERROR);
                        res.errorMessage  = 'Duplicate Mobile No found for the same Application';
                        
                        return res;                        
                    }
                    
                    else{                    
                      //  insert acc; 
                         if(applicantRequestobj.CBScustID!=null && !Utility.ISStringBlankorNull(applicantRequestobj.CBScustID)){
                    upsert acc CBS_Customer_ID__c; 
                    }
                    else{
                      insert acc;  
                    }
                        res.DOCStructure  = APPDocCategoryCreation.CreateDocCat(App,acc,applicantRequestobj.CretePrimaryAcc);
                        account ac=[select id,name,FirstName,LastName,MiddleName,Gender__pc,PersonBirthdate,Aadhaar_Number__pc,Pan_Number__c,PersonMobilePhone,
                                PersonMailingStreet,PersonMailingCity,PersonMailingPostalCode,PersonMailingState,Full_Name__c from account where id=:acc.id limit 1];
                        Creq=  WS_CibilServiceTU_HL.prepareRequest(ac,Constants.IDVCall) ;   
                        
                        genesis__Application_Parties__c Party= PartyService.createPartyRep(acc.id, applicantRequestobj.PartyType,applicantRequestobj.RelationToBorrower,App.Id,App.genesis__Account__c,applicantRequestobj.LoanType);
                        
                        res.ApplicantDetails=acc; 
                        
                        res.PartyDetails=Party;                
                        res.ApplicationDetails= App;
                        res.IdvRequest=Creq;
                        res.TUcall='IDV';
                        return res;
                    }
                }
                else if(applicantRequestobj.CretePrimaryAcc == null){
                    res.status          = 'ERROR';
                    res.statusCode  = string.valueOf(genesis.Constants.HTTP_INTERNAL_ERROR);
                    res.errorMessage  = 'Please Select the Applicant type';
                    
                    return res;
                }
                else{
                    //  res.errorCode       = genesis.Constants.API_EXCEPTION;
                    res.status          = 'ERROR';
                    res.statusCode  = string.valueOf(genesis.Constants.HTTP_INTERNAL_ERROR);
                    res.errorMessage  = 'Please provide Application ID';
                    
                    return res;
                    
                    
                }
            }
                else{
                    string s='';
                    if(Utility.ISStringBlankorNull(applicantRequestobj.Fname)){
                           s=s+'Name';
                      }
                    if(Utility.ISStringBlankorNull(applicantRequestobj.Gender)){
                        if(s.length()>1){
                               s=s+',Gender ';                 }
                        else{
                            s=s+'Gender ';}}
                    if(Utility.ISStringBlankorNull(string.valueof(applicantRequestobj.Pbirthdate))){
                        if(s.length()>1){
                        s=s+', Birth Date ';
                        }
                        else{
                            s=s+'Birth Date ';}}
                    if(Utility.ISStringBlankorNull(applicantRequestobj.AdhaarNumber)){
                        if(s.length()>8){
                        s=s+', Aadhar Number ';
                        }
                        else{
                            s=s+'Aadhar Number ';}}
                    if(Utility.ISStringBlankorNull(applicantRequestobj.PanNumber)){
                        if(s.length()>8){
                        s=s+' , Pan Number ';
                        }
                        else{
                            s=s+'Pan Number ';}}
                    if(Utility.ISStringBlankorNull(applicantRequestobj.PersonMobile)){
                        if(s.length()>1){
                        s=s+' , Mobile number ';
                        }
                        else{
                            s=s+'Mobile number '; }}
                    if(Utility.ISStringBlankorNull(applicantRequestobj.PMstreet)){
                        if(s.length()>8){
                        s=s+', Street Address ';
                        }
                        else{
                            s=s+'Street Address ';}}
                    if(Utility.ISStringBlankorNull(applicantRequestobj.PMcity)){
                        if(s.length()>8){
                        s=s+' , City ';
                        }
                        else{
                            s=s+'City ';
                        }}
                    if(Utility.ISStringBlankorNull(applicantRequestobj.PMpostalcode))
                        {
                        if(s.length()>8){
                        s=s+', Postal Code ';
                        }
                        else{
                        s=s+'Postal Code ';
                        }}
                    if(Utility.ISStringBlankorNull(applicantRequestobj.PMstate)){
                        if(s.length()>8){
                        s=s+' , PMstate ';
                        }
                        else{
                           s=s+' PMstate '; 
                        }
                    }
                   s='Please Provide the '+s;
                    
            res.status          = Constants.WS_ERROR_STATUS;
            res.errorMessage    = s ;
            res.statusCode      = Constants.WS_ERROR_CODE;
            return res;            
                }
            }
            catch(exception e){
                system.debug('Exception in Creatapplicant'+e.getMessage()+' Stack '+e.getStackTraceString());
                // res.errorCode       = genesis.Constants.API_EXCEPTION;
                res.status          = 'ERROR';
                res.statusCode  = string.valueOf(genesis.Constants.HTTP_INTERNAL_ERROR);
                res.errorMessage  = e.getMessage();
                
                return res;
            }
        }
        return res;
    }
    
    public static void ValidateApp(genesis__Applications__c app){
        if(app.id==null){
            Throw new CustomException('AppID not found For Primary Applicant ');
        }
    }
    
}