public class HL_Digio_Service {
    
    @future (callout=true)
     public static void docGenAndSignDoc(String appId ,String Filename){
        
         try{ 
          String docEncodeFile  = '';
            Integer pageCount       = 0;
         string Class2Doc='';
               string FF='';
              string Docid='';
         genesis__Applications__c appln = [Select id,genesis__Account__c,Document_Generation__c,(select id,genesis__Party_Account_Name__c,genesis__Party_Type__c,genesis__Party_Account_Name__r.id from genesis__Application_Parties__r where Active__c=true AND genesis__Party_Account_Name__r.PersonEmail != null AND genesis__Party_Account_Name__r.PersonMobilePhone != null) from genesis__Applications__c where Id =: appId];
                                           
                                            
          String Document_Gen = appln.Document_Generation__c != null ? appln.Document_Generation__c : 'None';
       //  if(!appln.Sanction_Doc_Generation_Check__c){
             if(Filename==Constants.Acknowledgement_For_Sanction){
             if(!Document_Gen.contains(Constants.C1docGen) && !Document_Gen.contains(Constants.C1class2Sign)  && !Document_Gen.contains(Constants.C1DocID)){
           FF=Filename;
             }
                 else{
                     FF='';
                     system.debug('C1 Document Generated.');
                 }
             }
             else if(Filename==Constants.PRE_Approval_Sanction_HL){
                 if(!Document_Gen.contains(Constants.PreApproveGenerated) && !Document_Gen.contains(Constants.PreApproveClass2)){
                  FF=Filename;
                 } else{
                     FF='';
                     system.debug('Pre Approval Document Generated.');
                 }
             }
        string templateId   = Utility.getDIGIOTemplateId(FF); 
             system.debug('templateId'+templateId);
              //Calling the document DIGIO generation service
              Digioe_Docs_Service.DocGenResponse docResp= Digioe_Docs_Service.getEDocs(appId,FF,templateId);
              docEncodeFile = docResp.outMap.Data.outfile;
              pageCount   = Integer.valueOf(docResp.outMap.Data.totalpages);
              system.debug('docEncodeFile'+docEncodeFile);
              if(docEncodeFile == '' || docEncodeFile == null){
               // throw new CustomException('Document Generation Failed');
            }
         
            
            KVB_Company_Details__c company = KVB_Company_Details__c.getOrgDefaults();
           
            //Calling the Class-2 service
            class2Doc = Digioe_Docs_Service.class2Signer(new Digioe_Docs_Service.DocSignerRequest(SYSTEM.LABEL.DigioKeyPersonName,FF,company.City__c,docEncodeFile),pageCount,FF);
            if(class2Doc == null && docEncodeFile != null) {
               // Digioe_Docs_Service.upsertDoc(appId,Filename+'.pdf',docEncodeFile);
            }else{
               // Digioe_Docs_Service.upsertDoc(appId,Filename+'.pdf',class2Doc);
            }
            
            if(class2Doc == '' || class2Doc == null){
               
            }
          
            system.debug('class2Doc'+class2Doc);
             if(FF==Constants.Acknowledgement_For_Sanction){
            Docid=DigioDocID_EsignHL.DocumentIDGeneration(appId, FF, pageCount,class2Doc);
             
             if(Docid!=null && (!String.isBlank(Docid))){
              if(Document_Gen!=null){
                                                
                                                Document_Gen= Document_Gen + ';' + Constants.C1docGen+';'+Constants.C1class2Sign +';'+Constants.C1DocID;
                                            }
                                            else{
                                                Document_Gen=Constants.C1docGen+';'+Constants.C1class2Sign +';'+Constants.C1DocID;
                                            } 
             }
                 else{
                      throw new CustomException(FF+'  Class-2 Sign Failed');
                 }
               
            }
            List<Attachment> attList = new List<Attachment>();
        Attachment att = new Attachment();
        attList = [Select Id,Body,ParentId,Name from Attachment where ParentId=:appId AND Name=:FF+'.pdf' limit 1];
        if(!attList.isEmpty()){
            att = attList[0];
            att.Body = EncodingUtil.base64Decode(class2Doc);
            update att;
        }else{

            att.Name = FF+'.pdf';
            att.ParentId = appId;
            att.Body = EncodingUtil.base64Decode(class2Doc);
            insert att;
        }
             if(FF==Constants.PRE_Approval_Sanction_HL){
                  if(Document_Gen!=null){
                                                
                                                Document_Gen= Document_Gen + ';' + Constants.PreApproveGenerated+';'+Constants.PreApproveClass2 ;
                                            }
                                            else{
                                                Document_Gen= Constants.PreApproveGenerated+';'+Constants.PreApproveClass2 ;
                                            } 

             }
           appln.Document_Generation__c=Document_Gen;
             update appln;
          List<genesis__Application_Document_Category__c> GAdc=[select id from genesis__Application_Document_Category__c where name=:Constants.LoanDocuments And 	genesis__Application__c=:AppID limit 1];
			 List<genesis__AppDocCatAttachmentJunction__c> GaList=[select id,genesis__AttachmentId__c from genesis__AppDocCatAttachmentJunction__c where genesis__AttachmentId__c=:att.id and 	genesis__Application_Document_Category__c=:GAdc[0].id and   Document_Name__c=:FF+'.pdf' limit 1] ;
            
             if(GaList.isEmpty()){
                  genesis__AppDocCatAttachmentJunction__c AAJ=new genesis__AppDocCatAttachmentJunction__c();
                  AAJ.genesis__Application_Document_Category__c=GAdc[0].id;
                  AAJ.genesis__AttachmentId__c=att.id;
                  AAJ.Document_Name__c =FF;
                 insert AAJ;
             }
             if(FF==Constants.Acknowledgement_For_Sanction){
                 List<Digio_Document_ID__c> Dlist=[select id,Application__c,Document_ID__c from Digio_Document_ID__c where 	Name=:FF and Application__c =:appId];
             Digio_Document_ID__c Did=new Digio_Document_ID__c();
				  
           	 Did.Name=FF;
             Did.Application__c=appId;
             Did.Document_ID__c=Docid;
                 if(Dlist.size()>0){
              DELETE Dlist  ; 
                 }
             insert Did;            
           set<id> AcciDs=new set<Id>();
               AcciDs.add(appln.genesis__Account__c) ;
                 					    integer pcount=0;
                                        integer count=0;
                       for(genesis__Application_Parties__c Apart:appln.genesis__Application_Parties__r){
                                            if(Apart.genesis__Party_Type__c ==Constants.Co_Borrower){
                                                if(count<=2){
                                                    count++;
                                                   AcciDs.add(Apart.genesis__Party_Account_Name__r.id);
                                                }
                                            }
                                           else if(Apart.genesis__Party_Type__c ==Constants.Gurantor){
                                                if(pcount<=9){
                                                    pcount++;
                                                    AcciDs.add(Apart.genesis__Party_Account_Name__r.id);
                                                }
                                            }
                                        }
                 if(AcciDs.size()>0){
                      List<Document_Applicant__c> DocApp=new List<Document_Applicant__c>();
                     for(string s:AcciDs){
                         Document_Applicant__c DApp=new Document_Applicant__c();
                       						    DApp.Account__c=s;
                                                DApp.Digio_Document_ID__c=Did.Id;
                                                DocApp.add(DApp);
                     }
                     insert DocApp;
                 }
             }
            
         }
            
         catch(exception e){
             system.debug('Error'+e.getLineNumber()+e.getStackTraceString());
         }
     }
       

         
      
    // }
}