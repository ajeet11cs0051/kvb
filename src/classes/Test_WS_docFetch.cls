@isTest
public class Test_WS_docFetch {
@isTest
    public static void method1(){
        
         genesis__Applications__c app=TestUtility.intialSetUp('HomeLoan',true);
        Account acc=new Account(Name='TestName');
        insert acc;

         RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/getDoc'; 
        req.httpMethod  = 'GET';
        req.params.put('appId',app.id);
       
        RestContext.request = req;
        RestContext.response = res;
        Test.startTest();
        WS_docFetch.getDetails();
        Test.stopTest();
    }
    
    @isTest
    public static void method2(){
        
         genesis__Applications__c app=TestUtility.intialSetUp('HomeLoan',true);
        Account acc=new Account(Name='TestName');
        insert acc;

         RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/getDoc'; 
        req.httpMethod  = 'GET';
        req.params.put('appIdw',app.id);
       
        RestContext.request = req;
        RestContext.response = res;
        Test.startTest();
        WS_docFetch.getDetails();
        Test.stopTest();
    }
}