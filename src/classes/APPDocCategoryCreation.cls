/*
 * Name    : ApplicationDocument Category Structure
 * Company : ET Marlabs
 * Purpose : This class will be used for Creating Application Document Category
 * Author  : Venu Gopal Namala
*/




public class APPDocCategoryCreation {
    
    public static Boolean CreateDocCat(genesis__Applications__c APP,Account Acc,Boolean AccountType){
        try{
         Map<String,ID> AppDocCatID=new Map<string,ID>();
        Map<String,ID> DocCatID=new  Map<String,ID>();
        List<genesis__Application_Document_Category__c> ParentList=new List<genesis__Application_Document_Category__c>();
        List<genesis__Application_Document_Category__c> SubParentList=new List<genesis__Application_Document_Category__c>();
        set<string> DocCateNames=DocCatNamesset();
        set<string> FolderNames=Foldernamescapture();
        set<string> SubFolderNames1=subFoldernamesCapture1();  
        set<string> SubFolderNames2=subFoldernamesCapture2();         
 List<genesis__Document_Category__c> DocCatList=[select id,name,genesis__Category_Name__c from genesis__Document_Category__c where  genesis__Category_Name__c IN:DocCateNames];
        for(genesis__Document_Category__c DCID:DocCatList){
            DocCatID.put(DCID.genesis__Category_Name__c, DCID.Id);
            
        }
            system.Debug(DocCatID);
            if(AccountType==True){
      genesis__Application_Document_Category__c ParentAppDocCat=new genesis__Application_Document_Category__c();
        ParentAppDocCat.name= Constants.ParentDOCCatgName;
                ParentAppDocCat.genesis__Application__c=APP.id;
        insert ParentAppDocCat;
        for(string s:FolderNames){
         genesis__Application_Document_Category__c ChildAppDocCat=new genesis__Application_Document_Category__c();
        ChildAppDocCat.Name=s;
        ChildAppDocCat.genesis__Application__c=APP.id;
        ChildAppDocCat.genesis__Parent_Application_Document_Category__c=ParentAppDocCat.Id;
            ParentList.add(ChildAppDocCat);
        }
        insert ParentList;
        
        for(genesis__Application_Document_Category__c subFolder1:ParentList){
            
            if(subFolder1.Name==Constants.PropVerifiReport){
                for(string Sfd1:SubFolderNames1){
                    genesis__Application_Document_Category__c SAPPDOcCat1=new genesis__Application_Document_Category__c();
                SAPPDOcCat1.Name=sfd1;
                SAPPDOcCat1.genesis__Application__c=APP.id;
                SAPPDOcCat1.genesis__Parent_Application_Document_Category__c=subFolder1.Id;
                SubParentList.add(SAPPDOcCat1);  
                }
                
            }
            
            else if(subFolder1.Name==Constants.IncomeDocuments){
                 for(string Sfd2:SubFolderNames2){
             genesis__Application_Document_Category__c SAPPDOcCat2=new genesis__Application_Document_Category__c(); 
                SAPPDOcCat2.Name=sfd2;
                SAPPDOcCat2.genesis__Application__c=APP.id;
                SAPPDOcCat2.genesis__Parent_Application_Document_Category__c=subFolder1.Id;
                SubParentList.add(SAPPDOcCat2);  
                }
            }
            
              
        }
        insert SubParentList;
                List<genesis__Application_Document_Category__c> TotalList=new List<genesis__Application_Document_Category__c>();
                TotalList.addAll(SubParentList);
                TotalList.addAll(ParentList);
                system.debug('TotalList'+TotalList);
        system.debug('SubParentList'+SubParentList);
        system.debug('DocCatID'+DocCatID);
        
        system.debug('ParentList'+ParentList);
                
                for(genesis__Application_Document_Category__c DOcList:TotalList){
                    AppDocCatID.put(DOcList.Name, DOcList.id);
                }
            }
            
             else if(AccountType==false){
                list<genesis__Application_Document_Category__c> ListAppDocCat=[select id,name,genesis__Parent_Application_Document_Category__c,genesis__Application__c from genesis__Application_Document_Category__c where genesis__Application__c=:App.id];
                for(genesis__Application_Document_Category__c DOcList:ListAppDocCat){
                    AppDocCatID.put(DOcList.Name, DOcList.id);
                }
                
            }
                list<genesis__Application_Document_Category__c> ApplicantDocCat=new List<genesis__Application_Document_Category__c>();
                for(string s:DocCatID.keySet()){
                    if(AppDocCatID.containsKey(s)){
                    genesis__Application_Document_Category__c    SubAPPDoc=new genesis__Application_Document_Category__c();
                        SubAPPDoc.Applicant__c=Acc.id;
                        SubAPPDoc.genesis__Document_Category__c=DocCatID.get(s);
                        SubAPPDoc.genesis__Parent_Application_Document_Category__c=AppDocCatID.get(s);
                        SubAPPDoc.genesis__Application__c=App.id;
                        SubAPPDoc.Name=s+'-'+Acc.Lastname;
                        ApplicantDocCat.add(SubAPPDoc);
                    }
                    
                }
             insert  ApplicantDocCat ;
            return true;
            }
           
        
        catch(exception e){
            system.debug('error message for DOC Category'+e.getLineNumber()+'Class name  '+e.getStackTraceString());
             return false;
        
    }
        
    }
    
    
    public static set<string> Foldernamescapture(){
        set<string> abc= new set<string>();
        abc.add(Constants.CustPhoto);
        abc.add(Constants.CustAddresProof);
        abc.add(Constants.PropVerifiReport);
        abc.add(Constants.FIReport);
        abc.add(Constants.IncomeDocuments);
        abc.add(Constants.PropertyDocuments);
        abc.add(Constants.LoanDocuments);
        abc.add(Constants.CreditInsurancePolicy);
        return abc;
    }
       
    
    public static set<string> subFoldernamesCapture1(){
          set<string> xyz= new set<string>();
        xyz.add(Constants.LegalOpinion);
        xyz.add(Constants.ValuReport);
        return xyz;
    }
    public static set<string> subFoldernamesCapture2(){
          set<string> PQR= new set<string>();
        PQR.add(Constants.MainIncome);
      //  PQR.add(Constants.ITR);
        PQR.add(Constants.AgriIncomeProof);
        PQR.add(Constants.ShareProfIncoProof);
        PQR.add(Constants.RentalIncomeProof);
        PQR.add(Constants.PensIncoemProof);
        PQR.add(Constants.Others);
        
        return PQR;
        
    }
    
     public static set<string> DocCatNamesset(){
          set<string> MNO= new set<string>();
        MNO.add(Constants.CustPhoto);
        MNO.add(Constants.CustAddresProof);
      //  MNO.add(Constants.PropertyDocuments);
        MNO.add(Constants.FIReport);
      //  MNO.add(Constants.LoanDocuments);
      //  MNO.add(Constants.LegalOpinion);
      //  MNO.add(Constants.ValuReport);
        MNO.add(Constants.MainIncome);
       // MNO.add(Constants.ITR);
        MNO.add(Constants.AgriIncomeProof);
        MNO.add(Constants.ShareProfIncoProof);
        MNO.add(Constants.RentalIncomeProof);
        MNO.add(Constants.PensIncoemProof);
        MNO.add(Constants.Others);

        
        return MNO;
        
    } 

}