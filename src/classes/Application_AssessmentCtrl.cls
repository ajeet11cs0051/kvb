/*
* Name		: Application_AssessmentCtrl
* Company	: ET Marlabs
* Purpose 	: Controller class for Application_AssessmentVf page.
* Author	: Raushan
*/
public class Application_AssessmentCtrl {
    
    genesis__Applications__c appObj = new genesis__Applications__c();
    List<M68_Balance_Sheet_Analysis__c> mBalancesheetList = new List<M68_Balance_Sheet_Analysis__c>();
    public Funded_Facilities fundedFacilitiesObj {get;set;}
    public Id appId	{get;set;}
    Decimal estimated_NetSale{get;set;}
    Decimal projected_NetSale{get;set;}
    double netWorking_Capital_Est{get;set;}
    double netWorking_Capital_Proj{get;set;}
    double estimated_Purchases{get;set;}
    List<String> financialTypeList = new List<String>{'Estimated','Projected','Actual'};
        //constructor 
        public Application_AssessmentCtrl(){
            try{
                appId = apexpages.currentpage().getparameters().get('id');
                appObj = [select id,genesis__Account__c,Lead_Time__c,Usance_Period__c,Renewal_Due_Date__c from genesis__Applications__c where id =:appId];
                if(appObj !=null){
                    Integer currentYear = Utility.getCurrentYear();
                    String cyString = String.valueOf(currentYear);
                    String currentFY = (currentYear-1)+'-'+Integer.valueOf(cyString.subString(cyString.length()-2,cyString.length()));
                    String prevFY = (currentYear-2)+'-'+Integer.valueOf(String.valueOf(currentYear-1).subString(String.valueOf(currentYear-1).length()-2,String.valueOf(currentYear-1).length()));
                    String estimatedFY = currentYear+'-'+Integer.valueOf(String.valueOf(currentYear+1).subString(String.valueOf(currentYear+1).length()-2,String.valueOf(currentYear+1).length()));
                    String projectedFY = (currentYear+1)+'-'+Integer.valueOf(String.valueOf(currentYear+2).subString(String.valueOf(currentYear+2).length()-2,String.valueOf(currentYear+2).length()));
                    //Integer currentYear = Utility.getCurrentYear();
            		//String fiscalYear = currentYear +'-'+ (currentYear+1);
                    //String currentFiscalYear = (currentYear-1) +'-'+ currentYear;
                    //String ficalYearNext = (currentYear+1) +'-'+ (currentYear+2);
                    List<String> fiscalyearList = new List<String>{estimatedFY,projectedFY,currentFY};
                    System.debug('Fisacal year list'+fiscalyearList);    
                    System.debug('projectedFY ::'+projectedFY);
                    mBalancesheetList = [select id,Fiscal_Year__c,Net_sales__c,Net_Working_Capital__c,Purchases__c,Account__c,Financial_type__c from M68_Balance_Sheet_Analysis__c where Account__c =:appObj.genesis__Account__c AND Fiscal_Year__c IN :fiscalyearList AND Financial_type__c IN :financialTypeList];
                    //AND Financial_type__c IN :financialTypeList
                    System.debug('M68 ::'+mBalancesheetList);
                    if(mBalancesheetList !=null){
                    for(M68_Balance_Sheet_Analysis__c m68Obj : mBalancesheetList){
                        if(m68Obj.Financial_type__c == 'Actual' && m68Obj.Fiscal_Year__c == currentFY) {
                          	If(m68Obj.Net_Working_Capital__c != null){
                                netWorking_Capital_Proj = m68Obj.Net_Working_Capital__c; 
                                netWorking_Capital_Est  = m68Obj.Net_Working_Capital__c;
                            }  
                        }
                        if(m68Obj.Financial_type__c == 'Projected' && m68Obj.Fiscal_Year__c == projectedFY){
                            If(m68Obj.Net_sales__c !=null)
                                projected_NetSale	= m68Obj.Net_sales__c;
                        }
                       if(m68Obj.Financial_type__c =='Estimated' && m68Obj.Fiscal_Year__c == estimatedFY){
                           System.debug('m68Obj.Net_sales__c'+m68Obj.Net_sales__c);
                           If(m68Obj.Net_sales__c !=null)
                                estimated_NetSale 	= m68Obj.Net_sales__c;
                           
                            If(m68Obj.Purchases__c != null)    
                            	estimated_Purchases = m68Obj.Purchases__c;
                        }
                    }
                    
                    
                }
                 System.debug(estimated_NetSale); 
                fundedFacilitiesObj= new Funded_Facilities(estimated_NetSale,netWorking_Capital_Est,estimated_Purchases,projected_NetSale,netWorking_Capital_Proj,appObj.Lead_Time__c,appObj.Usance_Period__c,appObj.Renewal_Due_Date__c);
                System.debug('fundedFacilitiesObj@@@@'+fundedFacilitiesObj);
              } 
            }catch(Exception ex){
                System.debug('Exception'+ex.getStackTraceString());
                System.debug('Line Number'+ex.getLineNumber());
            }
        }
    
    //Method to calculate Facilities by Assessment. 
    public void calculateFLC_ILCAssessment(){
        double totalsum=0;
        try{
        fundedFacilitiesObj.facilityCategory ='';
        for(Facility__c facilityObj :[select id,Existing_Limit__c,CL_Product__r.Facility_Category__c,Application__r.Name from Facility__c where Application__r.id =:appId]){
            
            If(facilityObj !=null){
                fundedFacilitiesObj.facilityCategory += facilityObj.CL_Product__r.Facility_Category__c;
                If(facilityObj.CL_Product__r.Facility_Category__c =='ILC' || facilityObj.CL_Product__r.Facility_Category__c =='FLC'){
                    fundedFacilitiesObj.calculateEstimatedNetSaleILC();
                }
                
                If(facilityObj.CL_Product__r.Facility_Category__c =='FLC' || facilityObj.CL_Product__r.Facility_Category__c =='PCFC' || facilityObj.CL_Product__r.Facility_Category__c =='FBN' || facilityObj.CL_Product__r.Facility_Category__c =='FBP' || facilityObj.CL_Product__r.Facility_Category__c =='EBD'){
                    If(facilityObj !=null){
                        If(facilityObj.Existing_Limit__c !=null){
                            totalsum += facilityObj.Existing_Limit__c;
                            fundedFacilitiesObj.totalLimitSum 	= totalsum;
                            fundedFacilitiesObj.cELRequirement 	= (totalsum*12.5)/100; 
                        }
                    }
                }
                
                If(facilityObj.CL_Product__r.Facility_Category__c =='BG'){
                    If(facilityObj.Existing_Limit__c !=null){
                        fundedFacilitiesObj.existingBGlimit = facilityObj.Existing_Limit__c;
                        fundedFacilitiesObj.bglimit 		= facilityObj.Existing_Limit__c; 
                    }
                }	
            } 
        }
       }catch(Exception e){
           System.debug('Exception'+ e.getMessage());
           System.debug('Line Number'+ e.getLineNumber());
            
      }
  }      
    
    
    //Wrapper Class.
    public class Funded_Facilities{
        public Decimal estimated_sale				{get;set;}
        public Decimal projected_sale				{get;set;}		
        public Decimal working_Requirement			{get;set;}
        public Decimal borrowers_contribution		{get;set;}
        public Decimal nwcPrevYearBalanceEstimated	{get;set;}
        public Decimal nwcPrevYearBalanceProjected  {get;set;}
        public Decimal item							{get;set;}
        public Decimal item2						{get;set;}
        public Decimal smalleritem					{get;set;}
        public double estimatedTotalPurchases		{get;set;}
        public Decimal leadTime						{get;set;}
        public Decimal usancePeriod					{get;set;}
        public double localPurchases				{get;set;}
        public Decimal totalNoofdays				{get;set;}
        public double requirementILC				{get;set;}
        public String facilityCategory				{get;set;}	
        public double totalLimitSum					{get;set;}
        public double cELRequirement				{get;set;}
        public double existingBGlimit				{get;set;}
        public double bglimit						{get;set;}
        public Date renewalDate						{get;set;}
        public Integer month						{get;set;}
        public Decimal sale							{get;set;}
        public Decimal NWCperpreviousyear			{get;set;}
        
        //Constructor
        public Funded_Facilities(Decimal estimated_sale,Decimal nwcPrevYearBalanceEstimated,double estimatedTotalPurchases,Decimal projected_sale,Decimal nwcPrevYearBalanceProjected,Decimal leadTime,Decimal usancePeriod,Date renewalDate){
            this.estimated_sale = estimated_sale.setScale(1);
            this.nwcPrevYearBalanceEstimated = nwcPrevYearBalanceEstimated;
            this.estimatedTotalPurchases = estimatedTotalPurchases;
            this.projected_sale = projected_sale.setScale(1);
            this.nwcPrevYearBalanceProjected = nwcPrevYearBalanceProjected;
            this.leadTime = leadTime;
            this.usancePeriod = usancePeriod;
            this.renewalDate = renewalDate;
            month =renewalDate.month();
            try{
               	If(month>=4 && month<=9){
                    If(estimated_sale !=null){
                        sale = estimated_sale.setScale(1);
                        NWCperpreviousyear = nwcPrevYearBalanceEstimated.setScale(1);
                        working_Requirement = ((sale*25)/100);
                        borrowers_contribution = ((sale*5)/100);
                        item = working_Requirement-borrowers_contribution;
                        If(NWCperpreviousyear !=null) 
                        	item2 = working_Requirement-NWCperpreviousyear;
                        if(item < item2)
                            smalleritem = item.setScale(1);
                        else
                            smalleritem = item2.setScale(1);
                    }
                    
                }else{
                    If(projected_sale !=null){
                        sale= projected_sale.setScale(1);
                        NWCperpreviousyear = nwcPrevYearBalanceProjected;
                        working_Requirement = ((sale*25)/100);
                        borrowers_contribution = ((sale*5)/100);
                        item = working_Requirement-borrowers_contribution;
                        If(NWCperpreviousyear !=null) 
                            item2 = working_Requirement-NWCperpreviousyear;
                        if(item < item2)
                            smalleritem = item.setScale(1);
                        else
                            smalleritem = item2.setScale(1);  
                    }
                
            	} 
            }catch(Exception e){
               System.debug('Exception'+ e.getMessage()); 
               System.debug('Line Number'+ e.getLineNumber()); 
            }
              
        }
        
        // Calculate Line Assessment by facility Category.
        public void calculateEstimatedNetSaleILC(){
            if(estimatedTotalPurchases !=null && usancePeriod !=null && leadTime !=null) {
                localPurchases =((estimatedTotalPurchases*90)/100);
                totalNoofdays = usancePeriod+leadTime;
                requirementILC = ((localPurchases*totalNoofdays)/360);
                
          }
            
        } 
        
    }   
    
}