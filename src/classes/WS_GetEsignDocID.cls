/*
* Name 	    : WS_EsignDocID
* Compnay 	: ET Marlabs
* Purpose	: Get Esign Document ID.
* Author 	: Raushan
*/ 
@RestResource(urlMapping='/getEsignDocID')
global with sharing class WS_GetEsignDocID {

    //Response Structure.
     global class Response extends WS_Response{
     	public eSignResponse eSignRes;
     	public Response(){
     		eSignRes = new eSignResponse();
     	}
    }
    //Wrapper Class.
    public class eSignResponse{
        public String LOS_PARTY_TYPE_ID;
        public string docID;
        public string LOS_APPID;
    }
    //Wrapper Class.
    public class ReqWrapObj {
		public String LOS_APPID;
		public String ESIGN_ACCEPTED;
		public String LOS_PARTY_TYPE_ID;
	}
    
    @HttpPost
    global static Response getEsignDocId(){
        RestRequest req      = Restcontext.Request;
        Response res         = new Response();    
        
        if(req == null || req.requestBody == null){
            res.status         	= Constants.WS_ERROR_STATUS;
            res.errorMessage   	= Constants.WS_REQ_BODY_IS_NULL;
            res.statusCode 	   	= Constants.WS_ERROR_CODE;
            //return res;            
        }else{
            String jsonData 	= req.requestBody.toString();        
            ReqWrapObj reqObj 	= (ReqWrapObj)Json.deserialize(jsonData, ReqWrapObj.class);
            system.debug('reqObj::'+reqObj);
            //res = SME_Digio_Service.doESignProcess(reqObj.LOS_APPID);
            res = SME_Digio_Service.doESignUpdated(reqObj.LOS_APPID,reqObj.LOS_PARTY_TYPE_ID);
            System.debug('resObj::'+res);
        }
        
         return res;
    }        
}