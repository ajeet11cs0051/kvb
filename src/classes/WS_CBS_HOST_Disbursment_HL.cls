/*
* Name    : WS_CBS_HOST_Disbursment_HL
* Company : ET Marlabs
* Purpose : This class Will be used to CBS (HOST_Disbursment) 
* Author  : Subas
*/

public class WS_CBS_HOST_Disbursment_HL {
    public class CBS_HOST_Request{
        public String exe_tenantId;	//cuecent_tenant
        public String owner_tenantId;	//cuecent_tenant
        public cls_inputVariables inputVariables;        
    }
    public class cls_inputVariables {
        public cls_in_msg in_msg;
    }
    public class cls_in_msg {
        public String AccountNo;	//1152753000002996
        public String DisbursementAmount;	//500000
        public String DisbursementMode;	//2
        public String ScheduleType;	//3001
        public String AuthId;	//SYSTEM01
        public String WaiveDednInd;	//Y
        public String ExtUniqueRefId;	//1
        public String TransactionBranch;	//1152
        public String AmountInstal;	//0
        public String EPIPrincRepayInstal;	//
        public String EPIPrincFreq;	//
        public String EPIInstalAmt;	//
        public String EPIDatRpayPrinc;	//
        public String EPITermYr;	//
        public String EPITermMonth;	//
        public String IPIPrincRepayInstal;	//
        public String IPIPrincFreq;	//
        public String IPIInstalAmt;	//
        public String IPIDatRpayPrinc;	//
        public String IPIIntRepayInstal;	//
        public String IPIIntFreq;	//
        public String IPIDatRpayInt;	//
        public String IPITermYr;	//
        public String IPITermMonth;	//
        public String IPICompfreq;	//
        public String IOIIntRepayInstal;	//
        public String IOIIntFreq;	//
        public String IOIDatRpayInt;	//
        public String IOITermYr;	//
        public String IOITermMonth;	//12
        public String FPIPrincRepayInstal;	//
        public String FPIPrincFreq;	//
        public String FPIInstalAmt;	//
        public String FPIDatRepayPrinc;	//
        public String FPIIntRepayInstal;	//
        public String FPIIntFreq;	//
        public String FPITermYr;	//
        public String FPITermMonth;	//
        public String MORTermYr;	//
        public String MORTermMonth;	//
    }
    //Response structure
    public class CBS_HOST_Response{
        public String bpms_error_code;	//00
        public cls_out_msg out_msg;
        public String bpms_error_msg;	//Success
    }
    public class cls_out_msg {
        public String Response;	//99
        public String SuccessMessage;	//
        public String ReasonMessage;	// Internal OLTP Error. 
        public String ErrorCode;	//4814
        public String ErrorMessage;	//Disbursement amount above sanctioned amount
        public String ReasonCode;	//29
    }
    @future (Callout=true)
    public static void Host_request(String appID){
        genesis__Applications__c application = new genesis__Applications__c();
        application = queryService.getApp(appID);
        WS_CBS_HOST_Disbursment_HL.CBS_HOST_Request cbs = new WS_CBS_HOST_Disbursment_HL.CBS_HOST_Request();
        cbs.exe_tenantId = 'cuecent_tenant';
        cbs.owner_tenantId = 'cuecent_tenant';
        cbs.inputVariables = new WS_CBS_HOST_Disbursment_HL.cls_inputVariables();
        WS_CBS_HOST_Disbursment_HL.cls_inputVariables cbsInput = new WS_CBS_HOST_Disbursment_HL.cls_inputVariables();
        cbsInput.in_msg = new WS_CBS_HOST_Disbursment_HL.cls_in_msg();
        cbs.inputVariables = cbsInput;
        cbsInput.in_msg.AccountNo 			= application.Loan_Account_Number__c ;
        cbsInput.in_msg.DisbursementAmount  = String.ValueOf(application.Disbursement_Amount__c);
        if(application.Record_Type_Name__c == Constants.LAPLOAN && application.Record_Type_Name__c == Constants.PERSONALLOAN){
            cbsInput.in_msg.DisbursementMode    = '2';
        }else{
            cbsInput.in_msg.DisbursementMode    = '5'; //changed from 2
        }        
        cbsInput.in_msg.ScheduleType        = application.Holiday_Period__c != null ? application.Holiday_Period__c != 0 ? '3002' :'3001' : '3001';
        cbsInput.in_msg.AuthId              = 'SYSTEM01';
        cbsInput.in_msg.WaiveDednInd        = 'Y';
        cbsInput.in_msg.ExtUniqueRefId      = application.Loan_Account_Number__c +'1';// to be decided
        cbsInput.in_msg.TransactionBranch   = application.Branch_Code__c;
        cbsInput.in_msg.AmountInstal        = '0';
        cbsInput.in_msg.EPIPrincRepayInstal = '';
        cbsInput.in_msg.EPIPrincFreq        = '';
        cbsInput.in_msg.EPIInstalAmt        = '';
        cbsInput.in_msg.EPIDatRpayPrinc     = '';
        cbsInput.in_msg.EPITermYr           = '';
        cbsInput.in_msg.EPITermMonth        = '';
        cbsInput.in_msg.IPIPrincRepayInstal = '';
        cbsInput.in_msg.IPIPrincFreq        = '';
        cbsInput.in_msg.IPIInstalAmt        = '';
        cbsInput.in_msg.IPIDatRpayPrinc     = '';
        cbsInput.in_msg.IPIIntRepayInstal   = '';
        cbsInput.in_msg.IPIIntFreq          = '';
        cbsInput.in_msg.IPIDatRpayInt       = '';
        cbsInput.in_msg.IPITermYr           = '';
        cbsInput.in_msg.IPITermMonth        = '';
        cbsInput.in_msg.IPICompfreq         = '';
        cbsInput.in_msg.IOIIntRepayInstal   = '';
        cbsInput.in_msg.IOIIntFreq          = '';
        cbsInput.in_msg.IOIDatRpayInt       = '';
        cbsInput.in_msg.IOITermYr           = '';
        cbsInput.in_msg.IOITermMonth        = application.Holiday_Period__c != null ? String.ValueOf(application.Holiday_Period__c) : '';
        if(application.genesis__Disbursement_Date__c != null){
            cbsInput.in_msg.FPIPrincRepayInstal = String.ValueOf(application.FPIPrincRepayInstal__c.format()).replace('/','-');
        }else{
            cbsInput.in_msg.FPIPrincRepayInstal = '';
        }       
        cbsInput.in_msg.FPIPrincFreq        = '';
        cbsInput.in_msg.FPIInstalAmt        = '';
        cbsInput.in_msg.FPIDatRepayPrinc    = '';
        if(application.genesis__Disbursement_Date__c != null){
            cbsInput.in_msg.FPIIntRepayInstal   = String.ValueOf(application.FPIIntRepayInstal__c.format()).replace('/','-');
        }else{
            cbsInput.in_msg.FPIIntRepayInstal   = '';
        }        
        cbsInput.in_msg.FPIIntFreq          = '';
        cbsInput.in_msg.FPITermYr           = '';
        cbsInput.in_msg.FPITermMonth        = '';
        cbsInput.in_msg.MORTermYr           = '';
        cbsInput.in_msg.MORTermMonth        = '';
        callHostDisb(JSON.serialize(cbs),AppId);
    }
    public static void callHostDisb(String HostData, String AppId){
        system.debug('@@@@'+HostData);
        KVB_Endpoint_URLs__c kvb = KVB_Endpoint_URLs__c.getValues('HOST_Disb_HL');
        genesis__Applications__c AppIDPL = queryService.getApp(appId);
        Map<String,String> headerMap                            = new Map<String,String>();
        headerMap.put('Content-Type','application/json');
        HTTPResponse response                                   = new HTTPResponse();
        String endPoint                                         = kvb.Endpoint_URL__c;
        response = HttpUtility.sendHTTPRequest(endPoint, 'POST', null,HostData,headerMap,label.CA_CERTIFICATE);   
        system.debug('#####'+response);
        system.debug('$$$$'+response.getBody());
        If(response.getStatusCode() == 200 || response.getStatusCode() == 201){
            String jsonString = response.getBody();            
            WS_CBS_HOST_Disbursment_HL.CBS_HOST_Response res = (WS_CBS_HOST_Disbursment_HL.CBS_HOST_Response)Json.deserialize(jsonString,WS_CBS_HOST_Disbursment_HL.CBS_HOST_Response.class);
            System.debug(res);
            System.debug(res.out_msg.ErrorMessage);
            if(res.out_msg.ErrorMessage == null || res.out_msg.ErrorMessage ==''){
                 
                //WS_CBS_MIS_Update_HL.Mis_Update(AppId);
                WS_CBS_Loan_Disbursment_HL.callDisb(AppId);
                            
            }else{
                CBS_API_Log__c log = new CBS_API_Log__c();    
                log.Application__c = AppId;
                log.API_Name__c = 'Host_Disb';
                log.Status__c = 'Failure';
                log.Success_Error_Message__c = res.out_msg.ErrorMessage;
                log.Sequence_No__c = '9';  
                insert log;
            }
        }
    }
}