/*
* Name      : CommercialPRErrorHandler
* Company   : ET Marlabs
* Purpose   : Handler class of CommercialPRErrorService . 
* Author    : Raushan
*/
public class CommercialPRErrorHandler {
    
    public static void getCommercialPRError(CommercialPRErrorService commPRErrorObj){
        
        List<TU_Error__c> listTuError = new List<TU_Error__c>();
        
        //List of Out Msg
        List<CommercialPRErrorService.Out_msg>  listOutMsg  =   commPRErrorObj.out_msg;
        List<genesis__Applications__c> listAppUpdate    = new List<genesis__Applications__c>();
        
        If(listOutMsg !=null){
            
            Map<String,String> mapCustID    =   new Map<String,String>();
            for(CommercialPRErrorService.Out_msg  commPRErrObj  : listOutMsg){
                If(commPRErrObj !=null){
                    TU_Error__c TuErrObj = new TU_Error__c();
                    If(!Utility.ISStringBlankorNull(commPRErrObj.Req_id))
                        TuErrObj.Request_Id__c =  commPRErrObj.Req_id;
                    If(!Utility.ISStringBlankorNull(commPRErrObj.ID))
                        TuErrObj.Error_Id__c =  commPRErrObj.ID;
                    If(!Utility.ISStringBlankorNull(commPRErrObj.Error))
                        TuErrObj.Error__c =  commPRErrObj.Error;
                    If(!Utility.ISStringBlankorNull(commPRErrObj.Error))
                        TuErrObj.Error__c =  commPRErrObj.Error;
                    If(!Utility.ISStringBlankorNull(commPRErrObj.Date_Processed))
                        TuErrObj.Date_Processed__c =  commPRErrObj.Date_Processed;
                    If(!Utility.ISStringBlankorNull(commPRErrObj.Member_Code))
                        TuErrObj.Member_Code__c =  commPRErrObj.Member_Code;
                    If(!Utility.ISStringBlankorNull(commPRErrObj.Refrence_Number)){
                        TuErrObj.Reference_Number__c =  commPRErrObj.Refrence_Number;
                        mapCustID.put(commPRErrObj.Refrence_Number, commPRErrObj.Response);
                        TuErrObj.Account__r = new Account(CBS_Customer_ID__c = commPRErrObj.Refrence_Number); 
                    }
                    If(!Utility.ISStringBlankorNull(commPRErrObj.Error_Description))
                        TuErrObj.Error_Description__c =  commPRErrObj.Error_Description;
                    
                    If(!Utility.ISStringBlankorNull(commPRErrObj.Ticket_id))
                        TuErrObj.Ticket_Id__c =  commPRErrObj.Ticket_id;
                    
                    If(!Utility.ISStringBlankorNull(commPRErrObj.Response))
                        TuErrObj.Response__c =  commPRErrObj.Response;     
                    
                    listTuError.add(TuErrObj);
                }    
            }
            List<genesis__Applications__c> listApp = [select id,CustomerID__c,CommercialPR_Stage__c from genesis__Applications__c where CustomerID__c IN : mapCustID.keySet() AND Active__c = true];
            If(listApp !=null){
                for(genesis__Applications__c    appObj  :  listApp){
                    genesis__Applications__c appObject = new genesis__Applications__c();
                    appObject.id    =   appObj.id;
                    If(mapCustID.get(appObj.CustomerID__c)=='No-Hit')   {
                        appObject.CommercialPR_Stage__c =   'Completed';        
                    }else{
                        appObject.CommercialPR_Stage__c =   'Error'; 
                    }
                    listAppUpdate.add(appObject); 
                }
                
            }
        }
        
        if(listTuError.size() >0)
            insert listTuError;
        if(listAppUpdate.size() > 0){
            ApplicationTriggerHandler.IsFirstRun=false;
            update listAppUpdate; 
        }
           
    }     
    
}