@isTest
public class Test_EsignApplicantsMapping {
@isTest
    public static void method1(){
        genesis__Applications__c  app=TestUtility.intialSetUp('TestName',true);
        Digio_Document_ID__c docobj=new Digio_Document_ID__c();
        docobj.Application__c=app.id;
        docobj.Borrower__c=true;
        docobj.Document_ID__c='udiu778';
        docobj.Name='C11';
        insert docobj;
        
        genesis__Application_Parties__c par=new genesis__Application_Parties__c();
        par.genesis__Application__c=app.id;
        par.genesis__Party_Type__c='Co-Borrower';
        insert par;
        EsignApplicantsMapping.CreateApplicantSign(docobj,app.id);
    }
}