/*
* Name      : AccountEOD_Helper
* Company   : ET Marlabs
* Purpose   : Helper class for AccountEODResponse. 
* Author    : Dushyant
*/

public class AccountEOD_Helper {
    
    public static Map<String,genesis__Applications__c> getAppDetails(List<AccountEODResponse.cls_ACCOUNT> listAccount){
        Map<String,genesis__Applications__c> custIdAppMap = new Map<String,genesis__Applications__c>();

        try{

            Account acc = new Account();

            List<String> custIdList = new List<String>();

            List<genesis__Applications__c> activeApplicationList = new List<genesis__Applications__c>();
            
            for(AccountEODResponse.cls_ACCOUNT  accountWrppObj  :   listAccount){
                custIdList.add(accountWrppObj.customer_id);
                for(AccountEODResponse.cls_PARTIES partyObj : accountWrppObj.PARTIES){
                    custIdList.add(partyObj.customer_id);
                }
            }
            //System.debug('custIdList::'+custIdList);
            for(genesis__Applications__c app : [SELECT Id,Application_Stage__c,CustomerID__c,Renewal_Due_Date__c,External_Id__c
                                                FROM genesis__Applications__c
                                                WHERE CustomerID__c IN : custIdList AND Active__c = true AND RecordType.DeveloperName =: Constants.SME_APP_RECORD_TYPE]){
                custIdAppMap.put(app.CustomerID__c,app);
            }

            RETURN custIdAppMap;
        }
        catch(Exception e){
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
            RETURN custIdAppMap;
        }
    }
    //Owner assignment of successfully inserd customers
    /*public static void ownershipAssignment(List<WS_AccountEODDeltaChange.AccSuccessResponse> successList){
        System.debug('successList--->'+successList.size());
        try{
            String condition = '';//used in where condition of update
            String role ='Branch Manager';
            Set<String> custIdSet = new Set<String>();
           
            
            
            for(WS_AccountEODDeltaChange.AccSuccessResponse res : successList){
                custIdSet.add(res.cust_Id);
            }
            System.debug('custIdSet--->'+custIdSet);
            ownershipAssignment(custIdSet);
            checkLimitValue(custIdSet);
        }
        catch(Exception e){
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
        }
    }*/
    public static void ownershipAssignment(Set<String> branchCodeSet){
        try{
            List<User> userList = new List<User>();
            String role ='Branch Manager';
            userList = [SELECT Id FROM User WHERE Office_Code__c IN : branchCodeSet AND Role_Name__c =:role AND isActive = true];
            for(User ur : userList){
                ur.Assignment_Required__c = true;
            }
            UPDATE userList;
        }
        catch(Exception e){
            System.debug(e.getMessage());
            System.debug(e.getStackTraceString());
        }
    }
    /*@future
    static void ownershipAssignment(Set<String> custIdSet){
        String role ='Branch Manager';
        List<genesis__Applications__c> appList = new List<genesis__Applications__c>();
        Set<String> branchCodeSet = new Set<String>();
        List<User> userList = new List<User>();
        
        appList = [SELECT Id,Branch_Code__c FROM genesis__Applications__c WHERE CustomerID__c IN: custIdSet AND Active__c = true];
        for(genesis__Applications__c app: appList){
            branchCodeSet.add(app.Branch_Code__c);
        }
        //System.debug('branchCodeSet:::'+branchCodeSet);
        userList = [SELECT Id,Office_Code__c FROM User WHERE Role_Name__c =:role AND Office_Code__c IN: branchCodeSet];
        //System.debug('userList:::'+userList);
        if(!userList.isEMpty()){
            Branch_Ownership_Assignment obj = new Branch_Ownership_Assignment();
            Database.BatchableContext BC;
            obj.execute(BC,userList);
        }
    }*/
    @future
    static void checkLimitValue(Set<String> custIdSet){
        List<genesis__Applications__c> appList = new List<genesis__Applications__c>();
        appList = [SELECT Id,Application_Stage__c,(SELECT Id,Existing_Limit__c FROM Facilities__r) FROM genesis__Applications__c WHERE RecordType.DeveloperName = 'SME_Renewal' AND Active__c = true AND CustomerID__c IN:custIdSet]; 
         List<genesis__Applications__c> appToUpdate = new List<genesis__Applications__c>();
        for(genesis__Applications__c app: appList){
            if(!app.Facilities__r.isEmpty()){
                Decimal totalLimit = 0;
                for(Facility__c fac : app.Facilities__r){
                    if(fac.Existing_Limit__c != null) totalLimit += fac.Existing_Limit__c;
                }
                if(totalLimit > 10000000){
                    appToUpdate.add(new genesis__Applications__c(Id = app.Id,Application_Stage__c = 'Greater than 1Cr.'));
                }
            }
        }
        if(!appToUpdate.isEmpty()){
            ApplicationTriggerHandler.IsFirstRun = false;
            UPDATE appToUpdate;
        }
    }
}