/*
* Name    : GmraCal
* Company : ET Marlabs
* Purpose : This class is used to calculate GMRA amount
* Author  : Subas
*/
public class GmraCal {
    
    Public static Decimal totalNetIncome = 0.00; //Total Net Income of Applicant
    Public static Decimal netIncome = 0.00; //Net Income of Applicant
    Public static Decimal fractionAmt = 0.00; //Fraction on Net Amount
    Public static Decimal LoanFracAmt = 0.00; //loan fraction amount
    Public static Decimal gmraIndAmt =0.00; //Amount for Co-borrower
    Public static Decimal gmraIndAmt2 =0.00; //Amount for Primary Applicant
    Public static Decimal totalGmra = 0.00; //sum of Primary and Co borrower
    Public static Integer pApplicantAge = 0; //Primary Applicant Age
    Public static Integer coApplicantAge = 0; //Co borrower Age
    Public static Decimal gmra1 = 0.00;
    Public static Decimal gmra2 = 0.00;
    Public static Decimal gmra3 = 0.00;
    Public static Decimal gmra4 = 0.00;
    Public static Decimal gmra5 = 0.00;
    //Public static List<GMRA_Master__c> gmrList = GMRA_Master__c.getAll().values();
    
    public static List<GMRA__c> calGMRA(String AppId){
        try{
           
            genesis__Applications__c application = [Select Id,genesis__Account__r.Gender__pc,genesis__Account__r.age__c,genesis__Interest_Rate__c,genesis__Term__c,genesis__Loan_Amount__c,GMRA_Amount__c,genesis__Account__r.Net_Monthly_Income__c,
                                                    (SELECT ID,genesis__Application__r.genesis__Account__r.age__c,genesis__Party_Account_Name__r.age__c,
                                                    genesis__Party_Account_Name__r.Net_Monthly_Income__c,genesis__Application__r.genesis__Account__r.Net_Monthly_Income__c,                                                                                                                      
                                                    genesis__Application__r.genesis__Account__r.Gender__pc,genesis__Party_Account_Name__r.Gender__pc  FROM genesis__Application_Parties__r Where genesis__Party_Type__c !=:'Gurantor') 
                                                    From genesis__Applications__c Where Id =:AppId];                                                                                                                      
            
            List<GMRA_Master__c> gmrList = new List<GMRA_Master__c>();
            gmrList = [Select Id,Age__c,Bank__c,Max_Interest__c,Max_Term__c,Min_Interest__c,Min_Term__c,Value__c 
                       From GMRA_Master__c Where (Max_Term__c >=: application.genesis__Term__c AND Min_Term__c <=: application.genesis__Term__c) AND(Max_Interest__c >=: application.genesis__Interest_Rate__c AND Min_Interest__c <=: application.genesis__Interest_Rate__c)];
            
            system.debug('^^^^'+'Term'+application.genesis__Term__c+'Interest'+application.genesis__Interest_Rate__c);
            system.debug('@@@@@@@@'+gmrList.size());

            gmra1 = gmraOp1(application,gmrList);
            gmra2 = gmraOp2(application,gmrList);
            gmra3 = gmraOp3(application,gmrList);
            gmra4 = gmraOp4(application,gmrList);
            gmra5 = gmraOp5(application,gmrList);
            
            List<GMRA__c> delgmraList = new List<GMRA__c>();
            delgmraList = [Select Id From GMRA__c Where Application__c =:application.ID];
            if(delgmraList.size()>0){
                Delete delgmraList;
            }
            List<GMRA__c> gmraList = new List<GMRA__c>();
            GMRA__c gm = new GMRA__c();
            gm.Name = 'SBI: 100% Prim Appl.';
            gm.Application__c =application.ID;
            gm.Amount__c = gmra1;
            gmraList.add(gm);
            
            GMRA__c gm2 = new GMRA__c();
            gm2.Name = 'SBI: Each Applicant - 100%';
            gm2.Application__c =application.ID;
            gm2.Amount__c = gmra2;
            gmraList.add(gm2);
            
            GMRA__c gm3 = new GMRA__c();
            gm3.Name = 'SBI: As per Income contr.%';
            gm3.Application__c =application.ID;
            gm3.Amount__c = gmra3;
            gmraList.add(gm3);
            
            GMRA__c gm4 = new GMRA__c();
            gm4.Name = 'BSLI: 100% Prim Appl.';
            gm4.Application__c =application.ID;
            gm4.Amount__c = gmra4;
            gmraList.add(gm4);
            
            GMRA__c gm5 = new GMRA__c();
            gm5.Name = 'BSLI: Each applicant - 100%.';
            gm5.Application__c =application.ID;
            gm5.Amount__c = gmra5;
            gmraList.add(gm5);
            
            Insert gmraList;
            return gmraList;
            //} 
        }catch(exception e){
            system.debug('error'+e.getLineNumber()+'tracetag'+e.getMessage()+'sasasss'+e.getStackTraceString());
            return null;
        } 
    }
    public static Decimal gmraOp1(genesis__Applications__c application,List<GMRA_Master__c>gmrList){
        Decimal Loan_Amount = application.genesis__Loan_Amount__c - (application.GMRA_Amount__c != null ? application.GMRA_Amount__c : 0);	
        gmraIndAmt2 = 0.00;
            LoanFracAmt = Loan_Amount;            
            for(GMRA_Master__c gr : gmrList){
                if(gr.Age__c == application.genesis__Account__r.age__c && (gr.Max_Term__c>=application.genesis__Term__c && gr.Min_Term__c<=application.genesis__Term__c) && (gr.Max_Interest__c>=application.genesis__Interest_Rate__c && gr.Min_Interest__c<=application.genesis__Interest_Rate__c) && gr.Bank__c =='SBI'){
                    system.debug('%%LF%%%'+LoanFracAmt);
                    system.debug('%%%VL%%'+gr.Value__c);
                    system.debug('%%%AG%%'+application.genesis__Account__r.age__c);
                    gmraIndAmt2 = ((LoanFracAmt * gr.Value__c)/1000) * 1.18;
                }                    
            }
        
        system.debug('LoanFracAmt'+LoanFracAmt);
        system.debug('gmraIndAmt2'+gmraIndAmt2);
        //application.GMRA_Amount__c = gmraIndAmt2;        
        return gmraIndAmt2;
    }
    public static Decimal gmraOp2(genesis__Applications__c application,List<GMRA_Master__c>gmrList){
        Decimal Loan_Amount = application.genesis__Loan_Amount__c - (application.GMRA_Amount__c != null ? application.GMRA_Amount__c : 0);
        gmraIndAmt = 0.00;
        gmraIndAmt2 = 0.00;
        for(genesis__Application_Parties__c pt : application.genesis__Application_Parties__r){
            LoanFracAmt = Loan_Amount;
            for(GMRA_Master__c gr : gmrList){
                if(gr.Age__c ==pt.genesis__Party_Account_Name__r.age__c && (gr.Max_Term__c>=application.genesis__Term__c && gr.Min_Term__c<=application.genesis__Term__c) && (gr.Max_Interest__c>=application.genesis__Interest_Rate__c && gr.Min_Interest__c<=application.genesis__Interest_Rate__c) && gr.Bank__c =='SBI'){
                    gmraIndAmt = gmraIndAmt + (LoanFracAmt * gr.Value__c)/1000;
                }                    
            }
        }        
            LoanFracAmt = Loan_Amount;
            for(GMRA_Master__c gr : gmrList){
                if(gr.Age__c ==application.genesis__Account__r.age__c && (gr.Max_Term__c>=application.genesis__Term__c && gr.Min_Term__c<=application.genesis__Term__c) && (gr.Max_Interest__c>=application.genesis__Interest_Rate__c && gr.Min_Interest__c<=application.genesis__Interest_Rate__c) && gr.Bank__c =='SBI'){
                    gmraIndAmt2 = (LoanFracAmt * gr.Value__c)/1000;
                }                    
            }
        if(gmraIndAmt == 0){
            totalGmra = (gmraIndAmt + gmraIndAmt2) * 1.18;
        }else{
            totalGmra = ((gmraIndAmt + gmraIndAmt2) * 0.95) * 1.18;
        }
           
        //application.GMRA_Amount__c = totalGmra;
        
        return totalGmra;
    }
    public static Decimal gmraOp3(genesis__Applications__c application,List<GMRA_Master__c>gmrList){
        Decimal Loan_Amount = application.genesis__Loan_Amount__c - (application.GMRA_Amount__c != null ? application.GMRA_Amount__c : 0);
        gmraIndAmt = 0.00;
        gmraIndAmt2 = 0.00;
        for(genesis__Application_Parties__c pt : application.genesis__Application_Parties__r){
            if(pt.genesis__Party_Account_Name__r.Net_Monthly_Income__c <> null){
                totalNetIncome = totalNetIncome + pt.genesis__Party_Account_Name__r.Net_Monthly_Income__c;  
            }
        }
        if(application.genesis__Account__r.Net_Monthly_Income__c<>null){
            totalNetIncome = totalNetIncome + application.genesis__Account__r.Net_Monthly_Income__c;
        }
        gmraIndAmt = 0.00;
        gmraIndAmt2 = 0.00;
        for(genesis__Application_Parties__c pt : application.genesis__Application_Parties__r){
            netIncome = pt.genesis__Party_Account_Name__r.Net_Monthly_Income__c;
            if(totalNetIncome<>0 && totalNetIncome <>null){
                fractionAmt = netIncome/totalNetIncome;
                system.debug('fractionAmt'+fractionAmt);
                system.debug('$$$'+Loan_Amount);
                if(Loan_Amount<>null){
                    LoanFracAmt = Loan_Amount * fractionAmt;
                    system.debug('**Loan_Fra_Co**'+LoanFracAmt);                    
                    for(GMRA_Master__c gr : gmrList){
                        if(gr.Age__c ==pt.genesis__Party_Account_Name__r.age__c && (gr.Max_Term__c>=application.genesis__Term__c && gr.Min_Term__c<=application.genesis__Term__c) && (gr.Max_Interest__c>=application.genesis__Interest_Rate__c && gr.Min_Interest__c<=application.genesis__Interest_Rate__c) && gr.Bank__c =='SBI'){                            
                            gmraIndAmt = gmraIndAmt + (LoanFracAmt * gr.Value__c * 1.18)/1000;
                        }                    
                    }
                }
            }
        }
        
            netIncome = application.genesis__Account__r.Net_Monthly_Income__c;
            if(netIncome<>0 && netIncome<>null){
                fractionAmt = netIncome/totalNetIncome;
                if(Loan_Amount<>null){
                    LoanFracAmt = Loan_Amount * fractionAmt;
                    for(GMRA_Master__c gr : gmrList){
                        if(gr.Age__c == application.genesis__Account__r.age__c && (gr.Max_Term__c>=application.genesis__Term__c && gr.Min_Term__c<=application.genesis__Term__c) && (gr.Max_Interest__c>=application.genesis__Interest_Rate__c && gr.Min_Interest__c<=application.genesis__Interest_Rate__c) && gr.Bank__c =='SBI'){
                            gmraIndAmt2 = (LoanFracAmt * gr.Value__c * 1.18)/1000;
                        }                    
                    }
                }
            }        
        totalGmra = gmraIndAmt + gmraIndAmt2;   
        //application.GMRA_Amount__c = totalGmra;
        return totalGmra;
    }
    public static Decimal gmraOp4(genesis__Applications__c application,List<GMRA_Master__c>gmrList){
        Decimal Loan_Amount = application.genesis__Loan_Amount__c - (application.GMRA_Amount__c != null ? application.GMRA_Amount__c : 0);
        gmraIndAmt = 0.00;
        gmraIndAmt2 = 0.00;        
            pApplicantAge = Integer.ValueOf(application.genesis__Account__r.age__c);
            if(application.genesis__Account__r.Gender__pc == 'Female' && application.genesis__Account__r.age__c >18){
                pApplicantAge = Integer.ValueOf(application.genesis__Account__r.age__c) - 3;
            }
            if(pApplicantAge <18){
                pApplicantAge = 18;
            }
            LoanFracAmt = Loan_Amount;
            for(GMRA_Master__c gr : gmrList){
                if(gr.Age__c == pApplicantAge && (gr.Max_Term__c>=application.genesis__Term__c && gr.Min_Term__c<=application.genesis__Term__c) && (gr.Max_Interest__c>=application.genesis__Interest_Rate__c && gr.Min_Interest__c<=application.genesis__Interest_Rate__c) && gr.Bank__c =='BSLI'){
                    gmraIndAmt2 = (LoanFracAmt * gr.Value__c * 1.18)/1000;
                }                    
            }
          
        //application.GMRA_Amount__c = gmraIndAmt2; 
        return gmraIndAmt2;
    }
    public static Decimal gmraOp5(genesis__Applications__c application,List<GMRA_Master__c>gmrList){
        Decimal Loan_Amount = application.genesis__Loan_Amount__c - (application.GMRA_Amount__c != null ? application.GMRA_Amount__c : 0);
        gmraIndAmt = 0.00;
        gmraIndAmt2 = 0.00;
        for(genesis__Application_Parties__c pt : application.genesis__Application_Parties__r){
            LoanFracAmt = Loan_Amount;
            coApplicantAge = Integer.ValueOf(pt.genesis__Party_Account_Name__r.age__c);
            if((pt.genesis__Party_Account_Name__r.Gender__pc == 'Female' || pt.genesis__Party_Account_Name__r.Gender__pc == 'F') && pt.genesis__Party_Account_Name__r.age__c >18){
                coApplicantAge = Integer.ValueOf(pt.genesis__Party_Account_Name__r.age__c) - 3;
            }
            if(coApplicantAge<18){
                coApplicantAge = 18;
            }
            
            for(GMRA_Master__c gr : gmrList){
                if(gr.Age__c == coApplicantAge && (gr.Max_Term__c>=application.genesis__Term__c && gr.Min_Term__c<=application.genesis__Term__c) && (gr.Max_Interest__c>=application.genesis__Interest_Rate__c && gr.Min_Interest__c<=application.genesis__Interest_Rate__c) && gr.Bank__c =='BSLI'){
                    system.debug('**Value**'+gr.Value__c+'**gmraIndAmt**'+gmraIndAmt);
                    gmraIndAmt = gmraIndAmt + (LoanFracAmt * gr.Value__c * 1.18)/1000;
                }                    
            }
        }
        
            LoanFracAmt = Loan_Amount;
            pApplicantAge = Integer.ValueOf(application.genesis__Account__r.age__c);
            if(application.genesis__Account__r.Gender__pc == 'Female' && application.genesis__Account__r.age__c >18){
                pApplicantAge = Integer.ValueOf(application.genesis__Account__r.age__c) - 3;
            }
            if(pApplicantAge <18){
                pApplicantAge = 18;
            }
            
            for(GMRA_Master__c gr : gmrList){
                if(gr.Age__c ==pApplicantAge && (gr.Max_Term__c>=application.genesis__Term__c && gr.Min_Term__c<=application.genesis__Term__c) && (gr.Max_Interest__c>=application.genesis__Interest_Rate__c && gr.Min_Interest__c<=application.genesis__Interest_Rate__c) && gr.Bank__c =='BSLI'){
                    gmraIndAmt2 = (LoanFracAmt * gr.Value__c * 1.18)/1000;
                }                    
            }
        System.debug('**co-borr**'+gmraIndAmt+'**Primary**'+gmraIndAmt2);
        totalGmra = gmraIndAmt + gmraIndAmt2;   
        //application.GMRA_Amount__c = totalGmra; 
        return totalGmra;
    }
}