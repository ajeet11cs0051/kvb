//Test Class.
@isTest
public class WS_SMECustomerListTest {
    static testmethod void getSMECustomerTest(){
    	try{
    		User testUser     			= Utility_Test.createUser('kvbtest@etkvb.com');
        System.runAs(testUser){
            Account  acc = Utility_Test.createAccount('KvbTest','987654333','CBS123456');
            insert acc;
            genesis__Applications__c app = Utility_Test.createApplication('987654333',acc.Id);
            insert app;
            Test.startTest(); 
            RestRequest req = new RestRequest(); 
   			RestResponse res = new RestResponse();        	
   			req.requestURI = '/services/apexrest/renewalCustomerList'; 
    		req.httpMethod = 'POST';
   			req.requestBody = Blob.valueOf('{"CUSTOMER_ID":"10009"}');
    		RestContext.request = req;
    		RestContext.response= res;
         	Test.setMock(HttpCalloutMock.class, new WS_SMECustomerListFetchCallOutMock());
         		WS_SMECustomerListFetch.getSMECustomers();
        	Test.stopTest(); 
     		}
            }catch(Exception ex){
              
            }
        
   }     
     
}