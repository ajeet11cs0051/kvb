/*
* Name     : CommercialPRBatch
* Company  : ET Marlabs
* Purpose  : Batch Class  to send CommercialPRCallout request to MiddleWare. 
*/
global class CommercialPRBatch_RUN implements Database.Batchable<sObject>,Database.Stateful, Database.AllowsCallouts{
    public Integer count=0;
    public String whereCondition = '';
    public CommercialPRBatch_RUN(String condition){
        whereCondition += condition;
    }
    global Database.QueryLocator start(Database.BatchableContext BC){ 
        //String query ='select id,CBS_Customer_ID__c,AccountNumber,Name,BillingStreet,BillingCity,BillingState,BillingPostalCode,BillingCountry,Pan_Number__c from Account where New_Data__c=true LIMIT 150';
        String query  = 'SELECT id,genesis__Account__c,Product_Code__c,genesis__Loan_Amount__c,Application_Stage__c,Pre_Renewal_Checklist__c,CommercialPR_Stage__c,genesis__Account__r.CBS_Customer_ID__c,genesis__Account__r.AccountNumber,genesis__Account__r.Name,genesis__Account__r.BillingStreet,genesis__Account__r.BillingCity,genesis__Account__r.BillingState,genesis__Account__r.BillingPostalCode,genesis__Account__r.BillingCountry,genesis__Account__r.Pan_Number__c,(Select id,genesis__Party_Account_Name__c from genesis__Application_Parties__r where genesis__Party_Account_Name__c!=null AND Active__c = true) from genesis__Applications__c';
        if(whereCondition != ''){
            query += ' WHERE '+  whereCondition;
        }
        return Database.getQueryLocator(query);
    }
    //Execute Method.
    global void execute(Database.BatchableContext BC, List<genesis__Applications__c> scope){
        try{
            List<Account> accList   = new List<Account>();
            System.debug('#####'+scope[0]);
            count += scope.size();
            List<genesis__Applications__c>  listApp                     =   new List<genesis__Applications__c>();
            CommercialPRRequest                         commPRReq       =   new CommercialPRRequest();
            commPRReq.inputVariables                                    =   new CommercialPRRequest.cls_inputVariables();
            
            commPRReq.inputVariables.in_msg                             =   new CommercialPRRequest.cls_in_msg();
            commPRReq.inputVariables.in_msg.requestJsonStr              =   new List<CommercialPRRequest.cls_requestJsonStr>();
            commPRReq.inputVariables.in_msg.Batch_ID                    =   Utility.generateRandomString();
            for(genesis__Applications__c                appObj          :   scope){
                If(appObj !=null){
                    //if(appObj.genesis__Application_Parties__r.size() > 0)parties.addAll(appObj.genesis__Application_Parties__r);
                    genesis__Applications__c                appObject       =   new genesis__Applications__c();
                    System.debug('#####'+appObj.id);
                    CommercialPRRequest.cls_requestJsonStr    applicantsObj =   new CommercialPRRequest.cls_requestJsonStr();
                    applicantsObj.Request_ID                                =   Utility.getBlankStringIfNull(appObj.genesis__Account__r.CBS_Customer_ID__c);
                    applicantsObj.Product_Code                              =   'P2';
                    applicantsObj.AddOn                                     =   'A1';
                    applicantsObj.Output_Format                             =   'CSV';
                    applicantsObj.Future_Use1                               =   '';
                    applicantsObj.Future_Use2                               =   '';
                    applicantsObj.Member_Reference_Number                   =   Utility.getBlankStringIfNull(appObj.genesis__Account__r.CBS_Customer_ID__c);
                    applicantsObj.Account_Number                            =   Utility.getBlankStringIfNull(appObj.genesis__Account__r.AccountNumber);
                    applicantsObj.Subject_Full_Name                         =   Utility.getBlankStringIfNull(appObj.genesis__Account__r.Name);
                    applicantsObj.Subject_Street_Address                    =   Utility.getBlankStringIfNull(appObj.genesis__Account__r.BillingStreet);
                    applicantsObj.City                                      =   Utility.getBlankStringIfNull(appObj.genesis__Account__r.BillingCity);
                    applicantsObj.State                                     =   Utility.getBlankStringIfNull(appObj.genesis__Account__r.BillingState);
                    applicantsObj.Pin_Code                                  =   Utility.getBlankStringIfNull(appObj.genesis__Account__r.BillingPostalCode);
                    applicantsObj.PAN                                       =   Utility.getBlankStringIfNull(appObj.genesis__Account__r.Pan_Number__c);
                    applicantsObj.CIN                                       =   '';
                    applicantsObj.CRN                                       =   '';
                    applicantsObj.TIN                                       =   '';
                    applicantsObj.Telephone_Type                            =   '';
                    applicantsObj.Telephone_Number                          =   '';
                    applicantsObj.Enquiry_Purpose                           =   '';
                    applicantsObj.Loan_Amount                               =   Utility.getBlankStringIfNull(String.valueOf(appObj.genesis__Loan_Amount__c));
                    
                    appObject.id                                            =   appObj.id;
                    appObject.CommercialPR_Stage__c                         =   'Sent';
                    
                    listApp.add(appObject);
                    accList.add(new Account(Id = appObj.genesis__Account__c,TU_Trigger_Time__c = System.now()));
                    commPRReq.inputVariables.in_msg.requestJsonStr.add(applicantsObj);
                }   
            }
            System.debug('response');
            Map<String,String> headerMap                            = new Map<String,String>();
            headerMap.put('Content-Type','application/json');
            HTTPResponse response                                   = new HTTPResponse();
            //String endPoint                                       = '';
            response = HttpUtility.sendHTTPRequest(Utility.getEndpoint('CommercialPR Req'), 'GET', null,JSON.serialize(commPRReq),headerMap,null);
            System.debug('response'+response);
            If(response.getStatusCode() == 200 || response.getStatusCode() == 201){
                String jsonData                                     = response.getBody(); 
                
            }else{                
                throw new CustomException('Status Code-'+response.getStatusCode()+' Status-'+response.getStatus());
            }
            If(listApp.size()>0){
                ApplicationTriggerHandler.IsFirstRun=false;
                update listApp;
                System.debug('#####'+listApp[0].Id);
            }
            if(!accList.isEmpty()){
                AccountTriggerHandler.isAccountTrigger = true;
                UPDATE accList;
            }
        }catch(Exception ex){
            String error = ex.getMessage();
            System.debug('Error Message'+error+ex.getMessage()+ex.getLineNumber());
        } 
    }
    
    global void finish(Database.BatchableContext BC){
        Set<String> personIds   = new set<String>();
        /*for(genesis__Application_Parties__c p : parties){
personIds.add(p.genesis__Party_Account_Name__c);
}
if(personIds.size() > 0){
Person_CIBIL_Call obj   = new Person_CIBIL_Call(personIds);
database.executeBatch(obj,1);
}*/
        List<String> listEmail  =   Commercial_PR_Trigger_Point__c.getOrgDefaults().Email__c.split(',');
        If(count > 0){
            for(String emailId  :   listEmail){
                System.debug('Email ID'+ emailId);
                String mailSubject = 'TU CommercialPR Notification';
                String emailBody = 'We have placed a request file in production for '+count+' customers on '+System.today().format()+', please process and send the response.';
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                message.toAddresses = new List<String>{emailId};
                    message.setSubject(mailSubject);
                message.setReplyTo('support@kvb.com');
                message.setSenderDisplayName('KVB TEAM');
                message.setBccSender(false);
                message.setHtmlBody(emailBody);
                Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
                    Messaging.sendEmail(messages);
            }
            String mobile_Number    =   Commercial_PR_Trigger_Point__c.getOrgDefaults().Mobile__c;
            String mobile_Message = 'We have placed a request file in production for '+count+' customers on '+ System.today().format() +', please process and send the response.';
            SMS_Services.sendSMSCall(mobile_Number,mobile_Message);    
        }
        
    }
    
}