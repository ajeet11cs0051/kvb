global class Re_try_CBS_HL {
    //Retry CBS API from Application
    Webservice static void retry_CBS(String AppId){    
        genesis__Applications__c  app = new genesis__Applications__c();
        app = [Select ID,Record_Type_Name__c,CBS_API_Sequence__c,Retry_CBS__c From genesis__Applications__c Where Id =:AppId];
        system.debug('^^^^^^'+app);
        if(app.Retry_CBS__c == False && (app.CBS_API_Sequence__c<>null || app.CBS_API_Sequence__c<>'')){          
            system.debug('**Retry**'+app.Retry_CBS__c+'**Sequence**'); 
            if(app.CBS_API_Sequence__c =='1'){
                //Customer Dedupe
                WS_CBS_Dedupe.callDedupe(app.Id);
            }else if(app.CBS_API_Sequence__c =='2'){
                //Customer EKYC 
                if(app.Record_Type_Name__c == 'SME Renewal') SME_CBS_Handler.eKYC_Updation(new List<Id>{app.Id});
                else {
                    WS_CBS_Dedupe.callDedupe(app.Id);
                    WS_CBS_eKYC_Creation_HL.cbs_eKYC_Creation(null,app.Id,null);
                }
            }else if(app.CBS_API_Sequence__c =='3'){
                //Customer ID creation
                if(app.Record_Type_Name__c == 'SME Renewal') SME_CBS_Handler.customerIdCreation(new List<Id>{appId}); 
                else{
                    WS_CBS_Dedupe.callDedupe(app.Id);
                     WS_CBS_eKYC_Creation_HL.cbs_eKYC_Creation(null,app.Id,null);
                     WS_CBS_Create_CustID.getCBSID(app.Id); 
                }
            }else if(app.CBS_API_Sequence__c =='4'){
                //Loan Creation
                WS_CBS_Loan_Creation_HL.createLoan(app.Id,null);
            }else if(app.CBS_API_Sequence__c =='5'){
                //Customer Link to loan
                if(app.Record_Type_Name__c == 'SME Renewal') SME_CBS_Handler.customerLinkage(new List<Id>{app.Id});
                else WS_CBS_Loan_Creation_HL.customerLinkage(null, app.Id, null);
            }else if(app.CBS_API_Sequence__c =='6'){
                //Collateral Creation
                WS_CBS_Collateral_Creation.Collateral_Creation(app.Id,null,null);
            }else if(app.CBS_API_Sequence__c =='7'){
                //Customer Linkage
                WS_CBS_Collateral_Linkage_HL.linkCollateral(app.Id);
            }else if(app.CBS_API_Sequence__c =='8'){
                //CBR file updation
                WS_CBS_CBR_DTLS_HL.CBR_DTLS(app.Id);
            }else if(app.CBS_API_Sequence__c =='9'){
                //Host Disbursment
                WS_CBS_HOST_Disbursment_HL.Host_request(app.Id);
            }else if(app.CBS_API_Sequence__c =='10'){
                //Mis file update
                WS_CBS_MIS_Update_HL.Mis_Update(app.Id);
            }else if(app.CBS_API_Sequence__c =='11'){
                //Photo update
                WS_CBS_UploadImage.cbs_photo_upload(app.Id);
            }
            else if(app.CBS_API_Sequence__c =='12'){ 
                //OD/TOD callout
                //ApplicationTriggerHelper.ODTOD_CallHandler(new List<Id>{appId});
                SME_CBS_Handler.limitUpdation_ChargeColllection(new List<Id>{appId});
            }
            else if(app.CBS_API_Sequence__c =='13'){
                //Charge Collection
                SME_CBS_Handler.chargeCollection(new List<Id>{appId});
            }
            else if(app.CBS_API_Sequence__c =='14'){
                //Charge Collection
                SME_StockStatementHandler.requesthandler(appId);
            }
        }
        else if(app.Retry_CBS__c == True && app.CBS_API_Sequence__c =='20'){
            //Branch Disbursment
            WS_CBS_Loan_Disbursment_HL.callDisb(app.Id);
        }
        //update app;
        
    }
}