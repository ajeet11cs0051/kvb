/*
* Name     : CommercialPR_EAE_Batch
* Company  : ET Marlabs
* Purpose  : Batch Class  to send CommercialPRCallout request to APIGEE and get Response. 
* Author   : Raushan
*/
public class CommercialPR_EAE_Batch {
    
    public static List<Account> listAccount =  new List<Account>();
    
    public static List < ints__TransUnion_Credit_Report__c > listTransUnion = new List < ints__TransUnion_Credit_Report__c > ();
    //Credit Profile
    public static List < Credit_Profile__c > listcreditProfile = new List < Credit_Profile__c > ();
    
    //Enquiry Summary
    public static List < Enquiry_Summary__c > listenquirySummary = new List < Enquiry_Summary__c > ();
    
    //DerogatoryInformation Parent
    public static List < Derogatory_Information__c > listderogatoryInformationParent = new List < Derogatory_Information__c > ();
    
    //DerogatoryInformation Child
    public static List < Derogatory_Information__c > listderogatoryInformation = new List < Derogatory_Information__c > ();
    
    //OustandingBalancebyCF
    public static List < OustandingBalancebyCF__c > listOustandingBalancebyCF = new List < OustandingBalancebyCF__c > ();
    
    //Location Detail
    public static List < Location_Detail__c > listlocationDetail = new List < Location_Detail__c > ();
    
    //Relationship Detail
    public static List < Relationship_Detail__c > listRelationshipDetail = new List < Relationship_Detail__c > ();
    
    //Credit Facility Detail
    public static List < Credit_Facility_Detail__c > listCreditFacilityDetail = new List < Credit_Facility_Detail__c > ();
    
    //Message
    public static List < TU_Message__c > listMessage = new List < TU_Message__c > ();
    
    //TU CFHistory
    public static List < TU_CFHistory__c > listTUCFHistory = new List < TU_CFHistory__c > ();
    
    //TU IDs
    public static List < TU_ID_s__c > listTUIDs = new List < TU_ID_s__c > ();
    
    //RankVec
    public static List < RankVec__c > listRankVecObj = new List < RankVec__c > ();
    //Borrower Delinquency Report
    public static List<Borrower_Delinquency_Report__c> listBorrowerDelinquencyReport  = new List<Borrower_Delinquency_Report__c>();
    
    //Credit facility Details as Guarantor
    public static List<Credit_facility_Details_as_Guarantor__c> listCreditfacilityDetailsGuarantor    = new List<Credit_facility_Details_as_Guarantor__c>();
    
    //Guarantor Addesss
    public static List<Guarantor_Address__c> listGuarantorAddress     =   new List<Guarantor_Address__c>();
    
    //SuitFilled
    public static List<SuitFilled__c> listSuitFilled      =   new List<SuitFilled__c>();
    
    //Credit Rating Summary
    public static List<Credit_Rating_Summary__c>  listCreditRatingSummary     =   new  List<Credit_Rating_Summary__c>();
    
    //Summary of Credit Facility
    public static List<Summary_Credit_Facility__c> listSummaryCreditFacility  =   new List<Summary_Credit_Facility__c>();
    
    public static List<String> extIdList = new List<String>();
    
    //TU Error
    public static List<TU_Error__c> listTUError =   new List<TU_Error__c>();
    
    // For ID Segments
    //public static List<TU_ID_s__c> listIDsegments = new List<TU_ID_s__c>(); 
    // For telephone Segments
    public static List<TU_Telephone__c>    listTelephonesegment = new List<TU_Telephone__c>(); 
    // For EMail Segments
    public static List<TU_Email__c>    listEmailsegment = new List<TU_Email__c>(); 
    // For TU Account
    public static List<TU_Account__c>    listTUAccount = new List<TU_Account__c>(); 
    // For TU Enquiry
    //public static List<Enquiry_Summary__c>    listTUEnquiry= new List<Enquiry_Summary__c>(); 
    // For TU Address
    // public static List<Location_Detail__c>    listTUAddres= new List<Location_Detail__c>(); 
    // For TU scores    
    public static List<TU_Score__c>   listTUscores= new List<TU_Score__c>();
    
    //Get List Of Customer Id
   /* public static void getCustomerID(String customerId){
       
        List<genesis__Applications__c> listAppObj = [select id,Application_Stage__c,CommercialPR_Stage__c,genesis__Account__r.CBS_Customer_ID__c,genesis__Account__r.Name,Execute_batch_in_days__c from genesis__Applications__c where genesis__Account__r.CBS_Customer_ID__c=:customerId];
        System.debug('listAppObj@@@'+listAppObj);
        List<String> listCustId         =   new List<String>();
        for(genesis__Applications__c    appObj  :   listAppObj){
            listCustId.add(appObj.genesis__Account__r.CBS_Customer_Id__c);
        }
        getCommercialPR_EAE_Request(listCustId);
    } */
    public static void getCommercialPR_EAE_Request(List<String> listCustId,String Type) {

        System.debug('Scope'+listCustId);
        
        try{
            for(String  custID  :   listCustId) {
                If(custID !=null){
                    CommercialPR_EAE_Request commercialEAEObj   =   sendRequest(custID,Type);
                    sendRequestAPIGEE(commercialEAEObj);
                }    
            }
            If(listTransUnion.size() > 0){
                insert listTransUnion;
                //accountObj.id = listTransUnion[0].Account__c;
                //update accountObj;    
            }
            If(listcreditProfile.size() > 0)
                insert listcreditProfile;
            
            If(listenquirySummary.size() > 0)
                insert listenquirySummary;
            
            If(listderogatoryInformationParent.size() > 0)
                insert listderogatoryInformationParent;
            
            If(listderogatoryInformation.size() > 0)
                insert listderogatoryInformation;
            
            If(listOustandingBalancebyCF.size() > 0)
                insert listOustandingBalancebyCF;
            
            If(listCreditfacilityDetailsGuarantor.size() > 0)
                insert listCreditfacilityDetailsGuarantor;
            
            If(listlocationDetail.size() > 0)
                insert listlocationDetail;
            
            If(listRelationshipDetail.size() > 0)
                insert listRelationshipDetail;
            
            If(listCreditFacilityDetail.size() > 0)
                insert listCreditFacilityDetail;
            
            If(listTUIDs.size() > 0)
                insert listTUIDs;
            
            If(listRankVecObj.size() > 0)
                insert listRankVecObj;
            
            If(listMessage.size() > 0)
                insert listMessage;
            
            If(listGuarantorAddress.size()>0)
                insert listGuarantorAddress; 
            
            If(listSuitFilled.size() > 0)
                insert listSuitFilled;
            
            If(listCreditRatingSummary.size()>0)
                insert listCreditRatingSummary;
            
            If(listSummaryCreditFacility.size()>0)
                insert listSummaryCreditFacility;
            
            If(listTUCFHistory.size() > 0)
                insert listTUCFHistory;
            
            If(listBorrowerDelinquencyReport.size() > 0)
                insert listBorrowerDelinquencyReport;
            
            //for Individual
            
            If(listTUscores !=null && listTUscores.size()>0){
                insert listTUscores;
            }
            If(listTelephonesegment !=null && listTelephonesegment.size()>0){
                insert listTelephonesegment;
            }
            //Error
            If(listTUError !=null && listTUError.size() >0){
               insert listTUError; 
            }
            If(listAccount.size()> 0){
                upsert listAccount CBS_Customer_ID__c;
            }
            List<String> listRecordTypeName =   new List<String>{'SME_Renewal','SME_Enhancement'};
            List<genesis__Applications__c> appList = [select id,CommercialPR_Stage__c from genesis__Applications__c where RecordType.DeveloperName IN : listRecordTypeName AND genesis__Account__r.CBS_Customer_ID__c IN :extIdList];
            If(!appList.isEmpty()){
                for(genesis__Applications__c app : appList){
                    app.CommercialPR_Stage__c = Constants.COMPLETED;
                }
                ApplicationTriggerHandler.IsFirstRun = false;   
                update appList;
            }
            
        }catch(Exception ex){
            System.debug('Exception'+ ex.getMessage());
            System.debug('Line Number'+ex.getLineNumber());
        }  
    }
    public static CommercialPR_EAE_Request sendRequest(String CustId,String Type){
        //List<String> listCustId     =   new List<String>();
        //listCustId.add(CustId);
        CommercialPR_EAE_Request commercialEAEObj   =   CommercialPR_EAE_Request_Handler.getRequest(CustId,Type);
        System.debug('commercialEAEObj@@###'+commercialEAEObj);
        return commercialEAEObj;
    }
    public static void sendRequestAPIGEE(CommercialPR_EAE_Request commercialEAEObj){
        System.debug('Inside APIGEE Method********');
        try{
            Map<String,String> headerMap                            = new Map<String,String>();
            headerMap.put('Content-Type','application/json');
            headerMap.put('Authorization',WS_ApiGatewayAccToken.getAccTkn());
            HTTPResponse response                                       = new HTTPResponse();
            //String endPoint                                               = 'https://kvb-dev.apigee.net/next/v1/commercial/report';
            
            System.debug('JSON.serialize(commercialEAEObj)'+JSON.serialize(commercialEAEObj));
            response = HttpUtility.sendHTTPRequest(Utility.getCommercialPR_APIGEE_Endpoint('CommercialPR_Apigee'), 'GET', null,JSON.serialize(commercialEAEObj),headerMap,null);
            System.debug('Response@@@@@'+response);
            If(response.getStatusCode() == 200 || response.getStatusCode() == 201){
                String jsonData                                     = response.getBody();
                System.debug('Json Data***'+jsonData);
                CommercialPR_EAE_Response commercialObj             = CommercialPR_EAE_Response.parse(jsonData);
                If(commercialObj !=null) {
                    try{
                        CommercialPR_EAE_Response_Handler.getResponse(commercialObj);  
                    }catch(Exception ex){
                        System.debug(ex.getLineNumber());
                        System.debug(ex.getStackTraceString());
                    }   
                }
            }else{                
                throw new CustomException('Status Code-'+response.getStatusCode()+' Status-'+response.getStatus());
            }
            
        }catch(Exception ex){
            String error = ex.getMessage();
            System.debug('Error Message'+error);
        } 
    }
   
}