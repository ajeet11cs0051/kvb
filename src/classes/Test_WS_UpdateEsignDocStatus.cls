@isTest
public class Test_WS_UpdateEsignDocStatus {
    Static genesis__Applications__c app;
    static Document_Applicant__c documentApplicantobj;
    
    //for valid Data
@isTest
    public static void getstatusValidData(){
        app=TestUtility.intialSetUp('HomeLoan',true);
        Account acc=new Account(Name='TestName');
        insert acc;
        System.debug('**Account id'+acc.id);
        Digio_Document_ID__c docobj=new Digio_Document_ID__c(Application__c=app.id,Borrower__c=true,Document_ID__c='111',Guarantor__c=true,Stamp_Charges__c=100);
        insert docobj;
        System.debug('**docobj id'+docobj.id);
        documentApplicantobj=new Document_Applicant__c(Account__c=acc.id,Digio_Document_ID__c=docobj.id,eSigned__c=true);
        insert documentApplicantobj;
        System.debug('**documentApplicantobj'+documentApplicantobj.Id);
        blob b=blob.valueOf(getdetails());
        TestUtility.webserviceRequest('services/apexrest/UpdateEsignDocStatus','POST',b);
        Test.startTest();
        WS_UpdateEsignDocStatus.Response res=WS_UpdateEsignDocStatus.getstatus();
        Test.stopTest();
        system.assert(res!=null);
        system.assertEquals('200',res.statusCode);
    }
     
    //for Invalid Data
@isTest
     public static void getstatusInValidData(){
        app=TestUtility.intialSetUp('HomeLoan',true);
        blob b=blob.valueOf(getdetails2());
        TestUtility.webserviceRequest('services/apexrest/UpdateEsignDocStatus','POST',b);
        Test.startTest();
        WS_UpdateEsignDocStatus.Response res=WS_UpdateEsignDocStatus.getstatus();
        Test.stopTest();
        system.assert(res!=null);
        system.assertEquals('400',res.statusCode);
    }
    //for Null value
@isTest
     public static void getstatusNull(){
        app=TestUtility.intialSetUp('HomeLoan',true);
        TestUtility.webserviceRequest('services/apexrest/UpdateEsignDocStatus','POST',null);
        Test.startTest();
        WS_UpdateEsignDocStatus.getstatus();
        Test.stopTest();
        
    }
public static string getdetails(){
        String json=	
	'{'+
	'"eSignStatus":true,'+
	'"docId":"'+documentApplicantobj.id+'" '+
	'}';
    return json;
}
    
    public static string getdetails2(){
        String json=	
	'{'+
	'"eSignStatus":"True",'+
	'"docId":"a6Q5D0000008P5e" '+
	'}';
    return json;
}
}