/*
* Name          : WS_LandingPage
* Description   : Web service to get application details against customer data
* Author        : Dushyant
*/
@RestResource(urlMapping = '/getCustomerList')
global class WS_LandingPage{
    
    //Response structure
    global class Response extends WS_Response{
        public LandingInfo landing_info;
        public Response(){
            landing_info = new LandingInfo();
        }
    }
    public class LandingInfo{
        public List<AppDetails> appln_info;
        public Customer360view.cls_LOANDETAIL loanInfo;
        
        public LandingInfo(){
            appln_info      = new List<AppDetails>();
            loanInfo 		= new Customer360view.cls_LOANDETAIL();
            
        }
    }
    public class AppDetails{
        public String app_stage;
        public String app_type;
        public String app_due_date;
        public String app_los_id;
        public String app_name;
        public String app_ui_stage;
        public String app_total_amount;
        public String Type;
    }
    
    //Request structure
    public class ReqWrapObj{
        String custId;
    }
    
    @HttpPost
    global static Response handleRequest(){
        RestRequest req = RestContext.request;
        Response res         = new Response(); 
        
        if(req == null || req.requestBody == null){
            res.status          = Constants.WS_ERROR_STATUS;
            res.errorMessage    = Constants.WS_REQ_BODY_IS_NULL;
            res.statusCode      = Constants.WS_ERROR_CODE;
                       
        }else{
            String jsonData     = req.requestBody.toString();        
            ReqWrapObj reqObj   = (ReqWrapObj)Json.deserialize(jsonData, ReqWrapObj.class);
            system.debug('reqObj::'+reqObj);
            res = (WS_LandingPage.Response)LandingPageHelper.landingPageRes(reqObj.custId,false);
            System.debug('resObj::'+res);
        }
        RETURN res;
    }  
}