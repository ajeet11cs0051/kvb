global class NMICalculations {
    
    Webservice static void Calculations(String AppID){
        try{
            genesis__Applications__c App=[select id,AVG_Balance__c,Total_Cheque_Bounces__c,genesis__Account__r.Average_Balances6_months__c,genesis__Account__r.Employment_Type__c,genesis__Account__r.Perfios_Captured__c,genesis__Account__r.Approved_NMI_From_ITR__c,
                                          genesis__Account__r.Total_ChequeECS_bounces__c,genesis__Account__r.NMI_Claimed_By_Customer__c,genesis__Account__r.NMI_Approved__c,genesis__Account__r.Additional_Income_Amount1__c,genesis__Account__r.Agriculture2__c,genesis__Account__r.NMI_as_per_BS__c,
                                          genesis__Account__r.Financial_Applicant__c, genesis__Account__r.NMI_as_per_26AS__c,genesis__Account__r.Annual_Agriculture_Income_ITR__c,genesis__Account__r.Net_Monthly_Income__c,genesis__Account__r.Computed_NMI__c,genesis__Account__r.Existing_Emis__c,(select id,name,genesis__Party_Account_Name__r.Average_Balances6_months__c,genesis__Party_Account_Name__r.Employment_Type__c,genesis__Party_Account_Name__r.Approved_NMI_From_ITR__c,
                                                                                                                                genesis__Party_Account_Name__r.Total_ChequeECS_bounces__c,genesis__Party_Account_Name__r.Annual_Agriculture_Income_ITR__c,genesis__Party_Account_Name__r.NMI_Approved__c,genesis__Party_Account_Name__r.NMI_Claimed_By_Customer__c,genesis__Party_Account_Name__r.Additional_Income_Amount1__c,genesis__Party_Account_Name__r.Agriculture2__c,
                                                                                                                                genesis__Party_Account_Name__r.Financial_Applicant__c,genesis__Party_Account_Name__r.NMI_as_per_26AS__c,genesis__Party_Account_Name__r.NMI_as_per_BS__c,genesis__Party_Account_Name__r.Computed_NMI__c,genesis__Party_Account_Name__r.Existing_Emis__c,genesis__Party_Account_Name__r.Net_Monthly_Income__c,genesis__Party_Account_Name__r.Perfios_Captured__c from genesis__Application_Parties__r 
                                                                                                                                where Active__c=true AND genesis__Party_Type__c=:Constants.Co_Borrower) from genesis__Applications__c where ID =:AppID];
            Decimal NMI_Approved=0;
            Decimal NMI_ClaimedCUST=0;
            Decimal Avgbalance6month=0;
            Decimal NoChequeBounce=0;
            decimal ExistingEmis=0;
            
            system.debug('Application'+App);
            system.debug('NMI_ClaimedCUST'+NMI_ClaimedCUST);
            system.debug('NMI_Approved'+NMI_Approved);
            
            if(App.genesis__Account__r.Financial_Applicant__c){
                if(App.genesis__Account__r.NMI_Approved__c!=null){
                    NMI_Approved=App.genesis__Account__r.NMI_Approved__c;
                }
                if(App.genesis__Account__r.NMI_Claimed_By_Customer__c!=null){
                    NMI_ClaimedCUST=App.genesis__Account__r.NMI_Claimed_By_Customer__c;
                }
                if(App.genesis__Account__r.Average_Balances6_months__c!=Null){
                    Avgbalance6month=App.genesis__Account__r.Average_Balances6_months__c;
                }
                if(App.genesis__Account__r.Total_ChequeECS_bounces__c!=null){
                    NoChequeBounce=App.genesis__Account__r.Total_ChequeECS_bounces__c;
                }
                 if(App.genesis__Account__r.Existing_Emis__c!=null){
                    ExistingEmis=App.genesis__Account__r.Existing_Emis__c;
                }
                system.debug('NMI_ClaimedCUST'+NMI_ClaimedCUST);
                system.debug('NMI_Approved'+NMI_Approved);
            }
            for(genesis__Application_Parties__c part:App.genesis__Application_Parties__r){
                system.debug('HI'+Part.Name);
                system.debug('NMI_ClaimedCUST'+NMI_ClaimedCUST);
                system.debug('NMI_Approved'+NMI_Approved);
                if(part.genesis__Party_Account_Name__r.Financial_Applicant__c){
                    if(part.genesis__Party_Account_Name__r.NMI_Approved__c!=null){
                        NMI_Approved=NMI_Approved+part.genesis__Party_Account_Name__r.NMI_Approved__c;
                    }
                    if(part.genesis__Party_Account_Name__r.NMI_Claimed_By_Customer__c!=null){
                        NMI_ClaimedCUST=NMI_ClaimedCUST+part.genesis__Party_Account_Name__r.NMI_Claimed_By_Customer__c;
                    }
                    if(part.genesis__Party_Account_Name__r.Average_Balances6_months__c!=Null){
                        Avgbalance6month=Avgbalance6month+part.genesis__Party_Account_Name__r.Average_Balances6_months__c;
                    }
                    if(part.genesis__Party_Account_Name__r.Total_ChequeECS_bounces__c!=null){
                        NoChequeBounce=NoChequeBounce+part.genesis__Party_Account_Name__r.Total_ChequeECS_bounces__c;
                    }
                     if(part.genesis__Party_Account_Name__r.Existing_Emis__c!=null){
                        ExistingEmis=ExistingEmis+part.genesis__Party_Account_Name__r.Existing_Emis__c;
                    }
                    
                    system.debug('NMI_ClaimedCUST'+NMI_ClaimedCUST);
                    system.debug('NMI_Approved'+NMI_Approved);
                } 
            }
            system.debug('NMI_ClaimedCUST'+NMI_ClaimedCUST);
            system.debug('NMI_Approved'+NMI_Approved);
            if(NMI_Approved!=0){
                app.NMI_Approved__c=NMI_Approved;
            }
            Decimal NMIClaimed = NMICal1(app,'Claimed');
            Decimal NMIApproved = NMICal1(app,'Approved');
            //app.NMI_Claimed_By_Customer__c=NMI_ClaimedCUST;
            app.NMI_Claimed_By_Customer__c= NMIClaimed;
            app.NMI_Approved__c = NMIApproved;
            app.AVG_Balance__c = Avgbalance6month;   
            app.Total_Cheque_Bounces__c = NoChequeBounce;
            app.Existing_EMI_s__c=ExistingEmis;
            system.debug('##### App Cheque bounce details '+ app);
            ApplicationTriggerHandler.IsFirstRun = False;
            update app;
            
        }
        catch(exception e){
            system.debug('Error'+e.getLineNumber()+e.getStackTraceString());
        }
    }
    Webservice static Decimal NMICal1(genesis__Applications__c App,String NMIType){        
        Decimal NmiClaimed = 0.00;
        Decimal NmiApproved = 0.00;
        
        if(NMIType == 'Claimed'){
            if(App.genesis__Account__r.Financial_Applicant__c){
            if(app.genesis__Account__r.Employment_Type__c == 'Agriculturist'){
                NmiClaimed = (app.genesis__Account__r.Net_Monthly_Income__c != null ? app.genesis__Account__r.Net_Monthly_Income__c : 0) + (app.genesis__Account__r.Additional_Income_Amount1__c != null ? (app.genesis__Account__r.Additional_Income_Amount1__c != 0 ? app.genesis__Account__r.Additional_Income_Amount1__c/12 : 0 ) : 0);
            }
            else if(app.genesis__Account__r.Employment_Type__c != 'Agriculturist' && app.genesis__Account__r.Perfios_Captured__c == false){
                Decimal minVal = Math.min((app.genesis__Account__r.Additional_Income_Amount1__c !=null ? app.genesis__Account__r.Additional_Income_Amount1__c != 0 ? app.genesis__Account__r.Additional_Income_Amount1__c/12 : 0 :0),(((app.genesis__Account__r.Net_Monthly_Income__c != null ? app.genesis__Account__r.Net_Monthly_Income__c : 0)/0.7)*0.3));
                NmiClaimed = (app.genesis__Account__r.Net_Monthly_Income__c != null ? app.genesis__Account__r.Net_Monthly_Income__c : 0) + Math.min(minVal,200000/12);
            }
            else if(app.genesis__Account__r.Perfios_Captured__c == true){            
                if(app.genesis__Account__r.NMI_as_per_26AS__c == app.genesis__Account__r.Computed_NMI__c || app.genesis__Account__r.NMI_as_per_BS__c == app.genesis__Account__r.Computed_NMI__c){
                    Decimal minVal = Math.min((app.genesis__Account__r.Additional_Income_Amount1__c !=null ? app.genesis__Account__r.Additional_Income_Amount1__c != 0 ? app.genesis__Account__r.Additional_Income_Amount1__c/12 : 0 : 0),(((app.genesis__Account__r.Net_Monthly_Income__c != null ? app.genesis__Account__r.Net_Monthly_Income__c : 0)/0.7)*0.3));
                    NmiClaimed = (app.genesis__Account__r.Net_Monthly_Income__c !=  null ? app.genesis__Account__r.Net_Monthly_Income__c : 0) + Math.min(minVal,200000/12); 
                }else if(app.genesis__Account__r.Net_Monthly_Income__c == app.genesis__Account__r.Computed_NMI__c){
                    Decimal aVal = (app.genesis__Account__r.Net_Monthly_Income__c != null ? app.genesis__Account__r.Net_Monthly_Income__c : 0) - (app.genesis__Account__r.Annual_Agriculture_Income_ITR__c != null ? app.genesis__Account__r.Annual_Agriculture_Income_ITR__c : 0);
                    system.debug('aVal'+aVal);
                    system.debug('^^^'+app.genesis__Account__r.Annual_Agriculture_Income_ITR__c+'***'+app.genesis__Account__r.Additional_Income_Amount1__c);
                    decimal Agri1 = ((app.genesis__Account__r.Annual_Agriculture_Income_ITR__c != null ? app.genesis__Account__r.Annual_Agriculture_Income_ITR__c : 0) + (app.genesis__Account__r.Additional_Income_Amount1__c != null ? app.genesis__Account__r.Additional_Income_Amount1__c : 0));
                    if(Agri1==0){
                        NmiClaimed = aVal + Math.Min(Agri1,Math.Min(aVal/(0.7*0.3),200000.00/12));
                        
                    }
                    else {
                        NmiClaimed = aVal + Math.Min(Agri1/12,Math.Min(aVal/(0.7*0.3),200000.00/12));
                    }
                    NmiClaimed = (aVal + Math.Min((((app.genesis__Account__r.Annual_Agriculture_Income_ITR__c != null ? app.genesis__Account__r.Annual_Agriculture_Income_ITR__c : 0) + (app.genesis__Account__r.Additional_Income_Amount1__c != null ? app.genesis__Account__r.Additional_Income_Amount1__c : 0))/12),Math.Min(((aVal/0.7)*0.3),200000.00/12)));
                }
            }
            }
            for(genesis__Application_Parties__c PT : App.genesis__Application_Parties__r){
                 if(PT.genesis__Party_Account_Name__r.Financial_Applicant__c){
                if(PT.genesis__Party_Account_Name__r.Employment_Type__c == 'Agriculturist'){
                    NmiClaimed = NmiClaimed + (PT.genesis__Party_Account_Name__r.Net_Monthly_Income__c != null ? PT.genesis__Party_Account_Name__r.Net_Monthly_Income__c : 0)+ (PT.genesis__Party_Account_Name__r.Additional_Income_Amount1__c != null ? PT.genesis__Party_Account_Name__r.Additional_Income_Amount1__c/12 : 0);
                }
                else if(PT.genesis__Party_Account_Name__r.Employment_Type__c != 'Agriculturist' && PT.genesis__Party_Account_Name__r.Perfios_Captured__c == false){
                    Decimal minVal = Math.min((PT.genesis__Party_Account_Name__r.Additional_Income_Amount1__c != null ? PT.genesis__Party_Account_Name__r.Additional_Income_Amount1__c/12 : 0),(((PT.genesis__Party_Account_Name__r.Net_Monthly_Income__c != null ? PT.genesis__Party_Account_Name__r.Net_Monthly_Income__c : 0) /0.7)*0.3));
                    NmiClaimed = NmiClaimed + (PT.genesis__Party_Account_Name__r.Net_Monthly_Income__c != null ? PT.genesis__Party_Account_Name__r.Net_Monthly_Income__c : 0) + Math.min(minVal,200000/12);
                }
                else if(PT.genesis__Party_Account_Name__r.Perfios_Captured__c == true){            
                    if(PT.genesis__Party_Account_Name__r.NMI_as_per_26AS__c == PT.genesis__Party_Account_Name__r.Computed_NMI__c || PT.genesis__Party_Account_Name__r.NMI_as_per_BS__c == PT.genesis__Party_Account_Name__r.Computed_NMI__c){
                        Decimal minVal = Math.min((PT.genesis__Party_Account_Name__r.Additional_Income_Amount1__c != null ? PT.genesis__Party_Account_Name__r.Additional_Income_Amount1__c/12 : 0),(((PT.genesis__Party_Account_Name__r.Net_Monthly_Income__c != null ? PT.genesis__Party_Account_Name__r.Net_Monthly_Income__c : 0) /0.7)*0.3));
                        NmiClaimed = NmiClaimed + (PT.genesis__Party_Account_Name__r.Net_Monthly_Income__c != null ? PT.genesis__Party_Account_Name__r.Net_Monthly_Income__c : 0) + Math.min(minVal,200000/12); 
                    }else if(PT.genesis__Party_Account_Name__r.Net_Monthly_Income__c == PT.genesis__Party_Account_Name__r.Computed_NMI__c){
                        Decimal aVal = (PT.genesis__Party_Account_Name__r.Net_Monthly_Income__c != null ? PT.genesis__Party_Account_Name__r.Net_Monthly_Income__c : 0) - (PT.genesis__Party_Account_Name__r.Annual_Agriculture_Income_ITR__c != null ? PT.genesis__Party_Account_Name__r.Annual_Agriculture_Income_ITR__c : 0);
                        NmiClaimed = NmiClaimed + (aVal + Math.Min(((PT.genesis__Party_Account_Name__r.Annual_Agriculture_Income_ITR__c != null ? PT.genesis__Party_Account_Name__r.Annual_Agriculture_Income_ITR__c : 0) + (PT.genesis__Party_Account_Name__r.Additional_Income_Amount1__c != null ? PT.genesis__Party_Account_Name__r.Additional_Income_Amount1__c :0))/12,Math.Min(((aVal/0.7)*0.3),200000.00/12)));
                    }
                }
            }
            }
            return NmiClaimed;
        }else if(NMIType == 'Approved'){
              if(App.genesis__Account__r.Financial_Applicant__c){
            if(app.genesis__Account__r.Employment_Type__c == 'Agriculturist'){
                NmiApproved = (app.genesis__Account__r.Approved_NMI_From_ITR__c != null ? app.genesis__Account__r.Approved_NMI_From_ITR__c : 0) + (app.genesis__Account__r.Agriculture2__c !=null ? app.genesis__Account__r.Agriculture2__c != 0 ? app.genesis__Account__r.Agriculture2__c/12 : 0 : 0);
            }
            else if(app.genesis__Account__r.Employment_Type__c != 'Agriculturist' && app.genesis__Account__r.Perfios_Captured__c == false){
                Decimal minVal = Math.min((app.genesis__Account__r.Agriculture2__c !=null ? app.genesis__Account__r.Agriculture2__c != 0 ? app.genesis__Account__r.Agriculture2__c/12 : 0 :0),(((app.genesis__Account__r.Approved_NMI_From_ITR__c != null ? app.genesis__Account__r.Approved_NMI_From_ITR__c : 0)/0.7)*0.3));
                NmiApproved = (app.genesis__Account__r.Approved_NMI_From_ITR__c != null ? app.genesis__Account__r.Approved_NMI_From_ITR__c : 0) + Math.min(minVal,200000/12);
            }
            else if(app.genesis__Account__r.Perfios_Captured__c == true){            
                if(app.genesis__Account__r.NMI_as_per_26AS__c == app.genesis__Account__r.Computed_NMI__c || app.genesis__Account__r.NMI_as_per_BS__c == app.genesis__Account__r.Computed_NMI__c){
                    Decimal minVal = Math.min((app.genesis__Account__r.Agriculture2__c !=null ? app.genesis__Account__r.Agriculture2__c != 0 ? app.genesis__Account__r.Agriculture2__c/12 : 0 : 0),(((app.genesis__Account__r.Approved_NMI_From_ITR__c != null ? app.genesis__Account__r.Approved_NMI_From_ITR__c : 0)/0.7)*0.3));
                    NmiApproved = (app.genesis__Account__r.Approved_NMI_From_ITR__c !=  null ? app.genesis__Account__r.Approved_NMI_From_ITR__c : 0) + Math.min(minVal,200000/12); 
                }else if(app.genesis__Account__r.Approved_NMI_From_ITR__c == app.genesis__Account__r.Computed_NMI__c){
                    Decimal aVal = (app.genesis__Account__r.Approved_NMI_From_ITR__c != null ? app.genesis__Account__r.Approved_NMI_From_ITR__c : 0) - (app.genesis__Account__r.Annual_Agriculture_Income_ITR__c != null ? app.genesis__Account__r.Annual_Agriculture_Income_ITR__c : 0);
                    NmiApproved = (aVal + Math.Min((((app.genesis__Account__r.Annual_Agriculture_Income_ITR__c != null ? app.genesis__Account__r.Annual_Agriculture_Income_ITR__c : 0) + (app.genesis__Account__r.Agriculture2__c != null ? app.genesis__Account__r.Agriculture2__c : 0))/12),Math.Min(((aVal/0.7)*0.3),200000.00/12)));
                }
            }  
              }
            for(genesis__Application_Parties__c PT : App.genesis__Application_Parties__r){
                 if(PT.genesis__Party_Account_Name__r.Financial_Applicant__c){
                if(PT.genesis__Party_Account_Name__r.Employment_Type__c == 'Agriculturist'){
                    NmiApproved = NmiApproved + (PT.genesis__Party_Account_Name__r.Approved_NMI_From_ITR__c != null ? PT.genesis__Party_Account_Name__r.Approved_NMI_From_ITR__c : 0)+ (PT.genesis__Party_Account_Name__r.Agriculture2__c != null ? (PT.genesis__Party_Account_Name__r.Agriculture2__c/12) : 0);
                }
                else if(PT.genesis__Party_Account_Name__r.Employment_Type__c != 'Agriculturist' && PT.genesis__Party_Account_Name__r.Perfios_Captured__c == false){
                    Decimal minVal = Math.min((PT.genesis__Party_Account_Name__r.Agriculture2__c != null ? PT.genesis__Party_Account_Name__r.Agriculture2__c/12 : 0),(((PT.genesis__Party_Account_Name__r.Approved_NMI_From_ITR__c != null ? PT.genesis__Party_Account_Name__r.Approved_NMI_From_ITR__c : 0) / 0.7)*0.3));
                    NmiApproved = NmiApproved + (PT.genesis__Party_Account_Name__r.Approved_NMI_From_ITR__c != null ? PT.genesis__Party_Account_Name__r.Approved_NMI_From_ITR__c : 0) + Math.min(minVal,200000/12);
                }
                else if(PT.genesis__Party_Account_Name__r.Perfios_Captured__c == true){            
                    if(PT.genesis__Party_Account_Name__r.NMI_as_per_26AS__c == PT.genesis__Party_Account_Name__r.Computed_NMI__c || PT.genesis__Party_Account_Name__r.NMI_as_per_BS__c == PT.genesis__Party_Account_Name__r.Computed_NMI__c){
                        Decimal minVal = Math.min((PT.genesis__Party_Account_Name__r.Agriculture2__c != null ? PT.genesis__Party_Account_Name__r.Agriculture2__c/12 : 0),(((PT.genesis__Party_Account_Name__r.Approved_NMI_From_ITR__c != null ? PT.genesis__Party_Account_Name__r.Approved_NMI_From_ITR__c : 0) /0.7)*0.3));
                        NmiApproved = NmiApproved + (PT.genesis__Party_Account_Name__r.Approved_NMI_From_ITR__c != null ? PT.genesis__Party_Account_Name__r.Approved_NMI_From_ITR__c : 0) + Math.min(minVal,200000/12); 
                    }else if(PT.genesis__Party_Account_Name__r.Approved_NMI_From_ITR__c == PT.genesis__Party_Account_Name__r.Computed_NMI__c){
                        Decimal aVal = (PT.genesis__Party_Account_Name__r.Approved_NMI_From_ITR__c != null ? PT.genesis__Party_Account_Name__r.Approved_NMI_From_ITR__c : 0) - (PT.genesis__Party_Account_Name__r.Annual_Agriculture_Income_ITR__c != null ? PT.genesis__Party_Account_Name__r.Annual_Agriculture_Income_ITR__c : 0);
                        NmiApproved = NmiApproved + (aVal + Math.Min(((PT.genesis__Party_Account_Name__r.Annual_Agriculture_Income_ITR__c != null ? PT.genesis__Party_Account_Name__r.Annual_Agriculture_Income_ITR__c : 0) + (PT.genesis__Party_Account_Name__r.Agriculture2__c != null ? PT.genesis__Party_Account_Name__r.Agriculture2__c : 0 ))/12,Math.Min(((aVal/0.7)*0.3),200000.00/12)));
                    }
                }
            }
            }
            
            return NmiApproved;
        }
        return 0.00;
    }
    public static boolean Recursing=true;
    public static void CallNMI(List<genesis__Applications__c> APlist,Map<Id,genesis__Applications__c> oldApp){
        List<genesis__Applications__c> ApplicationList=new List<genesis__Applications__c>();
        for(genesis__Applications__c Ap:APlist){
            genesis__Applications__c oldPlan = oldApp.get(Ap.Id);
            if(((oldPlan.Sub_Stage__c!=Ap.Sub_Stage__c && Ap.Sub_Stage__c==Constants.Sub_Stage_PropInfoCaptured) && (ap.Record_Type_Name__c ==Constants.LAPLOAN || ap.Record_Type_Name__c ==Constants.HOMELOAN )) || ((oldPlan.Cost_Of_Construction__c!=Ap.Cost_Of_Construction__c || oldPlan.Cost_Of_Flat_House__c!=Ap.Cost_Of_Flat_House__c  || oldPlan.Cost_Of_Land__c!=Ap.Cost_Of_Land__c  || oldPlan.Cost_Of_Repair__c!=Ap.Cost_Of_Repair__c || oldPlan.Age_Of_The_Building__c!=Ap.Age_Of_The_Building__c  || oldPlan.Whether_The_Construction_Completed__c!=Ap.Whether_The_Construction_Completed__c) && ap.Record_Type_Name__c ==Constants.HOMELOAN) || (oldPlan.Sub_Stage__c!=Ap.Sub_Stage__c && Ap.Sub_Stage__c==Constants.Sub_Stage_WorkInfoCaptured && ap.Record_Type_Name__c ==Constants.PERSONALLOAN) ){     
                Calculations(ap.ID);
            }
        }
    }   
}