/*
* Name    : WS_UpdateEsignDocStatus
* Company : ET Marlabs
* Purpose : This class is used to Esign Status
* Author  : Subas
*/
@RestResource(urlMapping='/UpdateEsignDocStatus')
global class WS_UpdateEsignDocStatus {
    
    public DgioDocStatus DgioStatus;
    
    global class Response extends WS_Response{
        public Response(){
            
        }
    }
    public class DgioDocStatus{
        public String eSignStatus;
        public String docId;
    }
    
    @HttpPost
    global static Response getstatus(){
        RestRequest req      = Restcontext.Request;
        Response res         = new Response();    
        
        if(req == null || req.requestBody == null){
            res.status          = Constants.WS_ERROR_STATUS;
            res.errorMessage    = Constants.WS_REQ_BODY_IS_NULL;
            res.statusCode      = Constants.WS_ERROR_CODE;
            return res;            
        }
        else{
            
            try{
                String jsonData  = req.requestBody.toString();
                DgioDocStatus resSt = (DgioDocStatus)Json.deserialize(jsonData, DgioDocStatus.class);
                Document_Applicant__c doc = new Document_Applicant__c();
                doc.eSigned__c = Boolean.valueOf(resSt.eSignStatus);
                doc.Id = resSt.docId;
                // update doc;
                //Download document service 
                if(Doc.eSigned__c==true){
                    Document_Applicant__c Dc = [select id,Digio_Document_ID__r.Application__r.Id,Digio_Document_ID__r.Document_ID__c,Digio_Document_ID__r.name,Digio_Document_ID__r.Application__c,Account__c,eSigned__c from Document_Applicant__c where id=:doc.id]; 
                    system.debug('111111111'+Dc.Digio_Document_ID__r.Application__c);
                    
                    List<Document_Applicant__c> Doclist = [select id,Account__c,eSigned__c from Document_Applicant__c where  Digio_Document_ID__c=:Dc.Digio_Document_ID__c];
                    integer listSize=0;
                    for(Document_Applicant__c dList:Doclist){
                        if(dList.eSigned__c==true){
                            listSize++;
                        }
                        system.debug( listSize);
                    }
                    if(Doclist.size()==(listSize+1)){
                        system.debug('AAAAAsuccess');
                        String pdfContent = Digioe_Docs_Service.downloadDocument(Dc.Digio_Document_ID__r.Document_ID__c);  
                        system.debug('pdfContent'+pdfContent);
                        Digioe_Docs_Service.upsertDoc(Dc.Digio_Document_ID__r.Application__c,Dc.Digio_Document_ID__r.name+'.pdf',pdfContent);
                        //Initiate CBS API calls
                        List<genesis__Applications__c> App=[select id,Sub_Stage__c from 	genesis__Applications__c where id=:Dc.Digio_Document_ID__r.Application__r.Id];                      
                        if(App.size()>0){
                            if(App[0].Sub_Stage__c==Constants.Sub_Stage_DisbuApprov){
                                App[0].Initiate_CBS_Call__c=true;
                                try{
                       				 CBS_API_Calling_HL.call_CBS_API(Dc.Digio_Document_ID__r.Application__r.Id);
                                }
                                catch(exception e){
                                    system.debug('CBS Error'+e.getLineNumber()+e.getStackTraceString());
                                }
                                update App[0];
                            }
                        }
                    }
                  /*  List <Document_Applicant__c> docApplicantList = [Select Id,eSigned__c From Document_Applicant__c Where Digio_Document_ID__r.Application__r.Id =:Dc.Digio_Document_ID__r.Application__r.Id];
                    List<String> eSignedList = new List<String>();
                    for(Document_Applicant__c d : docApplicantList){
                        if(d.eSigned__c == True){
                            eSignedList.add(d.Id);
                        }                        
                    }
                    system.debug('docApplicantList.size()'+docApplicantList.size());
                     system.debug('eSignedList.size()'+eSignedList.size());
                    if(docApplicantList.size() == eSignedList.size()){
                        system.debug('docApplicantList.size()'+'eSignedList.size()'+eSignedList.size());
                        system.debug(Dc.Digio_Document_ID__r.Application__r.Id);
                        //Initiate CBS API calls
                        CBS_API_Calling_HL.call_CBS_API(Dc.Digio_Document_ID__r.Application__r.Id);
                    }
                    */
                    update doc;
                    system.debug('digio_Document_ID__c '+Dc.Digio_Document_ID__c);  
                    system.debug('digio_Document_ID__c '+Dc.Digio_Document_ID__r.Document_ID__c);  
                    
                    
                    res.status      = Constants.WS_SUCCESS_STATUS;
                    res.statusCode  = Constants.WS_SUCCESS_CODE;
                }
            }
            Catch(Exception e){
                res.status          = Constants.WS_ERROR_STATUS;
                res.errorMessage   = e.getMessage();
                res.statusCode  = Constants.WS_ERROR_CODE;
                system.debug('Exception::'+e.getLineNumber()+'error as'+e.getStackTraceString());
                return res;
            }
        }
        return res;
    }
    
}