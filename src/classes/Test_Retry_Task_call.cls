@isTest
public class Test_Retry_Task_call {
@isTest
    public static void method1(){
        genesis__Applications__c app=TestUtility.intialSetUp('TestName',true);
        
        Account acc=new Account(Name='testname');
        acc.Financial_Applicant__c=true;
        acc.Employment_Type__c='Salaried';
        
        insert acc;
        app.genesis__Account__c=acc.id;
        app.Loan_Purpose__c='Property not Identified';
        app.Sub_Stage__c= 'All NSTP Processes Completed';
        update app;
        
        genesis__Application_Parties__c par=new genesis__Application_Parties__c();
        par.genesis__Application__c=app.id;
        par.genesis__Party_Account_Name__c=acc.id;
        par.genesis__Party_Type__c='Co-Borrower';
        insert par;
            
        Task newTask = new Task(Description = 'Survey Transaction',
                                        Priority = 'Normal', 
                                        Status = 'Completed', 
                                        Subject = 'Verify application flags', 
                                        IsReminderSet = true, 
                                        ReminderDateTime = System.now()+1,
                                        WhatID=app.id
                                         );             
                insert newTask;

        
         Task newTask2 = new Task(Description = 'Survey Transaction',
                                        Priority = 'Normal', 
                                        Status = 'Completed', 
                                        Subject = 'Verify current address', 
                                        IsReminderSet = true, 
                                        ReminderDateTime = System.now()+1,
                                        WhatID=app.id
                                         );             
                insert newTask2;
        
        /*  app.Id=newTask.WhatId;
        update app;*/
        Retry_Task_call.recallTask(newTask);
        
    }
    
    
    @isTest
    public static void method2(){
        genesis__Applications__c app=TestUtility.intialSetUp('TestName',true);
        app.genesis__Loan_Amount__c=0;
        update app;
        Account acc=new Account(Name='testname');
        acc.Financial_Applicant__c=true;
        acc.Employment_Type__c='Salaried';
        
        insert acc;
        
        genesis__Application_Parties__c par=new genesis__Application_Parties__c();
        par.genesis__Application__c=app.id;
        par.genesis__Party_Account_Name__c=acc.id;
        par.genesis__Party_Type__c='Co-Borrower';
        insert par;
            
        Task newTask = new Task(Description = 'Survey Transaction',
                                        Priority = 'Normal', 
                                        Status = 'Completed', 
                                        Subject = 'Verify application flags', 
                                        IsReminderSet = true, 
                                        ReminderDateTime = System.now()+1,
                                        WhatID=app.id
                                         );             
                insert newTask;

        
         Task newTask2 = new Task(Description = 'Survey Transaction',
                                        Priority = 'Normal', 
                                        Status = 'Completed', 
                                        Subject = 'Verify current address', 
                                        IsReminderSet = true, 
                                        ReminderDateTime = System.now()+1,
                                        WhatID=app.id
                                         );             
                insert newTask2;
        
        /*  app.Id=newTask.WhatId;
        update app;*/
        Retry_Task_call.recallTask(newTask);
        
    }
    
    
    @isTest
    public static void method3(){
        genesis__Applications__c app=TestUtility.intialSetUp('TestName',true);
        
        Account acc=new Account(Name='testname');
        acc.Financial_Applicant__c=true;
        acc.Employment_Type__c='Salaried';
        acc.Verify_PAN_Number__c='Yes';
        
        insert acc;
        app.genesis__Account__c=acc.id;
        app.Loan_Purpose__c='Property not Identified';
        app.Sub_Stage__c= 'All NSTP Processes Completed';
        update app;
        
        genesis__Application_Parties__c par=new genesis__Application_Parties__c();
        par.genesis__Application__c=app.id;
        par.genesis__Party_Account_Name__c=acc.id;
        par.genesis__Party_Type__c='Co-Borrower';
        insert par;
            
        Task newTask = new Task(Description = 'Survey Transaction',
                                        Priority = 'Normal', 
                                        Status = 'Completed', 
                                        Subject = 'Approve income', 
                                        IsReminderSet = true, 
                                        ReminderDateTime = System.now()+1,
                                        WhatID=app.id
                                         );             
                insert newTask;

        
       
        Retry_Task_call.ValidateTask(newTask);
        
    }
    
}