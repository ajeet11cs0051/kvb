/*
* Name      : WS_CBS_Collateral_Creation
* Compnay   : ET Marlabs
* Purpose   : For HL Service(Collateral Creation) 
* Author    : Subas
*/
public class WS_CBS_Collateral_Creation {
    //@future (Callout=true)
    public static void Collateral_Creation(String appId,Map<string,string>accMap,String loanNo){
        genesis__Applications__c application = new genesis__Applications__c();
        application = queryService.getApp(appId);
        Collateral_Creation_Helper_HL cbs = new Collateral_Creation_Helper_HL();
        cbs.inputVariables = new Collateral_Creation_Helper_HL.cls_inputVariables();
        Collateral_Creation_Helper_HL.cls_inputVariables cbsCol = new Collateral_Creation_Helper_HL.cls_inputVariables();        
        cbsCol.in_msg = new Collateral_Creation_Helper_HL.cls_in_msg();
        cbs.inputVariables = cbsCol;
        cbs.exe_tenantId = 'cuecent_tenant';
        cbs.owner_tenantId = 'cuecent_tenant';
        cbsCol.in_msg.serviceName = 'Loan_Collateral_Creation_HL';
        cbsCol.in_msg.ApplicationId = appId;
        cbsCol.in_msg.CollateralCreationDetails = new List<Collateral_Creation_Helper_HL.cls_CollateralCreationDetails>();

        Collateral_Creation_Helper_HL.cls_CollateralCreationDetails rq = new Collateral_Creation_Helper_HL.cls_CollateralCreationDetails();
        rq.ApplicantId = application.Primary_Applicant_Id__c;
        rq.Channel ='BRN';
        rq.TransactionBranch = application.Branch_Code__c;
        //string uniqueno = CreateUniqueId.uniqID();
        //string regex = '[a-zA-Z]{1,}|\\-';
        //String unique = uniqueno.replaceAll(regex, '');    
        String CollId = '';
        if(loanNo != null){
            CollId = loanNo + '1';  //For one collateral Id creation
        }else{
            CollId = application.Loan_Account_Number__c + '1';
        }    
        rq.CollateralID = CollId;//unique.subString(0,10);	//must be unique
        rq.CollateralCode ='107';	        
        rq.BranchCode = application.Branch_Code__c;	
        rq.DocumentCode ='';	
        rq.FlgOperation ='A';	        
        if(application.Type_Of_Charge__c =='First Charge'){
            rq.TypeofCharge = '1';
        }else if(application.Type_Of_Charge__c =='Second Charge	'){
            rq.TypeofCharge = '2';
        }else if(application.Type_Of_Charge__c =='Third Party Charge	'){
            rq.TypeofCharge = '3';
        }else{
            rq.TypeofCharge = '';
        }        
        rq.NameofLender = '';//'KVB';//application.Name_of_Lender__c;	
        rq.OriginalValue = application.Total_Market_value__c != null ? String.ValueOf(application.Total_Market_value__c) : '';	
        rq.LastValue = application.Total_Market_value__c != null ? String.ValueOf(application.Total_Market_value__c) : '';	
        rq.MarketValue = application.Total_Market_value__c != null ? String.ValueOf(application.Total_Market_value__c) : '';	
        rq.DatOrginalValuation = application.Date_Of_Valuation__c != null ? String.ValueOf(application.Date_Of_Valuation__c).replace('-','') : '';	//Change format MW
        rq.DatLastValuation = application.Date_Of_Valuation__c != null ? String.ValueOf(application.Date_Of_Valuation__c).replace('-','') : '';	
        rq.DeedStatus = '';//application.Status_of_Deed__c;	
        rq.CustodianName = application.Name_of_Custodian__c;	
        rq.DateDeedsSent = '';//String.ValueOf(application.Sale_Deed_Date__c).replace('-','') : '';	
        rq.ExpectedReturnDate ='';	
        rq.DeedDetails ='deed';	
        rq.RegisteringAuth ='';	
        rq.InsurancePlanCode ='';	
        rq.InsurancePolicyNumber ='';	
        rq.InsurancePremiumBilling ='';	
        rq.BillingMode ='';	
        rq.Percentage ='';	
        rq.PremiumBilingAccount ='';	
        rq.InsurancePremiumAmount ='';	
        rq.NonStandardCollId ='';	
        rq.Description1 ='';	
        rq.Description2 ='';	
        rq.ChasisNumber ='';	
        rq.EngineNumber ='';	
        rq.RegistrationNumber ='';	
        rq.ModelNumber ='';	
        rq.YearofMfg ='';	
        rq.DescriptionAuto1 ='';	
        rq.DescriptionAuto2 ='';	
        rq.SecurityCode ='';	
        rq.NumberofUnits ='';	
        rq.SeriesNumber1 ='';	
        rq.SeriesNumber2 ='';	
       rq.Location = application.Location_Of_The_Property__c;	
        rq.AreaUnit ='squrefit';	
        rq.TotalArea = application.Area_of_the_Land__c;	
        rq.TypeofProperty ='0';//'Freehold' - 0;	
        rq.DatLeaseExpiry ='';	
        rq.ForcedSaleValue ='';	
        rq.QuitRentValue ='';	
        rq.DescriptionProp1 ='.';	
        rq.DescriptionProp2 ='.';	
        rq.Make ='';	
        rq.SLNum ='';	
        rq.RegNo ='';	
        rq.EngNo ='';	
        rq.HPCapacity ='';	
        rq.MachDesciption ='';	
        rq.AreaCult ='';	
        rq.AreaLand ='';	
        rq.DetailsCult ='';	
        rq.VillageNam ='';	
        rq.SurveyNo ='';	
        rq.SecDescription1 ='';	
        rq.SecDescription2 ='';	
        rq.Breed ='';	
        rq.Age ='';	
        rq.Quantity ='';	
        rq.CatDescription1 ='';	
        rq.CatDescription2 ='';	
        rq.IdentMArk ='';	
        rq.ASNAcctNumber ='';	
        rq.TotNumUnits ='';	
        rq.CertNumber ='';	
        rq.CertNumber1 ='';	
        rq.CertNumber2 ='';	
        rq.CertNumber3 ='';	
        rq.CertNumber4 ='';	
        rq.DivDate ='';	
        rq.SecurityCodec ='';	
        rq.GrossWeight ='';	
        rq.NetWeight ='';	
        rq.AppraiserValue ='';	
        rq.DateValuation ='';	
        rq.DescCommodity ='';	
        rq.ExtUniqueRefId ='1';  //make it qnique
        cbsCol.in_msg.CollateralCreationDetails.add(rq);
        getCollateral(JSON.serialize(cbs),appId,accMap,loanNo);
    }
    public static void getCollateral(String collateralData, String applicationId,Map<string,string>accMap,String loanNo){
        System.debug('#######'+collateralData);
        List<Account> accountList = new List<Account>();
        KVB_Endpoint_URLs__c kvb = KVB_Endpoint_URLs__c.getValues('CBS_Bulk_API');
        Map<String,String> headerMap                            = new Map<String,String>();
        headerMap.put('Content-Type','application/json');
        HTTPResponse response                                   = new HTTPResponse();
        String endPoint                                         = kvb.Endpoint_URL__c;
        response = HttpUtility.sendHTTPRequest(endPoint, 'POST', null,collateralData,headerMap,label.CA_CERTIFICATE);   
        system.debug('#####'+response);
        system.debug('$$$$'+response.getBody());
        If(response.getStatusCode() == 200 || response.getStatusCode() == 201){
            String jsonString = response.getBody();
            Collateral_Creation_Helper_HL.CBS_ColletralCreation_response res = (Collateral_Creation_Helper_HL.CBS_ColletralCreation_response)JSON.deserialize(jsonString, Collateral_Creation_Helper_HL.CBS_ColletralCreation_response.class);
            system.debug('**Res**'+res);
            if(accMap != null){
            if(accMap.size()>0){
                for(String str : accMap.keySet()){
                    if(str.length()>=15){
                        account acc = new account();
                        acc.Id = str;
                        acc.CBS_Customer_ID__c = accMap.get(str);
                        accountList.add(acc);
                    }
                }
                if(accountList.size()>0){
                    update accountList;                
                }
            }
        }
            if(loanNo != null && applicationId !=null){
                genesis__Applications__c a= new genesis__Applications__c();
                a.Id = applicationId;
                a.Loan_Account_Number__c = loanNo;
                 a.Interest_Variation__c = WS_CBS_Loan_Creation_HL.VariationHL(applicationId);
                update a;
            }            
            if(res.out_msg.Status_Desc != 'Success'){
                CBS_API_Log__c log = new CBS_API_Log__c();    
                log.Application__c = applicationId;
                log.API_Name__c = 'Loan_Collateral_Creation_HL';
                log.Status__c = 'Failure';
                log.Success_Error_Message__c = res.out_msg.Status_Desc;
                log.Sequence_No__c = '6';  
                insert log; 
            }
        }else{
            CBS_API_Log__c log = new CBS_API_Log__c();    
            log.Application__c = applicationId;
            log.API_Name__c = 'Loan_Collateral_Creation_HL';
            log.Status__c = 'Failure';
            log.Success_Error_Message__c = 'CollateralCreation_CBS_Error_No_Hit';
            log.Sequence_No__c = '6';  
            insert log; 
        }
    }
}