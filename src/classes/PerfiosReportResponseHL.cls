/*
* Name          : PerfiosReportResponseHL
* Description   : Structurize Perfios data, to be inserted in the system
* Author        : Venu Gopal
*/
public class PerfiosReportResponseHL{
	public cls_customerInfo customerInfo;
	public cls_summaryInfo summaryInfo;
	public cls_monthlyDetails[] monthlyDetails;
	public cls_eODBalances[] eODBalances;
	public cls_highValueDebitXns[] highValueDebitXns;
	public cls_regularCredits[] regularCredits;
	public cls_highValueCreditXns[] highValueCreditXns;
	public cls_accountXns[] accountXns;
	public cls_customUserInfo customUserInfo;
	public cls_AdditionalMonthlyDetails[] AdditionalMonthlyDetails;
	public cls_AdditionalSummaryDetails AdditionalSummaryDetails;
	public cls_AdditionalForm26ASInfo AdditionalForm26ASInfo;
	public class cls_customerInfo {
		public String name;	//Shangu Shiva
		public String address;	//34 Tafiq Lane, Bangalore, Karnataka 560034
		public String landline;	//080-12345678
		public String mobile;	//9881234567
		public String email;	//shangu@perfios.com
		public String pan;	//AGMPV6437A
		public String perfiosTransactionId;	//AE241522146923157
		public String customerTransactionId;	//ae348f79-d0e8-bddf-c9ff-2edca11f2496
		public String bank;	//Acme Bank Ltd., India
		public Double instId;	//998
	}
	public class cls_summaryInfo {
		public String instName;	//Acme Bank Ltd., India
		public String accNo;	//5-5XX4XX-X06
		public String accType;	//Savings
		public Double fullMonthCount;	//4
		public cls_total total;
		public cls_average average;
	}
	public class cls_total {
		public Double bal15;	//468169.8
		public Double bal25;	//392712.46
		public Double bal5;	//258017.46
		public Double balAvg;	//383126.28
		public Double balLast;	//385125.95
		public Double balOpen;	//256892.06
		public Double credits;	//42
		public Double debits;	//99
		public Double emiIssues;	//0
		public Double inwBounces;	//0
		public Double inwECSBounces;	//0
		public Double outwBounces;	//0
		public Double outwECSBounces;	//0
		public Double salaries;	//0
		public Double stopPayments;	//0
		public Double totalCredit;	//564631.98
		public Double totalDebit;	//436398.09
		public Double totalEmiIssue;	//0
		public Double totalInvIncome;	//51541.81
		public Double totalInwECSBounce;	//0
		public Double totalOutwECSBounce;	//0
		public Double totalSalary;	//0
		public Double totalTransferOut;	//25200
	}
	public class cls_average {
		public Double bal15;	//117042.45
		public Double bal25;	//98178.12
		public Double bal5;	//64504.36
		public Double balAvg;	//95781.57
		public Double balLast;	//96281.49
		public Double balOpen;	//64223.01
		public Double credits;	//10
		public Double debits;	//25
		public Double emiIssues;	//0
		public Double inwBounces;	//0
		public Double inwECSBounces;	//0
		public Double outwBounces;	//0
		public Double outwECSBounces;	//0
		public Double salaries;	//0
		public Double stopPayments;	//0
		public Double totalCredit;	//141157.99
		public Double totalDebit;	//109099.52
		public Double totalEmiIssue;	//0
		public Double totalInvIncome;	//12885.45
		public Double totalInwECSBounce;	//0
		public Double totalOutwECSBounce;	//0
		public Double totalSalary;	//0
		public Double totalTransferOut;	//6300
	}
	public class cls_monthlyDetails {
		public Double bal15;	//83373.65
		public Double bal25;	//63408.6
		public Double bal5;	//1366.83
		public Double balAvg;	//56923.54
		public Double balLast;	//62012.06
		public Double balOpen;	//1166.83
		public Double credits;	//7
		public Double debits;	//26
		public Double emiIssues;	//0
		public Double inwBounces;	//0
		public Double inwECSBounces;	//0
		public String monthName;	//Nov-17
		public Double outwBounces;	//0
		public Double outwECSBounces;	//0
		public Double salaries;	//0
		public String startDate;	//2017-11-01
		public Double stopPayments;	//0
		public Double totalCredit;	//111605.26
		public Double totalDebit;	//50760.03
		public Double totalEmiIssue;	//0
		public Double totalInvIncome;	//0
		public Double totalInwECSBounce;	//0
		public Double totalOutwECSBounce;	//0
		public Double totalSalary;	//0
		public Double totalTransferOut;	//4800
	}
	public class cls_eODBalances {
		public String eODBalanceDate;	//2017-11-01
		public Double balance;	//1366.83
	}
  public  class cls_highValueDebitXns {
		public String hvdXnsDate;	//2017-11-02
		public String chqNo;	//1497
		public String narration;	//To Clg : Camlin Ltd KOKUYO CAMLIN
		public Double amount;	//-144295
		public String category;	//Transfer out
		public Double balance;	//296266.79
	}
	public class cls_regularCredits {
		public String regularChangeDate;	//2017-11-15
		public Double regularChangeGroup;	//1
		public String chqNo;	//
		public String narration;	//EBA//20091105083330
		public Double amount;	//16459.21
		public String category;	//Others
		public Double balance;	//98413.25
    
	}
  public  class cls_highValueCreditXns {
		public String hvcXnsDate;	//2017-11-23
		public String chqNo;	//
		public String narration;	//BLUECLICK OFFICE AUTOMATION INDIA-085966
		public Double amount;	//439434
		public String category;	//Others
		public Double balance;	//723274.79
	}
	public class cls_accountXns {
		public String accountNo;	//5-5XX4XX-X06
		public String accountType;	//Savings
		public cls_xns[] xns;
	}
	public class cls_xns {
		public String xnsDate;	//2018-02-12
		public String chqNo;	//
		public String narration;	//EBA//20100202081454
		public Double amount;	//16720.37
		public String category;	//Others
		public Double balance;	//198113.93
	}
	public class cls_customUserInfo {
		public String emiAmount;	//123.00
		public String employmentType;	//Salaried
		public String productType;	//HomeLoan
		public String facility;	//CC
	}
	public class cls_AdditionalMonthlyDetails {
		public String monthName;	//Nov-17
		public Double avgBalance;	//49383.03
	}
	public class cls_AdditionalSummaryDetails {
		public Double avgBalanceOf3Months;	//108734.25
		public Double avgBalanceOf6Months;	//0
		public Double avgBalanceOf12Months;	//0
		public Double avgCreditsOf6Months;	//0
		public Double avgDebitsOf6Months;	//0
		public Double avgTotalCreditOf6Months;	//0
		public Double avgTotalDebitOf6Months;	//0
		public Double avgBalanceOf6MonthsToEMI;	//0
		public String salaryContinuous;	//No
		public Double avgEstimatedIncome;
	}
	public class cls_AdditionalForm26ASInfo {
	}
	public static PerfiosReportResponseHL parse(String json){
		return (PerfiosReportResponseHL) System.JSON.deserialize(json, PerfiosReportResponseHL.class);
	}
}