/*
* Name          : SME_BRE_Batch
* Description   : Run BRE
* Author        : Dushyant
*/
global class SME_BRE_Batch implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext BC){
        Map<String, Schema.SObjectField> accountFieldMap = Schema.getGlobalDescribe().get('genesis__Applications__c' ).getDescribe().fields.getMap();
        
        String query = 'SELECT ';    
        for(Schema.SObjectField s : accountFieldMap.values()){
            query = query + s.getDescribe().getName()+',';    
        }
        query   = query + 'genesis__Account__r.Date_of_Incorporation__c,genesis__Account__r.CUSTSINCE__c,(SELECT Id,genesis__Party_Account_Name__r.CIBIL_Status__pc FROM genesis__Application_Parties__r WHERE Active__c = true AND genesis__Party_Account_Name__r.CIBIL_Score__c != null) FROM genesis__Applications__c WHERE Application_Stage__c = \'Identified for renewal\' AND Qualitative_input_done__c=true AND CommercialPR_Stage__c = \'Completed\' AND List_Matching_Stage__c = \'Completed\' AND Active__c = true';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<genesis__Applications__c> appList){
        //System.debug('appList.size()::'+appList.size());
        Integer renewDuration;
        List<genesis__Applications__c> applicationList = new List<genesis__Applications__c>();
        for(genesis__Applications__c app : appList){
            renewDuration = System.today().daysBetween(app.Renewal_Due_Date__c);
            if(renewDuration <= 30 && app.genesis__Application_Parties__r.size() > 0){
                Integer partyCount = 0;
                for(genesis__Application_Parties__c party : app.genesis__Application_Parties__r){
                    if(party.genesis__Party_Account_Name__r.CIBIL_Status__pc == 'Complete'){
                        partyCount += 1;
                    }
                    if(partyCount == app.genesis__Application_Parties__r.size())applicationList.add(app);
                }
            }
        }
        
        if(!applicationList.isEmpty()){
            for(genesis__Applications__c app : applicationList ){
                try{
                    //Secound parameter to specify if its BRE first run
                    SME_BRE_Score_Calculator.runBRERule(app,true);    
                }
                catch(Exception e){}
            }
        }
    }
    
    global void finish(Database.BatchableContext BC){}
}


//