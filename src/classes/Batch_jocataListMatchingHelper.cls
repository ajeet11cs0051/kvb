public class Batch_jocataListMatchingHelper {
    
    public static String getJocataTocken(genesis__Applications__c appObj){
        
        try{
            JocataListMatching_Model.JocataLMRequest jlmReq = new JocataListMatching_Model.JocataLMRequest();
            jlmReq.name= Utility.getBlankStringIfNull(appObj.genesis__Account__r.Name); 
            jlmReq.customerId= Utility.getBlankStringIfNull(appObj.genesis__Account__r.CBS_Customer_ID__c); 
            if(appObj.genesis__Account__r.PersonBirthdate != null)
                jlmReq.dob= Utility.getBlankStringIfNull(Utility.dateFormatter(appObj.genesis__Account__r.PersonBirthdate));   
            jlmReq.address= Utility.getBlankStringIfNull(appObj.genesis__Account__r.BillingStreet);   
            jlmReq.city= Utility.getBlankStringIfNull(appObj.genesis__Account__r.BillingCity);    
            jlmReq.state= Utility.getBlankStringIfNull(appObj.genesis__Account__r.BillingState);  
            jlmReq.country= Utility.getBlankStringIfNull(appObj.genesis__Account__r.BillingCountry);  
            jlmReq.nationality= Utility.getBlankStringIfNull(appObj.genesis__Account__r.Nationality__pc);   
            jlmReq.panId= Utility.getBlankStringIfNull(appObj.genesis__Account__r.Pan_Number__c);   
            jlmReq.driverlicenseid= Utility.getBlankStringIfNull(appObj.genesis__Account__r.Driving_License_ID__c); 
            jlmReq.adharNo= Utility.getBlankStringIfNull(appObj.genesis__Account__r.Aadhaar_Number__pc);    
            jlmReq.rationCardNo= Utility.getBlankStringIfNull(appObj.genesis__Account__r.Ration_Card__c);   
            jlmReq.phone1= Utility.getBlankStringIfNull(appObj.genesis__Account__r.PersonMobilePhone);  
            jlmReq.phone2= Utility.getBlankStringIfNull(appObj.genesis__Account__r.Phone);  
            jlmReq.email= Utility.getBlankStringIfNull(appObj.genesis__Account__r.PersonEmail);
            
            string reqStr = JSON.serialize(jlmReq);
            System.debug('req '+reqStr);
            return makeJocataCall(reqStr);                       
        }catch(Exception Ex){
            System.debug('Exception'+ex.getMessage()+ex.getLineNumber());
            return null;
        }
    }
    
    public static string getJocataTxnforAcc(Account accountInfo){
        try{
            JocataListMatching_Model.JocataLMRequest jlmReq = new JocataListMatching_Model.JocataLMRequest();
            jlmReq.name= Utility.getBlankStringIfNull(accountInfo.Name); 
            jlmReq.customerId= Utility.getBlankStringIfNull(accountInfo.CBS_Customer_ID__c); 
            if(accountInfo.PersonBirthdate != null)
                jlmReq.dob= Utility.getBlankStringIfNull(Utility.dateFormatter(accountInfo.PersonBirthdate));   
            jlmReq.address= Utility.getBlankStringIfNull(accountInfo.BillingStreet);   
            jlmReq.city= Utility.getBlankStringIfNull(accountInfo.BillingCity);    
            jlmReq.state= Utility.getBlankStringIfNull(accountInfo.BillingState);  
            jlmReq.country= Utility.getBlankStringIfNull(accountInfo.BillingCountry);  
            jlmReq.nationality= Utility.getBlankStringIfNull(accountInfo.Nationality__pc);   
            jlmReq.panId= Utility.getBlankStringIfNull(accountInfo.Pan_Number__c);   
            jlmReq.driverlicenseid= Utility.getBlankStringIfNull(accountInfo.Driving_License_ID__c); 
            jlmReq.adharNo= Utility.getBlankStringIfNull(accountInfo.Aadhaar_Number__pc);    
            jlmReq.rationCardNo= Utility.getBlankStringIfNull(accountInfo.Ration_Card__c);   
            jlmReq.phone1= Utility.getBlankStringIfNull(accountInfo.PersonMobilePhone);  
            jlmReq.phone2= Utility.getBlankStringIfNull(accountInfo.Phone);  
            jlmReq.email= Utility.getBlankStringIfNull(accountInfo.PersonEmail);
            
            string reqStr = JSON.serialize(jlmReq);
            System.debug('req '+reqStr);
            return makeJocataCall(reqStr);                       
        }catch(Exception Ex){
            System.debug('Exception'+ex.getMessage()+ex.getLineNumber());
            return null;
        }
    }
    
    public static string makeJocataCall(String requestString){
        HTTPResponse response = new HTTPResponse();
        Map<String,String> headerMap = new Map<String,String>();  
        headerMap.put('Content-Type','application/json'); 
        headerMap.put('Authorization',WS_ApiGatewayAccToken.getAccTkn());
        System.debug('Header'+headerMap); //Listmatching
        response = HttpUtility.sendHTTPRequest(Utility.getFullEndpoints('Listmatching'), 'POST', null,requestString,headerMap,null);
        
        System.debug('@@@@ response.getStatusCode()'+response.getStatusCode());
        System.debug(response.getBody());
        
        if(response.getStatusCode() == 200 || response.getStatusCode() == 201){
            
            String jsonData = response.getBody();
            
            JocataListMatching_Model.JocataLMResponse jlmRes = (JocataListMatching_Model.JocataLMResponse)Json.deserialize(jsonData, JocataListMatching_Model.JocataLMResponse.class);
            
            return String.ValueOf(jlmRes.evokeResponse.txnId);
            
        }else{
            return null;
        }
    }
}