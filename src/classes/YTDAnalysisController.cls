/**
* Created by ET-MARLABS on 21-05-2018.
*/

public with sharing class YTDAnalysisController {

    /**
    * get Estimate Sales Analysis Flag
    *
    * @param sObjectType - The type to register
    *
    */

    public static Map<String , Boolean> creditorDebtorsAnalysisMap      = new Map<String, Boolean>();

    public static Map<String , Boolean> ytdAnalysisMap                  = new Map<String, Boolean>();

    public static Map<String,List<Credit_Underwriting__c>>  getAllFlagAnalysis(String losCustId,String appId){


        String recordTypeName                                           = SOQL_Util.getRecordTypeNameByRecordId(appId);
        Boolean isExceedingAdhocLessThanFifty                           = false;

        if(recordTypeName==Constants.SME_APP_RECORD_TYPE_EXCEEDING || recordTypeName== Constants.SME_APP_RECORD_TYPE_ADHOC)
            isExceedingAdhocLessThanFifty                               = YTDAnalysisHelper.isExceedingAdhocLessThanFifty(appId) != null ? YTDAnalysisHelper.isExceedingAdhocLessThanFifty(appId) :false ;

        System.debug('@@@@isExceedingAdhocLessThanFifty---> '+isExceedingAdhocLessThanFifty);
        System.debug('@@@@recordTypeName---> '+recordTypeName);

        Map<String,List<Credit_Underwriting__c>> allFlagMap             = new Map<String,List<Credit_Underwriting__c>>();
        If(losCustId !=null && appId !=null ){
            String Str                                                  = YTDAnalysisHelper.getEstimateSalesAnalysis(losCustId,appId);
            system.debug('String@@@@'+Str);
            If(ytdAnalysisMap !=null){
                // send false for enhancement
                List<Credit_Underwriting__c> listOfCredit               = new List<Credit_Underwriting__c>();
                /*if(recordTypeName==Constants.SME_APP_RECORD_TYPE_EXCEEDING || recordTypeName== Constants.SME_APP_RECORD_TYPE_ADHOC){
                    listOfCredit = getCreditUnderwritingYTD(ytdAnalysisMap,isExceedingAdhocLessThanFifty);
                }else{
                    listOfCredit = getCreditUnderwritingYTD(ytdAnalysisMap,false);
                }*/
                listOfCredit                                            = getCreditUnderwritingYTD(ytdAnalysisMap,isExceedingAdhocLessThanFifty);

                If(listOfCredit !=null && !listOfCredit.isEmpty()){
                    allFlagMap.put('YTD Analysis',listOfCredit);
                    System.debug('List of Credit Underwriting '+listOfCredit);
                }
            }
        }
        If(losCustId !=null && recordTypeName==Constants.SME_APP_RECORD_TYPE_ENHANCEMENT){
            List<Credit_Underwriting__c> listOfCredit = new List<Credit_Underwriting__c>();
            listOfCredit                                                = YTDAnalysisHelper.getCreditorDebitorsFlag(losCustId);
            If(!listOfCredit.isEmpty()){
                allFlagMap.put('Debtors/Creditors Analysis',listOfCredit);
            }
        }
        If(allFlagMap !=null){
            System.debug('All MAP Flag'+allFlagMap);
            return allFlagMap;
        }
        return new Map<String,List<Credit_Underwriting__c>>();
    }


    //Flag update in Credit Underwriting object for YTD Analysis.
    public static List<Credit_Underwriting__c> getCreditUnderwritingYTD(Map<String , Boolean> ytdAnalysisMap,Boolean isExceedingAdhocLessThanFifty){

        String appraisal = 'YTD Analysis';

        If(ytdAnalysisMap !=null){
            
            List<Credit_Underwriting__c> listOfCredit                   =   new List<Credit_Underwriting__c>();
            for(String str    : ytdAnalysisMap.keySet()){

                If(str !=null && str !='' && !(isExceedingAdhocLessThanFifty)){

                    Credit_Underwriting__c creditUnderObj               = new Credit_Underwriting__c();
                    creditUnderObj.Appraisal__c                         = appraisal;
                    creditUnderObj.Variable_type__c                     = str;
                    creditUnderObj.Flags_Value__c                       = String.valueOf(ytdAnalysisMap.get(str));
                    creditUnderObj.Threshold_breached__c                = true;
                    listOfCredit.add(creditUnderObj);

                }else if(str !=null && str !=''){
                    if(str != 'YTD Sales Analysis' && str != 'YTD Purchase Analysis')
                        continue;
                    else{
                        Credit_Underwriting__c creditUnderObj           = new Credit_Underwriting__c();
                        creditUnderObj.Appraisal__c                     = appraisal;
                        creditUnderObj.Variable_type__c                 = str;
                        creditUnderObj.Flags_Value__c                   = String.valueOf(ytdAnalysisMap.get(str));
                        creditUnderObj.Threshold_breached__c            = true;
                        listOfCredit.add(creditUnderObj);
                    }

                }
            }
            If(!listOfCredit.isEmpty()){
                return listOfCredit;
            }

        }
        return new List<Credit_Underwriting__c>();
    }


}