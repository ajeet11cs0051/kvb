/*
* Name          : DigioDocID_EsignHL
* Description   : Generating Document IDS for HL 
* Author        : VenuGopal N
*/

public class DigioDocID_EsignHL {
    
    public static string DocumentIDGeneration(string Appid,String Filename,integer Pgcount,string class2doc){
          
        try{
        
        	 WS_GetEsignDocID.Response resp	= new WS_GetEsignDocID.Response(); 
       		 resp.eSignRes			= new WS_GetEsignDocID.eSignResponse();
           List<String> emailList= new List<String>();
            List<Digio_ESign_Service.cls_signers> signList    = new List<Digio_ESign_Service.cls_signers>();
            List<genesis__Application_Parties__c> parties   = new List<genesis__Application_Parties__c>();
            Digio_ESign_Service.ESignRequest eSignReq       = new Digio_ESign_Service.ESignRequest(); 
            Map<String,Map<Integer,List<Object>>> signMap   = new Map<String,Map<Integer,List<Object>>>();
       
        
         List<genesis__Applications__c> ApList=[select id,genesis__Account__r.Aadhaar_Number__pc,genesis__Account__r.PersonMobilePhone,(select id,Title_Holder__c,genesis__Party_Account_Name__r.PersonMobilePhone,genesis__Party_Account_Name__r.Aadhaar_Number__pc,genesis__Party_Type__c from genesis__Application_Parties__r where Active__c=true  AND genesis__Party_Account_Name__r.PersonEmail != null AND genesis__Party_Account_Name__r.PersonMobilePhone != null) from genesis__Applications__c where ID=:Appid];
       
            system.debug('ApList'+ApList);
            for(genesis__Applications__c App:ApList){
          //  emailList.add(App.genesis__Account__r.PersonEmail);
            string PAaadhar = App.genesis__Account__r.Aadhaar_Number__pc!=null?App.genesis__Account__r.Aadhaar_Number__pc:'';
                PAaadhar=WS_getAadharNo.getAadharNo(PAaadhar);
                system.debug('Applicant PAaadhar'+PAaadhar);
           // signList.add(new Digio_ESign_Service.cls_signers(App.genesis__Account__r.PersonEmail,PAaadhar,Constants.EMAIL_REASON));
                if(Filename!=Constants.A46_HL){
            signList.add(new Digio_ESign_Service.cls_signers(App.genesis__Account__r.PersonMobilePhone,PAaadhar,Constants.MOB_REASON));
                }
            
            for(genesis__Application_Parties__c Parti:App.genesis__Application_Parties__r){
            // emailList.add(Parti.genesis__Party_Account_Name__r.PersonEmail);
               // if(Constants.DOC_NAME_HL_BORROWER.contains(Filename)){
                   
                   // if(Parti.genesis__Party_Type__c==Constants.Co_Borrower){
                   string aadhar ;            
                       if(Filename==Constants.A46_HL){
                         if(Parti.genesis__Party_Type__c==Constants.Gurantor){
                           aadhar = Parti.genesis__Party_Account_Name__r.Aadhaar_Number__pc!=null?Parti.genesis__Party_Account_Name__r.Aadhaar_Number__pc:'';
                         }
                    
                    }
                else if(Filename==Constants.B17_HL || Filename==Constants.A23_HL){
                     if(Parti.genesis__Party_Type__c==Constants.Co_Borrower){
                           aadhar = Parti.genesis__Party_Account_Name__r.Aadhaar_Number__pc!=null?Parti.genesis__Party_Account_Name__r.Aadhaar_Number__pc:'';
                         }
          		 
                      //  aadhar=WS_getAadharNo.getAadharNo(aadhar);
                }
                 else if(Filename==Constants.B1_HL || Filename==Constants.B2_HL){
                     if(Parti.Title_Holder__c){
                           aadhar = Parti.genesis__Party_Account_Name__r.Aadhaar_Number__pc!=null?Parti.genesis__Party_Account_Name__r.Aadhaar_Number__pc:'';
                         }
                 }
               
                else{
                     aadhar = Parti.genesis__Party_Account_Name__r.Aadhaar_Number__pc!=null?Parti.genesis__Party_Account_Name__r.Aadhaar_Number__pc:''; 
                }
                          system.debug('Co -Applicant PAaadhar'+aadhar);
           		 //signList.add(new Digio_ESign_Service.cls_signers(Parti.genesis__Party_Account_Name__r.PersonEmail,aadhar,Constants.EMAIL_REASON));
           		
                if(aadhar!=null){
                     aadhar=WS_getAadharNo.getAadharNo(aadhar);
                signList.add(new Digio_ESign_Service.cls_signers(Parti.genesis__Party_Account_Name__r.PersonMobilePhone,aadhar,Constants.MOB_REASON));
                }
                   // }
               // }
                
            }
                
            
        }
        signMap= SMESanction_DocReq.genCordStructure((Integer)Pgcount,appId,Filename);
            system.debug('signMap'+signMap);
      // string  class2SignedDoc=class2doc;
            eSignReq.exe_tenantId	= 'cuecent_tenant';
            eSignReq.owner_tenantId	= 'cuecent_tenant';
            eSignReq.inputVariables	= new Digio_ESign_Service.cls_inputVariables();
            eSignReq.inputVariables.in_msg = '';
            eSignReq.inputVariables.inputMap = new Digio_ESign_Service.cls_map();
            eSignReq.inputVariables.inputMap.signers = new List<Digio_ESign_Service.cls_signers>();
            eSignReq.inputVariables.inputMap.signers = signList;
            eSignReq.inputVariables.inputMap.display_on_page ='custom';
            eSignReq.inputVariables.inputMap.expire_in_days =integer.valueOf(system.label.DIgio_Expiry_Days);
            eSignReq.inputVariables.inputMap.file_Name = Filename+'_'+appId;
            eSignReq.inputVariables.inputMap.file_data = class2doc;
            eSignReq.inputVariables.inputMap.sign_coordinates = signMap;
            eSignReq.inputVariables.inputMap.notify_signers = true;
            eSignReq.inputVariables.inputMap.send_sign_link = true;
        resp.eSignRes.docID =Digio_ESign_Service.getESigned(eSignReq);
        return  resp.eSignRes.docID;
        
    }
    
    catch(exception e){
        system.debug('Error line'+e.getLineNumber()+'error trace tag'+e.getStackTraceString());
        return null;
    }
       
    } 
    
}