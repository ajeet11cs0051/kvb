/*
* Name          : SME_Digio_Service
* Description   : Handle SME Digio Service(Doc Generation, Class-2 Sign and E-Sign)
* Author        : Amritesh
*/ 
public class SME_Digio_Service {
    
    // Purpose: This method handles document generation and Class-2 Sign 
    // and returns class-2 signed pdf document for SME 
    public static WS_GenerateDocumentDigio.Response docGenAndSignDoc(String appId){
        
        WS_GenerateDocumentDigio.Response resp  = new WS_GenerateDocumentDigio.Response(); 
        resp.docResObj          = new WS_GenerateDocumentDigio.DOC_Response();
        String docEncodeFile    = '';
        Integer pageCount       = 0;
        
        genesis__Applications__c appln = [Select id,recordType.developerName,Application_Stage__c,Document_Page_Count__c,genesis__Account__c,Sanction_Doc_Generation_Check__c,Sanction_Class2_Check__c,Sanction_Letter_Name__c,Sanction_ESign_Check__c,
                                          (Select id,genesis__Party_Account_Name__r.Aadhaar_Number__pc,genesis__Party_Account_Name__r.PersonMobilePhone,Identifier__c,Signer_page_no__c,llx__c,lly__c,urx__c,ury__c 
                                           from genesis__Application_Parties__r WHERE  genesis__Party_Account_Name__r.PersonMobilePhone != null AND Active__c = true AND genesis__Party_Account_Name__r.RecordType.DeveloperName ='PersonAccount' AND Signatories__c =true) 
                                          from genesis__Applications__c where Id =: appId];
        
        if(appln.genesis__Application_Parties__r.size() > 0) appln.Cibil_Charges__c                     = appln.genesis__Application_Parties__r.size()*100;
        if(appln.Application_Stage__c == 'Interim Sanction' || appln.Application_Stage__c == 'Application filling initiated'){
            DigioTemplateService.SME_DOCUMENT_NAME  = Constants.PROV_SANCTION_TEMPLATE_SME;
            appln.Application_Stage__c              = 'Interim sanction offered';
        }
        else if(appln.Application_Stage__c == 'Final Sanction' || appln.Application_Stage__c== Constants.APP_FILLING_FINAL_STAGE){
            DigioTemplateService.SME_DOCUMENT_NAME      = Constants.SANCTION_TEMPLATE_SME;
            appln.Application_Stage__c                  = 'Final Sanction Offered';
        }
        System.debug(DigioTemplateService.SME_DOCUMENT_NAME);
        if(!appln.Sanction_Doc_Generation_Check__c && DigioTemplateService.SME_DOCUMENT_NAME != null){
            
            string templateId   = Utility.getDIGIOTemplateId(DigioTemplateService.SME_DOCUMENT_NAME);
            
            //Calling the document DIGIO generation service
            Digioe_Docs_Service.DocGenResponse docResp = Digioe_Docs_Service.getEDocs(appId,DigioTemplateService.SME_DOCUMENT_NAME,templateId);
            docEncodeFile = docResp.outMap.Data.outfile;
            pageCount     = Integer.valueOf(docResp.outMap.Data.totalpages);
            system.debug('docEncodeFile'+docEncodeFile);            
            if(docEncodeFile == '' || docEncodeFile == null){
                throw new CustomException('Document Generation Failed');
            }
            appln.Sanction_Doc_Generation_Check__c  = true;
            appln.Sanction_Letter_Name__c           = DigioTemplateService.SME_DOCUMENT_NAME;        
        }
        if(!appln.Sanction_Class2_Check__c && DigioTemplateService.SME_DOCUMENT_NAME != null){
            
            KVB_Company_Details__c company = KVB_Company_Details__c.getOrgDefaults();
            //Getting the stored docuement            
            docEncodeFile  = docEncodeFile != '' ? docEncodeFile : EncodingUtil.base64Encode(Digioe_Docs_Service.getAttachmentBody(appId,DigioTemplateService.SME_DOCUMENT_NAME+'.pdf'));
            
            //Calling the Class-2 service
            string class2Doc = Digioe_Docs_Service.class2Signer(new Digioe_Docs_Service.DocSignerRequest(SYSTEM.LABEL.DigioKeyPersonName,DigioTemplateService.SME_DOCUMENT_NAME,company.City__c,docEncodeFile),pageCount,DigioTemplateService.SME_DOCUMENT_NAME);
            if(class2Doc == null && docEncodeFile != null) {
                Digioe_Docs_Service.upsertDoc(appId,DigioTemplateService.SME_DOCUMENT_NAME+'.pdf',docEncodeFile);
            }else{
                Digioe_Docs_Service.upsertDoc(appId,DigioTemplateService.SME_DOCUMENT_NAME+'.pdf',class2Doc);
            }
            
            if(class2Doc == '' || class2Doc == null){
                throw new CustomException('Class-2 Sign Failed');
            }
            appln.Sanction_Class2_Check__c  = true;
            appln.Document_Page_Count__c    = pageCount;
            appln.Sanction_Letter_Name__c   = DigioTemplateService.SME_DOCUMENT_NAME;  
            appln.Sanction_Date__c          = DigioTemplateService.Sanction_Date;
            appln.Next_Renewal_Date__c      = DigioTemplateService.NEXT_RENEW_DATE;
            appln.Processing_Charges__c     = DigioTemplateService.FINAL_PROCESSING_CHARGE;
            resp.docResObj.LOS_APPID        = appId;
            resp.docResObj.docPDFContent    = class2Doc;
            resp.status                     = Constants.WS_SUCCESS_STATUS;
            resp.statusCode                 = Constants.WS_SUCCESS_CODE;
            update appln;
        }
        try{
            system.debug('DigioTemplateService.facility::'+DigioTemplateService.facilityUpdate);
            if(DigioTemplateService.facilityUpdate.size() > 0){
                update DigioTemplateService.facilityUpdate;
            }
        }catch(Exception ex){}
        
        SavePoint sp = Database.setSavepoint();
        try{
            updatePartySignerDetail(appln);
        }
        catch(Exception e){
            System.debug(e.getMessage());
            System.debug(e.getLineNumber());
            Database.rollback(sp);
        }
        return resp;
    }
    public static WS_GetEsignDocID.Response doESignUpdated(String appId,String partyId){
        genesis__Applications__c            appln   = new genesis__Applications__c();
        genesis__Application_Parties__c     partyToUpdate   = new genesis__Application_Parties__c();
        String class2SignedDoc  = '';
        //Boolean partySignDone = false;
        //Boolean otherPartySigned = true;
        WS_GetEsignDocID.Response resp  = new WS_GetEsignDocID.Response(); 
        resp.eSignRes           = new WS_GetEsignDocID.eSignResponse();
        
        appln =[SELECT Id,Sanction_Class2_Check__c,Sanction_Doc_Generation_Check__c,Sanction_ESign_Check__c,Sanction_Letter_Name__c,Application_Stage__c,Sanction_ESign_Id__c,(SELECT Id,Status__c,genesis__Party_Account_Name__r.Aadhaar_Number__pc,genesis__Party_Account_Name__r.PersonMobilePhone,Identifier__c,Signer_page_no__c,llx__c,lly__c,urx__c,ury__c FROM genesis__Application_Parties__r WHERE Active__c = true) FROM genesis__Applications__c WHERE Id =: appId];
        if(appln.Sanction_Class2_Check__c && appln.Sanction_Doc_Generation_Check__c && appln.Sanction_Letter_Name__c != null && !appln.genesis__Application_Parties__r.isEmpty()){
            for(genesis__Application_Parties__c party : appln.genesis__Application_Parties__r){
                if(party.Id == partyId){
                    List<Digio_ESign_Service.cls_signers> signList  = new List<Digio_ESign_Service.cls_signers>();
                    List<genesis__Application_Parties__c> parties   = new List<genesis__Application_Parties__c>();
                    Digio_ESign_Service.ESignRequest eSignReq       = new Digio_ESign_Service.ESignRequest(); 
                    Map<String,Map<Integer,List<Object>>> signMap   = new Map<String,Map<Integer,List<Object>>>();
                    Map<Integer,List<Object>> tempMap               = new Map<Integer,List<Object>>();
                    
                    string aadhar = party.genesis__Party_Account_Name__r.Aadhaar_Number__pc!=null?party.genesis__Party_Account_Name__r.Aadhaar_Number__pc:'';
                    aadhar        = WS_getAadharNo.getAadharNo(aadhar);
                    System.debug(aadhar);
                    signList.add(new Digio_ESign_Service.cls_signers(party.genesis__Party_Account_Name__r.PersonMobilePhone,aadhar,Constants.MOB_REASON));
                    Digio_ESign_Service.cls_1 cord = new Digio_ESign_Service.cls_1(party.llx__c,party.lly__c,party.urx__c,party.ury__c);
                    tempMap.put(Integer.valueOf(party.Signer_page_no__c), new List<Object>{cord});
                    signMap.put(party.genesis__Party_Account_Name__r.PersonMobilePhone, tempMap);
                    class2SignedDoc  = EncodingUtil.base64Encode(Digioe_Docs_Service.getAttachmentBody(appln.Id,appln.Sanction_Letter_Name__c+'.pdf'));
                    System.debug('signList::'+signList);
                    System.debug(signMap);
                    eSignReq.exe_tenantId   = 'cuecent_tenant';
                    eSignReq.owner_tenantId = 'cuecent_tenant';
                    eSignReq.inputVariables = new Digio_ESign_Service.cls_inputVariables();
                    eSignReq.inputVariables.in_msg = '';
                    eSignReq.inputVariables.inputMap = new Digio_ESign_Service.cls_map();
                    eSignReq.inputVariables.inputMap.signers = new List<Digio_ESign_Service.cls_signers>();
                    eSignReq.inputVariables.inputMap.signers = signList;
                    eSignReq.inputVariables.inputMap.display_on_page ='custom';
                    eSignReq.inputVariables.inputMap.expire_in_days =10;
                    eSignReq.inputVariables.inputMap.file_Name = appln.Sanction_Letter_Name__c+'_'+appln.Id;
                    eSignReq.inputVariables.inputMap.file_data = class2SignedDoc;
                    eSignReq.inputVariables.inputMap.sign_coordinates = signMap;
                    eSignReq.inputVariables.inputMap.notify_signers = true;
                    eSignReq.inputVariables.inputMap.send_sign_link = true;
                    
                    //Calling the E-Sign service of DIGIO. This will return doc Id
                    resp.eSignRes.docID = Digio_ESign_Service.getESigned(eSignReq);
                    resp.eSignRes.LOS_APPID             = appln.Id;
                    resp.eSignRes.LOS_PARTY_TYPE_ID     = partyId;
                    resp.status                         = Constants.WS_SUCCESS_STATUS;
                    resp.statusCode                     = Constants.WS_SUCCESS_CODE;
                    appln.Sanction_ESign_Id__c          = resp.eSignRes.docID;
                    appln.Sanction_ESign_Check__c   = true;
                    System.debug('DocId:::'+resp.eSignRes.docID);
                    
                    //partySignDone = true;
                    //party.eSign_done__c = true;
                    //UPDATE party;
                    
                    Attachment att = new Attachment();
                    att.Id = Digioe_Docs_Service.attachmentId;
                    att.Description = resp.eSignRes.docID;
                    UPDATE att;
                    
                      
                    if(appln.Application_Stage__c == 'Interim sanction offered') appln.Application_Stage__c         = 'E-sign pending';
                    else if(appln.Application_Stage__c == 'Final Sanction Offered') appln.Application_Stage__c      = 'E-sign pending-final';
                    
                }
                //else if(party.Status__c != 'Signed') otherPartySigned = false;
                
            }
        }
        /*if(otherPartySigned && partySignDone){
            appln.All_Party_Signed__c   = true;
            if(appln.Application_Stage__c == 'Interim sanction offered' || appln.Application_Stage__c == 'E-sign pending') appln.Application_Stage__c = 'Sanction complete';
            else if(appln.Application_Stage__c == 'Final Sanction Offered' || appln.Application_Stage__c == 'E-sign pending-final') appln.Application_Stage__c = 'Sanction complete- Final Sanction';
        }*/
        
        update appln;
        
        RETURN resp;
    }
    
    public static WS_GetEsignDocID.Response doESignProcess(string appId){
        system.debug('appId:::'+appId);
        WS_GetEsignDocID.Response resp  = new WS_GetEsignDocID.Response(); 
        resp.eSignRes           = new WS_GetEsignDocID.eSignResponse();
        String fileName;
        String class2SignedDoc  = '';
        
        genesis__Applications__c appln = [Select id,Application_Stage__c,Document_Page_Count__c,genesis__Account__c,Sanction_Doc_Generation_Check__c,Sanction_Class2_Check__c,
                                          Sanction_Letter_Name__c,Sanction_ESign_Check__c from genesis__Applications__c where Id =: appId];
        
        fileName    = appln.Sanction_Letter_Name__c;
        
        if(!appln.Sanction_ESign_Check__c && fileName != null){            
            //AND Guarantor__c = true
            List<Digio_ESign_Service.cls_signers> signList  = new List<Digio_ESign_Service.cls_signers>();
            List<genesis__Application_Parties__c> parties   = new List<genesis__Application_Parties__c>();
            Digio_ESign_Service.ESignRequest eSignReq       = new Digio_ESign_Service.ESignRequest(); 
            Map<String,Map<Integer,List<Object>>> signMap   = new Map<String,Map<Integer,List<Object>>>();
            
            parties = [Select id,genesis__Application__c,Guarantor__c,genesis__Party_Account_Name__c,Signatories__c,
                       genesis__Party_Account_Name__r.Name,Party_Sub_Type__c,genesis__Party_Type__c,Active__c,
                       genesis__Party_Account_Name__r.PersonEmail,genesis__Party_Account_Name__r.Aadhaar_Number__pc,
                       genesis__Party_Account_Name__r.PersonMobilePhone
                       from genesis__Application_Parties__c where genesis__Application__c = :appId 
                       AND Active__c =true AND Signatories__c =true AND genesis__Party_Account_Name__r.PersonMobilePhone!= null
                       AND genesis__Party_Account_Name__r.RecordType.DeveloperName ='PersonAccount' order by CreatedDate desc];
            
            For(genesis__Application_Parties__c p : parties){
                string aadhar = p.genesis__Party_Account_Name__r.Aadhaar_Number__pc!=null?p.genesis__Party_Account_Name__r.Aadhaar_Number__pc:'';
                aadhar        = WS_getAadharNo.getAadharNo(aadhar);
                //signList.add(new Digio_ESign_Service.cls_signers(p.genesis__Party_Account_Name__r.PersonEmail,aadhar,Constants.EMAIL_REASON));
                signList.add(new Digio_ESign_Service.cls_signers(p.genesis__Party_Account_Name__r.PersonMobilePhone,aadhar,Constants.MOB_REASON));
                //Digio_ESign_Service.cls_1 cord = new Digio_ESign_Service.cls_1(444.24,236.16,559.44,285.12);
                
                //  Map<Integer,List<Object>> tempMap = new Map<Integer,List<Object>>();
                //  tempMap.put(1, new List<Object>{cord});
                // signMap.put(p.genesis__Party_Account_Name__r.PersonMobilePhone, tempMap);
            }
            System.debug(signList);
            signMap = SMESanction_DocReq.genCordStructure((Integer)appln.Document_Page_Count__c,appId,fileName+'.pdf');
            //Getting the stored docuement
            class2SignedDoc  = EncodingUtil.base64Encode(Digioe_Docs_Service.getAttachmentBody(appId,fileName+'.pdf'));
            
            eSignReq.exe_tenantId   = 'cuecent_tenant';
            eSignReq.owner_tenantId = 'cuecent_tenant';
            eSignReq.inputVariables = new Digio_ESign_Service.cls_inputVariables();
            eSignReq.inputVariables.in_msg = '';
            eSignReq.inputVariables.inputMap = new Digio_ESign_Service.cls_map();
            eSignReq.inputVariables.inputMap.signers = new List<Digio_ESign_Service.cls_signers>();
            eSignReq.inputVariables.inputMap.signers = signList;
            eSignReq.inputVariables.inputMap.display_on_page ='custom';
            eSignReq.inputVariables.inputMap.expire_in_days =10;
            eSignReq.inputVariables.inputMap.file_Name = fileName+'_'+appId;
            eSignReq.inputVariables.inputMap.file_data = class2SignedDoc;
            eSignReq.inputVariables.inputMap.sign_coordinates = signMap;
            eSignReq.inputVariables.inputMap.notify_signers = true;
            eSignReq.inputVariables.inputMap.send_sign_link = true;
            
            //Calling the E-Sign service of DIGIO. This will return doc Id
            resp.eSignRes.docID = Digio_ESign_Service.getESigned(eSignReq);
            resp.eSignRes.LOS_APPID         = appId;
            resp.status                     = Constants.WS_SUCCESS_STATUS;
            resp.statusCode                 = Constants.WS_SUCCESS_CODE;
            appln.Sanction_ESign_Id__c      = resp.eSignRes.docID;
            appln.Sanction_ESign_Check__c   = true;   
            appln.Application_Stage__c      = 'E-sign pending';
            update appln;
            
            Attachment att = new Attachment();
            att.Id = Digioe_Docs_Service.attachmentId;
            att.Description = resp.eSignRes.docID;
            UPDATE att;
        } 
        return resp;
    }
    
    public static void updatePartySignerDetail(genesis__Applications__c appln){
        String fileName;
        Map<String,genesis__Application_Parties__c> identifierPartyMap = new Map<String,genesis__Application_Parties__c>();
        Map<String,Map<Integer,List<Object>>> signMap   = new Map<String,Map<Integer,List<Object>>>();
        
        fileName    = appln.Sanction_Letter_Name__c;
        System.debug((Integer)appln.Document_Page_Count__c+'--'+appln.Id+'--'+fileName+'.pdf');
        signMap = SMESanction_DocReq.genCordStructure((Integer)appln.Document_Page_Count__c,appln.Id,fileName+'.pdf');
        for(genesis__Application_Parties__c party : appln.genesis__Application_Parties__r){
            System.debug(party.genesis__Party_Account_Name__r.PersonMobilePhone);
            identifierPartyMap.put(party.Id,party);
        }
        //Update signerdetails on Party
        if(appln.recordType.developerName != null && appln.recordType.developerName == Constants.SME_APP_RECORD_TYPE){
            for(String ident : signMap.keySet()){
                identifierPartyMap.get(ident).Identifier__c         = ident;
                for(Integer pgNo : signMap.get(ident).keySet()){
                    identifierPartyMap.get(ident).Signer_page_no__c = pgNo;
                    identifierPartyMap.get(ident).llx__c            = ((SMESanction_DocReq.cordWrapClass)signMap.get(ident).get(pgNo)[0]).llx;
                    identifierPartyMap.get(ident).lly__c            = ((SMESanction_DocReq.cordWrapClass)signMap.get(ident).get(pgNo)[0]).lly;
                    identifierPartyMap.get(ident).urx__c            = ((SMESanction_DocReq.cordWrapClass)signMap.get(ident).get(pgNo)[0]).urx;
                    identifierPartyMap.get(ident).ury__c            = ((SMESanction_DocReq.cordWrapClass)signMap.get(ident).get(pgNo)[0]).ury;
                }
            }
            if(!identifierPartyMap.values().isEmpty()) UPDATE identifierPartyMap.values();
        }
    }
    
}