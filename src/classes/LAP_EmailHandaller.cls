/*
* Name      : SMS_Services
* Compnay   : ET Marlabs
* Purpose   : For Email Handaller - LAP. 
* Author    : Niladri
*/
public class LAP_EmailHandaller {
    
    public static void LAP_Email(String emailid,string emailbody,string emailSubj){
        try{
        
        String imageLogo = SME_Email_Handler.getDocumentLogoUrl();
                       
                        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                        message.toAddresses = new List<String>{emailid};
                        message.setSubject(emailSubj);
                        message.setReplyTo('support@kvb.com');
                        message.setSenderDisplayName('KVB TEAM');
                        message.setBccSender(false);
                        message.setHtmlBody(emailbody);
                        System.debug('message::'+message);
                        
                        Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
                        Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                        
                        if (results[0].success) {
                            System.debug('The email was sent successfully.');
                        } else {
                            System.debug('The email failed to send: ' + results[0].errors[0].message);
                        }
            
            
        }
        catch(Exception e){
            System.debug('e'+e);
        }
                    }
        
        
    

}