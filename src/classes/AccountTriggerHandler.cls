/*
* Name      : AccountTriggerHandler
* Company   : ET Marlabs
* Purpose   : Handler class for AccountTrigger Trigger 
* Author    : Raushan
*/
public class AccountTriggerHandler {
    public static boolean isAccountTrigger =false;
    private static AccountTriggerHandler instance;
    //Singleton class Method.
    public static AccountTriggerHandler getInstance(){
        if(instance==null){
            instance=new AccountTriggerHandler();
        }
        return instance;
    }
    
    //Method for Update Previous and Current Value.
    public  void beforeUpdate(Map<id,Account> newMapObj,Map<id,Account> oldMapObj){
        try{
            for(Account acc : oldMapObj.values()){
                if(oldMapObj.get(acc.id).Constitution__c != newMapObj.get(acc.id).Constitution__c && acc.Previous_Constitution__c==null){
                    newMapObj.get(acc.id).Previous_Constitution__c = oldMapObj.get(acc.Id).Constitution__c;
                }
                if(!newMapObj.get(acc.id).Firm_Company_was_Reconstituted__c && newMapObj.get(acc.id).Previous_Constitution__c != null){
                    newMapObj.get(acc.id).Constitution__c   = acc.Previous_Constitution__c;
                }
            } 
        }Catch(Exception e){}  
    }
    
    //method to check by comparing that Net_Monthly_Income__c and Computed_NMI__c, and update value of Is_Bank_Statement_required and Is_ITR_required
    public  void updateITRAndBankStatement(List<Account> newList, Map<Id,Account> OldMap){
        List<Account> accList = new List<Account>();
        Account accRec;
        for(Account acc : newList){
            System.debug('acc:::'+acc);
            if(acc.Net_Monthly_Income__c != oldMap.get(acc.id).Computed_NMI__c){
                System.debug('@@@@'+'inside if');
                accRec = new Account(Id=acc.Id);
                accRec.Perfios_Captured__c = false;
                accRec.Is_Bank_Statement_required__c = true;
                accRec.Is_ITR_required__c = true;
                accList.add(accRec);
            }else{
                System.debug('@@@@'+'inside else');
                accRec = new Account(Id=acc.Id);
                accRec.Is_Bank_Statement_required__c = false;
                accRec.Is_ITR_required__c = false;
                accRec.Perfios_Captured__c = True;
                accRec.Id = acc.Id;
                accList.add(accRec);
            }
            
            
        }
        if(!accList.isEmpty()){
            isAccountTrigger = true;
            System.debug('@@@@'+accList);
            update accList;            
        }
        
        /*try{
List<Account> accList = new List<Account>();
System.debug('newList:::'+newList);
Account accRec;
for(Account acc : newList){
accRec = new Account();            
System.debug('acc:::'+acc);
if(acc.Net_Monthly_Income__c != oldMap.get(acc.id).Computed_NMI__c){
//acc.Is_Bank_Statement_required__c = true;
//acc.Is_ITR_required__c = true;
System.debug('@@@@'+'inside if');
accRec.Perfios_Captured__c = true;
accRec.Id = acc.Id;
}else{
System.debug('@@@@'+'inside else');
//acc.Is_Bank_Statement_required__c = false;
//acc.Is_ITR_required__c = false;
accRec.Perfios_Captured__c = false;
accRec.Id = acc.Id;
}
accList.add(accRec);
}
if(!accList.isEmpty())
update accList;
}catch(Exception e){
System.debug('@@@@'+e.getMessage()+' AT '+e.getLineNumber());
}*/
    }
    
    //method to calculate loan amount and loan exposure amount
    public static Boolean limitCalculation(genesis__Applications__c app){
        List<String> recTypeList = new List<String>{'SME_Renewal','Term_Loan'};
            List<String> productCodeList = new List<String>{'WC','TL'};
                List<String> appStageList = new List<String>{'Application close-enhancement','Limit renewed/Application close'};
                    Decimal loanAmount = 0;
        Decimal exposureAmount = 0;
        List<genesis__Applications__c> appNew = [SELECT Id,RecordType.DeveloperName,genesis__CL_Product__r.Product_Code__c,(SELECT Id,Existing_Limit__c,Limit_Amount__c,Balance_Outstandings__c,Amount_Yet_disbursed__c FROM Facilities__r) FROM genesis__Applications__c WHERE Id =: app.Id AND genesis__CL_Product__r.Product_Code__c IN :productCodeList AND RecordType.DeveloperName IN :recTypeList AND Application_Stage__c  NOT IN :appStageList];
        if(!appNew.isEmpty()){
            loanAmount = loamAmountCalculation(appNew[0]);
            exposureAmount = loanAmount + exposureAmountCalculation(appNew[0]);
        }
        if(loanAmount <= 2500000 && exposureAmount <= 2500000){
            RETURN true;
        }
        else
            RETURN false;
    }
    
    //Limit Amount calculation
    public static Decimal loamAmountCalculation(genesis__Applications__c app){
        Decimal loanAmount = 0.0;
        if(app != null && app.RecordType.DeveloperName == 'SME_Renewal'){
            if(!app.Facilities__r.isEmpty()){
                for(Facility__c facility : app.Facilities__r){
                    if(facility.Limit_Amount__c!= null){
                        loanAmount += facility.Limit_Amount__c;
                    }
                }
            }
        }
        System.debug(loanAmount);
        RETURN loanAmount;
    }
    //Exposure Amount calculation
    public static Decimal exposureAmountCalculation(genesis__Applications__c app){
        Decimal exposureAmount = 0.0;
        if(app != null && app.RecordType.DeveloperName == 'Term_Loan'){
            if(!app.Facilities__r.isEmpty()){
                for(Facility__c facility : app.Facilities__r){
                    decimal balcOut        = 0;
                    decimal amtyetDis      = 0;
                    if(facility.Balance_Outstandings__c != null){
                        if(facility.Balance_Outstandings__c < 0){
                            balcOut = (-1)*facility.Balance_Outstandings__c;
                        }else{
                            balcOut = facility.Balance_Outstandings__c;
                        } 
                    }
                    if(facility.Amount_Yet_disbursed__c != null){
                        if(facility.Amount_Yet_disbursed__c < 0){
                            amtyetDis = (-1)*facility.Amount_Yet_disbursed__c;
                        }else{
                            amtyetDis = facility.Amount_Yet_disbursed__c;
                        }
                        
                    }
                    exposureAmount += balcOut + amtyetDis;
                }
            }
        }
        System.debug(exposureAmount);
        RETURN exposureAmount;
    }
    //method to check if thresold breached for flags
    public static Boolean checkFinancialFlagStatus(String appId){
        try{
            List<String> flagNames = new List<String>{'Sales trend (CAGR-3 yrs)','Operating Margin(%)'};
                List<Credit_Underwriting__c> cuFlagList = new List<Credit_Underwriting__c>();
            Boolean thresoldBreachedFlag = false;
            cuFlagList = [SELECT Id,Threshold_breached__c FROM Credit_Underwriting__c WHERE Variable_type__c IN : flagNames AND Underwriting_Variable__r.Application__r.Id =: appId];
            if(!cuFlagList.isEmpty()){
                for(Credit_Underwriting__c cuChild : cuFlagList){
                    if(cuChild.Threshold_breached__c){
                        thresoldBreachedFlag = true;
                        break;
                    }
                }
            }
            RETURN thresoldBreachedFlag;
        }
        catch(Exception e){
            System.debug(e.getMessage());
            System.debug(e.getLineNumber());
            RETURN false;
        }
    }
}