/*
* Name      : WS_CBS_eKYC_Creation_HL
* Compnay   : ET Marlabs
* Purpose   : For HL Service(Customer eKYC creation in CBS) 
* Author    : Subas
*/

public class WS_CBS_eKYC_Creation_HL {
    //@future (Callout=true)
    public static void cbs_eKYC_Creation(List<ID> accIds, String AppId, Map<string,string>accMap){
        List <Account> accList = new List <Account>();
        genesis__Applications__c application = new genesis__Applications__c();
        application = queryService.getApp(appId);        
        if(accIds !=null){
            if(accList.size() > 0){
             accList = queryService.accListCust(accIds);   
            }else{
                accList = queryService.accList(AppId);
            }            
        }else{
            accList = queryService.accList(AppId);
        }        
        system.debug('##EKYCAccountList##'+accList.size());         
        CBS_eKYC_Creation_Handler cbs = new CBS_eKYC_Creation_Handler();
        cbs.inputVariables = new CBS_eKYC_Creation_Handler.cls_inputVariables();
        CBS_eKYC_Creation_Handler.cls_inputVariables clsInput = new CBS_eKYC_Creation_Handler.cls_inputVariables();
        clsInput.in_msg = new CBS_eKYC_Creation_Handler.cls_in_msg();
        cbs.inputVariables = clsInput;
        cbs.exe_tenantId = 'cuecent_tenant';
        cbs.owner_tenantId = 'cuecent_tenant';
        clsInput.in_msg.ApplicationId = AppId;
        clsInput.in_msg.serviceName = 'EKYCUpdation_HL';
        clsInput.in_msg.EKYCDetails = new List<CBS_eKYC_Creation_Handler.EKYCDetails>();
        for(Account acc : accList){
            CBS_eKYC_Creation_Handler.EKYCDetails rq = new CBS_eKYC_Creation_Handler.EKYCDetails();            
            rq.ApplicantID = acc.ID;
            rq.PrimaryPassword ='';	
            rq.BankCode ='53';	
            rq.ExternalBatchNumber ='';	
            rq.ExternalReferenceNo ='';	
            rq.ExternalSystemAuditTrailNumber ='';	
            rq.LocalDateTimeText ='';	
            rq.OriginalReferenceNo ='';	
            rq.OverridenWarnings ='';	
            rq.PostingDateText ='';	
            rq.ServiceCode ='';	
            rq.SessionTicket ='';	
            rq.TransactionBranch = application.Branch_Code__c;	//pass branch code
            rq.UserId ='SYSTEM01';	
            rq.UserReferenceNumber ='';	
            rq.ValueDateText ='';	
            rq.CodCustIc = acc.Customer_IC__c;	
            rq.FlgCustTyp ='RESIDENT INDIAN';	
            rq.CodCustId = '0';//acc.CBS_Customer_ID__c;	
            string aadharcardNo = WS_getAadharNo.getAadharNo(acc.Aadhaar_Number__pc); //'499118665246';
            if(aadharcardNo.isNumeric() == True){
                rq.CodCustUniqueId = aadharcardNo;
            }else{
                rq.CodCustUniqueId = '';
            }                          
            if(acc.Pan_Number__c <> null){
                rq.FlgPanCard ='Y';	
            }else{rq.FlgPanCard ='N';}
            rq.CodPanCard = acc.Pan_Number__c;	
            if(acc.Voter_Id__c<>null){
                rq.FlgVoterId ='Y';
            }else{rq.FlgVoterId ='N';}        	
            rq.CodVoterId = acc.Voter_Id__c;	
            if(acc.Passport_Number__pc<>null){
                rq.FlgPassport = 'Y';	
            }else{rq.FlgPassport = 'N';}
            rq.CodPassport = acc.Passport_Number__pc;	
            if(acc.Driving_License_ID__c<>null){
                rq.FlgDriveLiscense ='Y';
            }else{rq.FlgDriveLiscense ='N';}        
            rq.CodDriveLiscense = acc.Driving_License_ID__c;	
            if(acc.National_Identification_Code__c<>null){
                rq.FlgNatIdCard ='Y';
            }else{rq.FlgNatIdCard ='N';}        	
            rq.CodNatIdCard = acc.National_Identification_Code__c;	
            rq.FlgLetterRecogAuth ='N';	
            rq.CodLetterRecogAuth ='';	
            if(acc.Other_identification_number__c<>null){
                rq.FlgOthers ='Y';
            }else{rq.FlgOthers ='N';}        
            rq.CodOthers = acc.Other_identification_number__c;	
            rq.FlgPhotoObtained ='Y';	
            rq.DatPhotoObtained = acc.CreatedDate != null ? String.valueOf(date.valueOf(acc.CreatedDate).format()) : '';	//'10/03/2018'; //
            rq.DatCustIdData = String.ValueOf(date.valueOf(system.today()).format()) != null ? String.ValueOf(date.valueOf(system.today()).format()) : '';  // 10/03/2018'; //
            rq.CodCustRiskCat ='2';	
            rq.DatRiskCatReview = system.today() != null ? String.ValueOf(date.valueOf(system.today()).format()) : ''; //'10/03/2018'; 
            rq.CodReasonRiskCat ='';
            if(acc.Telephone_Bill__c<>null){
                rq.FlgTeleBillIndiv ='Y';
            }else{rq.FlgTeleBillIndiv ='N';}                   
            rq.FlgTelephoneBillComp ='N';
            rq.FlgLtrRcgAuthInd ='N';	
            rq.FlgElecBillIndiv ='N';
            if(acc.Ration_Card__c<>null){
                rq.FlgRationCardIndiv ='Y';
            }else{rq.FlgRationCardIndiv ='N';}  
            if(acc.Employment_Letter__c<>null){
                rq.FlgEmpLetterIndiv ='Y';
            }else{rq.FlgEmpLetterIndiv ='N';}        	
            rq.CodOtherDocindiv ='2';	
            if(acc.Same_Present_Address_Permanent_Address__c<>null){
                rq.FlgSameIdProofIndiv ='Y';
            }else{rq.FlgSameIdProofIndiv ='N';}        	
            rq.FlgIncorpCertifComp ='N';	
            rq.FlgMoaComp ='N';	
            rq.FlgAoaComp ='N';	
            rq.FlgBodResolutionComp ='N';	
            rq.FlgPoaDocumentComp ='N';	
            if(acc.Bank_Statement__c<>null){
                rq.FlgBankStmtIndiv ='Y';
            }else{rq.FlgBankStmtIndiv ='N';}	
            rq.FlgPancrdAltLetComp ='N';	
            rq.CodOtherDocComp ='2';	
            rq.FlgRegisterCertifPf ='N';	
            rq.FlgPartnershipDeedPf ='N';	
            rq.FlgPoaDocumentPf ='N';	
            rq.CodOffValidDocPf ='';	
            rq.FlgTelephoneBillPf ='N';	
            rq.FlgRegisterCertifTf ='N';	
            rq.FlgPoaDocumentTf ='N';	
            rq.CodOffValidDocTf ='';	
            rq.FlgResolMangBodyTf ='N';	
            rq.FlgTelephoneBillTf ='N';	
            rq.FlgTelBillOthers ='N';	
            rq.FlgElecBillOthers ='N';	
            rq.FlgRegCertifOthers ='N';	
            rq.CodOtherDocOthers ='';	
            rq.ThresholdPct ='100';	
            rq.FlgThresholdAlert ='Y';	
            rq.AnualTurnOver = String.ValueOf((acc.NMI_Approved__c != null ? Integer.ValueOf(acc.NMI_Approved__c) : 0 ) * 12);	           
            rq.FormStatus ='';	
            rq.FlgEkyc ='Y';
            string uniqueno = CreateUniqueId.uniqID();
            string regex = '[a-zA-Z]{1,}|\\-';
            String unique = uniqueno.replaceAll(regex, '');
            rq.EkycRefNo = unique.subString(0,9);	
            rq.channel ='';
            clsInput.in_msg.EKYCDetails.add(rq);
        }        
        
        geteKYC(json.serialize(cbs),AppId,accMap);
    }
    public static void geteKYC(String eKYCData,String AppId,Map<string,string>accMap){
        system.debug('^^^^^'+eKYCData);
        List<Account> accountList = new List<Account>();
        KVB_Endpoint_URLs__c kvb = KVB_Endpoint_URLs__c.getValues('CBS_Bulk_API');
        Map<String,String> headerMap                            = new Map<String,String>();
        headerMap.put('Content-Type','application/json');
        HTTPResponse response                                   = new HTTPResponse();
        String endPoint                                         = kvb.Endpoint_URL__c;
        response = HttpUtility.sendHTTPRequest(endPoint, 'POST', null,eKYCData,headerMap,label.CA_CERTIFICATE);
        system.debug('#####'+response);
        system.debug('$$$$'+response.getBody());
        If(response.getStatusCode() == 200 || response.getStatusCode() == 201){
            String jsonString = response.getBody(); 
            CBS_eKYC_Creation_Handler.CBS_eKYC_Response res = (CBS_eKYC_Creation_Handler.CBS_eKYC_Response)Json.deserialize(jsonString, CBS_eKYC_Creation_Handler.CBS_eKYC_Response.Class);
            system.debug('@@@'+res);
            if(accMap != null){
            if(accMap.size()>0){
                for(String str : accMap.keySet()){
                    if(str.length()>=15){
                        account acc = new account();
                        acc.Id = str;
                        acc.CBS_Customer_ID__c = accMap.get(str);
                        accountList.add(acc);
                    }
                }
                if(accountList.size()>0){
                    update accountList;                
                }
            }
        }
            if(res.out_msg.Status_Desc != 'Success'){
                CBS_API_Log__c log = new CBS_API_Log__c();    
                log.Application__c = AppId;
                log.API_Name__c = 'EKYCUpdation_HL';
                log.Status__c = 'Failure';
                log.Success_Error_Message__c = res.out_msg.Status_Desc;
                log.Sequence_No__c = '2';  
                insert log;
            }
        }else{
            CBS_API_Log__c log = new CBS_API_Log__c();    
            log.Application__c = AppId;
            log.API_Name__c = 'EKYCUpdation_HL';
            log.Status__c = 'Failure';
            log.Success_Error_Message__c = 'CustomerEKYC_CBS_Error_No_Hit';
            log.Sequence_No__c = '2';  
            insert log;
        }
    }
}