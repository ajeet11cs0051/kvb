@isTest(SeeAllData=false)
public class Test_Branch_Ownership_Assignment {
    
    @testSetup static void setup() {
        //User testUser = TestUtility.createUser('lname','fname','maaaaaail@ggmail.com','mfffddssaaaaaail@gmail.com','100','BR_MGR_MEDIUM_OLD','999999999','187267272');
        Profile p = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator']; 
        //UserRole userRMRol = [SELECT DeveloperName,Id FROM UserRole WHERE DeveloperName =: role]; 
        User userData = new User(Alias = 'standt1', 
                                 Email= 'maaaaaail@ggmail.com', 
                                 EmailEncodingKey='UTF-8', 
                                 FirstName = 'FName',
                                 LastName = 'LName', 
                                 //EmployeeNumber = empCode,                                   
                                 LanguageLocaleKey='en_US', 
                                 LocaleSidKey='en_AU', 
                                 TimeZoneSidKey = 'Asia/Kolkata',
                                 CommunityNickname = 'LName',
                                 ProfileId = p.Id, 
                                 MobilePhone = '2222222221',
                                 phone = '1212121212',
                                 UserName= 'mfffddssaaaaaail@gmail.com',
                                 Office_Code__c = '100'
                                );
        
        
        
        
        insert userData;
        
        Account buisnessAcc =  new Account();
        buisnessAcc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        //'0127F0000009zg6QAA';//0127F000000A1gEQAS
        buisnessAcc.Name = 'Buisness Account';
        insert buisnessAcc;
        
        Account personAcc1 = new Account();
        personAcc1.LastName = 'personacc1';
        personAcc1.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        //'0127F000000A1gEQAS';
        insert personAcc1;
        
        Account personAcc2 = new Account();
        personAcc2.LastName = 'personacc2';
        personAcc2.RecordTypeId =  Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
        insert personAcc2;
        
        
        clcommon__CL_Product__c clProd = new clcommon__CL_Product__c();
        clProd.clcommon__Product_Name__c = 'CL_Product';
        insert clProd;
        
        genesis__Applications__c geAppl =  new genesis__Applications__c();
        geAppl.genesis__Account__c  =  buisnessAcc.id;
        geAppl.RecordTypeId = Schema.SObjectType.genesis__Applications__c.getRecordTypeInfosByName().get('SME Renewal').getRecordTypeId();
        geAppl.Branch_Code__c = '100'; 
        geAppl.Active__c = true;
        geAppl.genesis__Loan_Amount__c = 0.0;
        geAppl.Application_Stage__c = 'Identified for renewal';
        insert geAppl;
        
        genesis__Application_Parties__c party = new genesis__Application_Parties__c();
        party.genesis__Application__c = geAppl.id;
        party.Key_Contact__c = personAcc1.id;
        insert party;
        
        Facility__c facility =  new Facility__c();
        facility.Application__c = geAppl.id;
        facility.Active__c = true;
        facility.Existing_Limit__c  = 1000;
        facility.RecordTypeId = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Parent').getRecordTypeId();
        insert facility;
        
        genesis__Application_Collateral__c applColl = new genesis__Application_Collateral__c();
        applColl.Application__c = geAppl.id;
        insert applColl;
        
        ints__TransUnion_Credit_Report__c transUnion = new ints__TransUnion_Credit_Report__c();
        transUnion.Account__c = buisnessAcc.id;
        insert transUnion;
        
        
    }
    
    
    @isTest
    static void testMethod1(){
        
        List<User> userList = [Select Id,Name,Office_Code__c from User where Office_Code__c = '100'];
        Branch_Ownership_Assignment cb = New Branch_Ownership_Assignment();
        Database.QueryLocator ql = cb.start(null);
        cb.execute(null,userList);
        cb.Finish(null);
    }
    
    @isTest
    static void testMethod2(){
        
        List<genesis__Applications__c> applList = [SELECT id,genesis__Loan_Amount__c,(SELECT id,Existing_Limit__c,RecordType.DeveloperName FROM Facilities__r WHERE Active__c =  true AND RecordType.DeveloperName = 'Parent') FROM genesis__Applications__c];
        Test.startTest();
        AnonymousBatchClass cb = New AnonymousBatchClass('Application_Stage__c = \'Identified for renewal\' AND RecordType.DeveloperName = \'SME_Renewal\'');
        Database.QueryLocator ql = cb.start(null);
        cb.execute(null,applList);
        cb.Finish(null);
        Test.stopTest();
    }
    
    
    
    
    
}