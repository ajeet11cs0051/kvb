/*
* Name    : ApplicationTiggerHandler for Processing fee calculation
* Company : ET Marlabs
* Purpose : This class will be used for Processing fee calculation
* Author  : Venu
*/
public with sharing class ProcessingFee{
    public static Boolean IsFirstRun = true;
    
    
    public static void CallCalculatefee(List<genesis__Applications__c> Applist,Map<Id,genesis__Applications__c> oldApp,List<genesis__Applications__c> oldList,Map<Id,genesis__Applications__c> newApp){
       
       
        set<id> appId= new set<id>();
        set<id> accid=new set<id>();
        Set<String> applicationIds  = new Set<String>();
        
        for(genesis__Applications__c app : Applist){
            
            if((oldApp.get(app.id).Loan_Purpose__c!=app.Loan_Purpose__c) || (oldApp.get(app.id).genesis__Loan_Amount__c!=app.genesis__Loan_Amount__c) || (oldApp.get(app.id).Sub_Stage__c!=app.Sub_Stage__c  && app.Sub_Stage__c ==Constants.PersonalDetailsCapt)) {
                system.debug('######'+app.id);
                appId.add(app.id);
                if(app.genesis__Account__c!=null){
                    accid.add(app.genesis__Account__c);
                }                
            }            
        }
        system.debug('appId'+appId);
        if((appId.size()>0 || accid.size()>0)  && ProcessingFee.IsFirstRun){
            
            calculateFeeList(appId,accid);
        }
    }
    public static void calculateFeeList(set<id> Appid,set<id> accid){
         try{
        IsFirstRun = false;
        system.debug('accid'+accid);
        list<genesis__Applications__c> updateApplist = new list<genesis__Applications__c>();
        list<genesis__Applications__c> ListApp=[select id,(select id,genesis__Application__c from genesis__Application_Parties__r where Active__c=true AND Product_Type__c=:Constants.HL_Value) from genesis__Applications__c where ID IN:AppID];

       // list<account> Acclist=[select id,(select id from genesis__Applications_account__r ),(select id,Key_Contact__c,genesis__Party_Account_Name__c from Parties__r where active__c=true and Product_Type__c='HL') from account where ID IN:accid];
        system.debug('ListApp'+ListApp);
        map<id,id> accAppId=new map<id,id>();
        
        map<id,integer> accCount=new map<id,integer>();
        map<id,integer> appPartiescount=new map<id,integer>();
        
        for(genesis__Applications__c App:ListApp){
             accCount.put(App.id, 0);
            for(genesis__Application_Parties__c Party:App.genesis__Application_Parties__r){
                  if(accCount.containsKey(party.genesis__Application__c)){
                    integer pCount  = accCount.get(party.genesis__Application__c) + 1;
                    accCount.put(party.genesis__Application__c,pCount);
                }else{
                    accCount.put(party.genesis__Application__c,1);
                }
                
            }
            
        }
        system.debug('accCount'+accCount);
       /* for(account acc:Acclist){
            
            for(genesis__Applications__c app:acc.genesis__Applications_account__r){
                accAppId.put(acc.id,app.id);
                
            }
            accCount.put(acc.id, 0);
            system.debug('acc.genesis__Parties__r'+acc.Parties__r);
            for(genesis__Application_Parties__c party:acc.Parties__r){
                
                if(accCount.containsKey(party.Key_Contact__c)){
                    integer pCount  = accCount.get(party.Key_Contact__c) + 1;
                    accCount.put(party.Key_Contact__c,pCount);
                }else{
                    accCount.put(party.Key_Contact__c,1);
                }
                
            }
            
        }*/
        system.debug(accCount);
        /*
        for(ID keyID:accAppId.keyset()){
            
            if(accCount.containskey(keyID)){
                appPartiescount.put(accAppId.get(keyID),accCount.get(keyID));
                
            }
            
        }  */
        system.debug(appPartiescount);   
        
        
        
        list<genesis__Applications__c> Applist =[select id,Record_Type_Name__c, Branch_State__c,Penal_Interest__c,Preclosure_Charges__c,Processing_Charges__c, genesis__Account__c,genesis__Loan_Amount__c,Loan_Purpose__c,genesis__Account__r.Count_Parties__c from genesis__Applications__c where id in:accCount.keyset()];
        system.debug('Applist'+Applist);
        
        
        for(genesis__Applications__c ap:Applist){
            integer partylist=0;
            Decimal LoanAmount;
            Decimal LAtwe_five;
            if(ap.genesis__Loan_Amount__c!=null){
                LoanAmount=ap.genesis__Loan_Amount__c;
                LAtwe_five=LoanAmount * Decimal.valueOf(System.Label.Legal_Valu_percent);
                system.debug('0.0025 percent of loan amount'+LAtwe_five);
            }
            
            // Cibil charges
            if(ap.genesis__Account__c!=null){
                partylist =accCount.get(ap.id) + 1;
            }
            else{
                partylist= 1;  
            }
            ap.Cibil_Charges__c= partylist *100;
            
            System.debug('loan amount:::'+LoanAmount);
            //Processing charges
            System.debug('####@@@');
            if(LoanAmount>=Decimal.valueOf(System.Label.Minimum_amount_zero) && LoanAmount<= Decimal.valueOf(System.Label.Loan_Amount25L) && ap.Record_Type_Name__c =='Home Loan'){
                ap.Processing_Charges__c=Decimal.valueOf(System.Label.Proces_fee25);
                System.debug('11111');
            }
            else if(LoanAmount>Decimal.valueOf(System.Label.Loan_Amount25L) && LoanAmount <= Decimal.valueOf(System.Label.Loan_Amount_50L) && ap.Record_Type_Name__c =='Home Loan'){
                ap.Processing_Charges__c= Decimal.valueOf(System.Label.Proces_fee50);
                System.debug('33333');
            }
            else if(LoanAmount>Decimal.valueOf(System.Label.Loan_Amount_50L) && ap.Record_Type_Name__c =='Home Loan'){
                ap.Processing_Charges__c= Decimal.valueOf(System.Label.Proces_fee75);
                System.debug('22222');
            }
            else if(ap.Record_Type_Name__c =='LAP'){
                ap.Processing_Charges__c= (Decimal.valueOf(System.Label.Processing_Fee_LAP)/100)*LoanAmount;
                System.debug('4444444');
            }
            //pl proccessing fees calculation   
           else if(ap.Record_Type_Name__c == Constants.PERSONALLOAN){
                 System.debug('55555');
                LoanProcess_Config__mdt proFees = queryService.processingFee('PL');
                System.debug('profees:::'+profees);
                if(String.valueOf(proFees.Fee_Percentage__c) <> null)
                    ap.Processing_Charges__c = (proFees.Fee_Percentage__c/100)*LoanAmount;
                if(String.valueOf(proFees.PreClosure_Charges__c) <> null)
                    ap.Preclosure_Charges__c = proFees.PreClosure_Charges__c;
                if(String.valueOf(proFees.Penal_Interest__c) <> null)
                    ap.Penal_Interest__c = proFees.Penal_Interest__c;
                
                 if(!Utility.ISStringBlankorNull(ap.Branch_State__c)){
                    System.debug('**branch'+ap.Branch_State__c); 
                    String branchState = string.valueOf(ap.Branch_State__c).toUpperCase();
                    Stamping_Charges__mdt stampchargePL=[select   MasterLabel,Personal_Loan_Agreement__c from   Stamping_Charges__mdt where   MasterLabel=:branchState Limit 1 ];
                    System.debug('***stampchargePL'+stampchargePL);
                    ap.Total_Stamp_Paper_Charge__c =stampchargePL.Personal_Loan_Agreement__c;
                }    
                
             
            }
            system.debug('end');
            // No need of other charges in case of PL.
            if(ap.Loan_Purpose__c!=Constants.Loanpurpose && ap.Record_Type_Name__c <> Constants.PERSONALLOAN){
               
                // Cersai calculations
                
                if(LoanAmount>=Decimal.valueOf(System.Label.Minimum_amount_zero) && LoanAmount<=Decimal.valueOf(System.Label.Cersai_50000)){
                       ap.Cersai_Charges__c=Decimal.valueOf(System.Label.Cersai);
                }
                else if(LoanAmount>Decimal.valueOf(System.Label.Cersai_50000)){
                    ap.Cersai_Charges__c= Decimal.valueOf(System.Label.Cersai_above5lac);
                }
                
                
                // Legal openion
                
                if(LAtwe_five<=Decimal.valueOf(System.Label.Legal_min)){
                    ap.Legal_Charges__c= Decimal.valueOf(System.Label.Legal_min);
                }
                else if(LAtwe_five>Decimal.valueOf(System.Label.Legal_min) && LAtwe_five<Decimal.valueOf(System.Label.Legal_fee_Max)){
                    ap.Legal_Charges__c=LAtwe_five;
                }
                else if(LAtwe_five>=Decimal.valueOf(System.Label.Legal_fee_Max)){
                    ap.Legal_Charges__c= Decimal.valueOf(System.Label.Legal_fee_Max);
                }
                
                
                // Valuation charges
                
                if(LAtwe_five<=Decimal.valueOf(System.Label.Legal_min)){
                    ap.Valuation_Charges__c= Decimal.valueOf(System.Label.Legal_min);
                }
                else if(LAtwe_five>Decimal.valueOf(System.Label.Legal_min) && LAtwe_five<Decimal.valueOf(System.Label.Valution_Fee_Max)){
                    ap.Valuation_Charges__c=LAtwe_five;
                }
                else if(LAtwe_five>=Decimal.valueOf(System.Label.Valution_Fee_Max)){
                    ap.Valuation_Charges__c= Decimal.valueOf(System.Label.Valution_Fee_Max);
                }
                
            }
            else{
                ap.Valuation_Charges__c=0;
                ap.Cersai_Charges__c=0;
                ap.Legal_Charges__c=0;
              }
            	 ap.Valuation_Charges__c=(ap.Valuation_Charges__c+(ap.Valuation_Charges__c*decimal.valueOf(system.label.Valuation_Markup))).setscale(2);
                ap.Cersai_Charges__c=ap.Cersai_Charges__c.setscale(2);
                ap.Legal_Charges__c= (ap.Legal_Charges__c+(ap.Legal_Charges__c*decimal.valueOf(system.label.Legal_Markup))).setscale(2);
                 ap.Processing_Charges__c= ap.Processing_Charges__c.setscale(2);
                ap.Cibil_Charges__c=ap.Cibil_Charges__c.setscale(2);
            system.debug('ap.Processing_Charges__c'+ap.Processing_Charges__c);
            updateApplist.add(ap);
            
        }
        update updateApplist;
    }
    
        catch(exception e){
            system.debug('Error'+e.getStackTraceString()+'Line number'+e.getLineNumber());
        }
    }
    
    // FOr updating the property details
    
    
    public static Void callPropertyDetails(list<genesis__Applications__c> Applist,Map<Id,genesis__Applications__c> oldApp){
        set<string> flatNo= new set<string> ();
        Map<Id,string> FlatID=new Map<Id,string>();
        
        Map<Id, genesis__Applications__c> rejectedStatements    = new Map<Id, genesis__Applications__c>{};
            for(genesis__Applications__c plan: Applist)
        {
            genesis__Applications__c oldPlan = oldApp.get(plan.Id);
            // For Propety Details Fetching HL
            
            if(plan.Flat_No__c!=oldPlan.Flat_No__c){
                flatNo.add(Plan.Flat_No__c);
                FlatID.put(plan.Id, Plan.Flat_No__c);
            }
            
            // for SME
            if (oldPlan.Pre_Renewal_Checklist__c != 'Rejected' 
                && plan.Pre_Renewal_Checklist__c == 'Rejected'){ 
                    rejectedStatements.put(plan.Id, plan);  
                }
            //Calculating GMRA
            //GmraCal.calGMRA(plan,oldPlan);
            
             // Document Generation // Add Personal loan type to not generate doc in PL
            if(plan.Sub_Stage__c!=oldPlan.Sub_Stage__c && plan.Record_Type_Name__c <> Constants.PERSONALLOAN) {
                
                if(plan.Sub_Stage__c=='Loan Pre - Approved STP' || plan.Sub_Stage__c=='Loan Pre - Approved NSTP'){ // it should be work for both the stages
                    
                    HL_Digio_Service.docGenAndSignDoc(plan.id, Constants.PRE_Approval_Sanction_HL);
                }
                if(plan.Sub_Stage__c=='Loan Sanctioned Non-STP' || plan.Sub_Stage__c=='Loan Sanctioned STP'||  plan.Sub_Stage__c=='Loan Sanctioned-Committee'){
                    
                    HL_Digio_Service.docGenAndSignDoc(plan.id, Constants.Acknowledgement_For_Sanction);
                }
                
               /* if(plan.Sub_Stage__c=='Disbursement Approved'){
                    system.debug('%%%%%'+plan.id+'####'+Constants.Execution_Certificate_C11_HL);
                    HL_Digio_Service.docGenAndSignDoc(plan.id, Constants.Execution_Certificate_C11_HL);
                }

               */
            }  
        }
        
        
        system.debug('BOSS'+flatNo);
        if(flatNo.size()>0){
            PropertyDetails(Trigger.new, flatNo);
        }
        
        // for SME
        if (!rejectedStatements.isEmpty())  
        {
            RejectedCheck(rejectedStatements);
        }
        
    }
    public static void PropertyDetails(list<genesis__Applications__c> Applist,set<string> FlatID){
        // List<ID> =New List<ID>();
        try{
            map<string,Flat_Master__c> ProjectMap=new map<string,Flat_Master__c>();
            List<Flat_Master__c> FLatList=[select id,Survey_number__c,Flat_No_House_No__c,ProjectCode__r.City_Town_Village__c,
                                           ProjectCode__r.Address_Line_1__c,ProjectCode__r.Address_Line_2__c,ProjectCode__r.Address_Line_3__c,
                                           ProjectCode__r.State__c,ProjectCode__r.Pin_Code__c,UDS__c,Floor_No__c,
                                           Recommended_Rate_Of_Flat_House_Rs_Sq__c,Built_Up_Area_Sq_Feet__c,Date_Of_Valuation_Update__c,
                                           Market_Value_Of_Property_Rs__c,ProjectCode__r.Boundary_East__c,ProjectCode__r.Boundary_North__c,
                                           ProjectCode__r.Boundary_South__c, ProjectCode__r.Project_Name__c,ProjectCode__r.Boundary_West__c,ProjectCode__r.Location_Of_The_Proeprty__c,
                                           ProjectCode__r.Extent_Area_Sq_Feet__c,ProjectCode__r.District__c,Tower_Construction_Start_Date__c,Construction_End_Date__c,Stage_Of_Construction__c,
                                           Valuers_Name__c,Market_Value_Of_the_Building__c,Market_Value_Of_The_Land__c,MAST_Of_Property__c,Presence_of_Amenities__c,
                                           Quality_Of_Construction_Condition__c,Estimated_ValueonForcedSale__c
                                           from Flat_Master__c where Flat_No_House_No__c IN:FlatID];
            for(Flat_Master__c FM:FLatList){
                ProjectMap.put(FM.Flat_No_House_No__c, FM);
            }
            system.debug('ProjectMap'+ProjectMap);
            for(genesis__Applications__c App:Applist){
                if(ProjectMap.containsKey(App.Flat_No__c)){
                    App.Survey_No_Katha_No_Other_No_s__c=ProjectMap.get(App.Flat_No__c).Survey_number__c;
                    App.Estimated_Value_on_Forced_Sale__c=ProjectMap.get(App.Flat_No__c).Estimated_ValueonForcedSale__c;
                    App.Property_Address__c =ProjectMap.get(App.Flat_No__c).ProjectCode__r.Address_Line_1__c+','+ProjectMap.get(App.Flat_No__c).ProjectCode__r.Address_Line_2__c+','+ProjectMap.get(App.Flat_No__c).ProjectCode__r.Address_Line_3__c;
                    App.Property_City__c    =ProjectMap.get(App.Flat_No__c).ProjectCode__r.City_Town_Village__c;
                    App.Property_State__c=ProjectMap.get(App.Flat_No__c).ProjectCode__r.State__c;
                   App.Location_Of_The_Property__c=ProjectMap.get(App.Flat_No__c).ProjectCode__r.Location_Of_The_Proeprty__c;
                    App.Property_Pincode__c=ProjectMap.get(App.Flat_No__c).ProjectCode__r.Pin_Code__c;
                    App.Boundaries_East__c  =ProjectMap.get(App.Flat_No__c).ProjectCode__r.Boundary_East__c;
                    App.Boundaries_North__c=ProjectMap.get(App.Flat_No__c).ProjectCode__r.Boundary_North__c;
                    App.Boundaries_South__c=ProjectMap.get(App.Flat_No__c).ProjectCode__r.Boundary_South__c;
                    App.Boundaries_West__c=ProjectMap.get(App.Flat_No__c).ProjectCode__r.Boundary_West__c;
                    App.Area_of_the_Land__c=ProjectMap.get(App.Flat_No__c).ProjectCode__r.Extent_Area_Sq_Feet__c;
                    App.UDS_Sq_Ft__c=ProjectMap.get(App.Flat_No__c).UDS__c;
                    App.Name_Of_The_Socity__c=ProjectMap.get(App.Flat_No__c).ProjectCode__r.Project_Name__c;
                    App.Property_District__c=  ProjectMap.get(App.Flat_No__c).ProjectCode__r.District__c;
                    //if()
                    App.Floor_No__c=ProjectMap.get(App.Flat_No__c).Floor_No__c;
                    App.Existing_Built_up_Area__c=ProjectMap.get(App.Flat_No__c).Built_Up_Area_Sq_Feet__c;
                    App.Date_Of_Commencement__c=ProjectMap.get(App.Flat_No__c).Tower_Construction_Start_Date__c;
                    App.Date_Of_Completion__c=ProjectMap.get(App.Flat_No__c).Construction_End_Date__c;
                    App.Percentage_of_Work_Completed__c=ProjectMap.get(App.Flat_No__c).Stage_Of_Construction__c;
                    App.Valuers_Name__c=ProjectMap.get(App.Flat_No__c).Valuers_Name__c;
                    App.Market_Value_Of_The_Land__c=ProjectMap.get(App.Flat_No__c).Market_Value_Of_The_Land__c;
                    App.Market_Value_Of_The_Building__c=ProjectMap.get(App.Flat_No__c).Market_Value_Of_the_Building__c;
                    App.MAST_Of_Property__c=ProjectMap.get(App.Flat_No__c).MAST_Of_Property__c;
                    App.Presence_of_Amenities__c=ProjectMap.get(App.Flat_No__c).Presence_of_Amenities__c;
                    App.Quality_Of_Construction_Condition__c=ProjectMap.get(App.Flat_No__c).Quality_Of_Construction_Condition__c;
                    
                    
                    
                }
                system.debug(' App.Survey_No_Katha_No_Other_No_s__c'+ App.Survey_No_Katha_No_Other_No_s__c);
            }
        }
        catch(exception e){
            
            system.debug('Updating the Proeprty Details Failed'+e.getMessage()+' Stack '+e.getStackTraceString());
        }
        
    }
    
    
    
    // SME for Rejeted check by Amritesh
    public static void RejectedCheck(Map<Id, genesis__Applications__c> rejectStatements){
        
        
        //Get the most recent approval process instance for the object.
        // If there are some approvals to be reviewed for approval, then
        // get the most recent process instance for each object.
        List<Id> processInstanceIds = new List<Id>{};
            
            for (genesis__Applications__c plans : [SELECT (SELECT ID
                                                           FROM ProcessInstances
                                                           ORDER BY CreatedDate DESC
                                                           LIMIT 1)
                                                   FROM genesis__Applications__c
                                                   WHERE ID IN :rejectStatements.keySet()])
        {
            processInstanceIds.add(plans.ProcessInstances[0].Id);
        }
        
        // Now that we have the most recent process instances, we can check
        // the most recent process steps for comments.  
        for (ProcessInstance pi : [SELECT TargetObjectId,
                                   (SELECT Id, StepStatus, Comments 
                                    FROM Steps
                                    ORDER BY CreatedDate DESC
                                    LIMIT 1 )
                                   FROM ProcessInstance
                                   WHERE Id IN :processInstanceIds
                                   ORDER BY CreatedDate DESC])   
        {                   
            if ((pi.Steps[0].Comments == null || 
                 pi.Steps[0].Comments.trim().length() == 0))
            {
                rejectStatements.get(pi.TargetObjectId).addError(
                    'Operation Cancelled: Please provide a rejection reason!');
            }
        }  
    }
    
}