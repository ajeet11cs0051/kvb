/* @Name           : M21AValueUpdateTriggerHandler
 * @Purpose        : To update value  field of clcommon__Collateral__c object and record type also at the same type
 * @Author         : Ashish Jain
 * @Date           : 10/05/2018 
*/ 
public class M21AValueUpdateTriggerHandler {
    public static void UpdateValueOnCollateral(List<M21_A__c> m21){
        List<clcommon__Collateral__c> collateralList = new List<clcommon__Collateral__c>();
        List<Id> colIds = new List<Id>();
        for(M21_A__c mObj : m21){
            colIds.add(mObj.Collateral__c);
        }
        collateralList = [select id ,recordType.developername, clcommon__Value__c from clcommon__Collateral__c where id IN :colIds];
        Id colRecType = Schema.SObjectType.clcommon__Collateral__c.getRecordTypeInfosByName().get('Land And Building').getRecordTypeId();

        for(M21_A__c mobj : m21){
            for(clcommon__Collateral__c colObj : collateralList){
                    if(mObj.Collateral__c == colObj.Id){
                    colObj.clcommon__Value__c = Decimal.valueOf(mobj.Estimated_value_Rs__c);
//colObj.RecordTypeId = colRecType;
                }
            }    
        }
        update collateralList;
    }
}