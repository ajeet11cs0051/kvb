global class DigioDocGenerationHL {
    
    Webservice static String CallDocuments(String AppID,string DocType){
        set<string> DocsList=new set<string>();
        Try{
            genesis__Applications__c App=[select id,Diff_B_w_ROI_MCLR__c,Branch_Code__c,Property_Type__c,Area_of_the_Land__c,Document_Generation__c,genesis__Expected_Close_Date__c,genesis__Expected_First_Payment_Date__c,Preclosure_Charges__c,Rating_Frequency__c,
                                          MCLR_Rate__c,Excess_MCLR__c,genesis__Interest_Calculation_Method__c,Loan_Purpose__c,Bnk_Auth_Sign__c,Holiday_Period__c,UDS_Sq_Ft__c,
                                          genesis__Amortization_Term__c,Sanction_Amount_Words__c,Sanction_Authority_Limit__c,genesis__Term__c,genesis__Interest_Rate__c,genesis__Loan_Amount__c,Sanction_Date__c,Sanction_Reference_Number__c,
                                          Xerox_Reason1__c,Xerox_Reason2__c,Xerox_Doc_Table__c,MOD_Date__c,MOD_Branch__c,Property_City__c,Name_Of_The_Socity__c,Primary_Applicant__c,
                                          Disbursal_Financial_Year__c,genesis__Disbursement_Date__c,MCLR_Type__c,Month_Of_Disbursal__c,Day_Of_Disbursal__c,Sub_Stage__c,Name,genesis__Account__r.Name,
                                          genesis__Account__r.PersonMobilePhone,genesis__Account__r.CIBIL_Score__c,Branch_Name__c,genesis__CL_Product_Name__c,Name_Of_The_Builder__c,
                                          genesis__Initial_Advance__c,Date_Of_Sale_Agreement__c,Nature_Of_Property__c,Survey_No_Katha_No_Other_No_s__c,Flat_No__c,Existing_Built_up_Area__c,
                                          Floor_No__c,Stage_Sector_Ward_Block_No__c,Street_Name__c,Taluka_Tehsil__c,Property_District__c,Property_State__c,Property_Pincode__c,
                                          genesis__Account__r.PersonMailingStreet,genesis__Account__r.PersonMailingCity,genesis__Account__r.PersonMailingState,
                                          genesis__Account__r.PersonMailingPostalCode,genesis__Account__r.PersonMailingCountry,( select id,genesis__Party_Type__c,genesis__Party_Account_Name__c,
                                                                                                                                genesis__Party_Account_Name__r.id,Title_Holder__c from genesis__Application_Parties__r where Active__c=true AND genesis__Party_Account_Name__r.PersonEmail != null AND genesis__Party_Account_Name__r.PersonMobilePhone != null)
                                          from genesis__Applications__c where ID =:AppID];            
            
            String Document_Gen = App.Document_Generation__c != null ? App.Document_Generation__c : 'none';
            system.debug('^^^^purpose^^^'+App.Loan_Purpose__c+'####Doc gen####'+Document_Gen);
            if(DocType == 'Disburse' && (App.Loan_Purpose__c==Constants.IdentifiedProperty  || App.Loan_Purpose__c==Constants.ConstructionOn) && !Document_Gen.contains(Constants.B17DocGen) && !Document_Gen.contains(Constants.B17DocID)){
                
                DocsList.add(Constants.B17_HL);
                
            }
            
            for(genesis__Application_Parties__c Apart:App.genesis__Application_Parties__r){
                
                if(DocType == 'Disburse' && Apart.genesis__Party_Type__c ==Constants.Gurantor &&  !Document_Gen.contains(Constants.A46DocGene) && !Document_Gen.contains(Constants.A46Class2sign)  && !Document_Gen.contains(Constants.A46DocID)){
                    DocsList.add(Constants.A46_HL);
                    system.debug('DocsList'+DocsList);
                }
            }
            if(DocType == 'Disburse'){
                if(!Document_Gen.contains(Constants.A23DocGen) && !Document_Gen.contains(Constants.A23Class2)  && !Document_Gen.contains(Constants.A23DocID)){
                    DocsList.add(Constants.A23_HL);
                }
                
                if(!Document_Gen.contains(Constants.C11DocGen) && !Document_Gen.contains(Constants.C11DocID)){
                    DocsList.add(Constants.Execution_Certificate_C11_HL);
                }
            }
            else if(DocType=='MOD'){
                 if(!Document_Gen.contains(Constants.B2DocGene) && !Document_Gen.contains(Constants.B2DocID)){
                DocsList.add(Constants.B2_HL);
                 }
                      if(!Document_Gen.contains(Constants.B1DOcGene) && !Document_Gen.contains(Constants.B1DOcID)){
                DocsList.add(Constants.B1_HL);
                      }
            }
            
            Map<String,String> DocCOntent=New Map<String,String>();
            Map<String,String> DocIds=New Map<String,String>();
            system.debug('123'+DocsList);
            integer pageCount =0;
            
            string ss='';
            string Class2Doc='';
            string Docid;
            string BranchState='';
          
            if(DocsList.size()>0){
                  if(!Utility.ISStringBlankorNull(App.Branch_Code__c))
           BranchState=[select id,CODCCBRN__c,State__c from Branch_Master__c where CODCCBRN__c=:App.Branch_Code__c limit 1].state__c;
                string AccToken=WS_ApiGatewayAccToken.getAccTkn(); 
                if(AccToken!=null){
                    if(!Utility.ISStringBlankorNull(BranchState)){
                    for(string s:DocsList){
                        if(s==Constants.Execution_Certificate_C11_HL){
                            
                            
                            string templateId   = Utility.getDIGIOTemplateId(s); 
                            system.debug('templateId'+templateId);
                            
                            //Calling the document DIGIO generation service
                            Digioe_Docs_Service.DocGenResponse docResp= Digioe_Docs_Service.getEDocs(AppID,s,templateId);
                            ss = docResp.outMap.Data.outfile;
                            if(ss!=null){
                                //  DocCOntent.put(s, ss);
                                pageCount   = Integer.valueOf(docResp.outMap.Data.totalpages);
                                KVB_Company_Details__c company = KVB_Company_Details__c.getOrgDefaults();
                                ss  = ss != '' ? ss : EncodingUtil.base64Encode(Digioe_Docs_Service.getAttachmentBody(AppID,s+'.pdf'));
                                system.debug('pageCount'+pageCount);
                                
                                
                                Docid=DigioDocID_EsignHL.DocumentIDGeneration(AppID, s, pageCount,ss);
                                if(Docid!=null && (!String.isBlank(Docid))){
                                    DocCOntent.put(s, ss);
                                    DocIds.put(s, Docid);
                                    if(Document_Gen!=null){
                                        
                                        Document_Gen= Document_Gen + ';' + Constants.C11DocGen+';'+Constants.C11DocID;
                                    }
                                    else{
                                        Document_Gen=Constants.C11DocGen+';'+Constants.C11DocID;
                                    }
                                }
                                
                            }
                            else  if( ss == null){
                                //  DocCOntent.put(s, ss);
                                // throw new CustomException('Document Generation Failed
                                system.debug('Doc Name '+s+'Document Generation Failed');
                            }
                            
                        }
                        else{
                            
                            WS_eStampDocGen.EstampDocGenResp EStamp =  WS_eStampDocGen.createDoc(App,s,AccToken,BranchState);
                            system.debug('EStamp'+EStamp.Content);
                            system.debug('EStamp'+EStamp.PageCount);
                            if(EStamp.Content!=null){
                                //  DocCOntent.put(s, ss); 
                                // DocCOntent.put(s, EStamp.Content);
                                system.debug('DocCOntent INSIDE'+DocCOntent);
                                KVB_Company_Details__c company = KVB_Company_Details__c.getOrgDefaults();
                                EStamp.Content  = EStamp.Content != '' ? EStamp.Content : EncodingUtil.base64Encode(Digioe_Docs_Service.getAttachmentBody(AppID,s+'.pdf'));
                                
                                //Calling the Class-2 service
                                
                                // if(s=Constants.B17_HL){
                                if(s==Constants.A23_HL || s==Constants.A46_HL){
                                    ss = Digioe_Docs_Service.class2Signer(new Digioe_Docs_Service.DocSignerRequest(SYSTEM.LABEL.DigioKeyPersonName,s,company.City__c,EStamp.Content),EStamp.PageCount ,s);
                                    
                                }
                                else{
                                    ss  = EStamp.Content; 
                                }
                                system.debug('Class2Doc'+ss);
                                
                                if(ss!=null){
                                    // DocCOntent.put(s, ss);
                                    Docid=DigioDocID_EsignHL.DocumentIDGeneration(AppID, s, EStamp.PageCount,ss);
                                    if(Docid!=null && (!String.isBlank(Docid))){
                                        DocCOntent.put(s, ss);
                                        DocIds.put(s, Docid);
                                        if(s==Constants.A23_HL){
                                            if(Document_Gen!=null){
                                                
                                                Document_Gen= Document_Gen + ';' + Constants.A23DocGen+';'+Constants.A23Class2 + ';' + Constants.A23DocID;
                                            }
                                            else{
                                                Document_Gen=Constants.A23DocGen+';'+Constants.A23Class2 + ';' + Constants.A23DocID;
                                            }  
                                        }
                                        
                                        else if(s==Constants.A46_HL){
                                            if(Document_Gen!=null){
                                                
                                                Document_Gen= Document_Gen + ';' + Constants.A46DocGene+';'+Constants.A46Class2sign + ';' + Constants.A46DocID;
                                            }
                                            else{
                                                Document_Gen=Constants.A46DocGene+';'+Constants.A46Class2sign + ';' + Constants.A46DocID;
                                            } 
                                        }
                                        
                                        else if(s==Constants.B17_HL){
                                            if(Document_Gen!=null){
                                                
                                                Document_Gen= Document_Gen + ';' + Constants.B17DocGen+';'+Constants.B17DocID ;
                                            }
                                            else{
                                                Document_Gen=Constants.B17DocGen+';'+Constants.B17DocID;
                                            } 
                                        }
                                        
                                        else if(s==Constants.B1_HL){
                                            if(Document_Gen!=null){
                                                
                                                Document_Gen= Document_Gen + ';' + Constants.B1DOcGene+';'+Constants.B1DOcID ;
                                            }
                                            else{
                                                Document_Gen=Constants.B1DOcGene+';'+Constants.B1DOcID;
                                            } 
                                        }
                                        
                                        else if(s==Constants.B2_HL){
                                            if(Document_Gen!=null){
                                                
                                                Document_Gen= Document_Gen + ';' + Constants.B2DocGene+';'+Constants.B2DocID ;
                                            }
                                            else{
                                                Document_Gen=Constants.B2DocGene+';'+Constants.B2DocID;
                                            } 
                                        }
                                    }
                                }
                                
                            }
                            
                            else if(EStamp.Content == '' || EStamp.Content == null){
                                //   DocCOntent.put(s, EStamp.Content);
                                //  throw new CustomException('Document Generation Failed');
                                system.debug('Doc Name '+s+'Document Generation Failed');
                            }
                            
                        }
                        
                        system.debug('DocCOntent'+DocCOntent);
                        
                    }
                    List<Digio_Document_ID__c> ListDOcIDS=new list<Digio_Document_ID__c>();
                    List<Attachment> UpdateAtt = new List<Attachment>();
                    List<Attachment> attList = new List<Attachment>();
                    for(string Docs:DocCOntent.keySet()){
                        system.debug('DocCOntent.get(Docs)'+DocCOntent.get(Docs));
                        Attachment att = new Attachment();
                        if(DocCOntent.get(Docs)!=null){
                            attList = [Select Id,Body,ParentId,Name from Attachment where ParentId=:AppID AND Name=:Docs+'.pdf' limit 1];
                            if(!attList.isEmpty()){
                                att = attList[0];
                                att.Body = EncodingUtil.base64Decode(DocCOntent.get(Docs));
                                UpdateAtt.add(att);
                                // update att;
                            }else{
                                
                                att.Name = Docs+'.pdf';
                                att.ParentId = AppID;
                                att.Body = EncodingUtil.base64Decode(DocCOntent.get(Docs));
                                UpdateAtt.add(att);
                            }
                            
                            // DOcumnet IDs storing
                            Digio_Document_ID__c Did=new Digio_Document_ID__c();
                            if(DocIds.get(Docs)!=null){
                                Did.Name=Docs;
                                Did.Application__c=AppID;
                                Did.Document_ID__c=DocIds.get(Docs);
                                ListDOcIDS.add(Did);
                            }
                            
                        }
                    }
                    if(UpdateAtt.size()>0){
                        upsert   UpdateAtt;
                        App.Document_Generation__c=Document_Gen;
                        update App;
                        
                        
                        List<genesis__Application_Document_Category__c> GAdc=[select id from genesis__Application_Document_Category__c where name=:Constants.LoanDocuments And 	genesis__Application__c=:AppID limit 1];
                        
                        
                        List<genesis__AppDocCatAttachmentJunction__c> GaList=new  List<genesis__AppDocCatAttachmentJunction__c>();
                        
                        for(Attachment a:UpdateAtt){
                            genesis__AppDocCatAttachmentJunction__c AtJunction=new genesis__AppDocCatAttachmentJunction__c();
                            AtJunction.genesis__Application_Document_Category__c=GAdc[0].id;
                            AtJunction.genesis__AttachmentId__c=a.id;
                            GaList.add(AtJunction);
                        }
                        
                        if(GaList.size()>0){
                            insert GaList;
                        }
                        if(ListDOcIDS.size()>0){
                            database.insert(ListDOcIDS,false);
                            List<Document_Applicant__c> DOCAppList=new List<Document_Applicant__c>();
                            map<id,id> AppDocID;
                            Map<id,id> OriginalAppDoc=new map<id,id>();
                            for(Digio_Document_ID__c d:ListDOcIDS){
								AppDocID =new map<id,id>();
                                if(d.Document_ID__c!=null && (!String.isBlank(d.Document_ID__c)) ){
                                    //@@@  C11
                                   if(d.Name==Constants.Execution_Certificate_C11_HL){
                                         AppDocID.put(App.genesis__Account__r.id,d.id);
                                        integer pcount=0;
                                        integer count=0;
                                        for(genesis__Application_Parties__c Apart:App.genesis__Application_Parties__r){
                                            if(Apart.genesis__Party_Type__c ==Constants.Co_Borrower){
                                                if(count<=2){
                                                    count++;
                                                    AppDocID.put(Apart.genesis__Party_Account_Name__r.id,d.id);
                                                }
                                            }
                                            else if(Apart.genesis__Party_Type__c ==Constants.Gurantor){
                                                if(pcount<=9){
                                                    pcount++;
                                                     AppDocID.put(Apart.genesis__Party_Account_Name__r.id,d.id);
                                                }
                                            }
                                        }
                                        
                                         system.debug('AppDocID'+d.Name+AppDocID);
                                    }
                                    //@@@  A23 && B17
                                   else if(d.Name==Constants.A23_HL ||d.Name==Constants.B17_HL){
                                       
                                      
                                        AppDocID.put(App.genesis__Account__r.id,d.id);
                                        integer count=0;
                                        for(genesis__Application_Parties__c Apart:App.genesis__Application_Parties__r){
                                            if(Apart.genesis__Party_Type__c ==Constants.Co_Borrower){
                                                if(count<=2){
                                                    count++;
                                                      AppDocID.put(Apart.genesis__Party_Account_Name__r.id,d.id);
                                                }
                                            }
                                        }
                                       system.debug('AppDocID'+d.Name+AppDocID);  
                                    }
                                    //@@@  A46
                                  else  if(d.Name==Constants.A46_HL){
                                        
                                        integer count=0;
                                        for(genesis__Application_Parties__c Apart:App.genesis__Application_Parties__r){
                                            if(Apart.genesis__Party_Type__c ==Constants.Gurantor){
                                                if(count<=9){
                                                    count++;
                                                    //if(AppDocID.c )
                                                   AppDocID.put(Apart.genesis__Party_Account_Name__r.id,d.id);
                                                }
                                            }
                                        }
                                        system.debug('AppDocID'+d.Name+AppDocID); 
                                    }
                                    
                                    //@@@  B1 && B2
                                  else  if(d.Name==Constants.B1_HL ||d.Name==Constants.B2_HL){
                                         AppDocID.put(App.genesis__Account__r.id,d.id);
                                        integer count=0;
                                        for(genesis__Application_Parties__c Apart:App.genesis__Application_Parties__r){
                                            if(Apart.Title_Holder__c){
                                                if(count<=12){
                                                    count++;
                                                    AppDocID.put(Apart.genesis__Party_Account_Name__r.id,d.id);
                                                }
                                            }
                                        }
                                        system.debug('AppDocID'+d.Name+AppDocID); 
                                    }
                                     
                                  // OriginalAppDoc.putAll(AppDocID);
                                    system.debug('OriginalAppDoc'+AppDocID);
                                for(string s:AppDocID.keySet()){
                                        if(AppDocID.containsKey(s)){
                                            Document_Applicant__c DocApp=new Document_Applicant__c();
                                            DocApp.Account__c=s;
                                            DocApp.Digio_Document_ID__c=AppDocID.get(s);
                                            DOCAppList.add(DocApp);
                                        }
                                    }
                                    system.debug('DOCAppList'+DOCAppList);
                                    
                                    /*  for(string s:DocAccMap.keySet()){
// if((DocAccMap.get(s)) && (s==d.name)){
system.debug('DocAccMap'+DocAccMap);
if(DocAccMap.get(s).size()>0 ){

for(string Dd:DocAccMap.get(s)){
Document_Applicant__c DocApp=new Document_Applicant__c();
system.debug('DocAccMap'+DocAccMap.get(s));
DocApp.Account__c=Dd;
DocApp.Digio_Document_ID__c=d.id;
DOCAppList.add(DocApp);

}



}
} */
                                    
                                }
                               
                            }
                            
                              
                            if(DOCAppList.size()>0){
                                insert DOCAppList;
                            }
                            
                        }
                    }
                    /*  for(string Docs:DocCOntent.keySet()){
system.debug('DocCOntent.get(Docs)'+DocCOntent.get(Docs));
if(DocCOntent.get(Docs)!=null){
Digioe_Docs_Service.upsertDoc(AppID,Docs+'.pdf',DocCOntent.get(Docs));
update App;
Digio_Document_ID__c Did=new Digio_Document_ID__c();
Did.Name=Docs;
Did.Application__c=AppID;
Did.Document_ID__c=DocIds.get(Docs);
ListDOcIDS.add(Did);
}

}
if(ListDOcIDS.size()>0){
database.insert(ListDOcIDS,false);
for(Digio_Document_ID__c DD:ListDOcIDS){
EsignApplicantsMapping.CreateApplicantSign(DD,AppID);
}  */
                    /* 
List<Document_Applicant__c> DOCAppList=new List<Document_Applicant__c>();
map<id,id> AppDocID=new map<id,id>();
for(Digio_Document_ID__c dd:ListDOcIDS){
AppDocID.put(App.genesis__Account__r.id, dd.id);
for(genesis__Application_Parties__c Apart:App.genesis__Application_Parties__r){
AppDocID.put(Apart.genesis__Party_Account_Name__r.id, dd.id);
}
}


for(string s:AppDocID.keySet()){
Document_Applicant__c DocApp=new Document_Applicant__c();
DocApp.Account__c=s;
DocApp.Digio_Document_ID__c=AppDocID.get(s);

DOCAppList.add(DocApp);


}
if(DOCAppList.size()>0){
Database.insert(DOCAppList, false) ;
}
*/				
                    if(DocCOntent.size() == DocsList.size()){
                        return 'Document Generated';
                    }else{
                        return 'Document Generation Failed';
                    }
                    }
                    else{
                        return 'Document Generation Failed - Branch State Not Avvailable'; 
                    }
                }
                else{
                    system.debug('Error from Apigee'+AccToken);
                    return 'APIGEE Failed';
                }
                //return 'Document Generated';
            }
            else{
                System.Debug('Documents Generated');
                return 'Document Generation Failed';
            }
        } 
        catch(exception e){
            system.debug('Doc generation error'+e.getStackTraceString()+'Line number'+e.getLineNumber());
            return 'Document Generation Failed';            
        }
    }
}