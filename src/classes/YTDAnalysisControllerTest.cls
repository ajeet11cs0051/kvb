/*
* Name     : YTDAnalysisControllerTest
* Company  : ET Marlabs
* Purpose  : This class is used as a test class for 'YTDAnalysisController' class
* Author   : Souvik Banik
*/

@isTest
public class YTDAnalysisControllerTest {

	@isTest
	public static void create_App_Acc_M68(){


		Account acc = new Account(FirstName = 'Test', LastName = 'Data');
        Insert acc;

        genesis__Applications__c genApp_Enhancement = TestUtility.create_genesis_Applications(acc.Id);
        genApp_Enhancement.Application_Stage__c = 'Enhancement- Application created';
        genApp_Enhancement.RecordTypeId = SOQL_Util.getRecordTypeId('SME_Enhancement');
        Insert genApp_Enhancement;

        genesis__Applications__c genApp_Exceeding = TestUtility.create_genesis_Applications(acc.Id);
        genApp_Exceeding.Application_Stage__c = 'Enhancement- Application created';
        genApp_Enhancement.RecordTypeId = SOQL_Util.getRecordTypeId('SME_Exceeding');
        Insert genApp_Exceeding;

        genesis__Applications__c genApp_AdHoc = TestUtility.create_genesis_Applications(acc.Id);
        genApp_AdHoc.Application_Stage__c = 'Enhancement- Application created';
        genApp_Enhancement.RecordTypeId = SOQL_Util.getRecordTypeId('SME_AdHoc');
        Insert genApp_AdHoc;


        M68_Balance_Sheet_Analysis__c current_Year_m68BalSheet 		= TestUtility.create_M68_Balance_Sheet_Analysis(acc.Id,'Actual','2017-18',200.23,100.41,800.45,700.56);
        Insert current_Year_m68BalSheet;

        M68_Balance_Sheet_Analysis__c n_1_Year_m68BalSheet 			= TestUtility.create_M68_Balance_Sheet_Analysis(acc.Id,'Actual','2016-17',400.23,300.41,900.45,600.56);
        Insert n_1_Year_m68BalSheet;

        M68_Balance_Sheet_Analysis__c n_2_Year_m68BalSheet 			= TestUtility.create_M68_Balance_Sheet_Analysis(acc.Id,'Actual','2015-16',300.23,200.41,1000.45,800.56);
        Insert n_2_Year_m68BalSheet;

        M68_Balance_Sheet_Analysis__c estimated_m68BalSheet 		= (M68_Balance_Sheet_Analysis__c)TestUtil_SME.createSobjectTypeRecord('M68_Balance_Sheet_Analysis__c',
        															  null,new Map<String,Object>{'Account__c'=>acc.Id,'Financial_type__c'=>'Estimated',
        															  'Fiscal_Year__c'=>'2018-19','Net_sales__c'=>700.23,'Purchases__c'=>200.0,'Sundry_Creditors__c'=>13,
        															  'Sundry_Debtors__c'=>100000});
		Insert estimated_m68BalSheet;
        
        M68_Balance_Sheet_Analysis__c projected_m68BalSheet 		= (M68_Balance_Sheet_Analysis__c)TestUtil_SME.createSobjectTypeRecord('M68_Balance_Sheet_Analysis__c',
        															  null,new Map<String,Object>{'Account__c'=>acc.Id,'Financial_type__c'=>'Projected',
        															  'Fiscal_Year__c'=>'2019-20','Net_sales__c'=>900.23,'Purchases__c'=>300.0,'Sundry_Creditors__c'=>13,
        															  'Sundry_Debtors__c'=>100000});
		Insert projected_m68BalSheet;
        
        M68_Balance_Sheet_Analysis__c ytd_m68BalSheet 				= (M68_Balance_Sheet_Analysis__c)TestUtil_SME.createSobjectTypeRecord('M68_Balance_Sheet_Analysis__c',
        															  null,new Map<String,Object>{'Account__c'=>acc.Id,'Financial_type__c'=>'YTD',
        															  'Fiscal_Year__c'=>'2017-18','Net_sales__c'=>500.23,'Purchases__c'=>100.0,'Sundry_Creditors__c'=>13,
        															  'Sundry_Debtors__c'=>100000});
		Insert ytd_m68BalSheet;
        //M68_Balance_Sheet_Analysis__c estimated_m68BalSheet 		= TestUtility.create_M68_Balance_Sheet_Analysis(acc.Id,'Estimated','2018-19',100.23,500.41,400.45,300.56);
        //Insert estimated_m68BalSheet;

        //M68_Balance_Sheet_Analysis__c projected_m68BalSheet 		= TestUtility.create_M68_Balance_Sheet_Analysis(acc.Id,'Projected','2019-20',100.23,500.41,400.45,300.56);
        //Insert projected_m68BalSheet;

        //M68_Balance_Sheet_Analysis__c ytd_m68BalSheet 			= TestUtility.create_M68_Balance_Sheet_Analysis(acc.Id,'YTD','2017-18',100.23,500.41,400.45,300.56);
        //Insert ytd_m68BalSheet; 


        Test.startTest();
         
        YTDAnalysisController.getAllFlagAnalysis(acc.Id,genApp_Enhancement.Id);
        YTDAnalysisController.getAllFlagAnalysis(acc.Id,genApp_Exceeding.Id);
        YTDAnalysisController.getAllFlagAnalysis(acc.Id,genApp_AdHoc.Id);
        YTDAnalysisHelper.getCreditorDebitorsFlag(acc.Id);
		YTDAnalysisHelper.getSalesAnalysisFlag(400.23,300.23,700.23,'EST');
		YTDAnalysisHelper.getSalesAnalysisFlag(400.23,300.23,900.23,'PROJ');
		YTDAnalysisHelper.getPurchaseAnalysisFlag(400.23,300.23,200.0,'EST');
		YTDAnalysisHelper.getPurchaseAnalysisFlag(400.23,300.23,300.23,'PROJ');
		YTDAnalysisHelper.getYTDAnalysis(ytd_m68BalSheet,'SALES',700.23);
		YTDAnalysisHelper.getYTDAnalysis(ytd_m68BalSheet,'PURCHASE',200.0);
		/*YTDAnalysisHelper.getDeltaFlag(calculateVelocity(current_Year_m68BalSheet,'CREDITOR'),calculateVelocity(n_1_Year_m68BalSheet,'CREDITOR'),calculateVelocity(n_2_Year_m68BalSheet,'CREDITOR'),estimated_m68BalSheet.Sundry_Creditors__c/estimated_m68BalSheet.Purchases__c);
        YTDAnalysisHelper.getDeltaFlag(calculateVelocity(current_Year_m68BalSheet,'DEBITOR'),calculateVelocity(n_1_Year_m68BalSheet,'DEBITOR'),calculateVelocity(n_2_Year_m68BalSheet,'DEBITOR'),estimated_m68BalSheet.Sundry_Debtors__c/estimated_m68BalSheet.Net_sales__c);
        YTDAnalysisHelper.getDeltaFlag(calculateVelocity(current_Year_m68BalSheet,'CREDITOR'),calculateVelocity(n_1_Year_m68BalSheet,'CREDITOR'),calculateVelocity(n_2_Year_m68BalSheet,'CREDITOR'),projected_m68BalSheet.Sundry_Creditors__c/projected_m68BalSheet.Purchases__c);
        YTDAnalysisHelper.getDeltaFlag(calculateVelocity(current_Year_m68BalSheet,'DEBITOR'),calculateVelocity(n_1_Year_m68BalSheet,'DEBITOR'),calculateVelocity(n_2_Year_m68BalSheet,'DEBITOR'),projected_m68BalSheet.Sundry_Debtors__c/projected_m68BalSheet.Net_sales__c);
*/
		
		Test.stopTest();

	}

}