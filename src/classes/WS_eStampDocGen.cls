/*
* Name    : WS_eStampDocGen
* Company : ET Marlabs
* Purpose : This class will generate document form Legal Desk
* Author  : Subas
*/
public class WS_eStampDocGen {
    
    public class EstampDocGenResp{
        public string  Content;
        public integer PageCount;
    }
    public Static String BorrowerDetails = '';
    public Static String Borrower1Names ='';
    public Static String Borrower2Names ='';
    public Static String Borrower3Names ='';
    public Static String Borrower4Names ='';
    public Static String Borrower5Names ='';
    public Static String Borrower1_Addline1 ='';
    public Static String Borrower2_Addline1 ='';
    public Static String Borrower3_Addline1 ='';
    public Static String Borrower4_Addline1 ='';
    public Static String Borrower1_Addline2 ='';
    public Static String Borrower2_Addline2 ='';
    public Static String Borrower3_Addline2 ='';
    public Static String Borrower4_Addline2 ='';
    public Static String Borrower1_Addline3 ='';
    public Static String Borrower2_Addline3 ='';
    public Static String Borrower3_Addline3 ='';
    public Static String Borrower4_Addline3 ='';
    public Static String Borrower1_FHDNAME ='';
    public Static String Borrower2_FHDNAME ='';
    public Static String Borrower3_FHDNAME ='';
    public Static String Borrower4_FHDNAME ='';
    
    public Static String Title_Holder1Name1 ='';
    public Static String Title_Holder1_Addline1 ='';
    public Static String Title_Holder1_Addline2 ='';
    public Static String Title_Holder1_Addline3 ='';
    
    public Static String Title_Holder2Name1 ='';
    public Static String Title_Holder2_Addline1 ='';
    public Static String Title_Holder2_Addline2 ='';
    public Static String Title_Holder2_Addline3 ='';
    
    public Static String Title_Holder3Name1 ='';
    public Static String Title_Holder3_Addline1 ='';
    public Static String Title_Holder3_Addline2 ='';
    public Static String Title_Holder3_Addline3 ='';
    
    public Static String Title_Holder4Name1 ='';
    public Static String Title_Holder4_Addline1 ='';
    public Static String Title_Holder4_Addline2 ='';
    public Static String Title_Holder4_Addline3 ='';
    
    public Static String Title_Holder5Name1 ='';
    public Static String Title_Holder5_Addline1 ='';
    public Static String Title_Holder5_Addline2 ='';
    public Static String Title_Holder5_Addline3 ='';
    
    public Static String Title_Holder6Name1 ='';
    public Static String Title_Holder6_Addline1 ='';
    public Static String Title_Holder6_Addline2 ='';
    public Static String Title_Holder6_Addline3 ='';
    
    public Static String Title_Holder7Name1 ='';
    public Static String Title_Holder7_Addline1 ='';
    public Static String Title_Holder7_Addline2 ='';
    public Static String Title_Holder7_Addline3 ='';
    
    public Static String Title_Holder8Name1 ='';
    public Static String Title_Holder8_Addline1 ='';
    public Static String Title_Holder8_Addline2 ='';
    public Static String Title_Holder8_Addline3 ='';
    
    public Static String Title_Holder9Name1 ='';
    public Static String Title_Holder9_Addline1 ='';
    public Static String Title_Holder9_Addline2 ='';
    public Static String Title_Holder9_Addline3 ='';
    
    public Static String Title_Holder10Name1 ='';
    public Static String Title_Holder10_Addline1 ='';
    public Static String Title_Holder10_Addline2 ='';
    public Static String Title_Holder10_Addline3 ='';
    
    public Static String Title_Holder11Name1 ='';
    public Static String Title_Holder11_Addline1 ='';
    public Static String Title_Holder11_Addline2 ='';
    public Static String Title_Holder11_Addline3 ='';
    
    public Static String Title_Holder12Name1 ='';
    public Static String Title_Holder12_Addline1 ='';
    public Static String Title_Holder12_Addline2 ='';
    public Static String Title_Holder12_Addline3 ='';
    
    public Static String Title_Holder13Name1 ='';
    public Static String Title_Holder13_Addline1 ='';
    public Static String Title_Holder13_Addline2 ='';
    public Static String Title_Holder13_Addline3 ='';
    
    public Static String Title_Holder14Name1 ='';
    public Static String Title_Holder14_Addline1 ='';
    public Static String Title_Holder14_Addline2 ='';
    public Static String Title_Holder14_Addline3 ='';
    
    public Static String Title_Holderdetails =''; 
    public Static String GuarantorNames =''; 
    public Static String Guarantor1Names =''; 
    public Static String Guarantor2Names ='';
    public Static String Guarantor3Names ='';
    public Static String Guarantor4Names ='';
    public Static String Guarantor5Names ='';
    public Static String Guarantor6Names ='';
    public Static String Guarantor7Names ='';
    public Static String Guarantor8Names ='';
    public Static String Guarantor9Names ='';
    public Static String Guarantor10Names ='';    
    public Static String GuarantorDetails ='';
    public Static String Title_HolderAddress ='';
    public Static String documentName ='';
    
    public class EstampResponse{  
        public String file_content;                   
        public String code;
        public String message;
        public String AppID;
    }
    /* public static void callE_stamp(genesis__Applications__c application){
if(application.Sub_Stage__c =='Disbursement approved'){
DIGIO_Templates__c objGame = DIGIO_Templates__c.getValues('A23');
String tempId = objGame.Template_Unique_Id__c;
//createDoc(application,tempId,'A23');
}
else If(application.Sub_Stage__c =='MOD initiated'){

}
} */
    public static EstampDocGenResp createDoc(genesis__Applications__c application , String DocName,string AcceToken,string stampState){
        try{   
            
            String TemplateID = '';
            String TempUID = '';
            string DocCateg='';
            Decimal stampCharge = 0.00;
            system.debug('DocName'+DocName);
            String Property_State = application.Property_State__c != null ? application.Property_State__c.toUpperCase() : '';
            List<Stamping_Charges__mdt> stampChargeAmt =[Select ID,MasterLabel,Declaration_Of_Title_Mortgage__c,Declaration_of_Original_Mortgage__c,
                                                         Create_Mortgage_Charges__c,Housing_Loan_Agreement__c,Agreement_of_Guarantee__c 
                                                         From Stamping_Charges__mdt Where MasterLabel =:Property_State Limit 1];
            system.debug('***Stmp_Charg***'+stampChargeAmt);
            createVal(application);
            if(stampChargeAmt.size()>0){
                if(DocName == Constants.A23_HL ){
                    stampCharge = stampChargeAmt[0].Housing_Loan_Agreement__c;
                }
                else if(DocName == Constants.A46_HL){
                    stampCharge = stampChargeAmt[0].Agreement_of_Guarantee__c;
                }
                else if(DocName == Constants.B1_HL){
                    stampCharge = stampChargeAmt[0].Declaration_Of_Title_Mortgage__c;
                }
                else if(DocName == Constants.B2_HL){
                    stampCharge = stampChargeAmt[0].Declaration_of_Original_Mortgage__c;
                }
                else if(DocName == Constants.B17_HL){
                    stampCharge = stampChargeAmt[0].Create_Mortgage_Charges__c;
                }
            }
			  DocCateg=system.label.Legal_Desk_Doc_Cat1;
                if(DocName == Constants.A23_HL ){
                    DIGIO_Templates__c objGame = DIGIO_Templates__c.getValues(DocName);                
                    TemplateID = objGame.Template_Unique_Id__c;
                    TempUID = objGame.UID__c;                    
                }
                else if(DocName == Constants.A46_HL){
                    DIGIO_Templates__c objGame = DIGIO_Templates__c.getValues(DocName);                
                    TemplateID = objGame.Template_Unique_Id__c;
                    TempUID = objGame.UID__c;                    
                }
                else if(DocName == Constants.B1_HL){
                    DIGIO_Templates__c objGame = DIGIO_Templates__c.getValues(DocName);                
                    TemplateID = objGame.Template_Unique_Id__c;
                    TempUID = objGame.UID__c;  
                      DocCateg=system.label.Legal_Desk_Doc_Cat8;
                }
                else if(DocName == Constants.B2_HL){
                    DIGIO_Templates__c objGame = DIGIO_Templates__c.getValues(DocName);                
                    TemplateID = objGame.Template_Unique_Id__c;
                    TempUID = objGame.UID__c; 
                      DocCateg=system.label.Legal_Desk_Doc_Cat8;
                }
                else if(DocName == Constants.B17_HL){
                    DIGIO_Templates__c objGame = DIGIO_Templates__c.getValues(DocName);                
                    TemplateID = objGame.Template_Unique_Id__c;
                    TempUID = objGame.UID__c;                    
                }            
            
            Estamp_HelperStructure.Estamp eStampSerObj                      =   new Estamp_HelperStructure.Estamp();
            eStampSerObj.ReferenceID                                        =   CreateUniqueId.uniqID();
            eStampSerObj.ApplicationID                                      =   application.Id;
            eStampSerObj.TemplateID                                         =   TemplateID; //A23;//'5a4e0bd91c32160854d5c687'; //TemplateID//Document Template ID
            eStampSerObj.StampPaperData                                     =   new Estamp_HelperStructure.StampPaperData();
            //Adding Stamp Paper Data               
            eStampSerObj.StampPaperData.FirstParty                          = 'First Party';
            eStampSerObj.StampPaperData.SecondParty                         = 'Second Party';
            eStampSerObj.StampPaperData.StampAmount                         = String.ValueOf(Integer.ValueOf(stampCharge));
            eStampSerObj.StampPaperData.StampDutyPaidBy                     = system.label.EstampDutyPaid; //'FirstParty';
            eStampSerObj.StampPaperData.DocumentCategory                    = DocCateg;
            eStampSerObj.StampPaperData.UID									= TempUID;
            eStampSerObj.StampPaperData.StampState							= stampState;
            
            eStampSerObj.FormData                                           =   new Estamp_HelperStructure.FormData();
            //Adding Form Data      
            
            eStampSerObj.FormData.Disb_Ref_Day                              = application.Day_Of_Disbursal__c;
            eStampSerObj.FormData.Disb_Ref_Month                            = application.Month_Of_Disbursal__c;
            eStampSerObj.FormData.Disb_Ref_Year                             = String.ValueOf(application.Disbursal_Financial_Year__c);
            eStampSerObj.FormData.Loc_Ref_Branch                            = application.Branch_Name__c;
            eStampSerObj.FormData.Borrower_Details                          = (application.Primary_Applicant__c != null ? application.Primary_Applicant__c :'') + (Borrower2Names != '' ? ' & '+Borrower2Names :'') + (Borrower3Names != '' ? ' & '+Borrower3Names :'') + (Borrower4Names != '' ? ' & '+Borrower4Names :'');
            eStampSerObj.FormData.Loan_Product                              = application.genesis__CL_Product_Name__c;
            eStampSerObj.FormData.Seller_Name                               = application.Name_Of_The_Builder__c;
            eStampSerObj.FormData.Advance_Amount                            = String.ValueOf(application.genesis__Initial_Advance__c);
            eStampSerObj.FormData.Sale_Agreem_Date                          = String.valueOf(application.Date_Of_Sale_Agreement__c);
            eStampSerObj.FormData.Nature_Property                           = application.Nature_Of_Property__c;
            eStampSerObj.FormData.Survey_No                                 = application.Survey_No_Katha_No_Other_No_s__c;
            eStampSerObj.FormData.Plot_Door_Bungalow_No                     = application.Flat_No__c;
            eStampSerObj.FormData.Built_up_area                             = application.Existing_Built_up_Area__c;
            eStampSerObj.FormData.Floor_No                                  = application.Floor_No__c;
            eStampSerObj.FormData.Ward_No                                   = application.Stage_Sector_Ward_Block_No__c;
            eStampSerObj.FormData.Building_Society_Name_No                  = application.Name_Of_The_Socity__c;
            eStampSerObj.FormData.Street_Name                               = application.Street_Name__c;
            eStampSerObj.FormData.Village_Town                              = application.Property_City__c;
            eStampSerObj.FormData.Taluka_Tehsil                             = application.Taluka_Tehsil__c;
            eStampSerObj.FormData.District_Name                             = application.Property_District__c;
            eStampSerObj.FormData.State_Name                                = application.Property_State__c;
            eStampSerObj.FormData.PIN_CODE                                  = application.Property_Pincode__c;
            eStampSerObj.FormData.Doc_Gen_date = formatDate(system.today()); //date field
            eStampSerObj.FormData.Title_Holder1_Name1     = Title_Holder1Name1;
            eStampSerObj.FormData.Title_Holder1_Add_line1 = Title_Holder1_Addline1;
            eStampSerObj.FormData.Title_Holder1_Add_line2 = Title_Holder1_Addline2;
            eStampSerObj.FormData.Title_Holder1_Add_line3 = Title_Holder1_Addline3;
            eStampSerObj.FormData.Title_Holder2_Name1     = Title_Holder2Name1;
            eStampSerObj.FormData.Title_Holder2_Add_line1 = Title_Holder2_Addline1;
            eStampSerObj.FormData.Title_Holder2_Add_line2 = Title_Holder2_Addline2;
            eStampSerObj.FormData.Title_Holder2_Add_line3 = Title_Holder2_Addline3;
            eStampSerObj.FormData.Title_Holder3_Name1     = Title_Holder3Name1;
            eStampSerObj.FormData.Title_Holder3_Add_line1 = Title_Holder3_Addline1;
            eStampSerObj.FormData.Title_Holder3_Add_line2 = Title_Holder3_Addline2;
            eStampSerObj.FormData.Title_Holder3_Add_line3 = Title_Holder3_Addline3;
            eStampSerObj.FormData.Title_Holder4_Name1 	  = Title_Holder4Name1;
            eStampSerObj.FormData.Title_Holder4_Add_line1 = Title_Holder4_Addline1;
            eStampSerObj.FormData.Title_Holder4_Add_line2 = Title_Holder4_Addline2;
            eStampSerObj.FormData.Title_Holder4_Add_line3 = Title_Holder4_Addline3;
            eStampSerObj.FormData.Title_Holder5_Name1     = Title_Holder5Name1;
            eStampSerObj.FormData.Title_Holder5_Add_line1 = Title_Holder5_Addline1;
            eStampSerObj.FormData.Title_Holder5_Add_line2 = Title_Holder5_Addline2;
            eStampSerObj.FormData.Title_Holder5_Add_line3 = Title_Holder5_Addline3;
            
            eStampSerObj.FormData.Title_Holder6_Name1     = Title_Holder6Name1;
            eStampSerObj.FormData.Title_Holder6_Add_line1 = Title_Holder6_Addline1;
            eStampSerObj.FormData.Title_Holder6_Add_line2 = Title_Holder6_Addline2;
            eStampSerObj.FormData.Title_Holder6_Add_line3 = Title_Holder6_Addline3;
            
            eStampSerObj.FormData.Title_Holder7_Name1     = Title_Holder7Name1;
            eStampSerObj.FormData.Title_Holder7_Add_line1 = Title_Holder7_Addline1;
            eStampSerObj.FormData.Title_Holder7_Add_line2 = Title_Holder7_Addline2;
            eStampSerObj.FormData.Title_Holder7_Add_line3 = Title_Holder7_Addline3;
            
            eStampSerObj.FormData.Title_Holder8_Name1     = Title_Holder8Name1;
            eStampSerObj.FormData.Title_Holder8_Add_line1 = Title_Holder8_Addline1;
            eStampSerObj.FormData.Title_Holder8_Add_line2 = Title_Holder8_Addline2;
            eStampSerObj.FormData.Title_Holder8_Add_line3 = Title_Holder8_Addline3;
            
            eStampSerObj.FormData.Title_Holder9_Name1     = Title_Holder9Name1;
            eStampSerObj.FormData.Title_Holder9_Add_line1 = Title_Holder9_Addline1;
            eStampSerObj.FormData.Title_Holder9_Add_line2 = Title_Holder9_Addline2;
            eStampSerObj.FormData.Title_Holder9_Add_line3 = Title_Holder9_Addline3;
            
            eStampSerObj.FormData.Title_Holder10_Name1     = Title_Holder10Name1;
            eStampSerObj.FormData.Title_Holder10_Add_line1 = Title_Holder10_Addline1;
            eStampSerObj.FormData.Title_Holder10_Add_line2 = Title_Holder10_Addline2;
            eStampSerObj.FormData.Title_Holder10_Add_line3 = Title_Holder10_Addline3;
            
            eStampSerObj.FormData.Title_Holder11_Name1     = Title_Holder11Name1;
            eStampSerObj.FormData.Title_Holder11_Add_line1 = Title_Holder11_Addline1;
            eStampSerObj.FormData.Title_Holder11_Add_line2 = Title_Holder11_Addline2;
            eStampSerObj.FormData.Title_Holder11_Add_line3 = Title_Holder11_Addline3;
            
            eStampSerObj.FormData.Title_Holder12_Name1     = Title_Holder12Name1;
            eStampSerObj.FormData.Title_Holder12_Add_line1 = Title_Holder12_Addline1;
            eStampSerObj.FormData.Title_Holder12_Add_line2 = Title_Holder12_Addline2;
            eStampSerObj.FormData.Title_Holder12_Add_line3 = Title_Holder12_Addline3;
            
            eStampSerObj.FormData.Title_Holder13_Name1     = Title_Holder13Name1;
            eStampSerObj.FormData.Title_Holder13_Add_line1 = Title_Holder13_Addline1;
            eStampSerObj.FormData.Title_Holder13_Add_line2 = Title_Holder13_Addline2;
            eStampSerObj.FormData.Title_Holder13_Add_line3 = Title_Holder13_Addline3;
            
            eStampSerObj.FormData.Title_Holder14_Name1     = Title_Holder14Name1;
            eStampSerObj.FormData.Title_Holder14_Add_line1 = Title_Holder14_Addline1;
            eStampSerObj.FormData.Title_Holder14_Add_line2 = Title_Holder14_Addline2;
            eStampSerObj.FormData.Title_Holder14_Add_line3 = Title_Holder14_Addline3;			
            
            eStampSerObj.FormData.MOD_Branch = application.MOD_Branch__c;
            eStampSerObj.FormData.Borrower_Names = application.Primary_Applicant__c;
            eStampSerObj.FormData.Title_Holder_details = (Title_Holder1Name1 != '' ? Title_Holder1Name1 :'') + (Title_Holder2Name1 != '' ? ' & '+Title_Holder2Name1 :'') + (Title_Holder3Name1 != '' ? ' & '+Title_Holder3Name1 :'') + (Title_Holder4Name1 != '' ? ' & '+Title_Holder4Name1 :'') + (Title_Holder5Name1 != '' ? ' & '+Title_Holder5Name1 :'');
            eStampSerObj.FormData.Mod_date = String.ValueOf(application.MOD_Date__c);
            eStampSerObj.FormData.Xerox_Doc_table = application.Xerox_Doc_Table__c;
            eStampSerObj.FormData.Xerox_Reason1 = application.Xerox_Reason1__c;
            eStampSerObj.FormData.Xerox_Reason2 = application.Xerox_Reason2__c;
            eStampSerObj.FormData.Sanction_Ref_Number = application.Sanction_Reference_Number__c;
            eStampSerObj.FormData.Sanction_date = formatDate(application.Sanction_Date__c); //date field
            eStampSerObj.FormData.Ap_LT = String.ValueOf(application.Sanction_Authority_Limit__c);//Sanction_Authority_Limit__c
            eStampSerObj.FormData.Guarantor_Details = GuarantorDetails;
            eStampSerObj.FormData.Int_ROI = string.valueOf(application.genesis__Interest_Rate__c);
            eStampSerObj.FormData.Loan_tenor = string.valueOf(application.genesis__Term__c);
            eStampSerObj.FormData.Holiday_period = string.valueOf(application.Holiday_Period__c);
            eStampSerObj.FormData.Guarantor1_Names  = Guarantor1Names;
            eStampSerObj.FormData.Guarantor2_Names  = Guarantor2Names;
            eStampSerObj.FormData.Guarantor3_Names  = Guarantor3Names;
            eStampSerObj.FormData.Guarantor4_Names  = Guarantor4Names;
            eStampSerObj.FormData.Guarantor5_Names  = Guarantor5Names;
            eStampSerObj.FormData.Guarantor6_Names  = Guarantor6Names;
            eStampSerObj.FormData.Guarantor7_Names  = Guarantor7Names;
            eStampSerObj.FormData.Guarantor8_Names  = Guarantor8Names;
            eStampSerObj.FormData.Guarantor9_Names  = Guarantor9Names;
            eStampSerObj.FormData.Guarantor10_Names = Guarantor10Names;	            
            eStampSerObj.FormData.Auth_Sign_Bnk = Constants.KvbBank ;
            eStampSerObj.FormData.Ap_LT_inwords = string.valueOf(application.Sanction_Amount_Words__c);
            eStampSerObj.FormData.Loan_Purpose = application.Loan_Purpose__c;
            eStampSerObj.FormData.Int_Type = application.genesis__Interest_Calculation_Method__c;
            eStampSerObj.FormData.ROI_MCLR = string.valueOf(application.Diff_B_w_ROI_MCLR__c);
            eStampSerObj.FormData.MCLR_Rate = string.valueOf(application.MCLR_Rate__c);
            eStampSerObj.FormData.Rating_Freq = (application.MCLR_Type__c =='One Year' ? '12' : '')+(application.MCLR_Type__c =='Three Months' ? '3' : ''); //string.valueOf(application.Rating_Frequency__c);
            eStampSerObj.FormData.PreClosure_Charges = application.Preclosure_Charges__c != null ? string.valueOf(application.Preclosure_Charges__c) : '0%';
            eStampSerObj.FormData.DD_FI = formatDate(application.genesis__Disbursement_Date__c + 30); //date field
            integer dis = (30 * (application.genesis__Term__c != null ? Integer.ValueOf(application.genesis__Term__c) : 0));
            eStampSerObj.FormData.DD_LI = formatDate(application.genesis__Disbursement_Date__c + dis); //date field
            eStampSerObj.FormData.Borrower1_Names = application.Primary_Applicant__c;
            eStampSerObj.FormData.Borrower2_Names = Borrower2Names;
            eStampSerObj.FormData.Borrower3_Names = Borrower3Names;
            eStampSerObj.FormData.Borrower4_Names = Borrower4Names;
            eStampSerObj.FormData.Borrower5_Names = Borrower5Names;
            String Ptype='';
            if(application.Property_Type__c == 'Flat'){
                Ptype = String.ValueOf(application.UDS_Sq_Ft__c);
            }else{
                Ptype = application.Area_of_the_Land__c;
            }
            eStampSerObj.FormData.Extent_Site_Uds = Ptype;
            
            eStampSerObj.StampPaperData.FirstPartyAddress = new Estamp_HelperStructure.FirstPartyAddress();
            eStampSerObj.StampPaperData.FirstPartyAddress.StreetAddress     = KVB_Company_Details__c.getOrgDefaults().StreetAddress_eStamp__c;
            eStampSerObj.StampPaperData.FirstPartyAddress.Locality          =KVB_Company_Details__c.getOrgDefaults().Locality_eStamp__c;
            eStampSerObj.StampPaperData.FirstPartyAddress.City              =KVB_Company_Details__c.getOrgDefaults().City_eStamp__c;
            eStampSerObj.StampPaperData.FirstPartyAddress.State             =KVB_Company_Details__c.getOrgDefaults().State_eStamp__c    ;
            eStampSerObj.StampPaperData.FirstPartyAddress.Pincode           =KVB_Company_Details__c.getOrgDefaults().Pincode_eStamp__c;
            eStampSerObj.StampPaperData.FirstPartyAddress.Country           =KVB_Company_Details__c.getOrgDefaults().Country_estamp__c;
            
            eStampSerObj.StampPaperData.SecondPartyAddress = new Estamp_HelperStructure.FirstPartyAddress();
            eStampSerObj.StampPaperData.SecondPartyAddress.StreetAddress    =application.genesis__Account__r.PersonMailingStreet;
            eStampSerObj.StampPaperData.SecondPartyAddress.Locality         ='';
            eStampSerObj.StampPaperData.SecondPartyAddress.City             =application.genesis__Account__r.PersonMailingCity;
            eStampSerObj.StampPaperData.SecondPartyAddress.State            =application.genesis__Account__r.PersonMailingState;
            eStampSerObj.StampPaperData.SecondPartyAddress.Pincode          =application.genesis__Account__r.PersonMailingPostalCode;
            eStampSerObj.StampPaperData.SecondPartyAddress.Country          =application.genesis__Account__r.PersonMailingCountry;
            
            //Inserting Table 
            eStampSerObj.FormData.TableData = new List<Estamp_HelperStructure.cls_TableData>();
            List<Estamp_HelperStructure.cls_TableData> tableList = new List<Estamp_HelperStructure.cls_TableData>();
            List<MOD_Documents__c> modList = new List<MOD_Documents__c>();
            modList = [Select Id,DOcNo__c,Date_Of_DOc__c,Nature_of_Doc__c,Parties_to_Document_From__c,Parties_to_Document_To__c From MOD_Documents__c Where Application__c =:application.Id  AND Document_Upload_Type__c =:'Copy'];
            Integer slNo = 0;
            if(modList.size()>0){
                for(MOD_Documents__c mod : modList){
                    slNo = slNo+1;
                    Estamp_HelperStructure.cls_TableData tb = new Estamp_HelperStructure.cls_TableData();
                    tb.sr_no = String.ValueOf(slNo);
                    tb.Doc_no = mod.DOcNo__c;  
                    tb.Doc_dt = formatDate(mod.Date_Of_DOc__c); //date field
                    tb.doc_nature = mod.Nature_of_Doc__c;
                    tb.transferor = mod.Parties_to_Document_From__c;
                    tb.transferee = mod.Parties_to_Document_To__c;
                    tableList.add(tb);
                }
                eStampSerObj.FormData.TableData = tableList;
            }
            
            
            return calleStampAPI(JSON.serialize(eStampSerObj),DocName,stampCharge,AcceToken);
        }catch(Exception e){
            system.debug('Error'+e.getStackTraceString()+'Line number'+e.getLineNumber());
        }
        return null;
    }
    public static EstampDocGenResp calleStampAPI(string eStampReq, String DocName, Decimal stampCharge, string ApiToken){
        
        try{
            EstampDocGenResp Est=new EstampDocGenResp();
            system.debug('$$$$'+eStampReq);
            KVB_Endpoint_URLs__c kvbEndpointURL = KVB_Endpoint_URLs__c.getValues('eStamp_url_test');
            Map<String,String> headerMap                            = new Map<String,String>();
            headerMap.put('Content-Type','application/json');
            headerMap.put('Authorization',ApiToken);
            HTTPResponse response                                   = new HTTPResponse();
            String endPoint                                         = kvbEndpointURL.Endpoint_URL__c;//'https://demo-eu05-test.apigee.net/next/v1/estamp'; 
            System.debug('Line 29@@@#####'+eStampReq);
            response = HttpUtility.sendHTTPRequest(endPoint, 'POST', null,eStampReq,headerMap,null);
            system.debug('$$$$'+response.getBody());
            If(response.getStatusCode() == 200 || response.getStatusCode() == 201){
                
                String jsonString = response.getBody();
                system.debug('jsonString'+DocName+'_____'+jsonString);
                Map<String, Object> obj = (Map<String, Object>) JSON.deserializeUntyped(jsonString);
                
                if(obj.get('status') == 'success'){
                    map<String,Object> obj1 = (map<String,Object>)obj.get('result');
                    //  System.debug((String)obj1.get('ApplicationID'));
                    //Inserting Document in Attachment
                    //  Digioe_Docs_Service.upsertDoc((String)obj1.get('ApplicationID'),DocName+'.pdf',(String)obj1.get('Content'));
                    //  
                    //  system.debug('12333'+'DocName'+DocName+'_'+(String)obj1.get('Content'));
                    // return (String)obj1.get('Content');
                    Est.Content=(String)obj1.get('Content');
                    Est.PageCount=(Integer)obj1.get('pageCount');
                    return Est; 
                }
                else{
                    system.debug('Status'+obj.get('status'));
                }
            }else{
                system.debug('Error'+response.getBody());
                // throw new CustomException('Status Code-'+response.getStatusCode()+' Status-'+response.getStatus());
            }
        }catch(Exception e){
            system.debug('Error'+e.getStackTraceString() +'LineNumber'+e.getLineNumber());
            //return null;
        } 
        return null;
    }
    public static void createVal(genesis__Applications__c app){
        List<genesis__Application_Parties__c> partiesList = [Select Id,genesis__Party_Type__c,Title_Holder__c,genesis__Party_Account_Name__r.PersonMailingStreet,genesis__Party_Account_Name__r.PersonMailingState,
                                                             genesis__Party_Account_Name__r.Name,genesis__Party_Account_Name__r.PersonMailingCity, 
                                                             genesis__Party_Account_Name__r.Father_Husband_Daughter_Name__c From genesis__Application_Parties__c where genesis__Application__c=:app.Id and active__c=true]; 
        
        List<genesis__Application_Parties__c> borrowerList = new List<genesis__Application_Parties__c>();
        List<genesis__Application_Parties__c> grantorList = new List<genesis__Application_Parties__c>();
        List<genesis__Application_Parties__c> titleList = new List<genesis__Application_Parties__c>();
        for(genesis__Application_Parties__c pt : partiesList){
            if(pt.genesis__Party_Type__c =='Co-Borrower'){
                borrowerList.add(pt);
            }
            if(pt.genesis__Party_Type__c == Constants.Gurantor){
                grantorList.add(pt);                
            }
        }
        for(genesis__Application_Parties__c pt : partiesList){
            if(pt.Title_Holder__c ==True){
                titleList.add(pt);
            }
            
        }   
        system.debug('$$$$'+titleList);
        
        if(borrowerList.size() > 0){            
            Borrower2Names = borrowerList[0].genesis__Party_Account_Name__r.Name;
            Borrower2_Addline1 = borrowerList[0].genesis__Party_Account_Name__r.PersonMailingStreet;
            Borrower2_Addline2 = borrowerList[0].genesis__Party_Account_Name__r.PersonMailingCity;
            Borrower2_Addline3 = borrowerList[0].genesis__Party_Account_Name__r.PersonMailingState;              
            Borrower2_FHDNAME = borrowerList[0].genesis__Party_Account_Name__r.Father_Husband_Daughter_Name__c;            
        }
        if(borrowerList.size() > 1){            
            Borrower3Names = borrowerList[1].genesis__Party_Account_Name__r.Name;
            Borrower3_Addline1 = borrowerList[1].genesis__Party_Account_Name__r.PersonMailingStreet;
            Borrower3_Addline2 = borrowerList[1].genesis__Party_Account_Name__r.PersonMailingCity;
            Borrower3_Addline3 = borrowerList[1].genesis__Party_Account_Name__r.PersonMailingState;              
            Borrower3_FHDNAME = borrowerList[1].genesis__Party_Account_Name__r.Father_Husband_Daughter_Name__c;            
        }
        if(borrowerList.size() > 2){            
            Borrower4Names = borrowerList[2].genesis__Party_Account_Name__r.Name;
            Borrower4_Addline1 = borrowerList[2].genesis__Party_Account_Name__r.PersonMailingStreet;
            Borrower4_Addline2 = borrowerList[2].genesis__Party_Account_Name__r.PersonMailingCity;
            Borrower4_Addline3 = borrowerList[2].genesis__Party_Account_Name__r.PersonMailingState;              
            Borrower4_FHDNAME  = borrowerList[2].genesis__Party_Account_Name__r.Father_Husband_Daughter_Name__c;            
        }
        if(grantorList.size() >0){
            Guarantor1Names = grantorList[0].genesis__Party_Account_Name__r.Name;  
        }
        if(grantorList.size() >1){
            Guarantor2Names = grantorList[1].genesis__Party_Account_Name__r.Name;  
        }
        if(grantorList.size() >2){
            Guarantor3Names = grantorList[2].genesis__Party_Account_Name__r.Name;  
        }
        if(grantorList.size() >3){
            Guarantor4Names = grantorList[3].genesis__Party_Account_Name__r.Name;  
        }
        if(grantorList.size() >4){
            Guarantor5Names = grantorList[4].genesis__Party_Account_Name__r.Name;  
        }
        if(grantorList.size() >5){
            Guarantor6Names = grantorList[5].genesis__Party_Account_Name__r.Name;  
        }        
        if(grantorList.size() >6){
            Guarantor7Names = grantorList[6].genesis__Party_Account_Name__r.Name;  
        }  
        if(grantorList.size() >7){
            Guarantor8Names = grantorList[7].genesis__Party_Account_Name__r.Name;  
        }   
        if(grantorList.size() >8){
            Guarantor9Names = grantorList[8].genesis__Party_Account_Name__r.Name;  
        } 
        if(grantorList.size() >9){
            Guarantor10Names = grantorList[9].genesis__Party_Account_Name__r.Name;  
        }        
        
        GuarantorDetails = (Guarantor1Names != '' ? Guarantor1Names : '') + (Guarantor2Names != '' ? '&'+Guarantor2Names :'') + (Guarantor3Names != '' ? '&'+Guarantor3Names : '') + (Guarantor4Names != '' ? '&'+Guarantor4Names : '' )+ Guarantor5Names != '' ? '&'+Guarantor5Names :'';
        
        
        Title_Holder1Name1 = app.Primary_Applicant__c;
        Title_Holder1_Addline1 = app.genesis__Account__r.PersonMailingStreet;
        Title_Holder1_Addline2 = app.genesis__Account__r.PersonMailingCity;
        Title_Holder1_Addline3 = app.genesis__Account__r.PersonMailingState;  
        //system.debug('@@@'+titleList[0].genesis__Party_Account_Name__r.PersonMailingState+'###'+titleList[0].genesis__Party_Account_Name__r.ID);
        
        if(titleList.size() >0){
            Title_Holder2Name1     = titleList[0].genesis__Party_Account_Name__r.Name;
            Title_Holder2_Addline1 = titleList[0].genesis__Party_Account_Name__r.PersonMailingStreet;
            Title_Holder2_Addline2 = titleList[0].genesis__Party_Account_Name__r.PersonMailingCity;
            Title_Holder2_Addline3 = titleList[0].genesis__Party_Account_Name__r.PersonMailingState;  
        }
        if(titleList.size() >1){
            Title_Holder3Name1     = titleList[1].genesis__Party_Account_Name__r.Name;
            Title_Holder3_Addline1 = titleList[1].genesis__Party_Account_Name__r.PersonMailingStreet;
            Title_Holder3_Addline2 = titleList[1].genesis__Party_Account_Name__r.PersonMailingCity;
            Title_Holder3_Addline3 = titleList[1].genesis__Party_Account_Name__r.PersonMailingState;  
        }
        if(titleList.size() >2){
            Title_Holder4Name1     = titleList[2].genesis__Party_Account_Name__r.Name;
            Title_Holder4_Addline1 = titleList[2].genesis__Party_Account_Name__r.PersonMailingStreet;
            Title_Holder4_Addline2 = titleList[2].genesis__Party_Account_Name__r.PersonMailingCity;
            Title_Holder4_Addline3 = titleList[2].genesis__Party_Account_Name__r.PersonMailingState;  
        }
        if(titleList.size() >3){
            Title_Holder5Name1     = titleList[3].genesis__Party_Account_Name__r.Name;
            Title_Holder5_Addline1 = titleList[3].genesis__Party_Account_Name__r.PersonMailingStreet;
            Title_Holder5_Addline2 = titleList[3].genesis__Party_Account_Name__r.PersonMailingCity;
            Title_Holder5_Addline3 = titleList[3].genesis__Party_Account_Name__r.PersonMailingState;  
        }
        if(titleList.size() >4){
            Title_Holder6Name1     = titleList[4].genesis__Party_Account_Name__r.Name;
            Title_Holder6_Addline1 = titleList[4].genesis__Party_Account_Name__r.PersonMailingStreet;
            Title_Holder6_Addline2 = titleList[4].genesis__Party_Account_Name__r.PersonMailingCity;
            Title_Holder6_Addline3 = titleList[4].genesis__Party_Account_Name__r.PersonMailingState;  
        }
        if(titleList.size() >5){
            Title_Holder7Name1     = titleList[5].genesis__Party_Account_Name__r.Name;
            Title_Holder7_Addline1 = titleList[5].genesis__Party_Account_Name__r.PersonMailingStreet;
            Title_Holder7_Addline2 = titleList[5].genesis__Party_Account_Name__r.PersonMailingCity;
            Title_Holder7_Addline3 = titleList[5].genesis__Party_Account_Name__r.PersonMailingState;  
        }
        if(titleList.size() >6){
            Title_Holder8Name1     = titleList[6].genesis__Party_Account_Name__r.Name;
            Title_Holder8_Addline1 = titleList[6].genesis__Party_Account_Name__r.PersonMailingStreet;
            Title_Holder8_Addline2 = titleList[6].genesis__Party_Account_Name__r.PersonMailingCity;
            Title_Holder8_Addline3 = titleList[6].genesis__Party_Account_Name__r.PersonMailingState;  
        }
        if(titleList.size() >7){
            Title_Holder9Name1     = titleList[7].genesis__Party_Account_Name__r.Name;
            Title_Holder9_Addline1 = titleList[7].genesis__Party_Account_Name__r.PersonMailingStreet;
            Title_Holder9_Addline2 = titleList[7].genesis__Party_Account_Name__r.PersonMailingCity;
            Title_Holder9_Addline3 = titleList[7].genesis__Party_Account_Name__r.PersonMailingState;  
        }
        if(titleList.size() >8){
            Title_Holder10Name1     = titleList[8].genesis__Party_Account_Name__r.Name;
            Title_Holder10_Addline1 = titleList[8].genesis__Party_Account_Name__r.PersonMailingStreet;
            Title_Holder10_Addline2 = titleList[8].genesis__Party_Account_Name__r.PersonMailingCity;
            Title_Holder10_Addline3 = titleList[8].genesis__Party_Account_Name__r.PersonMailingState;  
        }
        if(titleList.size() >9){
            Title_Holder11Name1     = titleList[9].genesis__Party_Account_Name__r.Name;
            Title_Holder11_Addline1 = titleList[9].genesis__Party_Account_Name__r.PersonMailingStreet;
            Title_Holder11_Addline2 = titleList[9].genesis__Party_Account_Name__r.PersonMailingCity;
            Title_Holder11_Addline3 = titleList[9].genesis__Party_Account_Name__r.PersonMailingState;  
        }
        if(titleList.size() >10){
            Title_Holder12Name1     = titleList[10].genesis__Party_Account_Name__r.Name;
            Title_Holder12_Addline1 = titleList[10].genesis__Party_Account_Name__r.PersonMailingStreet;
            Title_Holder12_Addline2 = titleList[10].genesis__Party_Account_Name__r.PersonMailingCity;
            Title_Holder12_Addline3 = titleList[10].genesis__Party_Account_Name__r.PersonMailingState;  
        }
        if(titleList.size() >11){
            Title_Holder13Name1 	= titleList[11].genesis__Party_Account_Name__r.Name;
            Title_Holder13_Addline1 = titleList[11].genesis__Party_Account_Name__r.PersonMailingStreet;
            Title_Holder13_Addline2 = titleList[11].genesis__Party_Account_Name__r.PersonMailingCity;
            Title_Holder13_Addline3 = titleList[11].genesis__Party_Account_Name__r.PersonMailingState;  
        }
        if(titleList.size() >12){
            Title_Holder14Name1 	= titleList[12].genesis__Party_Account_Name__r.Name;
            Title_Holder14_Addline1 = titleList[12].genesis__Party_Account_Name__r.PersonMailingStreet;
            Title_Holder14_Addline2 = titleList[12].genesis__Party_Account_Name__r.PersonMailingCity;
            Title_Holder14_Addline3 = titleList[12].genesis__Party_Account_Name__r.PersonMailingState;  
        }        
    }
    public static String formatDate(Date d) {
        if(d != null){
            return d.day() + '-' + d.month() + '-' + d.year();
        }
        return '';
    }
}