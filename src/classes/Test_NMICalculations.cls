@isTest
public class Test_NMICalculations {
@isTest
    public static void method1(){
         genesis__Applications__c app=TestUtility.intialSetUp('HomeLoan',true);
         Account acc=new Account(Name='TestName');
         acc.Financial_Applicant__c=true;
         acc.Salary_Credited_2__c=10000;
         insert acc;
        
         app.genesis__Account__c=acc.Id;
         
         update app;
        
         genesis__Application_Parties__c parobj=new genesis__Application_Parties__c(genesis__Application__c=app.id,genesis__Party_Account_Name__c=acc.id);
         insert parobj;
         NMICalculations.Calculations(app.Id);
    }
    
}