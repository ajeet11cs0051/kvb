/*
* Name 	    : SME_SendSMSService
* Compnay 	: ET Marlabs
* Purpose	: For SMS Service and Email. 
* Author 	: Raushan
*/
public class SME_SendSMSService {

    public static void sendSMSApp_SME(Set<String> applnIds){
    	try{
    		String endUrl = KVB_Company_Details__c.getOrgDefaults().FE_Portal_Url__c;
    		System.debug('applnIds::'+applnIds);
            
            List<genesis__Applications__c> listapplication = [select id,Name,genesis__Account__r.Name,genesis__Account__r.Cust_mobile_phone__c,Renewal_Due_Date__c,Application_Stage__c,Limit_Expiry_Date__c,genesis__Account__r.Company_Email__c,Branch_Name__c,Owner.Name,genesis__Account__r.Perfios_error_message__c,(select id,Name,Product_Name__c,Recommended_Limit__c,Existing_Limit__c from Facilities__r) from genesis__Applications__c where id IN :applnIds];
    		List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
    		If(listapplication !=null && listapplication.size() >0){
    			
    			for(genesis__Applications__c appObj : listapplication){
                    System.debug(appObj.Application_Stage__c);
    				String customerName = appObj.genesis__Account__r.Name;
    				String mobileNumber = appObj.genesis__Account__r.Cust_mobile_phone__c;
    				String email_id 	= appObj.genesis__Account__r.Company_Email__c;
    				
    				if(appObj.Application_Stage__c =='Identified for renewal'){
    					String messageAck ='Dear << Customer Name >>, your working capital loan with KVB is due for renewal on <<Due date>>. Please keep your audited financials ready for online submission.';
    					String DueDate		= String.valueOf(appObj.Renewal_Due_Date__c); 
    					messageAck = messageAck.replace('<< Customer Name >>',customerName);
    					messageAck = messageAck.replace('<<Due date>>',DueDate);
    					SMS_Services.sendSMSFutureCall(mobileNumber,messageAck);
    					
    					String emailMessage = SME_Email_Handler.getRenewalDueTemplates(appObj);
    					
    					emails.add(SME_Email_Handler.getEmailTemplates(email_id,emailMessage));
    				}
    				if(appObj.Application_Stage__c =='Application filling initiated'){
    					String messageAck = 'Dear << Customer Name >>, your working capital loan with KVB is due for renewal on <<Due date>>. Click here '+endUrl+' to renew your working capital loan online.';
    					String DueDate	= String.valueOf(appObj.Renewal_Due_Date__c);
    					messageAck = messageAck.replace('<< Customer Name >>',customerName);
    					messageAck = messageAck.replace('<<Due date>>',DueDate);
    					SMS_Services.sendSMSFutureCall(mobileNumber,messageAck);
    					String emailMessage = SME_Email_Handler.getRenewalApplicationSubmissionPending(appObj);
    					emails.add(SME_Email_Handler.getEmailTemplates(email_id,emailMessage));
    				}
    				if(appObj.Application_Stage__c =='Application review-Critical changes'){
    					String messageAck = 'Dear << Customer Name >>,  Thank you for submitting your working capital renewal application. Your working capital loan renewal application is under process. Please note the application no << app no >> for your reference';
    					String ApplicationNo	= appObj.Name;
    					messageAck = messageAck.replace('<< Customer Name >>',customerName);
    					messageAck = messageAck.replace('<< app no >>',ApplicationNo);
    					SMS_Services.sendSMSFutureCall(mobileNumber,messageAck);
    					String emailMessage = SME_Email_Handler.getLoanUnderProcessing(appObj);
    					emails.add(SME_Email_Handler.getEmailTemplates(email_id,emailMessage));
    				}
    				if(appObj.Application_Stage__c =='Interim Sanction' || appObj.Application_Stage__c =='Final Sanction'){
    					String messageAck = 'Dear << Customer Name >>,  Congratulations!!!. Your working capital loan renewal application has been sanctioned. You can login here to e-sign the sanction letter. A copy of the sanction letter has been sent to your registered email address.';
    					messageAck = messageAck.replace('<< Customer Name >>',customerName);
    					SMS_Services.sendSMSFutureCall(mobileNumber,messageAck);
    				}
    				/*if(appObj.Application_Stage__c =='E-sign pending'){
    					String messageAck = 'Dear << Customer Name >>, e-sign is pending on your working capital loan santion letter. Request you to complete the e-sign process by clicking here'+endUrl+';
    					messageAck = messageAck.replace('<< Customer Name >>',customerName);
    					SMS_Services.sendSMSFutureCall(mobileNumber,messageAck);
    				} */
    				if(appObj.Application_Stage__c =='E-sign pending'){
    					String messageAck = 'Dear << Customer Name >>,  your e-sign is pending on your working capital loan santion letter. Request you to complete the e-sign process by clicking here.'+endUrl;
    					messageAck = messageAck.replace('<< Customer Name >>',customerName);
    					SMS_Services.sendSMSFutureCall(mobileNumber,messageAck);
    					String emailMessage = SME_Email_Handler.getESignPartiallyCompleted_Individuals(appObj);
    					emails.add(SME_Email_Handler.getEmailTemplates(email_id,emailMessage));
    				}
    				if(appObj.Application_Stage__c =='Limit extended' || appObj.Application_Stage__c =='Sanction complete- Final sanction'){
    					String messageAck = 'Dear << Customer Name >>,  your working capital renewal application has been sanctioned and limit has been renewed upto << Limit validity date >>. You can download the sanction letter by clicking here'+endUrl;
    					String LimitValidityDate = String.valueOf(appObj.Limit_Expiry_Date__c);
    					messageAck = messageAck.replace('<< Customer Name >>',customerName);
    					messageAck = messageAck.replace('<< Limit validity date >>',LimitValidityDate);
    					SMS_Services.sendSMSFutureCall(mobileNumber,messageAck);
    					
    					String emailMessage = SME_Email_Handler.getSanctionedRenewed(appObj);
    					emails.add(SME_Email_Handler.getEmailTemplates(email_id,emailMessage));
    				}
    				if(appObj.Application_Stage__c =='Re-upload financials' || appObj.Application_Stage__c =='Interim Sanction'){
    					String messageAck = 'Dear << Customer Name >>,  Your working capital loan with Karur Vyasa Bank is due for renewal on <<Due date>>. You need to upload your audited financials by clicking here to complete the application'+endUrl;
    					String DueDate	= String.valueOf(appObj.Renewal_Due_Date__c);
    					messageAck = messageAck.replace('<< Customer Name >>',customerName);
    					messageAck = messageAck.replace('<<Due date>>',DueDate);
    					SMS_Services.sendSMSFutureCall(mobileNumber,messageAck);
    					
    					String emailMessage = SME_Email_Handler.getFinancialsSubmissionPending(appObj);
    					emails.add(SME_Email_Handler.getEmailTemplates(email_id,emailMessage));
    				}
    				if(appObj.Application_Stage__c =='Re-upload financials' || appObj.Application_Stage__c =='Interim Sanction'){
    					String messageAck = 'Dear << Customer Name >>,  Your working capital loan with Karur Vyasa Bank is due for renewal on <<Due date>>. You need to upload your income tax returns to complete the application.';
    					String DueDate	= String.valueOf(appObj.Renewal_Due_Date__c);
    					messageAck = messageAck.replace('<< Customer Name >>',customerName);
    					messageAck = messageAck.replace('<<Due date>>',DueDate);
    					SMS_Services.sendSMSFutureCall(mobileNumber,messageAck);
    					
    					
    					String emailMessage = SME_Email_Handler.getFinancialSubmissionPendingIndividuals(appObj);
    					emails.add(SME_Email_Handler.getEmailTemplates(email_id,emailMessage));
    				}
    				/*if(appObj.Application_Stage__c =='Financials Submission Pending'){
    					String messageAck ='';
    				} */
                    if(appObj.Application_Stage__c =='Re-upload financials'){
    					String messageAck ='Dear << Customer Name >>, Your working capital loan with Karur Vyasa Bank is due for renewal on <<Due date>>.You are requested to re-upload your audited financials by clicking here to complete the application'+endUrl;
                        String DueDate	= String.valueOf(appObj.Renewal_Due_Date__c);
    					messageAck = messageAck.replace('<< Customer Name >>',customerName);
    					messageAck = messageAck.replace('<<Due date>>',DueDate);
    					SMS_Services.sendSMSFutureCall(mobileNumber,messageAck);
                        
                       	String emailMessage = SME_Email_Handler.getPerfiosError(appObj);
    					emails.add(SME_Email_Handler.getEmailTemplates(email_id,emailMessage)); 
    				}
                    if(appObj.Application_Stage__c =='Perfios error'){
                        String imageLogo = SME_Email_Handler.getDocumentLogoUrl();
                        String mailSubject = 'Perfios data insert failed!!';
                        String body = 'Dear '+customerName+',<br><br>Perfios data upload got failed. please contact yourbranch manager.<br><br>Thanks.<br><br><img src="'+imageLogo+'"/>';
                        Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                        message.toAddresses = new List<String>{email_id};
                        message.setSubject(mailSubject);
                        message.setReplyTo('support@kvb.com');
                        message.setSenderDisplayName('KVB TEAM');
                        message.setBccSender(false);
                        message.setHtmlBody(body);
                        System.debug('message::'+message);
                        
                        Messaging.SingleEmailMessage[] messages =   new List<Messaging.SingleEmailMessage> {message};
                            Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                        
                        if (results[0].success) {
                            System.debug('The email was sent successfully.');
                        } else {
                            System.debug('The email failed to send: ' + results[0].errors[0].message);
                        }
    				}

    			}
    			Messaging.sendEmail(emails);
    		}
    	}catch(Exception ex){
    		System.debug('Line Number'+ex.getLineNumber());
    	}
    		
    }
}