/*
* Name          : M68C_Controller_Helper
* Description   : Web service to push perfios data into the system
* Author        : Dushyant
*/
@RestResource(urlMapping='/pushPerfiosData')
global class WS_PerfiosRequestProcess {
    //extending response structure
    global class Response extends WS_Response{
        public Response(){}
    }
    //handeling POST request
    @HttpPost
    global static Response handlePerfiosRequest(){   
        RestRequest req     = Restcontext.Request;
        Response res        = new Response();
        SavePoint sp     	= Database.setSavepoint();
        List<genesis__Applications__c> appList = new List<genesis__Applications__c>();
        //check for null request
        if(req == null || req.requestBody == null){
            return getWSResponse(res, Constants.WS_ERROR_STATUS, null, Constants.WS_ERROR_CODE, Constants.WS_REQ_BODY_IS_NULL);                     
        }else{
            try{
                String jsonString             = req.requestBody.toString();
                if(jsonString == null || jsonString == ''){
                    return getWSResponse(res, Constants.WS_ERROR_STATUS, null, Constants.WS_ERROR_CODE, 'Request body is null or blank');                    
                }
                else{
                    String fetchedFiscalYear = '';
                    Integer fetchedFiscalYearInt;
                    String fiscalYear;
                    List<Perfios__c> perfios = new List<Perfios__c>();
                    Map<String, Object> reqObj 			= (Map<String, Object>)JSON.deserializeUntyped(jsonString);
                    String trId 						= (String)reqObj.get('transaction_id');
                    Map<String, Object> fContent 		= (Map<String,Object>)reqObj.get('file_content');                    
                    Map<String, Object> fStatementObj 	= (Map<String,Object>)fContent.get('FinancialStatement');
                    Object custId						= fStatementObj.get('Organisation');
                    //Map<String,String> custIdMap		= (Map<String,String>)fStatementObj.get('Organisation');
                    
                    Map<String, Object> ratiosObj 		= (Map<String,Object>)fStatementObj.get('Ratios');
                    Map<String, Object> fYearObj;
                    Map<String, Object> fYearObjPrev;
                    
                    System.debug(trId);
                    System.debug(custId);
                    List<Account> accList = new List<Account>();
                    accList = [SELECT Id,(SELECT Application_Stage__c FROM genesis__Applications_account__r WHERE recordType.DeveloperName =: Constants.SME_APP_RECORD_TYPE) FROM Account WHERE CBS_Customer_ID__c =: String.valueOf(custId)];
                    if(accList.isEmpty()){
                        RETURN getWSResponse(res, Constants.WS_ERROR_STATUS,'', Constants.WS_ERROR_CODE, 'Customer Id not recognized!!!');
                    }
                    else{
                        for(Account acc : accList){
                            if(!acc.genesis__Applications_account__r.isEmpty()){
                                for(genesis__Applications__c app : acc.genesis__Applications_account__r){
                                    appList.add(app);
                                }
                            }
                        }
                        //appList = [SELECT Application_Stage__c FROM genesis__Applications__c WHERE genesis__Account__c =:perfios[0].Applicant_Name__c];
                    }
                    //Get fiscal year from the request
                    for(String key : fStatementObj.keySet()){
                        if(key.contains('FY')) fetchedFiscalYear = key;
                    }
                    if(fetchedFiscalYear == ''){
                        if(!appList.isEmpty()){
                            updateAppStatus(appList,false);
                        }
                        //return error message
                        RETURN getWSResponse(res, Constants.WS_ERROR_STATUS,'', Constants.WS_ERROR_CODE, 'Fiscal year is not in correct format');
                    }
                    else{
                        
                        try{
                            fetchedFiscalYearInt = Integer.valueOf(fetchedFiscalYear.subString(fetchedFiscalYear.length()-4,fetchedFiscalYear.length()));
                            fiscalYear = (fetchedFiscalYearInt-1)+'-'+Integer.valueOf(fetchedFiscalYear.subString(fetchedFiscalYear.length()-2,fetchedFiscalYear.length()));
                        }
                        catch(Exception e){
                            if(!appList.isEmpty()){
                                updateAppStatus(appList,false);
                            }
                            RETURN getWSResponse(res, Constants.WS_ERROR_STATUS,'', Constants.WS_ERROR_CODE, 'Fiscal year is not in correct format');
                        }
                        
                        perfios = [SELECT Id,Fiscal_Year__c,Applicant_Name__c,Data_received_for_financial_year__c FROM Perfios__c WHERE Transaction_Id__c =:trId];// AND Fiscal_Year__c =: fiscalYear
                        if(perfios.isEmpty()){
                            //Insert untagged perfios data
                            Perfios__c perf = new Perfios__c(Transaction_Id__c = trId,Fiscal_Year__c = fiscalYear,Applicant_Name__c = accList[0].Id);
                            INSERT perf;
                            perfios.add(perf);
                            //Insert Data
                            parse_insert_PerfiosData(fetchedFiscalYear,jsonString,perfios[0].Id);
                        }
                        else{
                            if(perfios[0].Fiscal_Year__c == fiscalYear){
                                if(!appList.isEmpty()){
                                    updateAppStatus(appList,false);
                                }
                                return getWSResponse(res, Constants.WS_SUCCESS_STATUS,'Perfios data already exist for financial year against pushed Transaction id', Constants.WS_SUCCESS_CODE,'');                   
                            }
                            else{
                                //Insert Data
                                perfios[0].Fiscal_Year__c = fiscalYear;
                                parse_insert_PerfiosData(fetchedFiscalYear,jsonString,perfios[0].Id);
                                UPDATE perfios[0];
                            }
                        }
                    }
                    try{
                        if(!appList.isEmpty()){
                            updateAppStatus(appList,true);
                        }
                        M68C_Computator.calculateM68Data(accList[0].Id,fiscalYear);
                    }
                    catch(Exception e){
                        System.debug(e.getMessage());
                        System.debug(e.getLineNumber());
                        RETURN getWSResponse(res, Constants.WS_SUCCESS_STATUS,'Success', Constants.WS_SUCCESS_CODE,'');  
                    }
                }
            }catch(Exception e){
                System.debug(e.getLineNumber());
                RETURN getWSResponse(res, Constants.WS_ERROR_STATUS,null, Constants.WS_ERROR_CODE, e.getMessage());                   
            }
        }
        return res;
    }
    //Update Application status perfios data insert fail
    public static void updateAppStatus(List<genesis__Applications__c> appList,Boolean errorStatus){
        for(genesis__Applications__c app : appList){
            if(errorStatus) app.Application_Stage__c = 'Perfios output done';
            else app.Application_Stage__c = 'Perfios error';
        }
        UPDATE appList;
        for(genesis__Applications__c app : appList){
            //Secound parameter to specify if its BRE first run
        	if(errorStatus) SME_BRE_Score_Calculator.runBRERule(app,false);
        }
        //if(!errorStatus) ApplicationTriggerHelper.deleteFinancialDataHelper(appList);
    }
    //method to parse/insert perfios data
    public static void parse_insert_PerfiosData(String fetchedFiscalYear, String jsonString, Id perfiosId){
        Map<String, Object> reqObj 			= (Map<String, Object>)JSON.deserializeUntyped(jsonString);
        Map<String, Object> fContent 		= (Map<String,Object>)reqObj.get('file_content');                    
        Map<String, Object> fStatementObj 	= (Map<String,Object>)fContent.get('FinancialStatement');
        Map<String, Object> ratiosObj 		= (Map<String,Object>)fStatementObj.get('Ratios');  
        Map<String, Object> fYearObj;
        PerfiosStructureBuilder.RatiosParse ratioParsedObj;
        //Parsing Ratio and insert
        ratioParsedObj = PerfiosStructureBuilder.ratioParser(JSON.serialize(ratiosObj.get(fetchedFiscalYear)));
        if(ratioParsedObj != null){
            PerfiosRequestHandler.inserRatios(ratioParsedObj,perfiosId);    
        }
        
        //CashFlowStatement handling
        PerfiosStructureBuilder.CashFlowStatementParse cfsParsedObj = PerfiosStructureBuilder.cfsParser(JSON.serialize(fStatementObj.get('CashFlowStatement')));
        if(cfsParsedObj != null){
            PerfiosRequestHandler.inserCFS(cfsParsedObj,perfiosId);  
        }
        
        //FY handling
        fYearObj = (Map<String,Object>)fStatementObj.get(fetchedFiscalYear);
        if(fYearObj != null){
            //P&L handling
            PerfiosStructureBuilder.ProfitAndLossParse plParsedObj 		= PerfiosStructureBuilder.plParser(JSON.serialize(fYearObj.get('ProfitAndLoss')));
            if(plParsedObj != null){
                PerfiosRequestHandler.inserPL(plParsedObj,perfiosId);    
            }
            //BalanceSheet handling
            PerfiosStructureBuilder.BalanceSheetParse bSheetParsedObj 	= PerfiosStructureBuilder.bSheetParser(JSON.serialize(fYearObj.get('BalanceSheet')));
            if(bSheetParsedObj != null){
                PerfiosRequestHandler.inserBSheet(bSheetParsedObj,perfiosId);    
            }
        }
    }
    //method to prepare response structure
    static Response getWSResponse(Response res, string status, string succMsg, string statusCode, string errMsg){
        res.status           = status;
        res.successMessage   = succMsg;
        res.statusCode       = statusCode;
        res.errorMessage  	 = errMsg;
        return res;
    }
}