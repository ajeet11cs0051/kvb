/*
* Name      : WS_CBS_Loan_Creation_HL
* Compnay   : ET Marlabs
* Purpose   : For HL Service(Open Loan Account in CBS and Link customer) 
* Author    : Subas
*/
public class WS_CBS_Loan_Creation_HL {
    @future (Callout=true)
    public Static void createLoanF(String appId,Map<string,string>accMap){
        createLoan(appID,accMap);
    }    
    public static Void createLoan(String appId,Map<string,string>accMap){
        genesis__Applications__c application = new genesis__Applications__c();
        application = queryService.getApp(appId);
        Loan_Creation_Helper_HL cbs = new Loan_Creation_Helper_HL();
        cbs.inputVariables = new Loan_Creation_Helper_HL.cls_inputVariables();
        Loan_Creation_Helper_HL.cls_inputVariables cbsInput = new Loan_Creation_Helper_HL.cls_inputVariables();
        cbsInput.in_msg = new Loan_Creation_Helper_HL.cls_in_msg();
        cbs.exe_tenantId ='cuecent_tenant';
        cbs.owner_tenantId = 'cuecent_tenant';
        cbs.inputVariables = cbsInput;
        cbsInput.in_msg.AssetValue = String.valueOf(application.Sanction_Authority_Limit__c);//String.valueOf(application.Total_Market_value__c);
        cbsInput.in_msg.BranchCode = application.Branch_Code__c;
        cbsInput.in_msg.Contribution = '0';
        cbsInput.in_msg.CustomerId = application.CustomerID__c;
        Decimal IntVar = VariationHL(application.id);
        // Need to add PL Interest variance
        if(application.Record_Type_Name__c == Constants.PERSONALLOAN){
            LoanProcess_Config__mdt proFees = queryService.processingFee('PL');
            System.debug('profees:::'+profees);
            if(String.valueOf(proFees.Indx_Variance__c) <> null && String.valueOf(application.genesis__Interest_Rate__c) <> null)
                IntVar = application.genesis__Interest_Rate__c - proFees.Indx_Variance__c;
        }
        if(IntVar != null){
            cbsInput.in_msg.InterestVariance = String.valueOf(IntVar);
        }
       // cbsInput.in_msg.InterestVariance = String.ValueOf(application.Interest_Variation_HL__c);
        cbsInput.in_msg.LoanAmount = String.valueOf(application.Sanction_Authority_Limit__c);
        cbsInput.in_msg.LoanPurpose = application.Loan_Purpose__c;
        cbsInput.in_msg.LoanTerm = String.valueOf(application.Sanctioned_Tenure__c);
        cbsInput.in_msg.ProductCode = application.Product_Code__c;
        cbsInput.in_msg.ScheduleName = 'EMI MONTHLY';
        cbsInput.in_msg.TransactionBranch = application.Branch_Code__c;
       
        if(accMap != null){
            cbsLoanCreation(JSON.serialize(cbs),appId,accMap);
        }else{
            cbsLoanCreation(JSON.serialize(cbs),appId,null);
        }
        
    }
    public static void cbsLoanCreation(String cbsCrt,String ApplicationId,Map<string,string>accMap){
        System.debug('#######'+cbsCrt);
        List<Account> accountList = new List<Account>();
        String LoanAccountNo = '';
        KVB_Endpoint_URLs__c kvb = KVB_Endpoint_URLs__c.getValues('CBS_Loan_Creation_HL');
        Map<String,String> headerMap                            = new Map<String,String>();
        headerMap.put('Content-Type','application/json');
        HTTPResponse response                                   = new HTTPResponse();
        String endPoint                                         = kvb.Endpoint_URL__c;
        response = HttpUtility.sendHTTPRequest(endPoint, 'POST', null,cbsCrt,headerMap,label.CA_CERTIFICATE);   
        system.debug('#####'+response);
        system.debug('$$$$'+response.getBody());
        If(response.getStatusCode() == 200 || response.getStatusCode() == 201){
            String jsonString = response.getBody();            
            Loan_Creation_Helper_HL.Loan_creation_response res = (Loan_Creation_Helper_HL.Loan_creation_response)Json.deserialize(jsonString,Loan_Creation_Helper_HL.Loan_creation_response.class);
            System.debug(res);
            if(res.out_msg.ErrorMessage == null){
                System.debug(res.out_msg.LoanResponse.AccountId);
                if(!String.isBlank(res.out_msg.LoanResponse.AccountId)){ 
                    LoanAccountNo = res.out_msg.LoanResponse.AccountId;
                    customerLinkage(LoanAccountNo,ApplicationId,accMap);
                }
            }
            else if(!String.isBlank(res.out_msg.ErrorMessage)){
                CBS_API_Log__c log = new CBS_API_Log__c();    
                log.Application__c = ApplicationId;
                log.API_Name__c = 'Loan_Account_Creation';
                log.Status__c = 'Failure';
                log.Success_Error_Message__c = res.out_msg.ErrorMessage;
                log.Sequence_No__c = '4';  
                insert log;
                system.debug('------'+accMap);
                if(accMap != null){
                    if(accMap.size()>0){
                        for(String str : accMap.keySet()){
                            if(str.length()>=15){
                                account acc = new account();
                                acc.Id = str;
                                acc.CBS_Customer_ID__c = accMap.get(str);
                                accountList.add(acc);
                            }
                        }
                        if(accountList.size()>0){
                            update accountList;                
                        }
                    }  
                }
            }
        }else{
            CBS_API_Log__c log = new CBS_API_Log__c();    
            log.Application__c = ApplicationId;
            log.API_Name__c = 'Loan_Account_Creation';
            log.Status__c = 'Failure';
            log.Success_Error_Message__c = 'LoanCreation_CBS_Error_No_Hit';
            log.Sequence_No__c = '4';  
            insert log;
        }
        
    }
    
    public Static Decimal VariationHL(string appId){
     genesis__Applications__c App=[select id,Age_Of_The_Building__c,genesis__Account__r.CIBIL_Score__c,genesis__Account__r.Financial_Applicant__c,(select id,genesis__Party_Type__c,genesis__Party_Account_Name__r.CIBIL_Score__c,genesis__Party_Account_Name__r.Financial_Applicant__c from genesis__Application_Parties__r where Active__c=true) from genesis__Applications__c where ID =:appId];
            system.debug('App'+App);
        decimal IntVaraitaion=0;
         Decimal MaxCiblScore = 0;
         List<Decimal> CibilScore=new List<Decimal>();
        if(App.genesis__Account__r.Financial_Applicant__c){
              CibilScore.add(App.genesis__Account__r.CIBIL_Score__c);
        }
         for(genesis__Application_Parties__c parties:App.genesis__Application_Parties__r){
                  
                    if(parties.genesis__Party_Type__c==Constants.Co_Borrower  && parties.genesis__Party_Account_Name__r.Financial_Applicant__c){
                        CibilScore.add(parties.genesis__Party_Account_Name__r.CIBIL_Score__c);
         }
         }
          if(CibilScore.size()>0){
                CibilScore.sort();
                MaxCiblScore=CibilScore[(CibilScore.size()-1)];    
            }
         Decimal IntProp = 0.00;  
         Decimal IntCIBIL = 0.00;
         if(App.Age_Of_The_Building__c > 30 && App.Age_Of_The_Building__c <= 50){
                            IntProp = 0.50;
                        }
         if(MaxCiblScore==10 || MaxCiblScore==9 || MaxCiblScore==8 || MaxCiblScore==7|| MaxCiblScore==6 || MaxCiblScore==5 || MaxCiblScore==1 || MaxCiblScore==2 ||  MaxCiblScore==3 || MaxCiblScore==4 || MaxCiblScore== -1 ) {
                        IntCIBIL = 0.50;
                      IntVaraitaion=  IntCIBIL +IntProp;
                                            }
                    else{
                        
                        if( MaxCiblScore>650  &&  MaxCiblScore<700){
                          IntVaraitaion=  2 + IntProp;
                                                        }
                        else if( MaxCiblScore>=700 &&  MaxCiblScore<750){
                          IntVaraitaion=  IntProp;
                            }
                        else if( MaxCiblScore>=750){
                             Decimal mclrrate_oneYear= ApplicationEligibiltyHandler.getMCLR(365,null,null);
                             Decimal mclrrate_ThreeMonths= ApplicationEligibiltyHandler.getMCLR(90,null,null);
                          IntVaraitaion=  mclrrate_ThreeMonths -mclrrate_oneYear + IntProp;
                            }
                    }
        return IntVaraitaion;
                    
    }
    
    /********************************Customer Linkage HL********************************/
    @future (Callout=true)
    public static void customerLinkageF(String loanNo, String appId,Map<string,string>accMap){
        customerLinkage(loanNo,appId,accMap);
    }    
    public static void customerLinkage(String loanNo, String appId,Map<string,string>accMap){
        List <Account> accList = queryService.accList(appId);
        genesis__Applications__c application = queryService.getApp(appId);
        if(loanNo ==null){
            loanNo = application.Loan_Account_Number__c;
        }        
        Map<String,String> partyMap = new Map<String,String>();
        List<genesis__Application_Parties__c> partyList = new List<genesis__Application_Parties__c>();
        partyList = [Select Id,genesis__Party_Type__c,genesis__Party_Account_Name__c From genesis__Application_Parties__c Where genesis__Application__c =: appId];
        for(genesis__Application_Parties__c pt : partyList){
            partyMap.put(pt.genesis__Party_Account_Name__c,pt.genesis__Party_Type__c);
        }
        CBS_CustomerLinkage_Formatter.RequestFormat cbsReqObj = new CBS_CustomerLinkage_Formatter.RequestFormat();
        cbsReqObj.exe_tenantId      = 'cuecent_tenant';
        cbsReqObj.owner_tenantId    = 'cuecent_tenant';
        cbsReqObj.inputVariables    = new CBS_CustomerLinkage_Formatter.cls_inputVariables();
        cbsReqObj.inputVariables.in_msg = new CBS_CustomerLinkage_Formatter.cls_in_msg();
        cbsReqObj.inputVariables.in_msg.TransactionBranch = application.Branch_Code__c;
        cbsReqObj.inputVariables.in_msg.Mode = 'M';
        cbsReqObj.inputVariables.in_msg.ValueDateText = String.ValueOf(system.today().format()).replace('/','-'); //!= null ? String.ValueOf(system.today().format()).replace('/','-') : '';//'01-01-2018';
        
        List<CBS_CustomerLinkage_Formatter.cls_CustDetl> custDetailList = new List<CBS_CustomerLinkage_Formatter.cls_CustDetl>();
        Integer co_brr = 0;
        Integer gr_brr = 0;
        for(Account acc : accList){
            if(partyMap.get(acc.Id) == Constants.Co_Borrower){    
                co_brr++;
            }
            else if(partyMap.get(acc.Id) == Constants.Gurantor){
                gr_brr++;
            }
        }
        for(Account acc : accList){
            CBS_CustomerLinkage_Formatter.cls_CustDetl custDetail = new CBS_CustomerLinkage_Formatter.cls_CustDetl();
            custDetail.AccountNo = loanNo;
            custDetail.CustId1 = acc.CBS_Customer_ID__c;
            if(accList.size() == 1){
                custDetail.CustRel1 = 'SOW';
            }
            else if(co_brr == 0 && gr_brr > 0){
                if(application.genesis__Account__c == acc.Id){
                    custDetail.CustRel1 = 'SOW';
                }else if(partyMap.get(acc.Id) == Constants.Gurantor){
                    custDetail.CustRel1 = 'GUA';
                }
            }else{
                if(application.genesis__Account__c == acc.Id){
                    custDetail.CustRel1 = 'JOF';                
                }else if(partyMap.get(acc.Id) == Constants.Co_Borrower){                
                    custDetail.CustRel1 = 'JOO';
                }else if(partyMap.get(acc.Id) == Constants.Gurantor){
                    custDetail.CustRel1 = 'GUA';
                }
            }      
            custDetail.FlgDel = 'N';
            custDetailList.add(custDetail);
        }
        cbsReqObj.inputVariables.in_msg.CustDetl = custDetailList;
        customerLinkageCallout(JSON.serialize(cbsReqObj),appId,accMap,loanNo);
        
    }
    //Helper method to make callout for Customer Linkage
    
    public static void customerLinkageCallout(String custData,String appId,Map<string,string>accMap,String loanNo){
        List<Account> accountList = new List<Account>();
        String CustLinkRes = '';
        KVB_Endpoint_URLs__c endPointUrl = KVB_Endpoint_URLs__c.getValues('CBS_CUST_LINKAGE');
         genesis__Applications__c AppIDPL = queryService.getApp(appId);
        if(endPointUrl != null){
            system.debug('####'+custData);
            Map<String,String> headerMap                            = new Map<String,String>();
            headerMap.put('Content-Type','application/json');
            HTTPResponse response                                   = new HTTPResponse();
            String endPoint                                         = endPointUrl.Endpoint_URL__c;
            response = HttpUtility.sendHTTPRequest(endPoint, 'POST', null,custData,headerMap,label.CA_CERTIFICATE);
            system.debug('$$$$'+response.getBody());
            If(response.getStatusCode() == 200 || response.getStatusCode() == 201){
                String jsonString = response.getBody();
                CBS_CustomerLinkage_Formatter.ResponseFormat res = (CBS_CustomerLinkage_Formatter.ResponseFormat)Json.deserialize(jsonString, CBS_CustomerLinkage_Formatter.ResponseFormat.class);
                system.debug('###'+res.out_msg.ErrorMessage);
                if(res.out_msg.ErrorMessage == null){
                     if(AppIDPL.Record_Type_Name__c == Constants.PERSONALLOAN)
                            { // For Personal Loan call CBR
                                 WS_CBS_CBR_DTLS_HL.CBR_DTLSF(AppId);
                            }else{
                                //Call Colleteral Creation
                                WS_CBS_Collateral_Creation.Collateral_Creation(appId,accMap,loanNo);
                            }
                }else{
                    //CustLinkRes = res.out_msg.ErrorMessage;
                    CBS_API_Log__c log = new CBS_API_Log__c();    
                    log.Application__c = appId;
                    log.API_Name__c = 'Loan_Account_Linkage';
                    log.Status__c = 'Failure';
                    log.Success_Error_Message__c = res.out_msg.ErrorMessage+'('+loanNo+')';
                    log.Sequence_No__c = '5';  
                    insert log;
                    if(accMap != null){
                        if(accMap.size()>0){
                            for(String str : accMap.keySet()){
                                if(str.length()>=15){
                                    account acc = new account();
                                    acc.Id = str;
                                    acc.CBS_Customer_ID__c = accMap.get(str);
                                    accountList.add(acc);
                                }
                            }
                            if(accountList.size()>0){
                                update accountList;                
                            }
                        }
                    }
                    if(loanNo != null && appId !=null){
                        genesis__Applications__c a= new genesis__Applications__c();
                        a.Id = appId;
                        a.Loan_Account_Number__c = loanNo;
                        a.Interest_Variation__c = VariationHL(appId);
                        update a;
                    }
                }
            }else{
                CBS_API_Log__c log = new CBS_API_Log__c();    
                log.Application__c = appId;
                log.API_Name__c = 'Loan_Account_Linkage';
                log.Status__c = 'Failure';
                log.Success_Error_Message__c = 'LoanLinkage_CBS_Error_No_Hit';
                log.Sequence_No__c = '5';  
                insert log; 
            }
        }
        
    }
    
}