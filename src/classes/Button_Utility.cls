global class Button_Utility {
    //method called from Application page on button click
    webService static void runBRE(String appId){
        genesis__Applications__c app;
        Map<String, Schema.SObjectField> accountFieldMap = Schema.getGlobalDescribe().get('genesis__Applications__c' ).getDescribe().fields.getMap();
        
        String query = 'SELECT ';    
        for(Schema.SObjectField s : accountFieldMap.values()){
            query = query + s.getDescribe().getName()+',';    
        }
        query   = query + 'genesis__Account__r.Date_of_Incorporation__c,genesis__Account__r.CUSTSINCE__c ';

        //query   = query.removeEnd(',');
        query   = query + ' FROM genesis__Applications__c WHERE Id =: appId';
        System.debug(query);
        app = Database.query(query);
        
        SME_BRE_Score_Calculator.runBRERule(app,true);
    }
    
    //method called from Application page on button click
    webService static void runSanctionAuth(String appId){
        //Re run sanction Auth
        If(appId !=null){
            Database.executeBatch(new SanctionMatrix_Calc_Batch(appId));
        }
    }
    webService static string calSecurityCoverage(String appId){
        try{
            User userRec;
            genesis__Applications__c     appRec;
            String bmDesignation                                    ='';
            String facilityChildRTypeID                             = Schema.SObjectType.Facility__c.getRecordTypeInfosByName().get('Child').getRecordTypeId();
            List<Facility__c> bmTaskList                            = new List<Facility__c>();
            if(appId != null && appId != ''){
                appRec                                              = [SELECT Id,OwnerId,RecordType.DeveloperName from genesis__Applications__c where Id=:appId];
                if(appRec.OwnerId != null){
                    userRec = [Select Id,Designation__c FROM User where id=:appRec.OwnerId];
                    if(userRec.Designation__c != null){
                        bmDesignation                               = userRec.Designation__c;
                    }
                }
            }
            
            string query                                            = 'Select id,Name,Application__c,Application__r.ownerId,Application__r.Application_Stage__c,Limit_Amount__c,Existing_Limit__c,Type__c,Application__r.genesis__Account__c,CL_Product__c'+', (select id,genesis__Collateral__c from Facility_Security__r) from Facility__c where Application__c =:appId';
            List<Facility__c> facList                               = Database.query(query);
            SanctionMatrix_Calculation sanc                         = new SanctionMatrix_Calculation();
            Map<String,Decimal>  securCoverage                      = sanc.calculateAuthority(facList,true,null,null);
            System.debug('@@@@'+securCoverage);
            genesis__Applications__c appln                          = new genesis__Applications__c();
            appln.Id                                                = appId;
            system.debug(securCoverage);
            appln.Estimated_Security_Coverage__c                    = securCoverage.get(appId);
            system.debug(appln);
            update appln;
            if(!facList.isEmpty()){
                for(Facility__c facRec:facList){
                    
                    Facility__c bmFacility                          = new Facility__c();
                    bmFacility.Facility__c                          = facRec.Id;
                    bmFacility.CL_Product__c                        = facRec.CL_Product__c;
                    bmFacility.EME_BM_User__c                       = TRUE ;
                    bmFacility.Higher_Authority_Order__c            = 1;
                    if(bmDesignation != '')
                        bmFacility.Role__c                          = bmDesignation;
                    bmFacility.RecordTypeId                         = facilityChildRTypeID;
                    bmTaskList.add(bmFacility);
                    
                }
            }
            Task taskForBM;
            if(appId != null && appRec.OwnerId != null){
                taskForBM                                           = SanctionUnderwriting_Process.getTaskObj(appId, appRec.OwnerId, 'Eme bm review needed', 'EME BM Review');
            }

            if(!bmTaskList.isEmpty()){
                //if(appRec.RecordType.DeveloperName == 'SME_Renewal')
                insert bmTaskList;
            }
            if(taskForBM != null ){
                insert taskForBM;
            }
            return String.ValueOf(securCoverage.get(appId));
        }catch(Exception ex){            
            system.debug('Exception::'+ex.getMessage());
            return null;
        }
    }
}