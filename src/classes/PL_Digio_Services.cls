public class PL_Digio_Services {
	 
	@future (callout=true)
    public static void docGenAndSignDocPL(String appId ,String Filename){
        
        try{ 
            String docEncodeFile  = '';
            Integer pageCount       = 0;
            string class2Doc='';
            string FF='';
            string Docid='';
            genesis__Applications__c appln = [Select id,genesis__Account__c,Docgen_Fail__c,Sanction_Doc_Generation_Check__c,Document_Generation__c from genesis__Applications__c where Id =: appId];
            
            
            String Document_Gen = appln.Document_Generation__c != null ? appln.Document_Generation__c : 'None';
            if(Filename==Constants.PRE_APPROVE_SANCTION_PL){
                /*if(!Document_Gen.contains(Constants.PreApproveGenerated) && !Document_Gen.contains(Constants.PreApproveClass2)){
                    FF=Filename;
                } else{
                    FF='';
                    system.debug('Pre Approval Document Generated.');
                }*/
                 FF=Filename;
            }
            string templateId   = Utility.getDIGIOTemplateId(FF); 
            system.debug('templateId'+templateId);
            //Calling the document DIGIO generation service
            Digioe_Docs_Service.DocGenResponse docResp= Digioe_Docs_Service.getEDocs(appId,FF,templateId);
            docEncodeFile = docResp.outMap.Data.outfile;
            pageCount   = Integer.valueOf(docResp.outMap.Data.totalpages);
            system.debug('docEncodeFile'+docEncodeFile);
            if(docEncodeFile == '' || docEncodeFile == null){
                // throw new CustomException('Document Generation Failed');
                appln.Docgen_Fail__c = TRUE;
            }else{
                appln.Sanction_Doc_Generation_Check__c  = TRUE;
            }
            
            
            KVB_Company_Details__c company = KVB_Company_Details__c.getOrgDefaults();
            
            //Calling the Class-2 service
            class2Doc = Digioe_Docs_Service.class2Signer(new Digioe_Docs_Service.DocSignerRequest(SYSTEM.LABEL.DigioKeyPersonName,FF,company.City__c,docEncodeFile),pageCount,FF);
            if(class2Doc == null && docEncodeFile != null) {
                // Digioe_Docs_Service.upsertDoc(appId,Filename+'.pdf',docEncodeFile);
            }else{
                // Digioe_Docs_Service.upsertDoc(appId,Filename+'.pdf',class2Doc);
            }
            
            if(class2Doc == '' || class2Doc == null){
                system.debug('##### class2Doc doc getting null ---------------> ');
            }
            
            system.debug('class2Doc'+class2Doc);
            if(FF==Constants.PRE_APPROVE_SANCTION_PL){
                Docid=DigioDocID_EsignPL.DocumentIDGeneration(appId, FF, pageCount,class2Doc);
                system.debug('##### doc idss---------------> '+ Docid);
                if(Docid!=null && (!String.isBlank(Docid))){
                    if(Document_Gen!=null){
                        system.debug('##### doc idss---------------> '+ Docid);
                        Document_Gen= Document_Gen + ';' + Constants.PL1_DOCGEN+';'+Constants.PL1_CLASS2 +';'+Constants.PL1_DOCID;
                    }
                    else{
                        Document_Gen=Constants.PL1_DOCGEN+';'+Constants.PL1_CLASS2 +';'+Constants.PL1_DOCID;
                    } 
                }
                else{
                    throw new CustomException(FF+'  Borrower Signner Failed');
                }
                
            }
            List<Attachment> attList = new List<Attachment>();
            Attachment att = new Attachment();
            attList = [Select Id,Body,ParentId,Name from Attachment where ParentId=:appId AND Name=:FF+'.pdf' limit 1];
            if(!attList.isEmpty()){
                att = attList[0];
                att.Body = EncodingUtil.base64Decode(class2Doc);
                update att;
            }else{
                system.debug('##### attachments class 2 store ');
                att.Name = FF+'.pdf';
                att.ParentId = appId;
                att.Body = EncodingUtil.base64Decode(class2Doc);
                insert att;
            }

            appln.Document_Generation__c=Document_Gen;
            
            List<genesis__Application_Document_Category__c> GAdc=[select id from genesis__Application_Document_Category__c where name=:Constants.LoanDocuments And 	genesis__Application__c=:AppID limit 1];
            List<genesis__AppDocCatAttachmentJunction__c> GaList=[select id,genesis__AttachmentId__c from genesis__AppDocCatAttachmentJunction__c where genesis__AttachmentId__c=:att.id and 	genesis__Application_Document_Category__c=:GAdc[0].id and   Document_Name__c=:FF+'.pdf' limit 1] ;
            
            if(GaList.isEmpty()){
                genesis__AppDocCatAttachmentJunction__c AAJ=new genesis__AppDocCatAttachmentJunction__c();
                AAJ.genesis__Application_Document_Category__c=GAdc[0].id;
                AAJ.genesis__AttachmentId__c=att.id;
                AAJ.Document_Name__c =FF;
                insert AAJ;
            }
            if(FF==Constants.PRE_APPROVE_SANCTION_PL){
                List<Digio_Document_ID__c> Dlist=[select id,Application__c,Document_ID__c from Digio_Document_ID__c where 	Name=:FF and Application__c =:appId];
                Digio_Document_ID__c Did=new Digio_Document_ID__c();
                
                Did.Name=FF;
                Did.Application__c=appId;
                Did.Document_ID__c=Docid;
                if(Dlist.size()>0){
                    DELETE Dlist  ; 
                }
                insert Did;            
                set<id> AcciDs=new set<Id>();
                AcciDs.add(appln.genesis__Account__c) ;
                integer pcount=0;
                integer count=0;
               
                if(AcciDs.size()>0){
                    List<Document_Applicant__c> DocApp=new List<Document_Applicant__c>();
                    for(string s:AcciDs){
                        Document_Applicant__c DApp=new Document_Applicant__c();
                        DApp.Account__c=s;
                        DApp.Digio_Document_ID__c=Did.Id;
                        DocApp.add(DApp);
                    }
                    insert DocApp;
                }
            }
            update appln;
        }
        
        catch(exception e){
            system.debug('Error'+e.getLineNumber()+e.getStackTraceString());
        }
    }

}