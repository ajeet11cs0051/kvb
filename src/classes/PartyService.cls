public class PartyService {
    
    public static genesis__Application_Parties__c createParty(id AccId,String PartyType,string RelationToBorrower,id AppID,Id ParentAppID){
        genesis__Application_Parties__c  party = createPartyRep(AccId,PartyType,RelationToBorrower,AppID,ParentAppID,null);
        return party;
    }
    public static genesis__Application_Parties__c createPartyRep(id AccId,String PartyType,string RelationToBorrower,id AppID,Id ParentAppID,String ProdType){    
        genesis__Application_Parties__c  party = new genesis__Application_Parties__c();
        
        party.genesis__Party_Account_Name__c = Accid;
        party.genesis__Party_Type__c = PartyType;
        party.genesis__Application__c = AppID;
        party.Key_Contact__c=ParentAppID;
        party.Relationship_to_Borrower__c=RelationtoBorrower;
        if(ProdType != null){
            party.Product_Type__c = ProdType; 
        }           
        insert party;
        return party;   
    }
    
    // using Applicant ID
    public static list<genesis__Application_Parties__c> getpartys(string accid){
        return [select id,genesis__Party_Account_Name__c,Active__c,genesis__Application__c from genesis__Application_Parties__c 
                where 	genesis__Party_Account_Name__c =:accid AND Active__c =:true];
        
    }
    
    // Using Application ID
    public static list<genesis__Application_Parties__c> getApppartys(string appid){
        return [select id,genesis__Party_Account_Name__c,Active__c,genesis__Application__c from genesis__Application_Parties__c 
                where 	genesis__Application__c =:appid AND Active__c =:true];
        
    }
}