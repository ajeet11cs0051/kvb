/*
*/ 
public class Handle_FetchSMEReq_Response {
    
    public static Customer360view getApplicationInfo(string customerID, string appId){
		
        Integer currentYear                                             = Utility.getCurrentYear();
        String cyString                                                 = String.valueOf(currentYear);
        String currentFY                                                = (currentYear-1)+'-'+Integer.valueOf(cyString.subString(cyString.length()-2,cyString.length()));
        String prevFY                                                   = (currentYear-2)+'-'+Integer.valueOf(String.valueOf(currentYear-1).subString(String.valueOf(currentYear-1).length()-2,String.valueOf(currentYear-1).length()));
        String nextEstFiscalYear                                        = currentYear+'-'+Integer.valueOf(String.valueOf(currentYear+1).subString(String.valueOf(currentYear+1).length()-2,String.valueOf(currentYear+1).length()));
        String nextProjFiscalYear                                       = (currentYear+1)+'-'+Integer.valueOf(String.valueOf(currentYear+2).subString(String.valueOf(currentYear+2).length()-2,String.valueOf(currentYear+2).length()));

        List<DocumentFetch.DocumentCategory> allDocCat                              = new List<DocumentFetch.DocumentCategory>();
        SMELoan_Helper.documentCategoryType                                         = new List<String>{'SME Financial Document','SME EME Document'};

        Map<Id,genesis__Applications__c> applnMap                                   = SOQL_Util.getApplicationDetail(appId,customerID);
        System.debug('wrp@@@@'+applnMap);
        //System.debug('wrp@@@@'+applnMap.values()[0]);
        genesis__Applications__c applInfo                                           = applnMap.values()[0];
        List<Facility__c> facilityList                                              = SOQL_Util.getFacilities(appID);
        Customer360view wrp                                                         = new Customer360view();
        
        wrp                                                                         = SMELoan_Helper.getCustomAppDetails(applnMap,customerID,appId,facilityList);
        //wrp.KEY_PERSON_DETAILS    												= new List<Customer360view.cls_KEY_PERSON_DETAILS>();
        
       
        //wrp.NEW_REQUEST.COLLATERAL_DETAILS										= new List<Customer360view.cls_NETWORTH_DETAILS>();
        System.debug('wrp@@@@'+applInfo.Application_Collateral__r);
		//wrp																		= getCollateral(wrp,applInfo.Application_Collateral__r);	
        //System.debug('wrp@@@@'+wrp);
        //wrp 																		= getKeyPersonDetails(wrp,applInfo.genesis__Application_Parties__r);
        //System.debug('wrp@@@@'+wrp.KEY_PERSON_DETAILS);
        //wrp.loanInfo                                                              = new Customer360view.cls_LOANDETAIL();


        wrp.LIMIT_ASSESMENT                                                          = new list<Customer360view.cls_LIMIT_ASSESMENT>();
        
       // wrp.MISSING_FINACIAL_YEARS                                                  = new List<String>();
        
        wrp.CUSTOMER_ID                                                             = Utility.getBlankStringIfNull(applinfo.customerId__c);
        wrp.LOS_CUSTOMER_ID															= Utility.getBlankStringIfNull(applinfo.genesis__Account__c);	
        wrp.APP_TYPE                                                                = applInfo.RecordType.DeveloperName;
        if(applInfo != null){
            if(applInfo.Cash_Flow_Method__c != null){
                wrp.COMPANY_DETAILS.cash_flow_method                                = applInfo.Cash_Flow_Method__c==true?'1':'0';
            }
        }
        wrp.COMPANY_DETAILS.CUSTOMER_NAME                                           = Utility.getBlankStringIfNull(applInfo.genesis__Account__r.Name);
        wrp.COMPANY_DETAILS.OPENING_BALANCE                                         = String.valueOf(applInfo.genesis__Account__r.Limit_Assesment_Closing_Balance__c);
        
        
        wrp.NEW_REQUEST                                                             = new Customer360view.cls_APP_REQUEST();
        wrp.NEW_REQUEST.REQ_REASON                                                  = '';
        wrp.NEW_REQUEST.REASON_FOR_APPLICATION                                      = '';
        wrp.NEW_REQUEST.LIMIT_PERIOD                                                = '';
        wrp.NEW_REQUEST.REQ_REASON                                             		= Utility.getBlankStringIfNull(applInfo.Request_Reason__c);
        wrp.NEW_REQUEST.REASON_FOR_APPLICATION                                 		= Utility.getBlankStringIfNull(applInfo.Reason_for_Application__c);
        wrp.NEW_REQUEST.LIMIT_PERIOD                                           		= Utility.getBlankStringIfNull(String.valueOf(applInfo.EAE_Renewal_Date_in_days__c));
        wrp.NEW_REQUEST.COLLATERAL_DETAILS                                          = new List<Customer360view.cls_NETWORTH_DETAILS>();
        //wrp.NEW_REQUEST.COLLATERAL_DETAILS.TILE_HOLDERS                             = new List<Customer360view.cls_TITLE_HOLDER>();
        wrp.NEW_REQUEST.FACILITY_INFO                                               = new List<Customer360view.cls_FACILITIES>();
        for(Facility__c fac : facilityList){
            Customer360view.cls_FACILITIES facInfo                                  = new Customer360view.cls_FACILITIES();
            facInfo.FACILITY_ID                                                     = fac.Id;
            facInfo.NAME_OF_THE_FACILITY                                            = Utility.getBlankStringIfNull(fac.Product_Name__c);
            facInfo.ACCOUNT_NUMBER                                                  = Utility.getBlankStringIfNull(fac.Account_Number__c);
            facInfo.LIMIT_AMT                                                       = Utility.getBlankStringIfNull(String.Valueof(fac.Existing_Limit__c));
            facInfo.INTEREST_RATE                                                   = Utility.getBlankStringIfNull(String.Valueof(fac.Existing_Rate__c));
            facInfo.PRODUCT_CODE                                                    = Utility.getBlankStringIfNull(String.Valueof(fac.CL_Product__r.Product_Code__c));
            facInfo.NEW_LIMIT_AMT                                              		= Utility.getBlankStringIfNull(String.Valueof(fac.New_Limit_Amount__c));
            wrp.NEW_REQUEST.FACILITY_INFO.add(facInfo);
        }

        wrp.SUMMARY_VIEW.REQUEST_SUMMARY                                            = new Customer360view.cls_REQUEST_SUMMARY();
        wrp.SUMMARY_VIEW.REQUEST_SUMMARY.REASON_FOR_APPLICATION                     = Utility.getBlankStringIfNull(applInfo.Reason_for_Application__c);
        wrp.SUMMARY_VIEW.REQUEST_SUMMARY.LIMIT_PERIOD                               = Utility.getBlankStringIfNull(String.valueOf(applInfo.EAE_Renewal_Date_in_days__c));
        wrp.SUMMARY_VIEW.REQUEST_SUMMARY.FACILITY_INFO                              = new List<Customer360view.cls_FACILITIES>();
        for(Facility__c fac : facilityList){
            Customer360view.cls_FACILITIES facInfo                                  = new Customer360view.cls_FACILITIES();
            facInfo.FACILITY_ID                                                     = fac.Id;
            facInfo.NAME_OF_THE_FACILITY                                            = Utility.getBlankStringIfNull(fac.Product_Name__c);
            facInfo.ACCOUNT_NUMBER                                                  = Utility.getBlankStringIfNull(fac.Account_Number__c);
            facInfo.LIMIT_AMT                                                       = Utility.getBlankStringIfNull(String.Valueof(fac.Existing_Limit__c));
            facInfo.INTEREST_RATE                                                   = Utility.getBlankStringIfNull(String.Valueof(fac.Existing_Rate__c));
            facInfo.PRODUCT_CODE                                                    = Utility.getBlankStringIfNull(String.Valueof(fac.CL_Product__r.Product_Code__c));
            If(fac.New_Limit_Amount__c !=null){
                facInfo.NEW_LIMIT_AMT                                               = String.valueOf(fac.New_Limit_Amount__c);
                wrp.SUMMARY_VIEW.REQUEST_SUMMARY.FACILITY_INFO.add(facInfo);
            }
        }


       
        // DEBTORS CREDITORS DETAILS
        wrp.DEBTORS_CREDITORS_DETAILS                                               = new Customer360view.cls_DEBTORS_CREDITORS_INFO();
        wrp.DEBTORS_CREDITORS_DETAILS.DEBOTORS_AS_FINACIAL_END                      = new List<Customer360view.cls_DEBTORS_CREDITORS>();
        wrp.DEBTORS_CREDITORS_DETAILS.DEBOTORS_AS_CURRENT_DATE                      = new List<Customer360view.cls_DEBTORS_CREDITORS>();
        wrp.DEBTORS_CREDITORS_DETAILS.CREDITORS_AS_FINACIAL_END                     = new List<Customer360view.cls_DEBTORS_CREDITORS>();
        wrp.DEBTORS_CREDITORS_DETAILS.CREDITORS_AS_CURRENT_DATE                     = new List<Customer360view.cls_DEBTORS_CREDITORS>();
        
        for(Debtors__c	debrorsObj		:	SOQL_Util.getDebtors(applinfo.customerId__c)) {
            Customer360view.cls_DEBTORS_CREDITORS	debtorsCreditorsObj			    =	new Customer360view.cls_DEBTORS_CREDITORS();
            //DEBOTORS_AS_FINACIAL_END 
            System.debug('Previous Fiscal Year'+currentFY);
            If(debrorsObj.Fiscal_Year__c  == currentFY){
            	 debtorsCreditorsObj.NAME											=	Utility.getBlankStringIfNull(debrorsObj.Customer_Name__c);
                 debtorsCreditorsObj.LOS_RECORD_ID									=	Utility.getBlankStringIfNull(debrorsObj.id);
                 debtorsCreditorsObj.AGE											=	Utility.getBlankStringIfNull(debrorsObj.Age__c);
                 debtorsCreditorsObj.AMOUNT										    =	Utility.getBlankStringIfNull(String.valueOf(debrorsObj.Amount_Collected__c));
                 wrp.DEBTORS_CREDITORS_DETAILS.DEBOTORS_AS_FINACIAL_END.add(debtorsCreditorsObj);
            }
            else If(debrorsObj.Fiscal_Year__c  == nextEstFiscalYear){
                System.debug('Fiscal Year'+nextEstFiscalYear);
            	 debtorsCreditorsObj.NAME											=	Utility.getBlankStringIfNull(debrorsObj.Customer_Name__c);
                 debtorsCreditorsObj.LOS_RECORD_ID									=	Utility.getBlankStringIfNull(debrorsObj.id);
                 debtorsCreditorsObj.AGE											=	Utility.getBlankStringIfNull(debrorsObj.Age__c);
                 debtorsCreditorsObj.AMOUNT										    =	Utility.getBlankStringIfNull(String.valueOf(debrorsObj.Amount_Collected__c));
                 wrp.DEBTORS_CREDITORS_DETAILS.DEBOTORS_AS_CURRENT_DATE.add(debtorsCreditorsObj);
            }
        }
        for(Sundry_Creditors__c	sundryCreditorsObj		:	SOQL_Util.getSundryCreditors(applinfo.customerId__c)) {
            Customer360view.cls_DEBTORS_CREDITORS	debtorsCreditorsObj			    =	new Customer360view.cls_DEBTORS_CREDITORS();
            //CREDITORS_AS_FINACIAL_END 
            If(sundryCreditorsObj.Fiscal_Year__c  == currentFY){
            	 debtorsCreditorsObj.NAME											=	Utility.getBlankStringIfNull(sundryCreditorsObj.Name_Of_Trade_Creditors__c);
                 debtorsCreditorsObj.LOS_RECORD_ID									=	Utility.getBlankStringIfNull(sundryCreditorsObj.id);
                 debtorsCreditorsObj.AGE											=	Utility.getBlankStringIfNull(sundryCreditorsObj.Age__c);
                 debtorsCreditorsObj.AMOUNT										    =	Utility.getBlankStringIfNull(String.valueOf(sundryCreditorsObj.Amount_To_Be_Paid__c));
                 wrp.DEBTORS_CREDITORS_DETAILS.CREDITORS_AS_FINACIAL_END.add(debtorsCreditorsObj);
            }else If(sundryCreditorsObj.Fiscal_Year__c  == nextEstFiscalYear){
            	 debtorsCreditorsObj.NAME											=	Utility.getBlankStringIfNull(sundryCreditorsObj.Name_Of_Trade_Creditors__c);
                 debtorsCreditorsObj.LOS_RECORD_ID									=	Utility.getBlankStringIfNull(sundryCreditorsObj.id);
                 debtorsCreditorsObj.AGE											=	Utility.getBlankStringIfNull(sundryCreditorsObj.Age__c);
                 debtorsCreditorsObj.AMOUNT										    =	Utility.getBlankStringIfNull(String.valueOf(sundryCreditorsObj.Amount_To_Be_Paid__c));
                 wrp.DEBTORS_CREDITORS_DETAILS.CREDITORS_AS_CURRENT_DATE.add(debtorsCreditorsObj);
            }
        }
		//cls_ACTUAL_EST_PRO
		//
       /* System.debug('####'+ applInfo.Debitor_Atual__c);
        System.debug('####'+ applInfo.Debitor_Estimated__c);
        System.debug('####'+ applInfo.Debitor_Projected__c);
        wrp.DEBITOR_AEP                                              				= new Customer360view.cls_ACTUAL_EST_PRO();
        wrp.DEBITOR_AEP.Actual														= Utility.getBlankStringIfNull(String.valueOf(applInfo.Debitor_Atual__c));
        wrp.DEBITOR_AEP.Estimated													= Utility.getBlankStringIfNull(String.valueOf(applInfo.Debitor_Estimated__c));
        wrp.DEBITOR_AEP.Projected													= Utility.getBlankStringIfNull(String.valueOf(applInfo.Debitor_Projected__c));
        System.debug('####'+ wrp.DEBITOR_AEP);
        
		wrp.SUNDARY_CREDITOR_AEP                                              		= new Customer360view.cls_ACTUAL_EST_PRO();
        wrp.SUNDARY_CREDITOR_AEP.Actual												= Utility.getBlankStringIfNull(String.valueOf(applInfo.Sundry_Creditor_Atual__c));
        wrp.SUNDARY_CREDITOR_AEP.Estimated											= Utility.getBlankStringIfNull(String.valueOf(applInfo.Sundry_Creditor_Estimated__c));
        wrp.SUNDARY_CREDITOR_AEP.Projected											= Utility.getBlankStringIfNull(String.valueOf(applInfo.Sundry_Creditor_Projected__c));
        System.debug('####'+ wrp.SUNDARY_CREDITOR_AEP);
       */ 
        List<genesis__Application_Document_Category__c> catList                     = DocumentFetch.checkDocumentCategory(appID,SMELoan_Helper.documentCategoryType);
        
        
        wrp.DOWNLOADABLE_TEMPLATES                                                  = new List<Customer360view.cls_TEMPLATES>();
        Customer360view.cls_TEMPLATES templates                                     = new Customer360view.cls_TEMPLATES();
        templates.TEMPLATE_LABEL                                                    = 'LIMIT_ASSESMENT_TEMPLATE';
        templates.TEMPLATE_LINK                                                     = 'https://kvb--SIT.cs58.my.salesforce.com/sfc/p/0l000000D0fN/a/0l0000008QiO/nh5STftZx4Odi9T4PuWH99ZKu9zZoeOlwA9mntFvo3A';
        wrp.DOWNLOADABLE_TEMPLATES.add(templates);
        return wrp;
    }
    public static Customer360view getKeyPersonDetails(Customer360view wrp ,List<genesis__Application_Parties__c> allKeyParties){
        System.debug('ALL Keyparties @@@@@@'+allKeyParties);
	for(genesis__Application_Parties__c ap : allKeyParties){
            Customer360view.cls_KEY_PERSON_DETAILS kpd  = new Customer360view.cls_KEY_PERSON_DETAILS();
            kpd.CBS_CUSTOMER_ID               = Utility.getBlankStringIfNull(ap.genesis__Party_Account_Name__r.CBS_Customer_ID__c);
            kpd.PASSPORT_NUMBER               = Utility.getBlankStringIfNull(ap.genesis__Party_Account_Name__r.Passport_Number__pc);
            kpd.DIGIO_STATUS               	  = Utility.getBlankStringIfNull(ap.Status__c);
            kpd.SEX                           = Utility.getBlankStringIfNull(ap.genesis__Party_Account_Name__r.Gender__pc);
            kpd.NATIONALITY                   = Utility.getBlankStringIfNull(ap.genesis__Party_Account_Name__r.Nationality__pc);
            kpd.FATHER_NAME                   = Utility.getBlankStringIfNull(ap.genesis__Party_Account_Name__r.Father_Name__pc);
            kpd.MARITAL_STATUS                = Utility.getBlankStringIfNull(ap.genesis__Party_Account_Name__r.Marital_Status__pc);
            kpd.SPOUSE_NAME                   = Utility.getBlankStringIfNull(ap.genesis__Party_Account_Name__r.Spouse_Name__pc);
            kpd.CASTE                         = Utility.getBlankStringIfNull(ap.genesis__Party_Account_Name__r.Caste__pc);
            kpd.RESIDENTIAL_STATUS            = Utility.getBlankStringIfNull(ap.genesis__Party_Account_Name__r.Residential_Status__pc);
            kpd.RESIDENCE_TYPE                = '';
            kpd.DIN_NUMBER                    = Utility.getBlankStringIfNull(String.valueOf(ap.genesis__Party_Account_Name__r.DIN_Number__pc));
            kpd.PARTY_TYPE                    = Utility.getBlankStringIfNull(ap.genesis__Party_Type__c);
            kpd.LOS_PARTY_TYPE_ID             = Utility.getBlankStringIfNull(ap.Id);
            kpd.GUARANTOR                     = ap.Guarantor__c==true?'Yes':'No';
            kpd.IS_ACTIVE                     = ap.Active__c==true?'Yes':'No';
            If(ap.Active__c==true && ap.Is_New__c==true && ap.genesis__Party_Account_Name__r.Name != null){
                wrp.SUMMARY_VIEW.NEW_KEYPERSON.NAMES +=' '+ap.genesis__Party_Account_Name__r.Name; 
            }
            If(ap.Active__c==false && ap.genesis__Party_Account_Name__r.CBS_Customer_ID__c !=null && ap.genesis__Party_Account_Name__r.Name != null){
                wrp.SUMMARY_VIEW.DELETE_KEYPERSON.NAMES  += ' '+ap.genesis__Party_Account_Name__r.Name;
            }
            kpd.DELETEKEYPERSON               = 'No';
            kpd.IS_PHYSTCALLY_HANDICAPPED     = ap.genesis__Party_Account_Name__r.Is_Physically_Handicapped__c==true?'Yes':'No';
            kpd.IS_EXSERVICE_MAN              = ap.genesis__Party_Account_Name__r.Is_Ex_Service_Man__c==true?'Yes':'No';
            kpd.NETWORTH_DETAILS              = new List<Customer360view.cls_NETWORTH_DETAILS>();
                    System.debug('#####'+kpd);
            for(Property__c propObj           : SOQL_Util.getProperty(ap.genesis__Party_Account_Name__c)){
                
                Customer360view.cls_NETWORTH_DETAILS netwrth = new Customer360view.cls_NETWORTH_DETAILS();
                If(propObj !=null){
                    
                    netwrth.LOS_PROPERTY_ID   		= Utility.getBlankStringIfNull(propObj.id);
                    netwrth.NATURE_OF_PROPERTY 		= Utility.getBlankStringIfNull(String.valueOf(propObj.Nature_of_Property__c));
                    netwrth.OWNERSHIP_TYPE    		= Utility.getBlankStringIfNull(propObj.Property_Type__c);
                    netwrth.PROPERTY_VALUE   		= Utility.getBlankStringIfNull(String.valueOf(propObj.Property_Value__c));
                    netwrth.SURVEY_NO   			= Utility.getBlankStringIfNull(String.valueOf(propObj.Survey_No__c));
                    netwrth.MORTGAGE_DETAILS   		= Utility.getBlankStringIfNull(String.valueOf(propObj.Property_Value__c));
                    netwrth.PERCENTAGE_OWNERSHIP   	= Utility.getBlankStringIfNull(String.valueOf(propObj.Ownership_percentage__c));
                    netwrth.DOOR_NO   				= Utility.getBlankStringIfNull(String.valueOf(propObj.Door_Number__c));
                    netwrth.NEAREST_LANDMARK   		= Utility.getBlankStringIfNull(String.valueOf(propObj.Nearest_Landmark__c));
                    netwrth.PROPERTY_AREA   		= Utility.getBlankStringIfNull(String.valueOf(propObj.Area__c));
                    netwrth.ASSET_TYPE   			= Utility.getBlankStringIfNull(String.valueOf(propObj.Asset_Type__c));
                    netwrth.ADDRESS          		= new Customer360view.cls_ADDRESS();
                    netwrth.ADDRESS.STREET   		= Utility.getBlankStringIfNull(propObj.Property_Street__c);
                    
                    netwrth.ADDRESS.CITY     = Utility.getBlankStringIfNull(propObj.Property_City__c);
                    netwrth.ADDRESS.STATE    = Utility.getBlankStringIfNull(propObj.Property_State__c);
                    netwrth.ADDRESS.COUNTRY  = Utility.getBlankStringIfNull(propObj.Property_Country__c);
                    netwrth.ADDRESS.PIN_CODE = Utility.getBlankStringIfNull(propObj.Property_Pincode__c);  
                    netwrth.ADDRESS.TALUK    = Utility.getBlankStringIfNull(propObj.Taluk__c);
                    kpd.NETWORTH_DETAILS.add(netwrth);
                }
            }  
            kpd.COMMUNICATION_DETAILS         						= new Customer360view.cls_COMMUNICATION_DETAILS();
            kpd.COMMUNICATION_DETAILS.MOBILE_NUMBER       			= Utility.getBlankStringIfNull(ap.genesis__Party_Account_Name__r.PersonMobilePhone);
            kpd.COMMUNICATION_DETAILS.EMAIL_ADDRESS       			= Utility.getBlankStringIfNull(ap.genesis__Party_Account_Name__r.PersonEmail);
            kpd.COMMUNICATION_DETAILS.PARMANENT_ADDRESS   		 	= new Customer360view.cls_ADDRESS();
            kpd.COMMUNICATION_DETAILS.PARMANENT_ADDRESS.STREET   	= Utility.getBlankStringIfNull(ap.genesis__Party_Account_Name__r.PersonMailingStreet);
            kpd.COMMUNICATION_DETAILS.PARMANENT_ADDRESS.CITY     	= Utility.getBlankStringIfNull(ap.genesis__Party_Account_Name__r.PersonMailingCity);
            kpd.COMMUNICATION_DETAILS.PARMANENT_ADDRESS.STATE    	= Utility.getBlankStringIfNull(ap.genesis__Party_Account_Name__r.PersonMailingState);
            kpd.COMMUNICATION_DETAILS.PARMANENT_ADDRESS.COUNTRY  	= Utility.getBlankStringIfNull(ap.genesis__Party_Account_Name__r.PersonMailingCountry);
            kpd.COMMUNICATION_DETAILS.PARMANENT_ADDRESS.PIN_CODE 	= Utility.getBlankStringIfNull(ap.genesis__Party_Account_Name__r.PersonMailingPostalCode);
            
            kpd.PRIMARY_DETAILS                           = new Customer360view.cls_PRIMARY_DETAILS();                
            kpd.PRIMARY_DETAILS.FIRST_NAME                = Utility.getBlankStringIfNull(ap.genesis__Party_Account_Name__r.FirstName);
            kpd.PRIMARY_DETAILS.LAST_NAME                 = Utility.getBlankStringIfNull(ap.genesis__Party_Account_Name__r.LastName);
            kpd.PRIMARY_DETAILS.DESIGNATION               = Utility.getBlankStringIfNull(ap.genesis__Party_Account_Name__r.Designation__pc);
            kpd.PRIMARY_DETAILS.DOB                       = Utility.getBlankStringIfNull(String.valueOf(ap.genesis__Party_Account_Name__r.PersonBirthdate));
            //System.debug('Date of Birth'+kpd.PRIMARY_DETAILS.DOB);
            kpd.PRIMARY_DETAILS.EDUCATIONAL_QUALIFICATION = Utility.getBlankStringIfNull(ap.genesis__Party_Account_Name__r.Education_Qualification__pc);
            kpd.PRIMARY_DETAILS.AADHAAR_NUMBER            = Utility.getBlankStringIfNull(ap.genesis__Party_Account_Name__r.Aadhaar_Number__pc);
            kpd.PRIMARY_DETAILS.PAN_NUMBER                = Utility.getBlankStringIfNull(ap.genesis__Party_Account_Name__r.Pan_Number__c);
            kpd.PRIMARY_DETAILS.LOS_PERSON_ACCOUNT_ID     = Utility.getBlankStringIfNull(ap.genesis__Party_Account_Name__c);
            
            kpd.SECTION_CHANGES                           = new Customer360view.cls_SECTION_CHANGES();
            kpd.SECTION_CHANGES.NAME                      = Utility.getBlankStringIfNull(ap.genesis__Party_Account_Name__r.Name_Of_Director_Related_To__c);
            kpd.SECTION_CHANGES.RELATIONSHIP              = Utility.getBlankStringIfNull(ap.genesis__Party_Account_Name__r.Relationship__c);
            kpd.SECTION_CHANGES.BANK_NAME                 = Utility.getBlankStringIfNull(ap.genesis__Party_Account_Name__r.Bank_name__c);
            kpd.SECTION_CHANGES.IS_KVB_DIRECTOR           = ap.genesis__Party_Account_Name__r.Is_KVB_Director__c==true?'Yes':'No'; 
            kpd.SECTION_CHANGES.FIRST_NAME                = '';  
            kpd.SECTION_CHANGES.LAST_NAME                 = '';                 
            
            //SECTION_CHANGES
            boolean isAnyChange     = false;
            Customer360view.cls_SECTION_CHANGES kpdSec      = new Customer360view.cls_SECTION_CHANGES();
            kpdSec.FIRST_NAME                               = Utility.getBlankStringIfNull(ap.genesis__Party_Account_Name__r.FirstName);
            kpdSec.LAST_NAME                                = Utility.getBlankStringIfNull(ap.genesis__Party_Account_Name__r.LastName);
            if(ap.genesis__Party_Account_Name__r.Name_Of_Director_Related_To__c != null && ap.genesis__Party_Account_Name__r.Name_Of_Director_Related_To__c != ''){
                kpdSec.NAME      = ap.genesis__Party_Account_Name__r.Name_Of_Director_Related_To__c;
                isAnyChange      = true;
            }else{kpdSec.NAME = '';}                    
            if(ap.genesis__Party_Account_Name__r.Relationship__c != null && ap.genesis__Party_Account_Name__r.Relationship__c != ''){
                kpdSec.RELATIONSHIP    = ap.genesis__Party_Account_Name__r.Relationship__c;
                isAnyChange            = true;
            }else{kpdSec.RELATIONSHIP = '';}
            if(ap.genesis__Party_Account_Name__r.Bank_name__c != null && ap.genesis__Party_Account_Name__r.Bank_name__c != ''){
                kpdSec.BANK_NAME       = Utility.getBlankStringIfNull(ap.genesis__Party_Account_Name__r.Bank_name__c);
                isAnyChange            = true;
            }else{kpdSec.BANK_NAME = '';}
            if(ap.genesis__Party_Account_Name__r.Is_KVB_Director__c){
                kpdSec.IS_KVB_DIRECTOR = 'Yes';
                isAnyChange            = true;
            }else{kpdSec.IS_KVB_DIRECTOR = '';}
            
            if(isAnyChange)wrp.SUMMARY_VIEW.SEC_20_CHANGES.add(kpdSec);
            wrp.KEY_PERSON_DETAILS.add(kpd);
        }
		return wrp;
	}
}