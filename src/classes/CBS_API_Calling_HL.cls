/*
* Name      : CBS_API_Calling_HL
* Compnay   : ET Marlabs
* Purpose   : Call CBS API in Sequence  
* Author    : Subas
*/
public class CBS_API_Calling_HL { 
    public static void call_CBS_API(String AppId){
        try{
            if(!String.isBlank(AppId)){
                WS_CBS_Dedupe.callDedupe(AppId);
            } 
        }Catch(Exception e){}
    }
    public static void call_CBS_log(List<CBS_API_Log__c > logList){
        List<ID> EkycAccSuccess = new List<ID>();
        List<ID> EkycAccFailure = new List<ID>();
        List<ID> CustAccSuccess = new List<ID>();
        List<ID> CustAccFailure = new List<ID>();
        List<ID> fileUpSuccess = new List<ID>();
        List<ID> fileUpFail = new List<ID>();
        List<Id> ODTODSuccess = new List<Id>();
        Boolean ODTODFailureFlag = false;
        String AppId = '';
        String recordType = '';
        for(CBS_API_Log__c log : logList){
            if(log.Sequence_No__c =='6' && log.Status__c=='Failure'){ 
                //call colletral creation
                //WS_CBS_Collateral_Creation.Collateral_Creation(log.Application__c);
            }if(log.Sequence_No__c =='6' && log.Status__c=='Success'){
                //call colletral linkage
                WS_CBS_Collateral_Linkage_HL.linkCollateralF(log.Application__c);
                
            }if(log.Sequence_No__c =='7' && log.Status__c=='Failure'){
                //call colletral linkage
                //WS_CBS_Collateral_Linkage_HL.linkCollateral(log.Application__c);
            }if(log.Sequence_No__c =='7' && log.Status__c=='Success'){
                //Call cbr file updation
                WS_CBS_CBR_DTLS_HL.CBR_DTLSF(log.Application__c);
                
            }if(log.Sequence_No__c =='2' && log.Status__c=='Success'){
                //call Customer ID creation                
                EkycAccSuccess.add(log.Application__c);
                AppId = log.Application__c;
                recordType = log.RecordType__c;
                
            }if(log.Sequence_No__c =='2' && log.Status__c=='Failure'){
                //call EKYC manually 
                EkycAccFailure.add(log.Applicant__c);
                
            }if(log.Sequence_No__c =='3' && log.Status__c=='Success'){
                //Call Loan Creation
                CustAccSuccess.add(log.Applicant__c);
                AppId = log.Application__c;
                recordType = log.RecordType__c;
            }if(log.Sequence_No__c =='3' && log.Status__c=='Failure'){
                //Call cust Id manually
                CustAccFailure.add(log.Applicant__c);
            }
            if(log.Sequence_No__c =='11' && log.Status__c=='Success'){
                fileUpSuccess.add(log.Applicant__c);
                AppId = log.Application__c ;
                recordType = log.RecordType__c;
            }
            if(log.Sequence_No__c =='11' && log.Status__c=='Failure'){
                fileUpFail.add(log.Applicant__c);
            }
            if(log.Sequence_No__c =='12' && log.Status__c !='Failure'){
                //OD/TOD modification    
                ODTODFailureFlag = true;
                ODTODSuccess.add(log.Applicant__c);            
                AppId = log.Application__c;
                recordType = log.RecordType__c;
            }
        } 
        if(ODTODFailureFlag  && ODTODSuccess.size() > 0){
            //call customer Id Creation
            if(recordType == 'SME Renewal'){
                //ApplicationTriggerHelper.ODTOD_CallHandler(ODTODSuccess); //OD/TOD modifiaction for SME
            }
            
            
        }
        if(EkycAccFailure.size() == 0 && EkycAccSuccess.size() > 0){
            //call customer Id Creation
            if(recordType == 'SME Renewal'){
                SME_CBS_Handler.customerIdCreation(EkycAccSuccess); //Customer ID creation for SME
            }/*else if(recordType == Constants.PERSONALLOAN){ // Personal loan 
                //Call Loan Creation for PL
                WS_CBS_Loan_Creation_HL.createLoanF(AppId,null);  // No Ekyc for PL
            }*/
            else{
                WS_CBS_Create_CustID.getCBSIDF(AppId); //Customer ID creation for HL
            }            
        }
        if(CustAccFailure.size() == 0 && CustAccSuccess.size() > 0){
            if(recordType == 'SME Renewal'){
                SME_CBS_Handler.customerLinkage(CustAccSuccess);   //Customer Linkage SME 
            }else{
                //Call Loan Creation HL
                WS_CBS_Loan_Creation_HL.createLoanF(AppId,null); 
            }
        }    
        if(fileUpFail.size() == 0 && fileUpSuccess.size()>0){
            
            genesis__Applications__c application = queryService.getApp(AppId);
            if(system.today() >= application.genesis__Disbursement_Date__c){
                WS_CBS_HOST_Disbursment_HL.Host_request(AppId);                
            }            
            
          /*  if(recordType == Constants.PERSONALLOAN){ 
                    system.debug('************* Photo upload ---> '+AppId);
                    WS_CBS_Loan_Disbursment_HL.callDisb(AppId); 
                }else{
                    // For HL logic 
                    genesis__Applications__c app = new genesis__Applications__c();
                    app.Id = AppId;
                    app.Retry_CBS__c = True;
                    update app;
                } */
        }
    }
    public static void initiateDisbursment(List<genesis__Applications__c> appList, Map<Id,genesis__Applications__c> oldList){
        //Initiate_Disbursement__c
        for(genesis__Applications__c app : appList){ 
            if(oldList.get(app.Id).Initiate_Disbursement__c <> app.Initiate_Disbursement__c && app.Initiate_Disbursement__c == True){          
                system.debug('*************'+app.Id);
                WS_CBS_HOST_Disbursment_HL.Host_request(app.Id); 
                //WS_CBS_Loan_Disbursment_HL.callDisb(app.Id);                
            }
        }
    }
}