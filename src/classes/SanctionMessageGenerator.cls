/*@Class Name : SanctionMessageGenerator
 *@Purpose    : To generate sanction message based on four scenario (mentioned in the code), when sub stage change to personal detail captured. 
 *@Date       : 18/05/2018
 *@Author     : Ashish Jain
*/  
public class SanctionMessageGenerator {
    //this method is called by ApplicationTriggerHandler when perticular sub stage changed.
    public static void sanctionMessageMethod(List<genesis__applications__c> newAppList, Map<Id,genesis__Applications__c> oldAppMap, List<genesis__applications__c> oldAppList, Map<Id,genesis__Applications__c> newAppMap){
        try{
            Set<Id> appId = new Set<Id>();
            List<genesis__applications__c> listtoUpdate = new List<genesis__applications__c>();   
            List<genesis__applications__c> newAppIdList = new List<genesis__applications__c>();
            Map<String, SanctionMessage__c> sanctionMessageCS = new Map<String, SanctionMessage__c>();
            for(SanctionMessage__c sm :[select name, Id, Message__c from SanctionMessage__c]){
                sanctionMessageCS.put(sm.name,sm);
            }
            System.debug('sanctionMessageCS:::'+sanctionMessageCS);
            for(genesis__applications__c app : newAppList){
                appId.add(app.Id);
            }
            newAppIdList = [ select id, Sanction_Message__c, Sub_Stage__c, Perfios_Captured__c , Additional_Income__c, genesis__Account__r.Same_Present_Address_Permanent_Address__c, genesis__Account__r.Are_you_An_Existing_Customer__c from genesis__applications__c where id IN : appId];
            System.debug('newAppIdList:::'+newAppIdList);
            System.debug('newAppIdList:::'+newAppIdList[0].genesis__Account__r.Same_Present_Address_Permanent_Address__c);
            genesis__applications__c appRec;
            //Here checking condition all four one-by-one and assigning message based on the condition.

            for(genesis__applications__c app : newAppIdList){
                System.debug(app);
                System.debug(app.genesis__Account__r.Same_Present_Address_Permanent_Address__c);
                appRec = new genesis__applications__c();
                if((oldAppMap.get(app.Id).Sub_Stage__c != app.Sub_Stage__c) && app.Sub_Stage__c == Constants.PersonalDetailsCapt){
                    //System.debug('app.genesis__Account__r.Current_Address_As_Permanent_Address__c:::'+app.genesis__Account__r.Current_Address_As_Permanent_Address__c);
                    System.debug('hello');
                    System.debug('####'+app.genesis__Account__r.Same_Present_Address_Permanent_Address__c);
                    System.debug('@@@@'+app.genesis__Account__r.Are_you_An_Existing_Customer__c);
                    if(app.Perfios_Captured__c == false && app.Additional_Income__c ==true && app.genesis__Account__r.Same_Present_Address_Permanent_Address__c == true && app.genesis__Account__r.Are_you_An_Existing_Customer__c == true){
                        System.debug('#@#@');
                        appRec.Sanction_Message__c = sanctionMessageCS.get('SM1').Message__c;
                        appRec.Id = app.Id;
                        listtoUpdate.add(appRec);
                        
                    }
                    else if(app.Perfios_Captured__c == false && app.Additional_Income__c == false && app.genesis__Account__r.Same_Present_Address_Permanent_Address__c == true && app.genesis__Account__r.Are_you_An_Existing_Customer__c == true){
                        System.debug('#@#@');
                        appRec.Sanction_Message__c = sanctionMessageCS.get('SM1').Message__c;
                        appRec.Id = app.Id;
                        listtoUpdate.add(appRec);
                    }
                    else if(app.Perfios_Captured__c == false && app.Additional_Income__c == true && app.genesis__Account__r.Same_Present_Address_Permanent_Address__c == false && app.genesis__Account__r.Are_you_An_Existing_Customer__c == true){
                        System.debug('#@#@');
                        appRec.Sanction_Message__c = sanctionMessageCS.get('SM3').Message__c;
                        appRec.Id = app.Id;
                        listtoUpdate.add(appRec);
                    }
                    else if(app.Perfios_Captured__c == false && app.Additional_Income__c == false && app.genesis__Account__r.Same_Present_Address_Permanent_Address__c == false && app.genesis__Account__r.Are_you_An_Existing_Customer__c == true){
                        System.debug('#@#@');
                        appRec.Sanction_Message__c = sanctionMessageCS.get('SM3').Message__c;
                        appRec.Id = app.Id;
                        listtoUpdate.add(appRec);
                    }
                    else if(app.Perfios_Captured__c == false && app.Additional_Income__c == true && app.genesis__Account__r.Same_Present_Address_Permanent_Address__c == true && app.genesis__Account__r.Are_you_An_Existing_Customer__c == false){
                        System.debug('#@#@');
                        appRec.Sanction_Message__c = sanctionMessageCS.get('SM3').Message__c;
                        appRec.Id = app.Id;
                        listtoUpdate.add(appRec);
                    }
                    else if(app.Perfios_Captured__c == false && app.Additional_Income__c == false && app.genesis__Account__r.Same_Present_Address_Permanent_Address__c == true && app.genesis__Account__r.Are_you_An_Existing_Customer__c == false){
                        System.debug('#@#@');
                        appRec.Sanction_Message__c = sanctionMessageCS.get('SM3').Message__c;
                        appRec.Id = app.Id;
                        listtoUpdate.add(appRec);
                    }
                    else if(app.Perfios_Captured__c == false && app.Additional_Income__c == true && app.genesis__Account__r.Same_Present_Address_Permanent_Address__c == false && app.genesis__Account__r.Are_you_An_Existing_Customer__c == false){
                        System.debug('#@#@');
                        appRec.Sanction_Message__c = sanctionMessageCS.get('SM3').Message__c;
                        appRec.Id = app.Id;
                        listtoUpdate.add(appRec);
                    }
                    else if(app.Perfios_Captured__c == false && app.Additional_Income__c == false && app.genesis__Account__r.Same_Present_Address_Permanent_Address__c == false && app.genesis__Account__r.Are_you_An_Existing_Customer__c == false){
                        System.debug('#@#@');
                        appRec.Sanction_Message__c = sanctionMessageCS.get('SM3').Message__c;
                        appRec.Id = app.Id;
                        listtoUpdate.add(appRec);
                    }
                    else if(app.Perfios_Captured__c == true && app.Additional_Income__c == true && app.genesis__Account__r.Same_Present_Address_Permanent_Address__c == true && app.genesis__Account__r.Are_you_An_Existing_Customer__c == true){
                        System.debug('#@#@');
                        appRec.Sanction_Message__c = sanctionMessageCS.get('SM1').Message__c;
                        appRec.Id = app.Id;
                        listtoUpdate.add(appRec);
                    }
                    else if(app.Perfios_Captured__c == true && app.Additional_Income__c == false && app.genesis__Account__r.Same_Present_Address_Permanent_Address__c == true && app.genesis__Account__r.Are_you_An_Existing_Customer__c == true){
                        System.debug('#@#@');
                        appRec.Sanction_Message__c = sanctionMessageCS.get('SM10').Message__c;
                        //appRec.Sub_Stage__c = Constants.PL_LOAN_STP;
                        appRec.Id = app.Id;
                        listtoUpdate.add(appRec);
                    }
                    else if(app.Perfios_Captured__c == true && app.Additional_Income__c == true && app.genesis__Account__r.Same_Present_Address_Permanent_Address__c == false && app.genesis__Account__r.Are_you_An_Existing_Customer__c == true){
                        System.debug('#@#@');
                        appRec.Sanction_Message__c = sanctionMessageCS.get('SM3').Message__c;
                        appRec.Id = app.Id;
                        listtoUpdate.add(appRec);
                    }
                    else if(app.Perfios_Captured__c == true && app.Additional_Income__c == false && app.genesis__Account__r.Same_Present_Address_Permanent_Address__c == false && app.genesis__Account__r.Are_you_An_Existing_Customer__c == true){
                        System.debug('#@#@');
                        appRec.Sanction_Message__c = sanctionMessageCS.get('SM12').Message__c;
                        appRec.Id = app.Id;
                        listtoUpdate.add(appRec);
                    }
                    else if(app.Perfios_Captured__c == true && app.Additional_Income__c == true && app.genesis__Account__r.Same_Present_Address_Permanent_Address__c == true && app.genesis__Account__r.Are_you_An_Existing_Customer__c == false){
                        System.debug('#@#@');
                        appRec.Sanction_Message__c = sanctionMessageCS.get('SM3').Message__c;
                        appRec.Id = app.Id;
                        listtoUpdate.add(appRec);
                    }
                    else if(app.Perfios_Captured__c == true && app.Additional_Income__c == false && app.genesis__Account__r.Same_Present_Address_Permanent_Address__c == true && app.genesis__Account__r.Are_you_An_Existing_Customer__c == false){
                        System.debug('#@#@');
                        appRec.Sanction_Message__c = sanctionMessageCS.get('SM12').Message__c;
                        appRec.Id = app.Id;
                        listtoUpdate.add(appRec);
                    }
                    else if(app.Perfios_Captured__c == true && app.Additional_Income__c == true && app.genesis__Account__r.Same_Present_Address_Permanent_Address__c == false && app.genesis__Account__r.Are_you_An_Existing_Customer__c == false){
                        System.debug('#@#@');
                        appRec.Sanction_Message__c = sanctionMessageCS.get('SM3').Message__c;
                        appRec.Id = app.Id;
                        listtoUpdate.add(appRec);
                    }
                    else if(app.Perfios_Captured__c == true && app.Additional_Income__c == false && app.genesis__Account__r.Same_Present_Address_Permanent_Address__c == false && app.genesis__Account__r.Are_you_An_Existing_Customer__c == false){
                        System.debug('#@#@');
                        appRec.Sanction_Message__c = sanctionMessageCS.get('SM12').Message__c;
                        appRec.Id = app.Id;
                        listtoUpdate.add(appRec);
                    }
                }
            }           
            ApplicationTriggerHandler.IsFirstRun = false;
            update listtoUpdate;
        }catch(Exception e){
            System.debug('####'+e.getMessage()+' At '+e.getLineNumber());
        }
    }
}