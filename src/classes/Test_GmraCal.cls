@isTest
public class Test_GmraCal {
    @isTest
    public static void method1(){
        genesis__Applications__c app=TestUtility.intialSetUp('HomeLoan',true);
        Account acc=new Account(Name='TestName');
        acc.Net_Monthly_Income__c=50000;
        insert acc;
        RecordType personAccountRecordType =  [SELECT Id FROM RecordType WHERE Name = 'Person Account' and SObjectType = 'Account'];
        Account newPersonAccount = new Account();
        
        // for person accounts we can not update the Name field instead we have to update the    FirstName and LastName individually
        
        newPersonAccount.FirstName = 'Fred';
        newPersonAccount.LastName = 'Smith';
        newPersonAccount.RecordType = personAccountRecordType;
        newPersonAccount.PersonBirthdate= Date.newInstance(2000, 12, 9); 
        insert newPersonAccount;
        
        app.genesis__Interest_Rate__c=10;
        app.genesis__Term__c=5;
        app.genesis__Account__c=acc.id;
        update app;
        
        genesis__Application_Parties__c parapp=new genesis__Application_Parties__c(genesis__Application__c=app.Id,genesis__Party_Account_Name__c=acc.id);
        insert parapp;
        
        GmraCal.calGMRA(app.Id) ;
    }
}