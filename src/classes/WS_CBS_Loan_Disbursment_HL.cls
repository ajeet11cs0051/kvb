/*
* Name      : WS_CBS_Loan_Disbursment_HL
* Compnay   : ET Marlabs
* Purpose   : For HL Service(Loan Disbursment) 
* Author    : Subas
*/
public class WS_CBS_Loan_Disbursment_HL {
    public class Loan_disbursment_Request{
        public String exe_tenantId;	//cuecent_tenant
        public String owner_tenantId;	//cuecent_tenant
        public cls_inputVariables inputVariables;
    }
    public class cls_inputVariables {
        public cls_in_msg in_msg;
    }
    public class cls_in_msg {
        public String LoanAccountNumber;	//1152753000002737
        public String AmountDisb;	//500000
        public String FlagTransfer;	//2
        public String CASAAccountNumber;	//1152223000000359
        public String Narration;	//DT:HLDISB
        public String TransactionBranch;	//1152
        public String GLAccountNumber;
        public String GLBranchCode;
    }
//Response structure
    public class Loan_disbursment_Response{
	public String bpms_error_code;	//00
	public cls_out_msg out_msg;
	public String bpms_error_msg;	//Success
    }
	class cls_out_msg {
		public String ErrorCode;	//EXT_RSP818
		public String ErrorMessage;	//Host leg of the Disbursement has not been done. OR Disbursement Done Today.
		public String Response;	//99
		public cls_Reason Reason;
		public cls_TransactionStatus TransactionStatus;
	}
	class cls_Reason {
		public String Code;	//29
		public String Message;	//Internal OLTP Error.
	}
	class cls_TransactionStatus {
		public String IsServiceChargeApplied;	//false
		public String ReplyCode;	//0
		public String Disbursed;	//
		public cls_ExtendedReply ExtendedReply;
		public String ErrorCode;	//
		public String IsOverriden;	//false
		public String SpReturnValue;	//0
		public String Memo;	//
		public String ExternalReferenceNo;	//136162017100400010017
		public String ReplyText;	//
	}
	class cls_ExtendedReply {
		public String MessagesArray;	//
	}
    //@future (Callout=true)
    public static void callDisb(String AppID){
        genesis__Applications__c application = new genesis__Applications__c();
        application = queryService.getApp(appID);
        WS_CBS_Loan_Disbursment_HL.Loan_disbursment_Request cbs = new WS_CBS_Loan_Disbursment_HL.Loan_disbursment_Request();
        cbs.inputVariables  = new WS_CBS_Loan_Disbursment_HL.cls_inputVariables();
        WS_CBS_Loan_Disbursment_HL.cls_inputVariables cbsInput = new WS_CBS_Loan_Disbursment_HL.cls_inputVariables();
        cbsInput.in_msg = new WS_CBS_Loan_Disbursment_HL.cls_in_msg();
        cbs.inputVariables = cbsInput;
        cbs.exe_tenantId = 'cuecent_tenant';
        cbs.owner_tenantId = 'cuecent_tenant';
        if(application.Record_Type_Name__c == Constants.LAPLOAN && application.Record_Type_Name__c == Constants.PERSONALLOAN){
            cbsInput.in_msg.AmountDisb = String.ValueOf(application.Disbursement_Amount__c);
            cbsInput.in_msg.CASAAccountNumber = application.CASA_Account_No__c != null ? application.CASA_Account_No__c : '';
            cbsInput.in_msg.GLAccountNumber = '';
            cbsInput.in_msg.FlagTransfer = '2';
            cbsInput.in_msg.LoanAccountNumber = application.Loan_Account_Number__c;
            if(application.Record_Type_Name__c == Constants.LAPLOAN){
                cbsInput.in_msg.Narration = 'DT:LAP DISB'+application.Loan_Account_Number__c;
                }else{
                cbsInput.in_msg.Narration = 'DT:PL DISB'+application.Loan_Account_Number__c;    
            }
            cbsInput.in_msg.TransactionBranch = application.Branch_Code__c;
            cbsInput.in_msg.GLBranchCode = '';            
        }else{
            cbsInput.in_msg.AmountDisb = String.ValueOf(application.Disbursement_Amount__c);
            cbsInput.in_msg.CASAAccountNumber = '';//'1152223000000359';
            cbsInput.in_msg.GLAccountNumber = '275300300';
            cbsInput.in_msg.FlagTransfer = '1';
            cbsInput.in_msg.LoanAccountNumber = application.Loan_Account_Number__c;
            cbsInput.in_msg.Narration = 'DT:HSL DISB'+application.Loan_Account_Number__c;
            cbsInput.in_msg.TransactionBranch = application.Branch_Code__c;
            cbsInput.in_msg.GLBranchCode = application.Branch_Code__c;            
        }
        getDisb(JSON.serialize(cbs),AppID);
    }
    public static void getDisb(String disbData, String AppId){
        system.debug('@@@@'+disbData);
        genesis__Applications__c appIDPL = new genesis__Applications__c();
        appIDPL = queryService.getApp(appID);

        KVB_Endpoint_URLs__c kvb = KVB_Endpoint_URLs__c.getValues('Branch_Disbursement_HL');
        Map<String,String> headerMap                            = new Map<String,String>();
        headerMap.put('Content-Type','application/json');
        HTTPResponse response                                   = new HTTPResponse();
        String endPoint                                         = kvb.Endpoint_URL__c;
        response = HttpUtility.sendHTTPRequest(endPoint, 'POST', null,disbData,headerMap,label.CA_CERTIFICATE);   
        system.debug('#####'+response);
        system.debug('$$$$'+response.getBody());
        If(response.getStatusCode() == 200 || response.getStatusCode() == 201){
            String jsonString = response.getBody();            
            WS_CBS_Loan_Disbursment_HL.Loan_disbursment_Response res = (WS_CBS_Loan_Disbursment_HL.Loan_disbursment_Response)Json.deserialize(jsonString,WS_CBS_Loan_Disbursment_HL.Loan_disbursment_Response.class);
            System.debug(res);
            System.debug(res.out_msg.TransactionStatus);
            if(res.out_msg.TransactionStatus != null){
                CBS_API_Log__c log = new CBS_API_Log__c();    
                log.Application__c = appId;
                log.API_Name__c = 'Loan_Disbursment_HL';
                log.Status__c = 'Success';
                log.Success_Error_Message__c = 'Success';
                log.Sequence_No__c = '20';  
                insert log;        
                genesis__Applications__c ap = new genesis__Applications__c();
                ap.Id = AppId;
                ap.Sub_Stage__c = 'Loan Account Opened';
                if(appIDPL.Record_Type_Name__c == Constants.PERSONALLOAN){
                    ap.Retry_CBS__c = True; // Retry true means success 
                }
                update ap;
            }else{
                CBS_API_Log__c log = new CBS_API_Log__c();    
                log.Application__c = appId;
                log.API_Name__c = 'Loan_Disbursment_HL';
                log.Status__c = 'Failure';
                if(res.out_msg.ErrorMessage != null){
                    log.Success_Error_Message__c = res.out_msg.ErrorMessage;
                }else{
                    log.Success_Error_Message__c = 'Error';
                }                
                log.Sequence_No__c = '20';  
                insert log;
                genesis__Applications__c ap = new genesis__Applications__c();
                ap.Id = AppId;
                ap.Initiate_Disbursement__c = False;
                update ap;
            }
            
        } 
    }
}