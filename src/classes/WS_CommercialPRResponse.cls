/*
 * Name     : WS_CommercialPRResponse
 * Company  : ET Marlabs
 * Purpose  : Used to read CommercialPR Processing and Storing CommercialPR data in LOS System against Account. 
 * Author   : Raushan
*/
@RestResource(urlMapping='/pushCommercialPRData')
global with sharing class WS_CommercialPRResponse {
    global class Response extends WS_Response{
        
        public CommercialPRResponse commercialPRResponseObj;
        public Response(){
            commercialPRResponseObj    = new CommercialPRResponse();            
        }
    }

    @HttpPost
    global static Response getCustomerView(){
        RestRequest req      = Restcontext.Request;
        Response res         = new Response();
        SavePoint sp         = Database.setSavepoint();
        if(req == null || req.requestBody == null){
            return getWSResponse(res, Constants.WS_ERROR_STATUS, null, Constants.WS_ERROR_CODE, Constants.WS_REQ_BODY_IS_NULL);                     
        }else{
            try{
                String jsonData                 = req.requestBody.toString();
                System.debug('jsonData'+jsonData);
                CommercialPRResponse commObj    = CommercialPRResponse.parse(jsonData);
                System.debug('CommericiapPRResponse'+commObj);
                if(commObj == null){
                    return getWSResponse(res, Constants.WS_ERROR_STATUS, 'Request Body is null.', Constants.WS_ERROR_CODE, null);                    
                }
                if(commObj != null){
                    try{
                         CommercialPRHandle.getCommercialPRDetails(commObj);
                         
                        CommercialPRResponse.Out_msg msg = new CommercialPRResponse.Out_msg();
                        msg.CUST_LIST = getCustList(commObj);
                        List<CommercialPRResponse.Out_msg> msgList = new List<CommercialPRResponse.Out_msg>();
                        msgList.add(msg);
                        res.commercialPRResponseObj.out_msg = msgList; 
                    }catch(Exception ex){
                        //database.rollback(sp);
                        System.debug(ex.getLineNumber());
                        System.debug(ex.getStackTraceString());
                        return getWSResponse(res, Constants.WS_ERROR_STATUS, null,Constants.WS_ERROR_CODE,ex.getMessage());                   
                    }
                }                
            }catch(Exception e){
                System.debug(e.getLineNumber());
                return getWSResponse(res, Constants.WS_ERROR_STATUS,null, Constants.WS_ERROR_CODE, e.getMessage());                   
            }
            
        }
        return res;
    }
    
    static Response getWSResponse(Response res, string status, string succMsg, string statusCode, string errMsg){
        res.status          = status;
        res.successMessage  = succMsg;
        res.statusCode      = statusCode;
        res.errorMessage    = errMsg;
        return res;
    }
    public static String getCustList(CommercialPRResponse commercialPRRes){
        String custList = '';
        List < CommercialPRResponse.Out_msg > listOutMsg = commercialPRRes.out_msg;
        for (CommercialPRResponse.Out_msg outMsgObj: listOutMsg) {
            CommercialPRResponse.ResponseReport responseReportObj = outMsgObj.responseReport;
            CommercialPRResponse.ReportHeaderRec reportHeaderRecObj = responseReportObj.reportHeaderRec;
            custList+=reportHeaderRecObj.applicationReferenceNumber+',';
        }
        
        
        RETURN custList;
    }
    
}