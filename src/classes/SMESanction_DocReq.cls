/*
* Digio Sanction letter doc generation Req. Class
*/
public class SMESanction_DocReq {
    public static Map<Integer,Integer> signerPageCountMap;
    public String exe_tenantId= 'cuecent_tenant';   //cuecent_tenant
    public String owner_tenantId= 'cuecent_tenant'; //cuecent_tenant
    // public String serviceName= 'TestJsonStr'; //TestJsonStr
    public cls_inputVariables inputVariables;
    public class cls_inputVariables {        
        public String in_msg= '';   //
        public String unique_key= '';   //
        public String pdf_unique_key= '';
        public Map<String,String> inputmap;
    }
    
    public static string getFundedRateStr       = 'Interest will be charged @{{Approved_Interest_Rate}}. % p.a. which is {{BRE_Spread_Rate}} % over and above the MCLR of the Bank, Compounded at Monthly rests.';
    public static string getNonFundedRateStr    = 'Commission will be charged @{{Approved_Interest_Rate}}. % p.a.';
    public static string companiesllpTnC        = 'In case Borrower is limited Company/LLP, Bank\'s charge/ modification in the charges over the primary and collateral securities, as applicable, should be registered with the ROC within the stipulated time.';    
    public static string getcorpAdvTnC(){
        return [Select Content__c from Audit__c where RecordType.DeveloperName ='Other' AND Tag_Name__c ='CORP_ADV_TNC' limit 1 ].Content__c;
    }
    public static string getForexAdvTnC(){
        return [Select Content__c from Audit__c where RecordType.DeveloperName ='Other' AND Tag_Name__c ='FORX_ADV_TNC' limit 1 ].Content__c;
    }
    
    public static List<MCLR_Master__c> getMCLRValues(){
        return [Select id,COD_Term_From__c,COD_Term_To__c,Effective_Date__c,
                Expiry_Date__c,Interest_Rate_Reset__c,Int_ID__c,MCLR_Of_Interest__c from MCLR_Master__c limit 10];
    }
    
    // Purpose: This method prepares signer cordinate structure 
    public static Map<String,Map<Integer,List<Object>>> genCordStructure(Integer totalPage, String appId, String docName){
        try{
            docName = docName.split('.pdf')[0];//Extracting file extension from file name
            List<String> identifierList = new List<String>();
            Boolean isHLDocument = false;//Check if Document is for HL
            Boolean isSMEApplication = false;
            List<integer> pageNoList = new List<Integer>();
            Integer grSize = 0;
            Integer brSize = 0;
            genesis__Applications__c appln = new genesis__Applications__c();
            List<genesis__Application_Parties__c> parties = new List<genesis__Application_Parties__c>();
            List<genesis__Application_Parties__c> coBorrowersList = new List<genesis__Application_Parties__c>();
            List<genesis__Application_Parties__c> guarentersList = new List<genesis__Application_Parties__c>();
            if(Constants.DOC_NAME_HL_BORR_PH.contains(docName)){
                parties = new List<genesis__Application_Parties__c>([Select id,genesis__Application__c,genesis__Party_Account_Name__c,Party_Mobile_No__c,
                                                                     genesis__Party_Account_Name__r.Name,Active__c,
                                                                     genesis__Party_Account_Name__r.PersonEmail,genesis__Party_Account_Name__r.Aadhaar_Number__pc,
                                                                     genesis__Party_Account_Name__r.PersonMobilePhone,CreatedDate                                                                        
                                                                     from genesis__Application_Parties__c WHERE genesis__Party_Account_Name__r.PersonMobilePhone != null AND Active__c = true AND genesis__Application__c =: appId AND Title_Holder__c = true order by CreatedDate asc LIMIT :Constants.TITLE_HOOLDER_LIMIT]);
                isHLDocument = true;
                System.debug('DOC_NAME_HL_BORR_PH');
            }
             else if(Constants.DOC_NAME_HL_BORROWER.contains(docName)){
                parties = new List<genesis__Application_Parties__c>([Select id,genesis__Application__c,genesis__Party_Account_Name__c,Party_Mobile_No__c,
                                                                     genesis__Party_Account_Name__r.Name,Active__c,
                                                                     genesis__Party_Account_Name__r.PersonEmail,genesis__Party_Account_Name__r.Aadhaar_Number__pc,
                                                                     genesis__Party_Account_Name__r.PersonMobilePhone,CreatedDate                                                                        
                                                                     from genesis__Application_Parties__c WHERE genesis__Party_Account_Name__r.PersonMobilePhone != null AND Active__c = true AND genesis__Application__c =: appId AND genesis__Party_Type__c ='Co-Borrower' order by CreatedDate asc LIMIT 3]);
                isHLDocument = true;
                System.debug('DOC_NAME_HL_BORROWER:::'+parties.size());
            }
            else if(Constants.DOC_NAME_HL_GUARANTER.contains(docName)){
                parties = new List<genesis__Application_Parties__c>([Select id,genesis__Application__c,genesis__Party_Account_Name__c,Party_Mobile_No__c,
                                                                     genesis__Party_Account_Name__r.Name,Active__c,
                                                                     genesis__Party_Account_Name__r.PersonEmail,genesis__Party_Account_Name__r.Aadhaar_Number__pc,
                                                                     genesis__Party_Account_Name__r.PersonMobilePhone,CreatedDate                                                                        
                                                                     from genesis__Application_Parties__c WHERE genesis__Party_Account_Name__r.PersonMobilePhone != null AND Active__c = true AND genesis__Application__c =: appId AND genesis__Party_Type__c ='Guarantor' order by CreatedDate asc]);
                //isHLDocument = true;
                System.debug('DOC_NAME_HL_GUARANTER');
            }
            else if(Constants.DOC_NAME_HL_COMMON.contains(docName)){
                List<String> partyTypeList = new List<String>{'Guarantor','Co-Borrower'};
                parties = new List<genesis__Application_Parties__c>([Select id,genesis__Application__c,genesis__Party_Account_Name__c,Party_Mobile_No__c,
                                                                     genesis__Party_Account_Name__r.Name,Active__c,genesis__Party_Type__c,
                                                                     genesis__Party_Account_Name__r.PersonEmail,genesis__Party_Account_Name__r.Aadhaar_Number__pc,
                                                                     genesis__Party_Account_Name__r.PersonMobilePhone,CreatedDate                                                                        
                                                                     from genesis__Application_Parties__c WHERE genesis__Party_Account_Name__r.PersonMobilePhone != null AND Active__c = true AND genesis__Application__c =: appId AND genesis__Party_Type__c IN :partyTypeList order by CreatedDate asc]);
                if(!parties.isEmpty()){
                    System.debug('PartySize'+parties.size());
                    for(genesis__Application_Parties__c party : parties ){
                        if(party.genesis__Party_Type__c == 'Guarantor' && guarentersList.size() <= 10) guarentersList.add(party);
                        if(party.genesis__Party_Type__c == 'Co-Borrower') coBorrowersList.add(party);
                    }
                }
                
                                                                     
                isHLDocument = true;
                System.debug('DOC_NAME_HL_COMMON');
            }
            // Personal loan document
            else if(Constants.PL_DOC_DEVELOPERNAME.contains(docName)){
                appln = [Select genesis__Account__r.PersonEmail,Sanction_Doc_Generation_Check__c,Sanction_Class2_Check__c,
                         genesis__Account__r.PersonMobilePhone,Sanction_ESign_Check__c from genesis__Applications__c where Id =: appId];
                
                identifierList.add(appln.genesis__Account__r.PersonMobilePhone);
                System.debug('PL_DOC_DEVELOPERNAME');
            }
            else{
                parties = new List<genesis__Application_Parties__c>([Select id,genesis__Application__c,genesis__Party_Account_Name__c,Party_Mobile_No__c,
                                                                     genesis__Party_Account_Name__r.Name,Active__c,
                                                                     genesis__Party_Account_Name__r.PersonEmail,genesis__Party_Account_Name__r.Aadhaar_Number__pc,
                                                                     genesis__Party_Account_Name__r.PersonMobilePhone,CreatedDate                                                                        
                                                                     from genesis__Application_Parties__c WHERE genesis__Party_Account_Name__r.PersonMobilePhone != null AND Active__c = true AND genesis__Application__c =: appId 
                                                                     AND genesis__Party_Account_Name__r.RecordType.DeveloperName ='PersonAccount' AND Signatories__c =true order by CreatedDate asc]);//query for parties
              isSMEApplication = true;
              System.debug('SME_CASE');
            }
            
            if(isHLDocument){
                appln = [Select id,Borrower_Docket_Id__c,Guarantor_Docket_Id__c,genesis__Account__c,genesis__Account__r.PersonEmail,genesis__Account__r.Aadhaar_Number__pc,Sanction_Doc_Generation_Check__c,Sanction_Class2_Check__c,
                         genesis__Account__r.PersonMobilePhone,Sanction_ESign_Check__c from genesis__Applications__c where Id =: appId];
                
                identifierList.add(appln.genesis__Account__r.PersonMobilePhone);
            }
            
            if(docName == Constants.SANCTION_TEMPLATE_SME || docName == Constants.SME_SANCTION_Letter123 || docName == Constants.SME_SANCTION_Letter1 
            || docName == Constants.SME_SANCTION_Letter12 || docName == Constants.SME_SANCTION_Letter13){
                docName    = Constants.SANCTION_TEMPLATE_SME;
            }
            
            System.debug(isSMEApplication+'---'+docName);
            System.debug('identifierList:::'+identifierList.size());
            if(!parties.isEmpty()){
                if(Constants.DOC_NAME_HL_COMMON.contains(docName)){ 
                    if(!coBorrowersList.isEmpty()){
                        Integer count = 0;
                        for(genesis__Application_Parties__c party : coBorrowersList){
                            brSize++;
                            identifierList.add(party.genesis__Party_Account_Name__r.PersonMobilePhone);
                            if(count == 2) break;
                            count++;
                        }
                    }
                    if(!guarentersList.isEmpty()){ 
                        grSize = guarentersList.size();
                        for(genesis__Application_Parties__c party : guarentersList){
                            identifierList.add(party.genesis__Party_Account_Name__r.PersonMobilePhone);
                            //count++;
                        }
                    }
                }
                else{
                    for(genesis__Application_Parties__c party : parties){
                        if(isSMEApplication) identifierList.add(party.Id);
                        else identifierList.add(party.genesis__Party_Account_Name__r.PersonMobilePhone);
                    }
                }
                
            }
            Integer partySizeLeft = identifierList.size();
            Integer sgnerCount = 0;
            System.debug('partySizeLeft----'+partySizeLeft);
            signerPageCountMap = new Map<Integer,Integer>();
            Map<Integer,List<String>> perPageSignerMap = new Map<Integer,List<String>>();
            
            List<SignerPerPage__mdt> signerPerPageList = [SELECT Page__c,SignerCount__c FROM SignerPerPage__mdt WHERE DocumentName__r.DeveloperName =: docName];
            for(SignerPerPage__mdt sPP : signerPerPageList){
                signerPageCountMap.put((Integer)sPP.Page__c,(Integer)sPP.SignerCount__c);
            }
            pageNoList.addAll(signerPageCountMap.keySet());
            //System.debug(signerPageCountMap);
            pageNoList.sort();
            //System.debug(pageNoList);
            for(Integer pageNo : pageNoList){
                if(partySizeLeft > signerPageCountMap.get(pageNo)){
                    for(Integer i = 0; i < signerPageCountMap.get(pageNo) ; i++){
                        if(!perPageSignerMap.keySet().contains(pageNo)){
                            perPageSignerMap.put(pageNo,new List<String>{identifierList.get(identifierList.size() - partySizeLeft+i)});
                            sgnerCount++;
                        }
                        else{
                            if(Constants.DOC_NAME_HL_COMMON.contains(docName)){
                                if(brSize > 0){
                                    perPageSignerMap.get(pageNo).add(identifierList.get(i));
                                    brSize--;
                                    sgnerCount++;
                                    if(brSize == 0) break;
                                }
                                else if(grSize > 0){
                                    perPageSignerMap.get(pageNo).add(identifierList.get(coBorrowersList.size()+i+1));
                                    sgnerCount++;
                                    grSize--;
                                }
                            }
                            else{
                                perPageSignerMap.get(pageNo).add(identifierList.get(i));
                                sgnerCount++;
                            }
                        }
                    }
                    
                    if(Constants.DOC_NAME_HL_COMMON.contains(docName) && brSize == 0) partySizeLeft = grSize;
                    else partySizeLeft -= signerPageCountMap.get(pageNo);
                }
                else{
                    List<String> tempList = new List<string>();
                    if(partySizeLeft > 0){
                        if(Constants.DOC_NAME_HL_COMMON.contains(docName)){// && brSize == 0
                            if(pageNo == 1){
                                tempList.add(identifierList[0]);
                                sgnerCount++;
                                if(brSize > 0){
                                    Integer count  = 0;
                                    count = brSize < 2 ? brSize : 2;
                                    for(Integer i = 0; i < count ; i++){
                                        tempList.add(identifierList.get(i+1));
                                        sgnerCount++;
                                    }
                                    brSize = 0; 
                                }
                                perPageSignerMap.put(pageNo,tempList);
                                continue;
                            }
                            
                            if(grSize > 0){
                                Integer leftCount = grSize-1;
                                for(Integer i = sgnerCount; i < identifierList.size() ; i++){
                                    tempList.add(identifierList.get(sgnerCount));
                                    sgnerCount++;
                                }
                                partySizeLeft =0;
                            }
                        }
                        else{
                            for(Integer i = 0; i < partySizeLeft ; i++){
                                tempList.add(identifierList.get(identifierList.size()-partySizeLeft+i));
                            }
                            partySizeLeft =0;
                        }
                        
                        perPageSignerMap.put(pageNo,tempList);
                    }
                    
                    
                }
            }
            System.debug(perPageSignerMap);
            
            RETURN genCordStructure1(perPageSignerMap,totalPage,docName);
        }
        catch(Exception e){
            System.debug(e.getMessage());
            System.debug(e.getLineNumber());
            RETURN null;
        }
    }
    public static Map<String,Map<Integer,List<Object>>> genCordStructure1(Map<Integer,List<String>> perPageSignerMap,Integer totalPage,String docName){
        System.debug(perPageSignerMap);
        Map<String,Map<Integer,List<Object>>> finalMap = new Map<String,Map<Integer,List<Object>>>();
        List<cls_PgSigner> finalWrapper = new List<cls_PgSigner>();
        List<Cordinate__mdt> cordList = new List<Cordinate__mdt>();
        Map<Integer,Integer> stPageReferenceMap = new Map<Integer,Integer>();
        Map<Integer,Map<Integer,List<cordWrapClass>>> cordMap = new Map<Integer,Map<Integer,List<cordWrapClass>>>();
        cordList = [SELECT CordinateSequence__c,SignStartPage__c,PageVarient__c,llx__c,lly__c,urx__c,ury__c FROM Cordinate__mdt WHERE DocumentName__r.DeveloperName =: docName ORDER BY CordinateSequence__c asc];
        System.debug(cordList.size());
        if(!cordList.isEmpty()){
            for(Cordinate__mdt cord : cordList){
                System.debug(cord.CordinateSequence__c+'--'+cord.SignStartPage__c+'--'+cord.PageVarient__c+'--'+cord.llx__c+'--'+cord.lly__c+'--'+cord.urx__c+'--'+cord.ury__c);
                stPageReferenceMap.put((Integer)cord.SignStartPage__c,(Integer)cord.PageVarient__c);
                if(!cordMap.keySet().contains((Integer)cord.SignStartPage__c)){
                    Map<Integer,List<cordWrapClass>> tempMap = new Map<Integer,List<cordWrapClass>>();
                    tempMap.put((Integer)cord.PageVarient__c,new List<cordWrapClass>{new cordWrapClass(cord.llx__c,cord.lly__c,cord.urx__c,cord.ury__c)});
                    cordMap.put((Integer)cord.SignStartPage__c,tempMap);
                }
                else{
                    Map<Integer,List<cordWrapClass>> tempMap = new Map<Integer,List<cordWrapClass>>();
                    cordWrapClass crd= new cordWrapClass(cord.llx__c,cord.lly__c,cord.urx__c,cord.ury__c);
                    cordMap.get((Integer)cord.SignStartPage__c).get((Integer)cord.PageVarient__c).add(crd);
                }
            }
        }
        System.debug(cordMap);
        Map<Integer,List<Object>> tempMap;
        //Integer tempVar = 0;
        for(Integer signerKey : perPageSignerMap.keySet()){
            Integer tempVar = 0;
            if(perPageSignerMap.get(signerKey) != null && !perPageSignerMap.get(signerKey).isEmpty()){
                for(String signer : perPageSignerMap.get(signerKey)){
                    tempMap = new Map<Integer,List<Object>>();
                    if(!finalMap.keySet().contains(signer)){
                        System.debug(stPageReferenceMap.get(signerKey));
                        System.debug(cordMap.get(signerKey).values());
                        cordWrapClass cordTemp = cordMap.get(signerKey).values().get(0).get(tempVar);
                        tempMap.put(totalPage-stPageReferenceMap.get(signerKey),new List<Object>{new cordWrapClass(cordTemp.llx,cordTemp.lly,cordTemp.urx,cordTemp.ury)});
                        finalMap.put(signer,tempMap);
                        tempVar++;
                    }
                }
            }
        }
        //System.debug(finalMap);
        System.debug(JSON.serialize(finalMap));
        RETURN finalMap;
    }
    
    public static Date getNextRenewalDate(Date currentRenwalDate, string sanctionType){
        try{
            Date finalDate;
            if(sanctionType == 'Interim Extension'){
                integer month   = currentRenwalDate.month();
                integer year    = currentRenwalDate.year();
                integer day = currentRenwalDate.day();
                
                if(month == 1 || month == 2){
                    month += 1;
                    finalDate = Date.newInstance(year,month,day);
                }
                else if(month == 3){
                    finalDate = Date.newInstance(year,03,31);
                }
                else if(month <= 6){
                    month += 6;
                    finalDate = Date.newInstance(year,month,day);
                }
                else
                    finalDate = Date.newInstance(year,12,31);
            }else{
                finalDate   = currentRenwalDate.addMonths(12);
            }
            RETURN finalDate;
        }
        catch(Exception e){
            System.debug(e.getMessage());
            System.debug(e.getLineNumber());
            RETURN null;
        }
    }

    public class cls_PgSigner{
        public Integer pgNo;
        public String signer;
        public cordWrapClass cord;
        public cls_PgSigner(Integer pgNo, String signer, cordWrapClass cord){
            this.pgNo = pgNo;
            this.signer = signer;
            this.cord = cord;
        }
    }
    public class cls_cordWrap{
        public Map<String,Map<Integer,List<Object>>> sign_coordinates;
        public cls_cordWrap(Map<String,Map<Integer,List<Object>>> sign_coordinates){
            this.sign_coordinates = sign_coordinates;
        }
    }
    public class cordWrapClass {
        public Decimal llx; 
        public Decimal lly; 
        public Decimal urx; 
        public Decimal ury; 
        public cordWrapClass(Decimal llx, Decimal lly, Decimal urx, Decimal ury){
            this.llx = llx;
            this.lly = lly;
            this.urx = urx;
            this.ury = ury;
        }
        public cordWrapClass(){}
    }
}