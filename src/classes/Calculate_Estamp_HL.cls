/*
* Name    : Calculate_Estamp_HL
* Company : ET Marlabs
* Purpose : This class will calculate Stamp Charge 
* Author  : Subas
*/
public class Calculate_Estamp_HL {
    public static void calEstamp(List<genesis__Applications__c> appList, Map<Id,genesis__Applications__c> oldList){
        try{
            String DocName1 = '';
            String DocName2 = '';
            String DocName3 = '';
            String DocName4 = '';
            String DocName5 = '';
            Decimal stampCharge1 = 0.00;
            Decimal stampCharge2 = 0.00;
            Decimal stampCharge3 = 0.00;
            Decimal stampCharge4 = 0.00;
            Decimal stampCharge5 = 0.00;
            Decimal totalStampChg = 0.00;        
            for(genesis__Applications__c app : appList){ 
                if((oldList.get(app.Id).Open_CASA_Account__c <> app.Open_CASA_Account__c || oldList.get(app.Id).Plan_Approval_available__c <> app.Plan_Approval_available__c  || oldList.get(app.Id).Verify_Sale_Agreement__c <> app.Verify_Sale_Agreement__c || oldList.get(app.Id).Verify_Construction__c <> app.Verify_Construction__c || oldList.get(app.Id).Verify_Advance_Payment__c <> app.Verify_Advance_Payment__c || oldList.get(app.Id).Execute_Tripartite_Agreement__c <> app.Execute_Tripartite_Agreement__c || oldList.get(app.Id).Verify_Latest_EC__c <> app.Verify_Latest_EC__c || oldList.get(app.Id).Verify_NOC__c <> app.Verify_NOC__c || oldList.get(app.Id).Verify_Margin_Payment__c <> app.Verify_Margin_Payment__c || oldList.get(app.Id).Complete_E_KYC_for_NTB_customers__c <> app.Complete_E_KYC_for_NTB_customers__c) && (app.Open_CASA_Account__c != 'No' && app.Verify_Sale_Agreement__c != 'No' && app.Verify_Construction__c != 'No' && app.Verify_Advance_Payment__c != 'No' && app.Execute_Tripartite_Agreement__c != 'No' && app.Plan_Approval_available__c!= 'No' && app.Verify_Latest_EC__c != 'No' && app.Verify_NOC__c != 'No' && app.Verify_Margin_Payment__c != 'No' && app.Complete_E_KYC_for_NTB_customers__c != 'No')){
                    system.debug('*************'+app.Id);                
                    DocName1 = Constants.A23_HL; //Always
                    List<genesis__Application_Parties__c> parties = [Select Id from genesis__Application_Parties__c where genesis__Application__c =:app.Id AND genesis__Party_Type__c = :Constants.Gurantor AND Active__c =:true];
                    if(parties.size()>0){
                        DocName2 = Constants.A46_HL; //Guarantor                    
                    }                                
                    if(app.Loan_Purpose__c !='Construction on Own Land' && app.Loan_Purpose__c !='Repair or Renovation of House'){
                        DocName5 = Constants.B17_HL; //based on loan purpose
                    }
                    if(app.Execute_Tripartite_Agreement__c == 'N/A'){
                        DocName3 = Constants.B1_HL; //execute tripartite agreement
                        DocName4 = Constants.B2_HL; //execute tripartite agreement
                    }                                
                    String Property_State = app.Property_State__c != null ? app.Property_State__c.toUpperCase() : '';
                    List<Stamping_Charges__mdt> stampChargeAmt =[Select ID,MasterLabel,Declaration_Of_Title_Mortgage__c,Declaration_of_Original_Mortgage__c,
                                                                 Create_Mortgage_Charges__c,Housing_Loan_Agreement__c,Agreement_of_Guarantee__c 
                                                                 From Stamping_Charges__mdt Where MasterLabel =:Property_State Limit 1];
                    system.debug('***Stmp_Charg***'+stampChargeAmt);
                    if(stampChargeAmt.size()>0){
                        if(DocName1 == Constants.A23_HL ){
                            stampCharge1 = stampChargeAmt[0].Housing_Loan_Agreement__c;
                        }
                        if(DocName2 == Constants.A46_HL){
                            stampCharge2 = stampChargeAmt[0].Agreement_of_Guarantee__c;
                        }
                        if(DocName3 == Constants.B1_HL){
                            stampCharge3 = stampChargeAmt[0].Declaration_Of_Title_Mortgage__c;
                        }
                        if(DocName4 == Constants.B2_HL){
                            stampCharge4 = stampChargeAmt[0].Declaration_of_Original_Mortgage__c;
                        }
                        if(DocName5 == Constants.B17_HL){
                            stampCharge5 = stampChargeAmt[0].Create_Mortgage_Charges__c;
                        }
                    }
                    totalStampChg = stampCharge1 + stampCharge2 + stampCharge3+ stampCharge4 +stampCharge5;
                    app.Total_Stamp_Paper_Charge__c = totalStampChg != null ? totalStampChg : 0;
                }
            }
        }catch(Exception e){}
    }
}