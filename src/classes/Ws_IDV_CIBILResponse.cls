/*
* Name    : Ws_IDV_CIBILResponse 
* Company : ET Marlabs
* Purpose : This class is used to Parse IDV Response from Appigee
* Author  : Venugopal N
*/

@RestResource(urlMapping='/IDV_CIBILResponse')
global class Ws_IDV_CIBILResponse {
    global class idResponse extends WS_Response{
        public IDVResponse IDVResponse;
        public CibilResponse CibilResponse;
        CibilRequestTU_HL CibilRequest;
        public string TUCall;
    }
    @HttpPost
    global static idResponse IDVCreating(){
        RestRequest req      = Restcontext.Request;
        RestResponse restRes = Restcontext.response;
        idResponse res         = new idResponse();
        string ReqCall = req.params.get('TUCall');
        string AppID=req.params.get('Appid');
        string ApplicationId=req.params.get('ApplicationId');
        if(req.requestBody == null){
            res.status          = Constants.WS_ERROR_STATUS;
            res.errorMessage    = Constants.WS_REQ_BODY_IS_NULL;
            res.statusCode      = Constants.WS_ERROR_CODE;
            return res;            
        }else{
            try{
                
                string Json=req.requestBody.toString();
                if(ReqCall==Constants.IDVCall && AppID!=null && ApplicationId!=null && !Utility.ISStringBlankorNull(AppID) && !Utility.ISStringBlankorNull(ApplicationId)){
                    
                    IDVResponseTU_HL IDvreq= IDVResponseTU_HL.parse(json);
                    system.debug('IDvreq'+IDvreq);
                    IDVResponse reg= new IDVResponse();
                    system.debug('reg'+reg);
                    reg= CibilTUService.getIDV(IDvreq);
                    system.debug('regg+'+reg);
                    system.debug('AppID+'+AppID);
                    account acc=[select id,FirstName,LastName,MiddleName,name,Gender__pc,PersonBirthdate,Aadhaar_Number__pc,Pan_Number__c,PersonMobilePhone,
                                 PersonMailingStreet,Verify_PAN_Number__c,Is_PAN_card_available__c,PersonMailingCity,PersonMailingPostalCode,PersonMailingState,Full_Name__c from account where ID=:AppID];
                    account ac=new account();
                    if(reg.Result==true){
                        CibilRequestTU_HL Crequest=new  CibilRequestTU_HL ();
                        Crequest=  WS_CibilServiceTU_HL.prepareRequest(acc,Constants.CIBIL) ;
                        res.CibilRequest=Crequest;
                        res.TUCall=Constants.CIBIL;
                        
                        if(reg.PanMatch=='1'){
                            ac.Verify_PAN_Number__c='Yes';
                        }
                        else if(reg.PanMatch=='0' && acc.Is_PAN_card_available__c==false){
                            ac.Verify_PAN_Number__c='No';
                        }
                        ac.id=acc.id;
                        update ac; 
                    }
                    else if(reg.Result==false){
                        list<genesis__Application_Parties__c>  GpList=[select id,genesis__Party_Account_Name__c,genesis__Application__c,Active__c from genesis__Application_Parties__c where genesis__Party_Account_Name__c=:AppID and genesis__Application__c=:ApplicationId and active__c=true];
                        
                        if(GpList.size()>0){
                            if(GpList[0].Active__c==true){
                                GpList[0].Active__c=false;
                                update  GpList[0];
                            } 
                        }
                        else{
                            list<genesis__Applications__c>  Applist=[select id,genesis__Account__c,Sub_Stage__c from genesis__Applications__c where ID=:ApplicationID];
                            if(Applist.size()>0){
                                Applist[0].Sub_Stage__c =Constants.IDVRejeted;
                                update  Applist[0];
                            }
                            
                        }
                        if(reg.PanMatch!=Null){
                            if(reg.PanMatch=='1'){
                                ac.Verify_PAN_Number__c='Yes';
                            }
                            else if(reg.PanMatch=='0' && acc.Is_PAN_card_available__c==false){
                                ac.Verify_PAN_Number__c='No';
                            }
                            ac.id=acc.id;
                            update ac; 
                            
                        }
                        res.status          = Constants.WS_ERROR_STATUS;
                        res.errorMessage    = reg.Status;
                        res.statusCode      = Constants.WS_ERROR_CODE;
                        res.CibilResponse =null;
                        res.IDVResponse=null;
                        return res;
                    }
                    else{
                        res.status          = Constants.WS_ERROR_STATUS;
                        res.errorMessage    = 'Exception from IDV Response';
                        res.statusCode      = Constants.WS_ERROR_CODE;
                        res.CibilResponse =null;
                        res.IDVResponse=null; 
                        return res;
                    }
                    
                    res.IDVResponse=reg; 
                }
                else if(ReqCall==Constants.CIBIL  && AppID!=null && ApplicationId!=null && !Utility.ISStringBlankorNull(AppID) && !Utility.ISStringBlankorNull(ApplicationId))
                {
                    System.debug('@@@@@'+json);
                    CibilResponseTU_HL Creq=CibilResponseTU_HL.parse(json);
                    //JSON2Apex Creq= (JSON2Apex) System.JSON.deserialize(json, JSON2Apex.class);
                    System.debug('@@@@@'+Creq);
                    CibilResponse Cres=CibilTUService.getcibildetails(Creq,Constants.HL_CibilRecordType);
                    system.debug('+++++++'+Utility.getCibilTScore('Neg'));
                    
                    // Start Code for Personal loan CIBIL check using record typeID
                    genesis__Applications__c appData = queryService.getApp(ApplicationId);
                    Id LPRecordTypeId = Schema.SObjectType.genesis__Applications__c.getRecordTypeInfosByName().get('Personal Loan').getRecordTypeId();
                    if(appData.RecordTypeId == LPRecordTypeId)
                    {
                        
                        if(Cres.Result==false ){
                            
                            list<genesis__Applications__c>  Applist=[select id,genesis__Account__c,Application_Stage__c,Sub_Stage__c from genesis__Applications__c where ID=:ApplicationID];
                            if(Applist.size()>0){
                                Applist[0].Sub_Stage__c =Constants.CIBILRejected;
                                update  Applist[0];
                            }
                            res.status          = Constants.WS_ERROR_STATUS;
                            res.errorMessage    = Constants.CIBILRejectedMsg;//Cres.Status;
                            res.statusCode      = Constants.WS_ERROR_CODE;
                        }
                        else if(Cres.Result==true && (Decimal.valueof(Cres.CibilScore)==Utility.getCibilTScore('Neg') || ( Decimal.valueof(Cres.CibilScore)>=1 && Decimal.valueof(Cres.CibilScore)<=5))){
                            system.debug('-1 Value in CIBIL');
                            list<genesis__Applications__c>  Applist=[select id,genesis__Account__c,Application_Stage__c,Sub_Stage__c from genesis__Applications__c where ID=:ApplicationID];
                            if(Applist.size()>0){
                                Applist[0].Sub_Stage__c =Constants.InvalidCIBIL;
                                Applist[0].Application_Stage__c =Constants.APP_REJECTED_STAGE;
                                update  Applist[0];
                            }
                            res.errorMessage    = Constants.CIBILRejectedMsg;
                             res.status       =Constants.WS_ERROR_STATUS;
                            
                        }else if(Cres.Result==true && (Decimal.valueof(Cres.CibilScore)==Utility.getCibilTScore('Neg') && String.valueOf(Cres.serviceType) == Constants.EXPERIAN || ( Decimal.valueof(Cres.CibilScore)>=1 && Decimal.valueof(Cres.CibilScore)<=10))){
                            system.debug('##### -1 for experian');
                            list<genesis__Applications__c>  Applist=[select id,genesis__Account__c,Application_Stage__c,Sub_Stage__c from genesis__Applications__c where ID=:ApplicationID];
                            if(Applist.size()>0){
                                Applist[0].Sub_Stage__c =Constants.InvalidCIBIL;
                                Applist[0].Application_Stage__c =Constants.APP_REJECTED_STAGE;
                                update  Applist[0];
                            }
                            res.errorMessage    = Constants.CIBILRejectedMsg;
                             res.status       =Constants.WS_ERROR_STATUS;
                        }
                        //Score validation
                        else if(Cres.Result==true && Decimal.valueof(Cres.CibilScore)<Utility.getCibilTScore('PLCibil')){
                            system.debug('Wrong one for -1');
                            
                            list<genesis__Applications__c>  Applist=[select id,genesis__Account__c,Application_Stage__c,Sub_Stage__c from genesis__Applications__c where ID=:ApplicationID];
                            if(Applist.size()>0){
                                Applist[0].Sub_Stage__c =Constants.LowCIBILScore;
                                Applist[0].Application_Stage__c =Constants.APP_REJECTED_STAGE;
                                update  Applist[0];
                            }
                            
                            res.errorMessage    = Constants.LowCIBILScoreMsg;   
                            res.status       =Constants.WS_ERROR_STATUS;
                            
                        }
                    }// END of personal loan cibil score check
                    else{   
                        
                        if(Cres.Result==false ){
                            
                            list<genesis__Application_Parties__c>  GpList=[select id,genesis__Party_Account_Name__c,genesis__Application__c,Active__c from genesis__Application_Parties__c where genesis__Party_Account_Name__c=:AppID and genesis__Application__c=:ApplicationId and active__c=true];
                            
                            if(GpList.size()>0){
                                if(GpList[0].Active__c==true){
                                    GpList[0].Active__c=false;
                                    update  GpList[0];
                                } 
                            }
                            else{
                                list<genesis__Applications__c>  Applist=[select id,genesis__Account__c,Sub_Stage__c from genesis__Applications__c where ID=:ApplicationID];
                                if(Applist.size()>0){
                                    Applist[0].Sub_Stage__c =Constants.CIBILRejected;
                                    update  Applist[0];
                                }
                            } 
                            
                            res.status          = Constants.WS_ERROR_STATUS;
                            res.errorMessage    = Cres.Status;
                            res.statusCode      = Constants.WS_ERROR_CODE;
                        }
                        
                        else if(Cres.Result==true && (Decimal.valueof(Cres.CibilScore)==Utility.getCibilTScore('Neg') || ( Decimal.valueof(Cres.CibilScore)>=1 && Decimal.valueof(Cres.CibilScore)<=10))){
                            
                            system.debug('-1 Value');
                        }
                        else if(Cres.Result==true && Decimal.valueof(Cres.CibilScore)<Utility.getCibilTScore('Cibil')){
                            system.debug('Wrong one for -1');
                            list<genesis__Application_Parties__c>  GpList=[select id,genesis__Party_Account_Name__c,genesis__Application__c,Active__c from genesis__Application_Parties__c where genesis__Party_Account_Name__c=:AppID and genesis__Application__c=:ApplicationId and active__c=true];
                            
                            if(GpList.size()>0){
                                if(GpList[0].Active__c==true){
                                    GpList[0].Active__c=false;
                                    update  GpList[0];
                                } 
                            }
                            else{
                                list<genesis__Applications__c>  Applist=[select id,genesis__Account__c,Sub_Stage__c from genesis__Applications__c where ID=:ApplicationID];
                                if(Applist.size()>0){
                                    Applist[0].Sub_Stage__c =Constants.LowCIBILScore;
                                    update  Applist[0];
                                }
                            } 
                            res.errorMessage    = 'Low Cibil Score';   
                            res.status       =Constants.WS_ERROR_STATUS;
                            
                        }
                    }
                    res.CibilResponse=Cres;
                    
                    
                    
                }else{
                    res.status          = Constants.WS_ERROR_STATUS;
                    res.errorMessage    = 'Please Select the TransUnion Type,AppID and ApplicationId';
                    res.statusCode      = Constants.WS_ERROR_CODE;
                    res.CibilResponse =null;
                    res.IDVResponse=null;
                }
                
                
                
                return res;  
            }
            catch(exception e){
                system.debug(e.getMessage()+''+e.getStackTraceString()+''+e.getLineNumber());
                res.status          = Constants.WS_ERROR_STATUS;
                res.errorMessage    = 'Please pass the Valid Request'+e.getLineNumber()+e.getMessage();
                res.statusCode      = Constants.WS_ERROR_CODE;
                res.CibilResponse =null;
                res.IDVResponse=null; 
                return res;
            }
        }
        return null;
    }
}