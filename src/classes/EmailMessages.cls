public class EmailMessages {
   
    public static void sendEmail( string Email,string Msg){
         string[] ToAddress=new string[]{};
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
        ToAddress.add(Email);
        		mail.setToAddresses(ToAddress);
                mail.setReplyTo('support@kvb.com');
                mail.setSenderDisplayName('KVB TEAM');
       		    mail.setPlainTextBody(Msg);
                mail.setBccSender(false);
                mail.setUseSignature(false);
         Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail}); 
    }

}