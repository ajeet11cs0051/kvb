/*
 * Name     : AnonymousBatchClass
 * Company  : ET Marlabs
 * Purpose  : To update loan amount 
 * Author   : Dushyant
*/
public class AnonymousBatchClass implements Database.Batchable<sObject>,Database.Stateful, Database.AllowsCallouts {
    
    public String whereCondition   = '';
    
    public AnonymousBatchClass(String condition){
        if(condition != null && condition != ''){
            whereCondition = ' WHERE ' +condition;
        } 
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        System.debug(whereCondition);
        String applQuery = 'SELECT id,genesis__Loan_Amount__c,(SELECT id,Existing_Limit__c,RecordType.DeveloperName FROM Facilities__r WHERE Active__c =  true AND RecordType.DeveloperName = \'Parent\') FROM genesis__Applications__c'+whereCondition;
        System.debug('applQuery*******'+applQuery);
        return Database.getQueryLocator(applQuery);
    }
    
    public void execute(Database.BatchableContext BC, List<genesis__Applications__c> appList) {
        
        List<genesis__Applications__c> appToUpdate = new List<genesis__Applications__c>();
        System.debug('appList*****'+appList);
        if(!appList.isEmpty()){
            for(genesis__Applications__c app : appList){
                Decimal total = 0.0;
                System.debug('app.Facilities__r*****'+app.Facilities__r);
                if(!app.Facilities__r.isEmpty()){
                    for(Facility__c fac : app.Facilities__r){
                        if(fac.Existing_Limit__c != null) total += fac.Existing_Limit__c;
                    }
                }      
                genesis__Applications__c genAppl = new genesis__Applications__c(Id = app.id, genesis__Loan_Amount__c = total);
                appToUpdate.add(genAppl);
                System.debug('appToUpdate****'+appToUpdate);
            }
            
            if(!appToUpdate.isEmpty()){
                ApplicationTriggerHandler.IsFirstRun = false;
                UPDATE appToUpdate;
            }            
        }
    }
    
    public void finish(Database.BatchableContext BC) {
        
    }
}