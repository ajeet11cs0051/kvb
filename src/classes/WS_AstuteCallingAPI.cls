/*
* Name      : WS_AstuteCallingAPI
* Compnay   : ET Marlabs
* Purpose   : Field Investigation Astute Request API 1
* Author    : Ashok
*/
public class WS_AstuteCallingAPI {
    public static string applicationId;
    public static Map<String,ID> accmap=new Map<String,ID>();
    public static Map<String,String> propertyMap=new Map<String,String>();
    public static boolean flag;
    
    public static void callAstute(String AppId,String caseid,String PropType){
        System.debug('inside WS_AstuteCallingAPI ***@@@');
        applicationId=AppId;
        flag=true;
        Boolean SalariedFlag = TRUE;
        String PickUpDate = String.valueOf(system.now());
        List <Account> FIAccList = new List<Account>();
        // For Final FI status update on Application level.
        set<id> AppleID = new set<id>();


        WS_AstuteCallingAPI.AstueRequest astutereqObj=new WS_AstuteCallingAPI.AstueRequest();
        astutereqObj.Case145_089 = new List<cls_Case>();
        genesis__Applications__c application = queryService.getApplication(AppId);
        List <Account> accList = queryService.accList(AppId);
        
        if(application.Record_Type_Name__c == Constants.PERSONALLOAN){
            for(Account acc: accList){
                if(acc.Are_you_An_Existing_Customer__c == False){
                    FIAccList.add(acc);
                }else if(acc.Same_Present_Address_Permanent_Address__c == False){
                    FIAccList.add(acc);
                }
                system.debug('##### Final FI Account List ---- '+ FIAccList);
            }
            System.debug('FI Final accList size'+FIAccList.size());
        }
        if(application.Record_Type_Name__c == Constants.LAPLOAN){
            if(application.genesis__Account__r.Employment_Type__c==Constants.EMPTYP_SELFEMPB ){
                Integer flag=0;
                for(Account acc: accList)  {
                    System.debug('accList for LAP -- '+accList);
                    if(ACC.ID != application.genesis__Account__r.ID ){
                        if(acc.CBS_Customer_ID__c!= null ){
                            flag=1;
                            if(FIAccList.size() > 0){
                                FIAccList.clear();
                            }
                            break;
                        }
                        FIAccList.add(acc);
                    }
                }
                System.debug('*FIAccList'+FIAccList);
            }else{
                for(Account acc: accList)  {
                    FIAccList.add(acc); 
                }
            }
        }
        if(application.Record_Type_Name__c == Constants.PERSONALLOAN && ( (application.genesis__Account__r.Employment_Type__c == Constants.EMPTYP_SELFEMPP) || (application.genesis__Account__r.Employment_Type__c == Constants.EMPTYP_AGRICULTUR))){
            
            SalariedFlag = FALSE;
        }else{
            SalariedFlag = TRUE;
        }
        
        if(SalariedFlag == TRUE && FIAccList.size() > 0 && (PropType == null || PropType == '' || PropType == Constants.ACTIVITY_DESCO)){
            // loop for office
            for(Account acc: FIAccList){
                //   if(acc.Employment_Type__c==Constants.EMPTYP_SALARIED || acc.Employment_Type__c==Constants.EMPTYP_SELFEMPB ){
                WS_AstuteCallingAPI.cls_Case cbsInput = new WS_AstuteCallingAPI.cls_Case();
                //For App FI status logic
                AppleID.add(AppId);  
                
                if(caseid ==null || caseid==''){
                    cbsInput.NotificationId=CreateUniqueId.uniqID();
                }else{
                    cbsInput.NotificationId=caseid;
                }
                cbsInput.ApplicationNumber=AppId;
                cbsInput.ApplicantId= acc.Id;
                cbsInput.ActivityDescription=Constants.ACTIVITY_DESCO;
                if(!Utility.ISStringBlankorNull(acc.Name))
                    cbsInput.ApplicantName=acc.Name;    //Sagar
                cbsInput.ApplicantType=Constants.BORROWER_TYPE; //Borrower
                if(!Utility.ISStringBlankorNull(acc.Pan_Number__c))    
                    cbsInput.PAN=acc.Pan_Number__c; //ABCDE1234G
                
                /// logic for  assigining address for salaried account and self employed buisness
                if(!Utility.ISStringBlankorNull(acc.Employment_Type__c)){
                    
                    /// for salaried
                    if(acc.Employment_Type__c==Constants.EMPTYP_SALARIED ){
                        
                        if(!Utility.ISStringBlankorNull(acc.PersonMailingStreet))       
                            cbsInput.VerificationAddress=acc.PersonMailingStreet;   //ShivajiNagar,Pune
                        // public cls_Landmark[] Landmark;
                        //cbsInput.Landmark = new List<cls_Landmark>();
                        if(!Utility.ISStringBlankorNull(acc.PersonMailingPostalCode))    
                            cbsInput.PINCode=acc.PersonMailingPostalCode;   //411516
                        if(!Utility.ISStringBlankorNull(acc.PersonOtherCity))    
                            cbsInput.Location=acc.PersonOtherCity;  //PUNE
                        if(!Utility.ISStringBlankorNull(acc.PersonOtherState))    
                            cbsInput.State=acc.PersonOtherState;    //MAHARASHTRA
                    }
                    //for self buisness
                    if(acc.Employment_Type__c==Constants.EMPTYP_SELFEMPB){
                        
                        if(!Utility.ISStringBlankorNull(acc.Firm_Address__c))    
                            cbsInput.VerificationAddress=acc.Firm_Address__c;   //411516
                        if(!Utility.ISStringBlankorNull(acc.Firm_Pincode__c ))  
                            cbsInput.PINCode=acc.Firm_Pincode__c;  //PUNE
                        
                        cbsInput.Location='';
                        cbsInput.State='';
                    }
                    
                }
                
                if(!Utility.ISStringBlankorNull(acc.PersonMobilePhone))     
                    cbsInput.MobileNumber=acc.PersonMobilePhone;    //12345567898
                if(!Utility.ISStringBlankorNull(acc.Company_phone__c))    
                    cbsInput.PhoneNumber=acc.Company_phone__c;  //679999
                if(!Utility.ISStringBlankorNull(acc.Company_Name__c))    
                    cbsInput.OfficeName=acc.Company_Name__c;    //Global Solutions
                cbsInput.PickUpDate=PickUpDate; //2018-11-04 15:19:10
                if(!Utility.ISStringBlankorNull(acc.Employment_Type__c))    
                    cbsInput.ApplicantCategory=acc.Employment_Type__c;  //Salaried
                if(!Utility.ISStringBlankorNull(acc.Employment_SubType__c))    
                    cbsInput.PrimaryOccupation=acc.Employment_SubType__c;   
                
                accmap.put(cbsInput.NotificationId,acc.id);
                System.debug(accmap);
                propertyMap.put(cbsInput.NotificationId,'OFFICE');
                
                astutereqObj.Case145_089.add(cbsInput);
                
            }
            ///end for loop
            
        }
        if(FIAccList.size() > 0 && (PropType == null || PropType == '' || PropType == Constants.ACTIVITY_DESCR )){
            // loop for  Residence
            for(Account acc: FIAccList){
                WS_AstuteCallingAPI.cls_Case cbsInput = new WS_AstuteCallingAPI.cls_Case();  
                if(caseid ==null || caseid==''){    
                    cbsInput.NotificationId=CreateUniqueId.uniqID();
                }else{
                    cbsInput.NotificationId=caseid; 
                }
                cbsInput.ApplicationNumber=AppId;
                cbsInput.ApplicantId= acc.id;
                cbsInput.ActivityDescription=Constants.ACTIVITY_DESCR;
                if(!Utility.ISStringBlankorNull(acc.Name))    
                    cbsInput.ApplicantName=acc.Name; //Sagar
                cbsInput.ApplicantType=Constants.BORROWER_TYPE; //Borrower
                if(!Utility.ISStringBlankorNull(acc.Pan_Number__c))    
                    cbsInput.PAN=acc.Pan_Number__c; //ABCDE1234G
                if(!Utility.ISStringBlankorNull(acc.PersonMailingStreet))      
                    cbsInput.VerificationAddress=acc.PersonMailingStreet; //ShivajiNagar,Pune
                if(!Utility.ISStringBlankorNull(acc.PersonMailingPostalCode))    
                    cbsInput.PINCode=acc.PersonMailingPostalCode;   //411516
                if(!Utility.ISStringBlankorNull(acc.PersonOtherCity))    
                    cbsInput.Location=acc.PersonOtherCity;   //PUNE
                if(!Utility.ISStringBlankorNull(acc.PersonOtherState))     
                    cbsInput.State=acc.PersonOtherState;  //MAHARASHTRA
                if(!Utility.ISStringBlankorNull(acc.PersonMobilePhone))     
                    cbsInput.MobileNumber=acc.PersonMobilePhone;    //12345567898
                if(!Utility.ISStringBlankorNull(acc.Phone))     
                    cbsInput.PhoneNumber=acc.Phone; //679999
                if(!Utility.ISStringBlankorNull(acc.Company_Name__c))    
                    cbsInput.OfficeName=acc.Company_Name__c;    //Global Solutions
                cbsInput.PickUpDate=PickUpDate; //2018-11-04 15:19:10
                if(!Utility.ISStringBlankorNull(acc.Employment_Type__c))     
                    cbsInput.ApplicantCategory=acc.Employment_Type__c;  //Salaried
                if(!Utility.ISStringBlankorNull(acc.Employment_SubType__c))       
                    cbsInput.PrimaryOccupation=acc.Employment_SubType__c;  
                
                accmap.put(cbsInput.NotificationId,acc.id);
                System.debug(accmap);
                propertyMap.put(cbsInput.NotificationId,'RESIDENCE');
                astutereqObj.Case145_089.add(cbsInput);
                // For app FI status update logic
                AppleID.add(AppId); 
            }
            //end for loop
        }
        if(FIAccList.size()>0){
            AstuteMethod(JSON.serialize(astutereqObj));
        }
        //ending if
        
        
    }
    
    
    
    ///end ofastute method
    
    
    public static void astuteRetry2(List<Astute_Verification__c> astutelist){
        try{
            System.debug('**astutelist retry method --- '+astutelist);
            WS_AstuteCallingAPI.AstueRequest astutereqObj=new WS_AstuteCallingAPI.AstueRequest();
            astutereqObj.Case145_089 = new List<cls_Case>();
            String PickUpDate = String.valueOf(system.now());
            
            for(Astute_Verification__c astuteObj :astutelist){
                
                String AccId=astuteObj.Account__c;
                WS_AstuteCallingAPI.cls_Case cbsInput = new WS_AstuteCallingAPI.cls_Case();  
                
                Account acc =queryService.getAccount(AccId); 
                if(astuteObj.Property_Type__c==Constants.ACTIVITY_DESCO){
                    
                    cbsInput.NotificationId=astuteObj.Case_Notification_Id__c;
                    cbsInput.ApplicationNumber=astuteObj.Application__c;
                    cbsInput.ApplicantId= astuteObj.Account__c;
                    cbsInput.ActivityDescription=Constants.ACTIVITY_DESCO;
                    if(!Utility.ISStringBlankorNull(acc.Name))
                        cbsInput.ApplicantName=acc.Name;    //Sagar
                    cbsInput.ApplicantType=Constants.BORROWER_TYPE; //Borrower
                    if(!Utility.ISStringBlankorNull(acc.Pan_Number__c))    
                        cbsInput.PAN=acc.Pan_Number__c; //ABCDE1234G
                    
                    /// logic for  assigining address for salaried account and self employed buisness
                    if(!Utility.ISStringBlankorNull(acc.Employment_Type__c)){
                        
                        /// for salaried
                        if(acc.Employment_Type__c==Constants.EMPTYP_SALARIED ){
                            
                            if(!Utility.ISStringBlankorNull(acc.PersonMailingStreet))       
                                cbsInput.VerificationAddress=acc.PersonMailingStreet;   //ShivajiNagar,Pune
                            // public cls_Landmark[] Landmark;
                            //cbsInput.Landmark = new List<cls_Landmark>();
                            if(!Utility.ISStringBlankorNull(acc.PersonMailingPostalCode))    
                                cbsInput.PINCode=acc.PersonMailingPostalCode;   //411516
                            if(!Utility.ISStringBlankorNull(acc.PersonOtherCity))    
                                cbsInput.Location=acc.PersonOtherCity;  //PUNE
                            if(!Utility.ISStringBlankorNull(acc.PersonOtherState))    
                                cbsInput.State=acc.PersonOtherState;    //MAHARASHTRA
                        }
                        //for self buisness
                        if(acc.Employment_Type__c==Constants.EMPTYP_SELFEMPB){
                            
                            if(!Utility.ISStringBlankorNull(acc.Firm_Address__c))    
                                cbsInput.VerificationAddress=acc.Firm_Address__c;   //411516
                            if(!Utility.ISStringBlankorNull(acc.Firm_Pincode__c ))  
                                cbsInput.PINCode=acc.Firm_Pincode__c;  //PUNE
                            
                            cbsInput.Location='';
                            cbsInput.State='';
                        }
                        
                    }
                    
                    //  public cls_SubUrban[] SubUrban;
                    //cbsInput.SubUrban = new List<cls_SubUrban>();
                    if(!Utility.ISStringBlankorNull(acc.PersonMobilePhone))     
                        cbsInput.MobileNumber=acc.PersonMobilePhone;    //12345567898
                    if(!Utility.ISStringBlankorNull(acc.Company_phone__c))    
                        cbsInput.PhoneNumber=acc.Company_phone__c;  //679999
                    if(!Utility.ISStringBlankorNull(acc.Company_Name__c))    
                        cbsInput.OfficeName=acc.Company_Name__c;    //Global Solutions
                    cbsInput.PickUpDate=PickUpDate; //2018-11-04 15:19:10
                    if(!Utility.ISStringBlankorNull(acc.Employment_Type__c))    
                        cbsInput.ApplicantCategory=acc.Employment_Type__c;  //Salaried
                    if(!Utility.ISStringBlankorNull(acc.Employment_SubType__c))    
                        cbsInput.PrimaryOccupation=acc.Employment_SubType__c;   
                    
                    accmap.put(cbsInput.NotificationId,acc.id);
                    System.debug(accmap);
                    propertyMap.put(cbsInput.NotificationId,'OFFICE');
                    
                    astutereqObj.Case145_089.add(cbsInput);
                    
                    
                }
                else{
                    cbsInput.NotificationId=astuteObj.Case_Notification_Id__c;
                    cbsInput.ApplicationNumber=astuteObj.Application__c;
                    cbsInput.ApplicantId= astuteObj.Account__c;
                    cbsInput.ActivityDescription=Constants.ACTIVITY_DESCR;
                    if(!Utility.ISStringBlankorNull(acc.Name))    
                        cbsInput.ApplicantName=acc.Name; //Sagar
                    cbsInput.ApplicantType=Constants.BORROWER_TYPE; //Borrower
                    if(!Utility.ISStringBlankorNull(acc.Pan_Number__c))    
                        cbsInput.PAN=acc.Pan_Number__c; //ABCDE1234G
                    if(!Utility.ISStringBlankorNull(acc.PersonMailingStreet))      
                        cbsInput.VerificationAddress=acc.PersonMailingStreet; //ShivajiNagar,Pune
                    if(!Utility.ISStringBlankorNull(acc.PersonMailingPostalCode))    
                        cbsInput.PINCode=acc.PersonMailingPostalCode;   //411516
                    if(!Utility.ISStringBlankorNull(acc.PersonOtherCity))    
                        cbsInput.Location=acc.PersonOtherCity;   //PUNE
                    if(!Utility.ISStringBlankorNull(acc.PersonOtherState))     
                        cbsInput.State=acc.PersonOtherState;  //MAHARASHTRA
                    if(!Utility.ISStringBlankorNull(acc.PersonMobilePhone))     
                        cbsInput.MobileNumber=acc.PersonMobilePhone;    //12345567898
                    if(!Utility.ISStringBlankorNull(acc.Phone))     
                        cbsInput.PhoneNumber=acc.Phone; //679999
                    if(!Utility.ISStringBlankorNull(acc.Company_Name__c))    
                        cbsInput.OfficeName=acc.Company_Name__c;    //Global Solutions
                    cbsInput.PickUpDate=PickUpDate; //2018-11-04 15:19:10
                    if(!Utility.ISStringBlankorNull(acc.Employment_Type__c))     
                        cbsInput.ApplicantCategory=acc.Employment_Type__c;  //Salaried
                    if(!Utility.ISStringBlankorNull(acc.Employment_SubType__c))       
                        cbsInput.PrimaryOccupation=acc.Employment_SubType__c;  
                    
                    accmap.put(cbsInput.NotificationId,acc.id);
                    System.debug(accmap);
                    propertyMap.put(cbsInput.NotificationId,'RESIDENCE');
                    astutereqObj.Case145_089.add(cbsInput); 
                    
                }
                
            }
            flag=false;
            System.debug('**flag'+flag);
            AstuteMethod(JSON.serialize(astutereqObj));
            
        }
        catch(Exception e){
            System.debug('ex'+e.getLineNumber());
            System.debug('ex message'+e.getMessage());
        }
    }
    
    
    
    public static void AstuteMethod(String  astreq){
        try{
            System.debug('json request for Astute'+astreq);
            String req= astreq.replace('Case145_089','Case');
            System.debug('++req'+req);
            
            Map<String,String> headerMap = new Map<String,String>();              
            //  headerMap.put('Content-Type','application/json'); 
            headerMap.put('Authorization',WS_ApiGatewayAccToken.getAccTkn());
            HTTPResponse response = new HTTPResponse();
            // String endPoint='https://kvb-test.apigee.net/next/v1/astute/case';
            // response = HttpUtility.sendHTTPRequest(endPoint, 'POST', null,req,headerMap,null);
            response = HttpUtility.sendHTTPRequest(Utility.getFullEndpoints('AstuteRequest1'), 'POST',null,req,headerMap,label.CA_CERTIFICATE);
            
            if(response.getStatusCode() == 200 || response.getStatusCode() == 201){
                String jsonData = response.getBody();
                system.debug('jsonData::'+jsonData);
                AstueResponse appReqObj = (AstueResponse)System.JSON.deserialize(jsonData, AstueResponse.class);
                System.debug('**appReqObj'+appReqObj);
                if(appReqObj == null ){
                    throw new CustomException('There is No Response from Astute.....');
                }else{
                    List<Astute_Verification__c> ListAstuteUpdt = new List<Astute_Verification__c>();
                    System.debug('**main method'+flag);
                    if(flag==true) {
                        for(cls_CaseDetails detls: appReqObj.CaseDetails){
                            Astute_Verification__c AstuteVefr = new Astute_Verification__c();
                            if(!Utility.ISStringBlankorNull(detls.Case_NotificationId))
                                AstuteVefr.Case_Notification_Id__c = String.valueOf(detls.Case_NotificationId);
                            if(!Utility.ISStringBlankorNull(detls.Status))
                                AstuteVefr.Status__c = String.valueOf(detls.Status);
                            if(!Utility.ISStringBlankorNull(detls.Message))
                                AstuteVefr.Message__c = String.valueOf(detls.Message);
                            if(!Utility.ISStringBlankorNull(detls.CaseId))
                                AstuteVefr.Case_Id__c = String.valueOf(detls.CaseId);
                            if(!Utility.ISStringBlankorNull(detls.StatusId))
                                AstuteVefr.Status_ID__c = String.valueOf(detls.StatusId);
                            
                            Id applicant_id=accmap.get(detls.Case_NotificationId);
                            AstuteVefr.Account__c=applicant_id;
                            AstuteVefr.Application__c=applicationId;
                            
                            String prop_type=propertyMap.get(detls.Case_NotificationId);
                            AstuteVefr.Property_Type__c=prop_type;
                            ListAstuteUpdt.add(AstuteVefr);
                        }
                        System.debug('** ListAstuteUpdt details'+ListAstuteUpdt);
                        insert ListAstuteUpdt;
                    }
                    
                    if(flag==false){
                        for(cls_CaseDetails detls: appReqObj.CaseDetails){
                            Astute_Verification__c AstuteVefr = new Astute_Verification__c();
                            if(!Utility.ISStringBlankorNull(detls.Case_NotificationId))
                                AstuteVefr.Case_Notification_Id__c = String.valueOf(detls.Case_NotificationId);
                            if(!Utility.ISStringBlankorNull(detls.Status))
                                AstuteVefr.Status__c = String.valueOf(detls.Status);
                            if(!Utility.ISStringBlankorNull(detls.Message))
                                AstuteVefr.Message__c = String.valueOf(detls.Message);
                            if(!Utility.ISStringBlankorNull(detls.CaseId))
                                AstuteVefr.Case_Id__c = String.valueOf(detls.CaseId);
                            if(!Utility.ISStringBlankorNull(detls.StatusId))
                                AstuteVefr.Status_ID__c = String.valueOf(detls.StatusId);
                            ListAstuteUpdt.add(AstuteVefr);
                        }
                        System.debug('ListAstuteUpdt -- upsert -- '+ListAstuteUpdt);
                        upsert ListAstuteUpdt Case_Notification_Id__c;
                    }
                    
                }
            }else{
                throw new CustomException('Status Code-'+response.getStatusCode()+' Status-'+response.getStatus());
            }
        }
        catch(Exception e){
            system.debug(e.getLineNumber()+' Exception on Astute request call.... '+e.getStackTraceString());
            System.debug('message'+e.getMessage());
        }
        //return 
    }
    
    // Method to update application FI Status based on request initiated
    public static void AppFIStatus_Update(set<id> appIds){

        List<genesis__Applications__c> AppUpdates = new List<genesis__Applications__c>();
        List<genesis__Applications__c> Listupdateapp = [Select id,FI_Status__c from genesis__Applications__c where ID =: appIds];
        
        for(genesis__Applications__c gapps: Listupdateapp){
            gapps.FI_Status__c = Constants.PL_FIREQ_INIT;
            AppUpdates.add(gapps);
        }
        //update AppUpdates;
    }

    // astue request
    
    public class AstueRequest{
        public cls_Case[] Case145_089;
    }
    public class cls_Case {
        public String NotificationId;   //CC20180404031945675432
        public String ApplicationNumber;    //#CCMNV51111
        //public cls_ApplicantId[] ApplicantId;
        public string ApplicantId;
        public String ActivityDescription;  //Office
        public String ApplicantName;    //Sagar
        public String ApplicantType;    //Borrower
        public String PAN;  //ABCDE1234G
        public String VerificationAddress;  //ShivajiNagar,Pune
        public cls_Landmark[] Landmark;
        public String PINCode;  //411516
        public String Location; //PUNE
        public String State;    //MAHARASHTRA
        public cls_SubUrban[] SubUrban;
        public String MobileNumber; //12345567898
        public String PhoneNumber;  //679999
        public String OfficeName;   //Global Solutions
        public String PickUpDate;   //2018-11-04 15:19:10
        public String ApplicantCategory;    //Salaried
        public String PrimaryOccupation;    //Salaried
    }
    /*public class cls_ApplicantId {
}*/
    public class cls_Landmark {
    }
    public class cls_SubUrban {
    }
    
    // astue response
    
    public class AstueResponse{
        public cls_CaseDetails[] CaseDetails;
    }
    public class cls_CaseDetails {
        public String Case_NotificationId;  //
        public String Status;   //Failed
        public String Message;  //Mandatory fields are blank NotificationId)
        public String CaseId;   //0
        public String StatusId; //202
    }
}