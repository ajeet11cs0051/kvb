/*
 * Name		: SME_Email_Handler
 * Company	: ET Marlabs
 * Purpose 	: Used as a Handler Class to Send Email to SME Customer . 
 * Author	: Raushan
 */
public class SME_Email_Handler {
    
    public static String endUrl = KVB_Company_Details__c.getOrgDefaults().FE_Portal_Url__c;
	
    public static string getDocumentLogoUrl(){
		List<Document> lstDocument = [Select Id,Name,LastModifiedById from Document where DeveloperName ='Kvb_logo_for_Email' limit 1];
		string strOrgId = UserInfo.getOrganizationId();
		string strDocUrl = system.label.Domain_Name+'/servlet/servlet.ImageServer?id=' + lstDocument[0].Id +'&oid=' + strOrgId;
		System.debug(strDocUrl);
		return strDocUrl;
	}
	
	public static Messaging.SingleEmailMessage getEmailTemplates(String Email_id, String HtmlBody){
		Messaging.SingleEmailMessage singleMail = new Messaging.SingleEmailMessage();
        if(Email_id != null){
            singleMail.toAddresses = new String[]{Email_id};
            singleMail.setHtmlBody(HtmlBody);
            singleMail.setSaveAsActivity(false);
        }  
        return singleMail;
    }
	public static String getRenewalDueTemplates(genesis__Applications__c appln){
		String logoUrl = getDocumentLogoUrl();
		String emailBody = 'Dear '+appln.genesis__Account__r.Name+'<br/>your working capital loan with KVB is due for renewal on '+appln.Renewal_Due_Date__c;
					
		for(Facility__c  f : appln.Facilities__r){
			emailBody += '<br/>'+f.Product_Name__c+' ,'+f.Existing_Limit__c+'<br/>';
		}
		emailBody += '<br/><br/>'+appln.Owner.Name+'<br/>'+appln.Branch_Name__c+'<br/>';
		emailBody +='<br/>'+'<img src="'+logoUrl+'"/>'+'\t <b>Karur Vysya Bank</b>';
		
		return emailBody;
	}
	
	public static String getRenewalApplicationSubmissionPending(genesis__Applications__c appln){
		String logoUrl = getDocumentLogoUrl();
		String emailBody = 'Dear '+appln.genesis__Account__r.Name+'<br/>your working capital loan with KVB is due for renewal on '+appln.Renewal_Due_Date__c+'<br/> Below are the list of facilities due for renewal:<br/>';
					
		for(Facility__c  f : appln.Facilities__r){
			emailBody += '<br/>'+f.Product_Name__c+' ,'+f.Existing_Limit__c;
		}
		emailBody +='<br/>You can now renew your working capital loans online by clicking'+endUrl+'and  using your corporate internet banking details.\n Please  contact us on <<phone>> for any assistance on the application.<br/><br/>';
		emailBody += '<br/><br/>'+appln.Owner.Name+'<br/>'+appln.Branch_Name__c+'<br/>';
		emailBody +='<br/>'+'<img src="'+logoUrl+'"/>'+'\t <b>Karur Vysya Bank</b>';
		return emailBody;
	}
	
	public static String getLoanUnderProcessing(genesis__Applications__c appln){
		String logoUrl = getDocumentLogoUrl();
		String emailBody = 'Dear '+appln.genesis__Account__r.Name+'<br/>Thank you for submitting your working capital renewal application.\n Your Working capital loan renewal application is under processing.<br/><br/>';
		
		emailBody +='<br/>Please note the application no '+appln.Name +' for your reference. \n You may use this refernce number for any communication regarding your working captial renewal application. <br/><br/>';
		emailBody += '<br/><br/>'+appln.Owner.Name+'<br/>'+appln.Branch_Name__c+'<br/>';
		emailBody +='<br/>'+'<img src="'+logoUrl+'"/>'+'\t <b>Karur Vysya Bank</b>';
		return emailBody;
	}
	
	public static String getESignPartiallyCompleted_Individuals(genesis__Applications__c appln){
		String logoUrl = getDocumentLogoUrl();
		String emailBody = 'Dear '+appln.genesis__Account__r.Name+',<br/> your e-sign is pending on your working capital loan santion letter.\n Request you to complete the e-sign process by clicking here'+endUrl+'<br/><br/>';
		emailBody += '<br/><br/>'+appln.Owner.Name+'<br/>'+appln.Branch_Name__c+'<br/>';
		emailBody +='<br/>'+'<img src="'+logoUrl+'"/>'+'\t <b>Karur Vysya Bank</b>';
		return emailBody;
	} 
	
	public static String getSanctionedRenewed(genesis__Applications__c appln){
		String logoUrl = getDocumentLogoUrl();
		String emailBody = 'Dear '+appln.genesis__Account__r.Name+'<br/> your working capital renewal application has been sanctioned and limit has been renewed upto '+appln.Limit_Expiry_Date__c+'<br/> Below are the list of facilities renewed:<br/>';
					
		for(Facility__c  f : appln.Facilities__r){
			emailBody += '<br/>'+f.Product_Name__c+' ,'+f.Recommended_Limit__c;
		}
		emailBody +='<br/>You can download the sanction letter by clicking here'+endUrl+' <br/><br/>';
		emailBody += '<br/><br/>'+appln.Owner.Name+'<br/>'+appln.Branch_Name__c+'<br/>';
		emailBody +='<br/>'+'<img src="'+logoUrl+'"/>'+'\t <b>Karur Vysya Bank</b>';
		return emailBody;
	}
	
	public static String getFinancialsSubmissionPending(genesis__Applications__c appln){
		String logoUrl = getDocumentLogoUrl();
		String emailBody = 'Dear '+appln.genesis__Account__r.Name+'<br/>  your working capital loan with Karur Vyasa Bank is due for renewal on '+appln.Renewal_Due_Date__c+'<br/>';
					
		for(Facility__c  f : appln.Facilities__r){
			emailBody += '<br/>'+f.Product_Name__c+' ,'+f.Existing_Limit__c;
		}
		emailBody +='<br/>You need to upload your audited financials by clicking here to complete the application <br/><br/>';
		emailBody += '<br/><br/>'+appln.Owner.Name+'<br/>'+appln.Branch_Name__c+'<br/>';
		emailBody +='<br/>'+'<img src="'+logoUrl+'"/>'+'\t <b>Karur Vysya Bank</b>';
		return emailBody;
	}
	
	public static String getFinancialSubmissionPendingIndividuals(genesis__Applications__c appln){
		String logoUrl = getDocumentLogoUrl();
		String emailBody = 'Dear '+appln.genesis__Account__r.Name+'<br/>  your working capital loan with Karur Vyasa Bank is due for renewal on '+appln.Renewal_Due_Date__c+'<br/>';
					
		for(Facility__c  f : appln.Facilities__r){
			emailBody += '<br/>'+f.Product_Name__c+' ,'+f.Existing_Limit__c;
		}
		emailBody +='<br/>You need to upload your income tax returns by clicking here to complete the application<br/><br/>';
		emailBody += '<br/><br/>'+appln.Owner.Name+'<br/>'+appln.Branch_Name__c+'<br/>';
		emailBody +='<br/>'+'<img src="'+logoUrl+'"/>'+'\t <b>Karur Vysya Bank</b>';
		return emailBody;
	}
    public static String getPerfiosError(genesis__Applications__c appln){
		String logoUrl = getDocumentLogoUrl();
		String emailBody = 'Dear '+appln.genesis__Account__r.Name+'<br/>  your working capital loan with Karur Vyasa Bank is due for renewal on '+appln.Renewal_Due_Date__c+'<br/>';
					
		for(Facility__c  f : appln.Facilities__r){
			emailBody += '<br/>'+f.Product_Name__c+' ,'+f.Existing_Limit__c;
		}
		emailBody +='<br/>You are requested to re-upload your audited financials due to the following reason:'+appln.genesis__Account__r.Perfios_error_message__c+'. You are requested to re-upload the audited financials by clicking here'+endUrl+'<br/><br/>';
		emailBody += '<br/><br/>'+appln.Owner.Name+'<br/>'+appln.Branch_Name__c+'<br/>';
		emailBody +='<br/>'+'<img src="'+logoUrl+'"/>'+'\t <b>Karur Vysya Bank</b>';
		return emailBody;
	}
}