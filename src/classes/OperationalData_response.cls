/*
* @name         : OperationalData_response
  @description  : operation data change API response structure
  @author       : Amritesh
*/
public class OperationalData_response{
    public String bpms_error_code;	//00
	public cls_out_msg out_msg;
	public String bpms_error_msg;	//Success
	public class cls_out_msg {
		public cls_Operational_Data[] Operational_Data;
		public Integer TotalSize;	//2
	}
	public class cls_Operational_Data {
		public String Customer_Id;	//9874514
		public cls_CustomerData[] CustomerData;
	}
	public class cls_CustomerData {
		public String FISCAL_YR;	//2015-2016
        public String FDATE;	//2015-2016
        public String TDATE;	//2015-2016
		public cls_Ops_Data[] Ops_Data;
	}
	public class cls_Ops_Data {
		public cls_InwdChqRtns[] InwdChqRtns;
		public cls_AvgLimit[] AvgLimit;
		public cls_TotalOutwdChqRecvd[] TotalOutwdChqRecvd;
		public cls_PromptCloseExceeding[] PromptCloseExceeding;
		public cls_LCBGDevpt[] LCBGDevpt;
		public cls_ForeignBills[] ForeignBills;
		public cls_CashWithdrwalAmtSum[] CashWithdrwalAmtSum;
		public cls_TotalInwdClrCheqs[] TotalInwdClrCheqs;
		public cls_CreditSummation[] CreditSummation;
		public cls_CashDepositCount[] CashDepositCount;
		public cls_CashWithdrawalCount[] CashWithdrawalCount;
		public cls_DebitSummation[] DebitSummation;
		public cls_DebitCount[] DebitCount;
		public cls_InterestEarned[] InterestEarned;
		public cls_ExceedingsIntrPaid[] ExceedingsIntrPaid;
		public cls_Top5CrSumParties[] Top5CrSumParties;
		public cls_OutwdChqRtns[] OutwdChqRtns;
		public cls_HighValChqRtns[] HighValChqRtns;
		public cls_LowValChqRtns[] LowValChqRtns;
		public String CustomerId;	//9874514
		public cls_Exceedings[] Exceedings;
		public cls_DelayServWorkCapInt[] DelayServWorkCapInt;
		public cls_CashDepositAmtSum[] CashDepositAmtSum;
		public cls_ServiceCharges[] ServiceCharges;
		public cls_Top5DrSumParties[] Top5DrSumParties;
		public cls_CreditCount[] CreditCount;
	}
	public class cls_InwdChqRtns {
		public String FISCAL_YR;	//2015-2016
		public String INWD_CHQ_RTN;	//14
		public String V_CUST_CODE;	//9874514
		public String TDATE;	//2016-03-31
		public String FDATE;	//2015-04-01
	}
	public class cls_AvgLimit {
		public String FISCAL_YR;	//2015-2016
		public String AMOUNT;	//-188880252.55
		public String V_CUST_CODE;	//9874514
		public String TDATE;	//2016-03-31
		public String FDATE;	//2015-04-01
	}
	public class cls_TotalOutwdChqRecvd {
		public String FISCAL_YR;	//2015-2016
		public String OUTW_CNT;	//337
		public String V_CUST_CODE;	//9874514
		public String TDATE;	//2016-03-31
		public String FDATE;	//2015-04-01
	}
	public class cls_PromptCloseExceeding {
		public String EXCEED_DAYS_SUM;	//36
		public String FISCAL_YR;	//2015-2016
		public String EXCEED_DAYS_CNT;	//2
		public String V_CUST_CODE;	//9874514
		public String TDATE;	//2016-03-31
		public String FDATE;	//2015-04-01
		public String EXCD_AVERAGE;	//18
	}
	public class cls_LCBGDevpt {
        public String DVBG_CNT;	//18
        public String DVLC_CNT;	//18
	}
	public class cls_ForeignBills {
        public String N_BILLS;	//18
        public String INR_BAL;	//18
        public String MAX_INR;	//18
        public String MIN_INR;	//18
	}
	public class cls_CashWithdrwalAmtSum {
		public String FISCAL_YR;	//2015-2016
		public String V_CUST_CODE;	//9874514
		public String TDATE;	//2016-03-31
		public String DR_CASH_AMT;	//34292500
		public String FDATE;	//2015-04-01
	}
	public class cls_TotalInwdClrCheqs {
		public String FISCAL_YR;	//2015-2016
		public String V_CUST_CODE;	//9874514
		public String INW_CNT;	//626
		public String TDATE;	//2016-03-31
		public String FDATE;	//2015-04-01
	}
	public class cls_CreditSummation {
		public String FIN_CR_AMT;	//809232794.92
		public String FISCAL_YR;	//2015-2016
		public String V_CUST_CODE;	//9874514
		public String TDATE;	//2016-03-31
		public String FDATE;	//2015-04-01
	}
	public class cls_CashDepositCount {
		public String CR_CASH_CNT;	//15
		public String FISCAL_YR;	//2015-2016
		public String V_CUST_CODE;	//9874514
		public String TDATE;	//2016-03-31
		public String FDATE;	//2015-04-01
	}
	public class cls_CashWithdrawalCount {
		public String FISCAL_YR;	//2015-2016
		public String V_CUST_CODE;	//9874514
		public String DR_CASH_CNT;	//98
		public String TDATE;	//2016-03-31
		public String FDATE;	//2015-04-01
	}
	public class cls_DebitSummation {
		public String INT_TR_AMOUNT;	//189438146
		public String FISCAL_YR;	//2015-2016
		public String FIN_DR_AMT;	//794737448.59
		public String V_CUST_CODE;	//9874514
		public String INT_TR_CNT;	//148
		public String TDATE;	//2016-03-31
		public String FDATE;	//2015-04-01
	}
	public class cls_DebitCount {
		public String FIN_DR_CNT;	//2432
		public String FISCAL_YR;	//2015-2016
		public String V_CUST_CODE;	//9874514
		public String TDATE;	//2016-03-31
		public String FDATE;	//2015-04-01
	}
	public class cls_InterestEarned {
		public String INT_REVD_AMT;	//31880986.5
		public String FISCAL_YR;	//2015-2016
		public String V_CUST_CODE;	//9874514
		public String TDATE;	//2016-03-31
		public String FDATE;	//2015-04-01
	}
	public class cls_ExceedingsIntrPaid {
		public String FISCAL_YR;	//2015-2016
		public String V_CUST_CODE;	//9874514
		public String EXCD_INT_DR;	//223246.81
		public String TDATE;	//2016-03-31
		public String FDATE;	//2015-04-01
	}
	public class cls_Top5CrSumParties {
		public String TO_ACC;	//AEGAN INDUSTRIES PRIVATE LIMITED-0920102000003551
		public String FISCAL_YR;	//2015-2016
		public String FROM_ACC;	//SWAMY COTTON MILL TI-1234223000000707
		public String V_CUST_CODE;	//9874514
		public String TYP;	//NEFT CREDIT
		public String CR_AMT;	//106739412
		public String CR_CNT;	//30
		public String TDATE;	//2016-03-31
		public String FDATE;	//2015-04-01
	}
	public class cls_OutwdChqRtns {
		public String FISCAL_YR;	//2015-2016
		public String V_CUST_CODE;	//9874514
		public String OUTWD_CHQ_RTN;	//6
		public String TDATE;	//2016-03-31
		public String FDATE;	//2015-04-01
	}
	public class cls_HighValChqRtns {
		public String MAX_RET_AMT;	//949297
		public String FISCAL_YR;	//2015-2016
		public String V_CUST_CODE;	//9874514
		public String TDATE;	//2016-03-31
		public String FDATE;	//2015-04-01
	}
	public class cls_LowValChqRtns {
		public String MIN_RET_AMT;	//16730
		public String FISCAL_YR;	//2015-2016
		public String V_CUST_CODE;	//9874514
		public String TDATE;	//2016-03-31
		public String FDATE;	//2015-04-01
	}
	public class cls_Exceedings {
		public String FISCAL_YR;	//2015-2016
		public String NO_OF_EXCD;	//16
		public String V_CUST_CODE;	//9874514
		public String TDATE;	//2016-03-31
		public String FDATE;	//2015-04-01
	}
	public class cls_DelayServWorkCapInt {
        public String INT_DR_CNT;	//4079299
	}
	public class cls_CashDepositAmtSum {
		public String CR_CASH_AMT;	//4079299
		public String FISCAL_YR;	//2015-2016
		public String V_CUST_CODE;	//9874514
		public String TDATE;	//2016-03-31
		public String FDATE;	//2015-04-01
	}
	public class cls_ServiceCharges {
		public String FISCAL_YR;	//2015-2016
		public String V_CUST_CODE;	//9874514
		public String TDATE;	//2016-03-31
		public String DR_SERV_CNT;	//296
		public String FDATE;	//2015-04-01
		public String DR_SERV_AMT;	//222510.99
	}
	public class cls_Top5DrSumParties {
		public String TO_ACC;	//PSR ASSOCIATE-1246261006260
		public String DR_CNT;	//74
		public String FISCAL_YR;	//2015-2016
		public String FROM_ACC;	//SWAMY COTTON MILL TI-1234223000000707
		public String V_CUST_CODE;	//9874514
		public String TYP;	//DEBIT
		public String TDATE;	//2016-03-31
		public String DR_AMT;	//86610434
		public String FDATE;	//2015-04-01
	}
	public class cls_CreditCount {
		public String FISCAL_YR;	//2015-2016
		public String FIN_CR_CNT;	//1066
		public String V_CUST_CODE;	//9874514
		public String TDATE;	//2016-03-31
		public String FDATE;	//2015-04-01
	}
	public static OperationalData_response parse(String json){
		return (OperationalData_response) System.JSON.deserialize(json, OperationalData_response.class);
	}
}