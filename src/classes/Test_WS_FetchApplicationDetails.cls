@isTest
public class Test_WS_FetchApplicationDetails {
    @isTest
    public static void method1(){
        genesis__Applications__c app=TestUtility.intialSetUp('HomeLoan',true);
        
        Account acc=new Account(Name='TestName');
        /*   acc.PersonMailingStreet='jadavpur';
acc.PersonMailingCity='kolkata';
acc.PersonMailingState='wb';
acc.PersonMailingCountry='India';
acc.PersonMailingPostalCode='700075';*/
        acc.Current_Address_Proof_Type__c='tavrekere';
        acc.Voter_Id__c='76127618';
        acc.Electricity_Customer_No__c='712878';
        acc.Customer_LPG_No__c='871287';
        acc.Water_Bill_No__c='8928213';
        acc.Driving_License_ID__c='928198';
        //   acc.Passport_Number__pc='8239';
        //    acc.Caste__pc='general';
        //     acc.Marital_Status__pc='unmaried';
        insert acc;
        
        genesis__Application_Parties__c parobj=new genesis__Application_Parties__c(genesis__Application__c=app.id,genesis__Party_Account_Name__c=acc.id);
        insert parobj;
        
        genesis__Application_Document_Category__c docobj=new genesis__Application_Document_Category__c(genesis__Application__c=app.id);
        insert docobj;
        
        genesis__AppDocCatAttachmentJunction__c	 junobj=new genesis__AppDocCatAttachmentJunction__c	(genesis__Application_Document_Category__c=docobj.Id);
        insert junobj;
        GMRA__c gmobj=new GMRA__c(Application__c=app.id);
        insert gmobj;
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/getAlldetailsHL'; 
        req.httpMethod  = 'GET';
        req.params.put('appId',app.id);
        
        RestContext.request = req;
        RestContext.response = res;
        Test.startTest();
        WS_FetchApplicationDetails.getDetails();
        Test.stopTest();
    }
}