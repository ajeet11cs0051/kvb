/**
 * Created by ET-MARLABS on 14-06-2018.
 */

public with sharing class YearDataConstant {
    public static Integer currentYear                                = Utility.getCurrentYear();
    public static String cyString                                    = String.valueOf(currentYear);
    public static String currFiscalYear                              = (currentYear-1)+'-'+Integer.valueOf(cyString.subString(cyString.length()-2,cyString.length()));
    public static String nthFiscalYear                               = (currentYear-2)+'-'+Integer.valueOf(String.valueOf(currentYear-1).subString(String.valueOf(currentYear-1).length()-2,String.valueOf(currentYear-1).length()));
    public static String n_1_FiscalYear                              = (currentYear-3)+'-'+Integer.valueOf(String.valueOf(currentYear-2).subString(String.valueOf(currentYear-1).length()-2,String.valueOf(currentYear-1).length()));
    public static String n_2_FiscalYear                              = (currentYear-4)+'-'+Integer.valueOf(String.valueOf(currentYear-3).subString(String.valueOf(currentYear-1).length()-2,String.valueOf(currentYear-1).length()));
    public static String nextEstFiscalYear                           = currentYear+'-'+Integer.valueOf(String.valueOf(currentYear+1).subString(String.valueOf(currentYear+1).length()-2,String.valueOf(currentYear+1).length()));
    public static String nextProjFiscalYear                          = (currentYear+1)+'-'+Integer.valueOf(String.valueOf(currentYear+2).subString(String.valueOf(currentYear+2).length()-2,String.valueOf(currentYear+2).length()));

}