@isTest
public class Test_WS_UpdatemandatoryFieldsAcc {
    Static genesis__Applications__c app;
    static Account acc;
    static Account acc1;
    //valid data
    @isTest
    public static void getMandateFieldsValid() {
        app = TestUtility.intialSetUp('HomeLoan', true);
        acc = new Account(Name = 'TestName');
        insert acc;
        blob b = blob.valueOf(getdetails());
        TestUtility.webserviceRequest('services/apexrest/updateMandateFields','POST', b);
       // List<WS_UpdatemandatoryFieldsAcc.OUT_MAP> lisobj=new List<WS_UpdatemandatoryFieldsAcc.OUT_MAP>();
       // WS_UpdatemandatoryFieldsAcc.Response res=new  WS_UpdatemandatoryFieldsAcc.Response[]{lisobj};
        Test.startTest();
        WS_UpdatemandatoryFieldsAcc.getMandateFields();
        Test.stopTest();
       
    }
    //Null data
     @isTest
    public static void getMandateFieldsValidNull() {
        app = TestUtility.intialSetUp('HomeLoan', true);
        acc = new Account(Name = 'TestName');
        insert acc;
        TestUtility.webserviceRequest('services/apexrest/updateMandateFields','POST', Null);
        Test.startTest();
        WS_UpdatemandatoryFieldsAcc.getMandateFields();
        Test.stopTest();
       
    }
    
    //Invalid data
    @isTest
    public static void getMandateFieldsInValid() {
        app = TestUtility.intialSetUp('HomeLoan', true);
        acc = new Account(Name = 'TestName');
        insert acc;
        blob b = blob.valueOf(getdetails1());
        TestUtility.webserviceRequest('services/apexrest/updateMandateFields','POST', b);
        Test.startTest();
        WS_UpdatemandatoryFieldsAcc.getMandateFields();
        Test.stopTest();
       
    }
    
    
     public static string getdetails() {
        
String json=
    
    '{ '+

'  "ApplicationID": "'+app.id+'",'+
' "Purpose_of_loan": "Home Loan",'+
'  "Property_type": "TEE",'+
'  "Purchase_cost_of_property": "1233331",'+
'  "Cost_of_land": "123333123",'+
' "Cost_of_construction": "1231233",'+
' "Cost_of_repair": "12312",'+
'  "Bank_of_existing_home_loan": "",'+
' "Loan_start_date_of_existing_home_loan": "",'+
'  "Tenure_of_existing_home_loan": "",'+
' "Outstanding_balance": "213366",'+
'  "Project_code": "HL",'+
'  "Builder_name": "BRIGADE",'+
'  "Project_name": "AAS",'+
'  "Tower": "SA",'+
'  "Flat": "SDs",'+
'  "Address": "ASDFASDFADF",'+
' "Total_repayment_amount": "123099",'+
' "EMI": "3423",'+
'  "Interest": "12",'+
' "Loan_required_amount": "10000000",'+
'  "Required_loan_tenure": "300",'+
' "Holiday_period": "23",'+
'  "GMRA_policy_is_selected": "TRUE",'+
' "Is_GMRA_policy_added_to_loan_amount": "TRUE",'+
'  "Branch_code": "SSF",'+
'  "Branch_name": "ASDF",'+
'  "Is_loan_processing_fee_paid": "TRUE",'+
'  "Documents_uploaded": "FALSE",'+
'  "Sourced_by": "ASDD",'+
'  "Sourced_by_office": "AASEAR",'+
'  "Stage": "LOGIN",'+
' "FETransId": "S",'+
'  "PGTransId": "S",'+
'  "TransDateTime": "",'+
'  "TotalAmnt": "",'+
'  "PgStatus": "",'+
'  "Term" :"",'+
'  "BorrowerEsign" : "",'+
'  "GuarantorEsign" : "",'+
' "BuildingAge" : "",'+
'  "ACCTDETAILS": [ '+
   ' {'+
     ' "AccountId": "0015D000006nygN",'+
     ' "presentAddress1": "ASSD",'+
     ' "presentAddress2": "sdjh",'+
     ' "presentAddress3": "jhdsf",'+
     ' "presentCity": "Bangalore",'+
     ' "presentState": "Karnataka",'+
     ' "presentCountry": "India",'+
     ' "presentZip": "560076",'+
     ' "addProof": "Voter",'+
     ' "voterId": "12331233",'+
     ' "electricityCustNo": "",'+
     ' "CustLpgNo": "",'+
     ' "waterBill": "",'+
     ' "driveLicense": "",'+
     ' "passport": "",'+
     ' "caste": "General",'+
     ' "maritalStatus": "Single",'+
     ' "branchCode": "saaa",'+
     ' "isPhyHandicap": "false",'+
     ' "isExserviceMan": "false",'+
     ' "religion": "Hindu",'+
     ' "eduQualification": "btech",'+
     ' "isAppRelatedBankDir": "false",'+
     ' "nameBankRelated": "axis",'+
     ' "nameDirRelated": "ram",'+
     ' "Are_you_an_existing_customer": "false",'+
     ' "Customer_since": "2017-12-29",'+
     ' "Customer_ID": "",'+
     ' "Aadhaar_No": "",'+
     ' "account_No": "",'+
     ' "PAN_No": "",'+
     ' "Mobile_Number": "9876543210",'+
     ' "E_mail": "aa@dd.com",'+
     ' "Reason_for_not_having_PAN_card": "sdffadsf",'+
     ' "First_Name": "SDADF",'+
     ' "Last_Name": "ASDA",'+
     ' "Middle_Name": "",'+
     ' "DOB": "1988-11-29",'+
     ' "Fathers_Name": "asdasdass",'+
     ' "Husbands_Name": "",'+
     ' "permanentAddress1": "",'+
     ' "permanentAddress2": "",'+
     ' "permanentAddress3": "",'+
     ' "permanentCity": "",'+
     ' "permanentState": "",'+
     ' "permanentZip": "",'+
     ' "permanentCountry": "India",'+
     ' "Nationality": "Indian",'+
     ' "Resdential_status": "",'+
     ' "Do_you_have_any_assets": "",'+
     ' "Immovable_assets": "",'+
     ' "Other_assets": "123321",'+
     ' "Employment_type": "",'+
     ' "Cust_Type": "",'+
     ' "Experience": "11",'+
     ' "Company_name": "ET",'+
     ' "Company_address": "Bang",'+
     ' "Net_monthly_income": "980000",'+
     ' "Mode_of_bank_statement_verification": "",'+
     ' "Mode_of_ITR_verification": "",'+
     ' "Additional_income": "1200",'+
     ' "Source_of_income1": "rent",'+
     ' "Additional_income_amount1": "3455",'+
     ' "Source_of_income2": "share",'+
     ' "Additional_income_amount2": "",'+
     ' "Source_of_income3": "",'+
     ' "Additional_income_amount3": "",'+
     ' "Source_of_income4": "",'+
     ' "decimal Additional_income_amount4": "",'+
     ' "Source_of_income5": "",'+
     ' "Additional_income_amount5": "",'+
	 ' "DisbursmentEsignCheck" : "True",'+
	 ' "ModEsignCheck" : "True",'+
	 ' "CBSAccNo1" : "123214aa",'+
	 ' "CBSAccNo2" : "123214ss",'+
	 ' "CBSAccNo3" : "123214kk",'+
	 ' "CBSAccNo4" : "123214ww",'+
	 ' "CBSAccNo5" : "123214rr",'+
	 ' "IsFinancial" : "True",'+
	 ' "PerfiosReqId1" : "jasgkjasdsddf",'+
	 ' "PerfiosReqId2" : "kdfjvkjsdlfklkdf",'+
	 ' "PerfiosUrl" : "www.perfios.com/jhj/hjggj"'+
   '} '+
 '] '+
 
     ' } '   ;

        return json;
    }



// invalid json
  public static string getdetails1() {
        
String json=
    
    '{ '+

'  "ApplicationID": "'+app.id+'",'+
' "Purpose_of_loan": "Home Loan",'+
'  "Property_type": "TEE",'+
'  "Purchase_cost_of_property": "1233331",'+
'  "Cost_of_land": "123333123",'+
' "Cost_of_construction": "1231233",'+
' "Cost_of_repair": "12312",'+
'  "Bank_of_existing_home_loan": "",'+
' "Loan_start_date_of_existing_home_loan": "",'+
'  "Tenure_of_existing_home_loan": "",'+
' "Outstanding_balance": "213366",'+
'  "Project_code": "HL",'+
'  "Builder_name": "BRIGADE",'+
'  "Project_name": "AAS",'+
'  "Tower": "SA",'+
'  "Flat": "SDs",'+
'  "Address": "ASDFASDFADF",'+
' "Total_repayment_amount": "123099",'+
' "EMI": "3423",'+
'  "Interest": "12",'+
' "Loan_required_amount": "10000000",'+
'  "Required_loan_tenure": "300",'+
' "Holiday_period": "23",'+
'  "GMRA_policy_is_selected": "TRUE",'+
' "Is_GMRA_policy_added_to_loan_amount": "TRUE",'+
'  "Branch_code": "SSF",'+
'  "Branch_name": "ASDF",'+
'  "Is_loan_processing_fee_paid": "TRUE",'+
'  "Documents_uploaded": "FALSE",'+
'  "Sourced_by": "ASDD",'+
'  "Sourced_by_office": "AASEAR",'+
'  "Stage": "LOGIN",'+
' "FETransId": "S",'+
'  "PGTransId": "S",'+
'  "TransDateTime": "",'+
'  "TotalAmnt": "",'+
'  "PgStatus": "",'+
'  "Term" :"",'+
'  "BorrowerEsign" : "",'+
'  "GuarantorEsign" : "",'+
' "BuildingAge" : "",'+
'  "ACCTDETAILS": [ '+
   ' {'+
     ' "AccountId": "0015D000006nygN",'+
     ' "presentAddress1": "ASSD",'+
     ' "presentAddress2": "sdjh",'+
     ' "presentAddress3": "jhdsf",'+
     ' "presentCity": "Bangalore",'+
     ' "presentState": "Karnataka",'+
     ' "presentCountry": "India",'+
     ' "presentZip": "560076",'+
     ' "addProof": "Voter",'+
     ' "voterId": "12331233",'+
     ' "electricityCustNo": "",'+
     ' "CustLpgNo": "",'+
     ' "waterBill": "",'+
     ' "driveLicense": "",'+
     ' "passport": "",'+
     ' "caste": "General",'+
     ' "maritalStatus": "Single",'+
     ' "branchCode": "saaa",'+
     ' "isPhyHandicap": "false",'+
     ' "isExserviceMan": "false",'+
     ' "religion": "Hindu",'+
     ' "eduQualification": "btech",'+
     ' "isAppRelatedBankDir": "false",'+
     ' "nameBankRelated": "axis",'+
     ' "nameDirRelated": "ram",'+
     ' "Are_you_an_existing_customer": "false",'+
     ' "Customer_since": "2017-12-29",'+
     ' "Customer_ID": "",'+
     ' "Aadhaar_No": "",'+
     ' "account_No": "",'+
     ' "PAN_No": "",'+
     ' "Mobile_Number": "9876543210",'+
     ' "E_mail": "aa@dd.com",'+
     ' "Reason_for_not_having_PAN_card": "sdffadsf",'+
     ' "First_Name": "SDADF",'+
     ' "Last_Name": "ASDA",'+
     ' "Middle_Name": "",'+
     ' "DOB": "1988-11-29",'+
     ' "Fathers_Name": "asdasdass",'+
     ' "Husbands_Name": "",'+
     ' "permanentAddress1": "",'+
     ' "permanentAddress2": "",'+
     ' "permanentAddress3": "",'+
     ' "permanentCity": "",'+
     ' "permanentState": "",'+
     ' "permanentZip": "",'+
     ' "permanentCountry": "India",'+
     ' "Nationality": "Indian",'+
     ' "Resdential_status": "",'+
     ' "Do_you_have_any_assets": "",'+
     ' "Immovable_assets": "",'+
     ' "Other_assets": "123321",'+
     ' "Employment_type": "",'+
     ' "Cust_Type": "",'+
     ' "Experience": "11",'+
     ' "Company_name": "ET",'+
     ' "Company_address": "Bang",'+
     ' "Net_monthly_income": "980000",'+
     ' "Mode_of_bank_statement_verification": "",'+
     ' "Mode_of_ITR_verification": "",'+
     ' "Additional_income": "1200",'+
     ' "Source_of_income1": "rent",'+
     ' "Additional_income_amount1": "3455",'+
     ' "Source_of_income2": "share",'+
     ' "Additional_income_amount2": "",'+
     ' "Source_of_income3": "",'+
     ' "Additional_income_amount3": "",'+
     ' "Source_of_income4": "",'+
     ' "decimal Additional_income_amount4": "",'+
     ' "Source_of_income5": "",'+
     ' "Additional_income_amount5": "",'+
	 ' "DisbursmentEsignCheck" : "True",'+
	 ' "ModEsignCheck" : "True",'+
	 ' "CBSAccNo1" : "123214aa",'+
	 ' "CBSAccNo2" : "123214ss",'+
	 ' "CBSAccNo3" : "123214kk",'+
	 ' "CBSAccNo4" : "123214ww",'+
	 ' "CBSAccNo5" : "123214rr",'+
	 ' "IsFinancial" : "True",'+
	 ' "PerfiosReqId1" : "jasgkjasdsddf",'+
	 ' "PerfiosReqId2" : "kdfjvkjsdlfklkdf",'+
	 ' "PerfiosUrl" : "www.perfios.com/jhj/hjggj"'+
   '} '+
 '], '+
 
     ' } '   ;

        return json;
    }

}