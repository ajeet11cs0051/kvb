public class ApplicationService {
    
    public static genesis__Applications__c createApplication(id AccId,String RTypeName,string SourcedBy,string sourcedbyOffice){
        genesis__Applications__c  App = new genesis__Applications__c();
        app = createApplication2(AccId,RTypeName,SourcedBy,sourcedbyOffice,null,null,null);
        return app;
    }
    
    public static genesis__Applications__c createApplication2(id AccId,String RTypeName,string SourcedBy,string sourcedbyOffice, String BranchCode, String BranchName, String LoanType){
        // string Appname;
        Id AppRecordTypeId = Schema.SObjectType.genesis__Applications__c.getRecordTypeInfosByName().get(RTypeName).getRecordTypeId();
        genesis__Applications__c  App = new genesis__Applications__c();
        
        App.genesis__Account__c=AccId;
        app.RecordTypeId=AppRecordTypeId;
        app.Sourced_By__c=SourcedBy;
        app.Sourced_By_Office__c=sourcedbyOffice;
        if(LoanType == null){
			app.genesis__CL_Product__c = getProduct(KVB_Company_Details__c.getOrgDefaults().HomeLoanName__c).id;
        }else if(LoanType == 'LAP'){
            app.genesis__CL_Product__c = getProduct(KVB_Company_Details__c.getOrgDefaults().LAP_NAME__c).id;
        }else if(LoanType == 'PL'){
            app.genesis__CL_Product__c = getProduct(KVB_Company_Details__c.getOrgDefaults().PL_NAME__c).id;
        }          
        app.Branch_Code__c = BranchCode;
        app.Branch_Name__c = BranchName;
        String Owner = BranchCode != null ? AssignBranch.getUser(BranchCode) : null;
        if(!String.isBlank(Owner )){
            app.OwnerId = Owner;
        }   
        // app.genesis__CL_Product__c=getProduct(Constants.HomeLoanName).id;
        insert App;
        // Appname=App.name;
        return App;   
    }
    public static genesis__Applications__c getApplication(string AccountID){
        
        return [select id,Name,genesis__Account__c from genesis__Applications__c where ID =:AccountID order by Createddate desc Limit 1];
        
    }
    public static clcommon__CL_Product__c getProduct(String name){
        
        return   getProducts(new list<String>{name}).size()>0 ?getProducts(new list<String>{name})[0]:null;
        
        
    }
    
    public static list<clcommon__CL_Product__c> getProducts(List<String> prodNames){
        return [select id,Name,clcommon__Product_Name__c,Product_Code__c from clcommon__CL_Product__c where clcommon__Product_Name__c IN:prodNames];
    }
    
}