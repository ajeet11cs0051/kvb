@isTest
public class Batch_ToCalculateGroupExposure_Test {
    public static testmethod void testMethod1() {
        List<Account> accList=new List<Account>();
        Account acc=new Account();
        acc.Name='TestAccount';
        Account acc1=new Account();
        acc1.Name='TestAccount1';
        Account acc2=new Account();
        acc2.Name='TestAccount2';
        Account acc3=new Account();
        acc3.Name='TestAccount3';
         Account acc4=new Account();
        acc4.Name='TestAccount4';
        accList.add(acc4);
        accList.add(acc);
        accList.add(acc1);
        accList.add(acc2);
        accList.add(acc3);
        insert accList;
        Group_Concern__c grCon=new Group_Concern__c();
        grCon.Group_1__c=acc.id;
        grCon.Group_2__c=acc1.id;
        insert grCon;
        Group_Concern__c grCon1=new Group_Concern__c();
        grCon1.Group_1__c=acc.id;
        grCon1.Group_2__c=acc3.id;
        insert grCon1;
        genesis__Application_Parties__c party=new genesis__Application_Parties__c();
        party.genesis__Party_Account_Name__c=acc2.id;
        party.Key_Contact__c=acc.id;
        insert party;
        genesis__Application_Parties__c party1=new genesis__Application_Parties__c();
        party1.genesis__Party_Account_Name__c=acc4.id;
        party1.Key_Contact__c=acc.id;
        insert party1;
        
        genesis__Applications__c app=new genesis__Applications__c();
        app.genesis__Account__c=acc.id;
        insert app;
        genesis__Applications__c app1=new genesis__Applications__c();
        app1.genesis__Account__c=acc2.id;
        //List<RecordType> rc=[SELECT Id FROM RecordType WHERE RecordType.DeveloperName = 'Term_Loan' limit 1];
         Id SORecordTypeId = Schema.SObjectType.genesis__Applications__c.getRecordTypeInfosByName().get('Term Loan').getRecordTypeId();
        app1.RecordTypeID =SORecordTypeId;
        insert app1;
        system.debug(app1);
        Facility__c fac =new Facility__c();
        fac.Application__c=app.id;
        fac.Existing_Limit__c=2344;
        insert fac;
        Test.startTest();
        database.executeBatch(new Batch_ToCalculateGroupExposure());
        Test.stopTest();
        
    }
}