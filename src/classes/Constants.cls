/* 
* Name      : Constansts
* Purpose   : This class is used for all constant values.
* Company   : ET Marlabs
* Author    : Amritesh
*/
public class Constants {
    
    // Generic
    public static final string WS_SUCCESS_STATUS                            = 'Success';
    public static final string WS_SUCCESS_CODE                              = '200';
    public static final string WS_NO_ERROR                                  = 'NO_ERROR';
    public static final string WS_ERROR_STATUS                              = 'Error';
    public static final string WS_ERROR_CODE                                = '400';
    public static final string WS_REQ_BODY_IS_NULL                          = 'REQUEST_BODY_IS_NULL';
    public static final string WS_UNABLE_TO_PARSE_REQ                       = 'UNABLE_PARSE_REQ_PARAMS';
    public static final string WS_NO_RECORD_FOUND                           = 'No Records Found';
    public static final string SME_APP_RECORD_TYPE                          = 'SME_Renewal';
    public static final string SME_APP_RECORD_TYPE_EXCEEDING                = 'SME_Exceeding';
    public static final string SME_APP_RECORD_TYPE_ADHOC                    = 'SME_AdHoc';
    public static final string SME_APP_RECORD_TYPE_ENHANCEMENT              = 'SME_Enhancement';
    public static final string SME_RECORD_TYPE_LABEL                        = 'SME Renewal';
    public static final string SME_RECORD_TYPE_LABEL_EXE                    = 'SME Exceeding';
    public static final string SME_RECORD_TYPE_LABEL_ADHOC                  = 'SME AdHoc';
    public static final string SME_RECORD_TYPE_LABEL_ENHANCEMENT            = 'SME Enhancement';
    public static final String SME_NEW_LOAN_RECORD_TYPE                     = 'SME_NEW_Loan';
    public static final String SME_NEW_LOAN_RECORD_TYPE_LABEL               = 'SME NEW Loan';
    public static final string SME_FACILITY                                 = 'Facility__c';


    public static final string SME_APP_STAGE_ENHANCEMENT_FINAL_SANC         = 'Enhancement- Final sanction';
    public static final string SME_APP_STAGE_ADHOC_FINAL_SANC               = 'Adhoc-Final sanction';
    public static final string SME_APP_STAGE_EXE_FINAL_SANC                 = 'Exceeding-Final sanction';

    public static final string HL_APP_RECORD_TYPE                           = 'Home_Loan';
    public static final string BUSINESS_ACCOUNT                             = 'Business_Account';
    public static final string PERSON_ACCOUNT                               = 'Person_Account';
    public static final string PERSON_ACCOUNT_DEVNAME                       = 'PersonAccount';
    public static final String PERFIOS_EXISTING_DATA_MESSAGE                = 'Existing perfios data found in LOS for the customer against received fiscal year';
    public static final String CLASS_OF_ACTIVITY                            =   'ALL OTHER LOANS NOT CLASSIFIED ELSEWHERE OR ACTIVITIES NOT ADEQUATELY DESCRIBED (THIS CODE SHOULD BE USED SPARINGLY)';
    public static final String APPLICANT_TYPE_COMPANY                       = 'Company';
    public static final String APPLICANT_TYPE_DIRECTOR                      = 'Director';
    public static final String HUF                                          = 'HUF';
    public static final String HINDU_UNDIVIDED_FAMILY                       = 'HINDU UNDIVIDED FAMILY';
    public static final String LLP                                          = 'LLP';
    public static final String COMPANIES                                    = 'Companies';
    public static final String NOT_CLASSIFIED                               = 'NOT CLASSIFIED';
    public static final String REGISTERED_OFFICE                            = 'REGISTERED OFFICE';
    public static final String RENEWAL_ENHANCEMENT                          = 'RENEWAL/ENHANCEMENT';
    public static final String CONSUMER_CIR_TRUE                            = 'true';
    public static final String CONSUMER_CIR_FALSE                           = 'false';
    public static final String COMPLETED                                    = 'Completed';
    
    public static final string SCORECARD_CREATION_SUCCESS_STATUS    = 'Successfully generated scorecard';
    // Application Stage
    public static final string Enhancement_App_Submitted        = 'Enhancement- Application submitted';
    public static final string Exceeding_App_Submitted          = 'Exceeding- Application submitted';
    public static final string Adhoc_App_Submitted              = 'Adhoc- Application submitted';
    public static final string INSUFFICIENTCOLLETRALCOVERAGE    = 'Application Sub Stage has been changed to Insufficient Collateral Stage';
    public static final string APPL_STAGE_LOAN_SANCTIONED_NON_STOP = 'Loan Sanctioned Non-STP';
    public static final string APPL_STAGE_LOAN_ACCOUNT_OPENED   = 'Loan Account opened';
    //Loan Types
    public static final string HOMELOAN = 'Home Loan';
    public static final string LAPLOAN = 'LAP';
    public static final string PERSONALLOAN = 'Personal Loan';
    public static final string SMElOAN = 'SME';
    public static final string HomeLoanName = 'Smart Home Loan - 797';  //Happy Home Loan - 753
    
    // Specific to SME
    public static final string SME_PRODUCT_CODE                    = 'WC';
    public static final string PRIMARY_SECURITIES                  = 'Primary Securities';
    public static final string COLLATERAL_SECURITIES               = 'Collateral Securities';
    public static final string SANCTION_TEMPLATE_SME               = 'SME_SanctionLetter';
    public static final string PROV_SANCTION_TEMPLATE_SME          = 'SME_ProvisionalSanctionLetter';
    public static final string SME_DOC_CATEGORY                    = 'SME Financial Document';
    public static final string EA_DOC_CATEGORY                     = 'SME EME Document';
    public static final string SITE_BASE_URL                       = System.Label.Site_doc_URL;
    public static final string SME_SANCTION_Letter123              = 'SME_SanctionLetter';
    public static final string SME_SANCTION_Letter12               = 'SME_Sanction_Letter12';
    public static final string SME_SANCTION_Letter1                = 'SME_Sanction_Letter1';
    public static final string SME_SANCTION_Letter13               = 'SME_Sanction_Letter13';
    public static final string M68C_BALANCE_SHEET_TEST_ID          = 'a7N5D0000008PGD';
    public static final string EMAIL_REASON                        = 'EMAIL_IDENTIFIER';
    public static final string MOB_REASON                          = 'Loan Sanctioned ';
    public static final string SME_YES                             = 'Yes';
    public static final string APP_STAGE_PERFIOS_ERROR             = 'Perfios error';
    public static final string APP_STAGE_CLOSE                     = 'Limit renewed/Application close';
    public static final string APP_STAGE_WB_CUST                   = 'Withdrawn by customer';
    public static final string CONSTITUTION_DOCUMENT_CATEGORY      = 'Constitution Document Category';
    public static final string DOC_NAME_SME                        = 'SME_SanctionLetter';
    public static final string FINANCIAL_DONE                      = 'Financials done';
    public static final string RLPC_FINANCIAL_TASK                 = 'Review financials uploaded by customer';
    public static final string COMMITTEE_EXCLUDE_DESIGNATION       = 'DOCC:BR_MGR_SMALL:BR_MGR_MEDIUM:BR_MGR_LARGE:BR_MGR_VERYLARGE:BR_MGR_XPLARGE';
    public static final String TASK_TEMPLATE_MANAGER               = 'Task_Notification';
    public static final String PROPERTY_LIST_INSERT                = 'Property list insert';
    public static final String PROPERTY_LIST_UPDATE                = 'Property list update';
    public static final String FACILITY_LIST_UPDATE                = 'Facility list update';
    public static final String LIST_OF_LIMIT_ASSESSMENT            = 'List of limit Assessment';
    public static final String LIST_OF_LIMIT_ASSESSMENT_C          = 'List of limit Assessment c';
    public static final String LIST_OF_ACCOUNT                     = 'List of Account';
    public static final String LIST_OF_DEBTORS_INSERT              = 'List of Debtors insert';
    public static final String LIST_OF_DEBTORS_UPDATE              = 'List of Debtors update';
    public static final String LIST_OF_CREDITORS_INSERT            = 'List of creditors insert';
    public static final String LIST_OF_CREDITORS_UPDATE            = 'List of creditors update';
    public static final String CUSTOMER_RESPONDED_TASK_SUBJECT     = 'Customer Responded to Query';
    public static final String COLLATERAL_RECTYPE_OTHER            = 'Other';
    public static final String COLLATERAL_TYPE_FIXED_DEPOSIT       = 'Fixed Deposit';
    public static final String COLLATERAL_TYPE_NSC_KVP             = 'NSC/KVP';
    public static final String COLLATERAL_TYPE_INSURANCE_POLICIES  = 'Insurance Policies';
    public static final String COLLATERAL_TYPE_VEHICLES            = 'Vehicles';
    public static final String COLLATERAL_TYPE_MACHINERY           = 'Machinery';
    
    //Specific to PL
    public static final string PL_PRODUCT_CODE              = 'PL910';
    public static final string InvalidCIBIL                 = 'Invalid CIBIL';
    public static final string CIBILRejectedMsg             = 'Customer does not have a valid CIBIL score, loan cannot be processed';
    public static final string LowCIBILScoreMsg             = 'Customer CIBIL score is less than 750, loan cannot be processed';
    public static final string APP_REJECTED_STAGE           = 'Application Rejected';
    public static final string EXPERIAN                     = 'experian';
    public static final string PL_LOAN_STP                  =  'Loan Sanctioned STP';
    public static final string PL_DISBURSMNTFAIL_SEQ_NO     = '20';
    public static final string SUBSTAGE_DISBURSEMENTESIGN   = 'Disbursement Document Esign';
    public static final string PL_SI_KVB                    = 'Karur Vysya Bank';
    //FI Status falgs
    public static final string PL_FIREQ_INIT                = 'Request yet to be initiated';
    public static final string PL_FIRES_OBTN                = 'Response to be obtained'; 
    public static final string PL_FIRES_POSI                = 'Positive';
    public static final string PL_FIRES_FRUD                = 'Fraud';
     // Document generation PL
    public static final string PRE_APPROVE_SANCTION_PL      = 'PL Sanction Letter';
    public static final string PL_DOC_DEVELOPERNAME         = 'PL_Sanction_Letter';
    public static final string PL1_DOCGEN                   = 'PL1 Doc Generation';
    public static final string PL1_CLASS2                   = 'PL1 Class 2 Sign';
    public static final string PL1_DOCID                    = 'PL1 Doc ID';
    public static final string PL_AGREEMENT_DOC             = 'PL Loan Agreement';
    public static final string PL2_DOCGEN                   = 'PL2 Doc Generation';
    public static final string PL2_CLASS2                   = 'PL2 Class 2 Sign';
    public static final string PL2_DOCID                    = 'PL2 Doc ID';
    
    //Astute API tags
    public static final string BORROWER_TYPE                 ='Borrower';
    public static final string ACTIVITY_DESCO                ='Office';
    public static final string ACTIVITY_DESCR                ='Residence';
    public static final string EMPTYP_SALARIED               ='Salaried';
    public static final string EMPTYP_SELFEMPP               ='Self-Employed Professional';
    public static final string EMPTYP_SELFEMPB               ='Self-Employed Businessman';
    public static final string EMPTYP_AGRICULTUR             ='Agriculturist';

    // Specific to HL
    public static final string HL_PRODUCT_CODE              = 'HL001';
    public static final string EmpType_Salaried             = 'Salaried';
    public static final string EmpType_Agricul             = 'Agriculturist';
    public static final string EmpType_SelfEmp              = 'Self Employed';
    public static final string MCLR_OneYear                 = 'One Year';
    public static final string MCLR_ThreeMonths             = 'Three Months';
    public static final string HL_Value                     = 'HL';
    public static final string PersonalDetailsCapt          ='Personal Details Captured';
    public static final string Sub_Stage_PropInfoCaptured   = 'Property Information Captured';
    public static final string Sub_Stage_WorkInfoCaptured   = 'Work Information Captured';
    public static final string Sub_Stage_DisbuApprov        = 'Disbursement Approved';
    public static final string Co_Borrower                  ='Co-Borrower';
    public static final string Gurantor                     ='Guarantor';
    public static final string IDVCall                      ='IDV';
    public static final string CIBIL                        ='CIBIL';
    public static final string PRE_Approval_Sanction_HL     = 'HL_Preapproval_Letter';
    public static final string Execution_Certificate_C11_HL = 'C11';
    public static final string Acknowledgement_For_Sanction = 'C1';
    public static final String APP_ATTACHMENT_COBORROWER    = 'Execution_Certificate_C11_HL.pdf:Acknowledgement_For_Sanction.pdf';
    public static final String APP_ATTACHMENT_GUARANTOR     = 'Execution_Certificate_C11_HL.pdf:Agreement of Guarantee.pdf:Acknowledgement_For_Sanction.pdf';
    public static final string DOCKET_UPLOAD_REASON_HL      = 'HL_DisbursmentLetter';
    public static final string ParentDOCCatgName            = 'HL Documents';
     public static final string ParentDOCCatgNamePL          = 'PL Documents';
    public static final string ParentDOCCatgNameLAP         = 'LAP Documents';
    public static final string DOC_NAME_HL_BORROWER         = 'A23:B17';
    public static final string DOC_NAME_HL_BORR_PH          = 'B1:B2';
    public static final string DOC_NAME_HL_GUARANTER        = 'A46';
    public static final string DOC_NAME_HL_COMMON           = 'C1:C11';
    public static final Integer TITLE_HOOLDER_LIMIT         = 13;
    // E-stamp document name
    public static final string A23_HL                       = 'A23';
    public static final string A46_HL                       = 'A46';
    public static final string B1_HL                        = 'B1';
    public static final string B2_HL                        = 'B2';
    public static final string B17_HL                       = 'B17';    
    
    // Loan Purpose
    
    public static final string Loanpurpose                  = 'Property not Identified';
    public static final string Construction             = 'Construction on Own Land';
    public static final string IdentifiedProperty           = 'Purchase of Identified Property';
    public static final string RepairHouse                  = 'Repair or Renovation of House';
    public static final string ConstructionOn               = 'Purchase of land and construction there on';
    
    // Cibil & IDV
    public static final string IDVRejeted                   = 'IDV Rejected';
    public static final string CIBILRejected                = 'CIBIL Rejected';
    public static final string LowCIBILScore                 = 'Low CIBIL Score';
    public static final string CIBILRejectMessage             = 'There are no financial applicants or co-applicant attached to this application with CIBIL score > -1';
    
   // HL DOc Generation Checks
    public static final string PreApproveGenerated                 ='Pre-Approval Doc generation';
    public static final string PreApproveClass2                    ='Pre-Approval Class2';
    public static final string C1docGen                            ='C1 Doc Generation';
    public static final string C1class2Sign                        ='C1 Class 2 Sign';
    public static final string C1DocID                             ='C1 Doc ID';
    public static final string C11DocGen                           ='C11 Doc Generation';
    public static final string C11DocID                            ='C11 Doc ID';
    public static final string A23DocGen                           ='A23 Doc Generation';
    public static final string A23Class2                           ='A23 Class 2 Sign';
    public static final string A23DocID                            ='A23 Doc ID';
    public static final string B17DocGen                           ='B17 Doc Generation';
    public static final string B17DocID                            ='B17 Doc ID';
    public static final string A46DocGene                          ='A46 Doc Generation';
    public static final string A46Class2sign                       ='A46 Class 2 Sign';
    public static final string A46DocID                            ='A46 Doc ID';
    public static final string B1DOcGene                           ='B1 Doc Generation';
    public static final string B1DOcID                             ='B1 Doc ID';
    public static final string B2DocGene                           ='B2 Doc Generation';
    public static final string B2DocID                             ='B2 Doc ID';
    
    // HL Amount Values
    public static final Decimal OneCrore                           = 10000000;
    public static final Decimal NintyThreeLakhs                    = 9375000;
    public static final Decimal SeventyFiveLakhs                   = 7500000;
    public static final Decimal SevenFivePer                       = 0.75;
    public static final Decimal EightyPer                          = 0.8;
    public static final Decimal Seventy                            = 0.7;
    public static final Decimal one                                = 1;
    public static final Decimal Fifteen                            = 15;
    // HL Document Categorys
    public static final string CustPhoto='Photo';
    public static final string CustAddresProof='Current_Add_Proof';
    public static final string PropVerifiReport='Property_verification_Docs';
    public static final string LegalOpinion='Legal_Opinion';
    public static final string ValuReport='Valuation_report';
    public static final string FIReport='FI_Report';
    public static final string IncomeDocuments='Income_Docs';
    public static final string MainIncome='Main_Income';
    public static final string ITR='ITR';
    public static final string AgriIncomeProof='Agri_Income';
    public static final string ShareProfIncoProof='Share_of_profit_income';
    public static final string RentalIncomeProof='Rental_income';
    public static final string PensIncoemProof='Pension_income';
    public static final string Others='Others';
    public static final string PropertyDocuments='Property_Docs';
    public static final string LoanDocuments='Loan_Docs';
    public static final string UploadEC='UploadEC';
    public static final string Uploadcomfortletter = 'Upload_comfort_letter'; 
    public static final string UploadInsurance = 'Upload_Property_Insurance'; //to upload property insurance related document (By Ashish Jain)
    public static final string CreditInsurancePolicy = 'Credit Insurance Policy';
     public static final string FormAS = '26AS'; 
    public static final string BankStatement = 'Bank Statement'; 
  
    //DIGIO Template Names
    public static final string Test_Template          = 'Test_HL';
    
    // HL_IDV CIbil
    public static final string HL_CibilRecordType   ='HL_TU';
    
    // HL_Perfios Key
    public static final string E_Amount ='E_AMOUNT_BALANCE_MISMATCH';
    public static final string E_Api    ='E_API_SESSION_EXPIRED';
    public static final string E_NAccept='E_NOT_ACCEPTED';
    public static final string E_N_Accnt='E_NO_ACCOUNT';
    public static final string E_NErr   ='E_NO_ERROR';
    public static final string E_othe   ='E_OTHER';
    public static final string E_SITE   ='E_SITE';
    public static final string E_sys    ='E_SYSTEM';
    public static final string E_UAct   ='E_USER_ACTION';
    public static final string E_usBloc ='E_USER_BLOCKED';
    public static final string E_usCanc ='E_USER_CANCELLED';
    public static final string E_uSExpir='E_USER_SESSION_EXPIRED';
    public static final string E_wCredent='E_WRONG_CREDENTIALS';
    
    // HL perfios values
    public static final string IBSPtry='Incorrect Bank Statements. Please try again.';
    public static final string Pta='Please try again.';
    public static final string IBAPtanothAcc='Invalid bank account. Please try another account or another mode of input.';
    public static final string NoErr='No errors';
    public static final string ILCPta24='Incorrect login credentials. Please try again after 24 hours or use upload option.';
    
    public static final string PFITR='ITR';
    
    //DIGIO Document Names
    public static final string Test_Document          = 'Test_HL';
    
    // Underwriting Dashboard Constants
    public static final string SANCTION_AUTHORITY_AUDIT_RTYPE   = 'Sanction_Authority';
    public static final string SANCTION_AUTHORITY_LABEL         = 'Sanction Authority';
    public static final string UNDERWRITING_T_N_C               = 'Underwriting T&C';
    public static final string UNDERWRITING_PRE_T_N_C           = 'Pre-Disbursement Terms and Condition';
    public static final string CU_CHILD_APP_SCORE_RECORD_TYPE   = 'Child_Application_Score';
    public static final string CU_CHILD_FLAG_RECORD_TYPE        = 'Child_Flag';
    public static final string SYSTEM_ADMIN_PROFILE             = 'System Administrator';
    public static final string APPLICATION_REVIEW_STAGE         = 'Application review';
    public static final string APP_DEVIATION_PENDING_STAGE      = 'Application deviation pending';
    public static final string APP_FILLING_FINAL_STAGE          = 'Application Filling - Final';
    public static final string APP_REVIEW_FINAL_STAGE           = 'Application Review - Final Authority';
    public static final string INTERIM_EXTENSION_LABEL          = 'For Interim Extension';
    public static final string RENEWAL_LABEL                    = 'For Renewal';
    public static final string BRE_DONE_FINANCIALS              = 'BRE rule engine completed-with financials';
    public static final string APP_REVIEW_FINANCIAL_STAGE       = 'Application review -with financials';
    public static final string APP_DEVIATION_FINANCIAL_STAGE    = 'Application deviation pending-with financials';
    public static final string RECOMMENDED_CHANGE_TYPE_HISTORY  = 'Recommendation Change';
    public static final string FACILITY_CHILD_RECORD_TYPE       = 'Child';
    public static final string FACILITY_PARENT_RECORD_TYPE      = 'Parent';
    public static final string NO_STRING                        = 'No';
    public static final string SOD_CATEGORY                     = 'SOD';
    public static final string OCC_CATEGORY                     = 'OCC';
    public static final string MANUFACTURING_INDUSTRY           = 'Manufacturing';
    public static final string TRADING_INDUSTRY                 = 'Trading';
    public static final string APPROVE_ACTION                   = 'Approve';
    public static final string RECOMMEND_TO_HIGHER_ACTION       = 'Recommend to higher authority';
    public static final string INTERIM_SANCTION_STAGE           = 'Interim Sanction';
    public static final string FINAL_SANCTION_STAGE             = 'Final Sanction';
    public static final string SME_RENEWAL_APPROVED             = 'Working Capital Renewal Approved.';
    public static final string SUCCESS_STATUS                   = 'Success';
    public static final string MARK_EXIT_ACTION                 = 'Mark as exit';
    public static final string REJECT_ACTION                    = 'Reject';
    public static final string EXIT_STAGE                       = 'Marked as exit';
    public static final string SME_RENEWAL_REJECTED             = 'Working Capital Renewal Rejected.';
    public static final string AUTHORITY_REVIEW_SENT            = 'Application sent for higher authority review.';
    public static final string DEVIATION_MESSAGE                = 'Deviation identified with respect to your current change.Notifed to higher authority.';
    public static final string DEVIATION_STATUS                 = 'Deviation';
    public static final string COMPLETED_STATUS                 = 'Completed';
    public static final string ERROR_STATUS                     = 'Error';
    public static final string EXCEPTION_MESSAGE                = 'There is some error while saving your changes. Please try later.';
    public static final string RECORD_SAVED_MSG                 = 'Input saved successfully.Application moved to next authority.';
    public static final string ERROR_MESSAGE                    = 'Oops! Somthing went wrong.Please try again.';
    public static final string PENDING_APP_SUBJECT              = 'Application Approval Pending: Deviation';
    public static final string APPROVE_ALERT_MESSAGE            = 'The deviation has been approved.';
    public static final string REJECTALERT_MESSAGE              = 'The deviation has been rejected.';
    public static final string SANCTION_SUCC_MESSAGE            = 'Approved.';
    public static final string SANCTION_REJ_MESSAGE             = 'Rejected.';
    public static final string COCC_ROLE                        = 'COCC';
    public static final string APPLN_INITIATED                  = 'Identified for renewal';
    public static final string APPLN_MORE_THAN_6_MONTH          = '>6 months due for renewal';
    public static final string TL_INITIATED                     = 'TL Open Applications';
    // Deviation Rule Name
    public static final string ROI_SCHEMATIC_RULE               = 'ROI';
    public static final string ROI_NON_SCHEMATIC_RULE           = 'ROI-Non Schematic';
    public static final string COMISSION_BG_RULE                = 'COMISSION BG';
    public static final string PROCESSING_CHARGE_SCHEMATIC      = 'PROCESSING CHARGE SCHEMATIC';
    public static final string PROCESSING_CHARGE_NON_SCHEMATIC  = 'PROCESSING CHARGE NON SCHEMATIC';
    public static final string MARGIN_SOD                       = 'Margin for SOD';
    public static final string MARGIN_STOCK_MANUFACTURING       = 'Margin Stock Manufacturing';
    public static final string MARGIN_STOCK_TRADING             = 'Margin Stock Trading';
    public static final string MARGIN_RECIEVABLES               = 'Margin Recievables';
    public static final string PROV_EXTENTION                   = 'Provisional Extension';
    public static final string FINAL_REVIEW                     = 'Renewed';
    public static final string LETTER_VALID_DATE                = 'one year';
    public static final string APP_FINAL_DEVIATION              = 'Application Review - Deviation Pending';
    public static final string UNDERWRITING_T_C_RTYPE           = 'Underwriting_T_C';
    public static final string PREDISBURSEMENT_T_C_RTYPE        = 'Pre_Disbursement_T_C';
    public static final string PRESIDENT_COO_API                = 'President_COO';
    public static final string PRESIDENT_COO_NAME               = 'President & COO';
    public static final string APPROVED_STATUS                  = 'Approved';
    public static final string REJECTED_STATUS                  = 'Rejected';
    
    //genesis__Aplication__c related record type constant
    public static final string SME_RENEWAL = 'SME_Renewal';
    public static final string SME_ADHOC = 'SME AdHoc';
    public static final string SME_ENHANCEMENT = 'SME Enhancement';
    public static final String LAND_BUILDING_OTHER  = 'Land_And_Buildings_Other';
    public static final String KvbBank  = 'Kvb Bank';


     //Jocata list match task creation field
    public static final String APPLICATION_STATGE_TNC_ACCEPTED = 'Terms and Conditions Accepted';
     
}