/*
* Name    : UpdateDocName_Handler
* Company : ET Marlabs
* Purpose : This class is used to fetch attachment Name
* Author  : Subas
*/
public class UpdateDocName_Handler {
    public static void getDocName(List<genesis__AppDocCatAttachmentJunction__c > catJunctionList){
        try{
            List <String> attIdList = new List<String>();
			Map<Id,genesis__AppDocCatAttachmentJunction__c> attIdJuncObjMap = new Map<id,genesis__AppDocCatAttachmentJunction__c>();
            for(genesis__AppDocCatAttachmentJunction__c catJuncObj : catJunctionList){
                attIdList.add(catJuncObj.genesis__AttachmentId__c);
				attIdJuncObjMap.put(catJuncObj.genesis__AttachmentId__c,catJuncObj);
            }
            system.debug('$$$$'+attIdList);
            if(attIdList.size() >0){
                List <Attachment> attachList = [SELECT ID,Name FROM Attachment WHERE Id IN: attIdList];
                system.debug('####'+attachList);
                    for(Attachment attach : attachList){
						attIdJuncObjMap.get(attach.Id).Document_Name__c = attach.Name;
                    }
            }
        }Catch(Exception e){
            System.debug(e.getMessage());
            System.debug(e.getLineNumber());
        }
    }
}