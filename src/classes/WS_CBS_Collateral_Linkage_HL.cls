/*
* Name      : WS_CBS_Collateral_Linkage_HL
* Compnay   : ET Marlabs
* Purpose   : For HL Service(Collateral linkage) 
* Author    : Subas
*/
public class WS_CBS_Collateral_Linkage_HL {
    @future (Callout=true)
    public static void linkCollateralF(String AppId){
        linkCollateral(AppId);
    }
    public static void linkCollateral(String AppId){
        genesis__Applications__c application = new genesis__Applications__c();
        application = queryService.getApp(appId);
        Collateral_Linkage_Helper_HL cbs = new Collateral_Linkage_Helper_HL();
        cbs.inputVariables = new Collateral_Linkage_Helper_HL.cls_inputVariables();
        Collateral_Linkage_Helper_HL.cls_inputVariables cbsCol = new Collateral_Linkage_Helper_HL.cls_inputVariables();
        cbsCol.in_msg = new Collateral_Linkage_Helper_HL.cls_in_msg();
        cbs.exe_tenantId = 'cuecent_tenant';
        cbs.owner_tenantId = 'cuecent_tenant';
        cbsCol.in_msg.serviceName = 'Term_Loan_Linkage_Collateral_HL';
        cbsCol.in_msg.ApplicationId = AppId;
        cbsCol.in_msg.LinkCollateralDetails = new List<Collateral_Linkage_Helper_HL.cls_LinkCollateralDetails>();
        Collateral_Linkage_Helper_HL.cls_LinkCollateralDetails rq = new Collateral_Linkage_Helper_HL.cls_LinkCollateralDetails();
        rq.ApplicantID = application.Primary_Applicant_Id__c;
        rq.TransactionBranch = application.Branch_Code__c;
        rq.AccountNo = application.Loan_Account_Number__c;
        rq.CollateralCode = '107';
        rq.CollateralID = application.Loan_Account_Number__c + '1';//application.Primary_Collateral_ID__c;
        rq.CollateralValue = application.Total_Market_value__c != null ? String.ValueOf(application.Total_Market_value__c) : '';//String.ValueOf(application.Cost_Of_Flat_House__c);
        rq.ExtUniqueRefId = '1';
        rq.FlgOperation = 'A';
        rq.SecurityType = 'P';
        rq.Channel = 'BRN';
        cbsCol.in_msg.LinkCollateralDetails.add(rq);
        cbs.inputVariables = cbsCol;        
        callCollateralLink(JSON.serialize(cbs),AppId);
    }
    public static void callCollateralLink(String collateralDetails, String AppId){
        System.debug('#######'+collateralDetails);
        KVB_Endpoint_URLs__c kvb = KVB_Endpoint_URLs__c.getValues('CBS_Bulk_API');
        Map<String,String> headerMap                            = new Map<String,String>();
        headerMap.put('Content-Type','application/json');
        HTTPResponse response                                   = new HTTPResponse();
        String endPoint                                         = kvb.Endpoint_URL__c;
        response = HttpUtility.sendHTTPRequest(endPoint, 'POST', null,collateralDetails,headerMap,label.CA_CERTIFICATE);   
        system.debug('#####'+response);
        system.debug('$$$$'+response.getBody());
        If(response.getStatusCode() == 200 || response.getStatusCode() == 201){
            String jsonString = response.getBody();
            Collateral_Linkage_Helper_HL.Collateral_Linkage_Response res = (Collateral_Linkage_Helper_HL.Collateral_Linkage_Response)Json.deserialize(jsonString,Collateral_Linkage_Helper_HL.Collateral_Linkage_Response.class);
			system.debug('**Res**'+res);
            if(res.out_msg.Status_Desc != 'Success'){
                CBS_API_Log__c log = new CBS_API_Log__c();    
                log.Application__c = AppId;
                log.API_Name__c = 'Term_Loan_Linkage_Collateral_HL';
                log.Status__c = 'Failure';
                log.Success_Error_Message__c = res.out_msg.Status_Desc;
                log.Sequence_No__c = '7';  
                insert log; 
            }           
        }else{
            CBS_API_Log__c log = new CBS_API_Log__c();    
            log.Application__c = AppId;
            log.API_Name__c = 'Term_Loan_Linkage_Collateral_HL';
            log.Status__c = 'Failure';
            log.Success_Error_Message__c = 'CollateralLinkage_CBS_Error_No_Hit';
            log.Sequence_No__c = '7';  
            insert log;             
        }
    }

}