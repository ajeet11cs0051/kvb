/*
* Name    : ApplicationTiggerHandler 
* Company : ET Marlabs
* Author  : Venu
*/

public class ApplicationTriggerHandler {
    //singleton instance
    private static ApplicationTriggerHandler instance = null;
    
    //constructor
    private ApplicationTriggerHandler(){}
    
    //get singleton instance
    public static ApplicationTriggerHandler getInstance(){
        if(instance == null){
            instance = new ApplicationTriggerHandler();
        }
        RETURN instance;
    }
    public static Boolean IsFirstRun = true;
    public Boolean recursionFlag = false;
    
    // After update
    public void AfterUpdateCls(List<genesis__Applications__c> Applist,Map<Id,genesis__Applications__c> oldApp,List<genesis__Applications__c> oldList,Map<Id,genesis__Applications__c> newApp){
        String scoreCardCreatedFlag;
        //Create Task for HL
        TaskFlow_Helper.createTask(Applist,oldList);
        Call_BRE_HL.call_BRE(Applist,oldApp);        
        //SendEmailHandler.SendEmail(Applist,oldList);
       	SendSMSService.termsSms(Applist,oldApp);
        SendEmailHandler.termsEmail(Applist,oldApp);
        NMICalculations.CallNMI(Applist,oldApp);
        String recordtypeIdSME = SOQL_Util.getRecordTypeId('SME_Renewal');
        List<genesis__Applications__c> changedAppList = new List<genesis__Applications__c>();
        
        ApplicationEligibiltyHandler.CheckEMI(Applist, oldApp);
        ProcessingFee.CallCalculatefee(Applist, oldApp, oldList, newApp);
        System.debug('before call');
        //for PL
        RejectionScenarioHandeller.RejectioMethodPersonalLoan(newApp, oldApp);
        SanctionMessageGenerator.sanctionMessageMethod(AppList, oldApp, oldList, newApp);
        System.debug('after call');
        Set<String> applicationIds  = new Set<String>();
       
         for(genesis__Applications__c app : Applist){
            // for SMS and EMAIL
            for(genesis__Applications__c oApp : oldList){ 
                if(app.Record_Type_Name__c  ==Constants.HOMELOAN){
                    if((app.Sub_Stage__c=='Loan Sanctioned Non-STP' || app.Sub_Stage__c=='Loan Sanctioned STP'||  app.Sub_Stage__c=='Loan Sanctioned-Committee' || app.Sub_Stage__c == 'Pre - Approval Approved' || app.Sub_Stage__c == 'Processing Fee Paid' || app.Sub_Stage__c == 'Disbursement Approved' || app.Sub_Stage__c == 'Sanction pending - Non STP' || app.Sub_Stage__c == 'MOD Initiated') && (app.Sub_Stage__c <> oApp.Sub_Stage__c)){
                        //SendSMSService.sendSMSApp(app);
                        System.debug('%%Call sms service%%');
                        SendSMSService.sendSMSApp(JSON.serialize(app));
                    }
                }
                
                 System.debug('inside application trigger handeller');
                    System.debug('**yyyy'+app.Record_Type_Name__c);
                     if(app.Record_Type_Name__c  ==Constants.LAPLOAN){
                     if((app.Sub_Stage__c=='Terms and Conditions Accepted' || app.Sub_Stage__c=='Loan Sanctioned NSTP - LAP' ||app.Sub_Stage__c=='Loan account opened' || app.Sub_Stage__c=='Disbursement Documents Esigned') & (app.Sub_Stage__c <> oApp.Sub_Stage__c)){
                        //SendSMSService.sendSMSApp(app);
                        System.debug('%%Call  sendSMSSubstage LAp%%');
                        LAP_SendSMS.sendSMSSubstage(JSON.serialize(app));
                    }
                     }
                //for personal loan
                 if(app.Record_Type_Name__c  ==Constants.PERSONALLOAN){
                   //  if((app.Sub_Stage__c=='Terms and Conditions Accepted' || app.Sub_Stage__c=='Loan Sanctioned NSTP - LAP' ||app.Sub_Stage__c=='Loan account opened' || app.Sub_Stage__c=='Disbursement Documents Esigned') & (app.Sub_Stage__c <> oApp.Sub_Stage__c)){
                        //SendSMSService.sendSMSApp(app);
                        System.debug('%%Call  sendSMSSubstage LAp%%');
                        LAP_SendSMS.sendSms(JSON.serialize(app));
                  //  }
                    // Call CBS SI API when stage chnage to "Loan Account Opened"
                   
                     /*if(app.Sub_Stage__c =='Loan Account Opened' && oApp.Sub_Stage__c <> 'Loan Account Opened' && app.SI_Bank_Name__c <> null && app.SI_Bank_Name__c <> '' && app.SI_Bank_Name__c == 'KVB Bank'){

                        // CBS_SI_CreateHandler.CBS_SI_Creation();
                     }
*/
                 }

                  // Document generation trigger point for PL....
                if(app.Record_Type_Name__c  ==Constants.PERSONALLOAN){
                    if( (app.Sub_Stage__c =='Loan Sanctioned STP' && oApp.Sub_Stage__c <> 'Loan Sanctioned STP') || (app.Sub_Stage__c =='Loan Sanctioned Non-STP' && oApp.Sub_Stage__c <> 'Loan Sanctioned Non-STP') ){
                        PL_Digio_Services.docGenAndSignDocPL(app.id ,Constants.PRE_APPROVE_SANCTION_PL);
                        system.debug('##### Document generation triggerd ');
                    }

                }
                
                 if(app.Record_Type_Name__c  ==Constants.PERSONALLOAN || app.Record_Type_Name__c  ==Constants.LAPLOAN){
                    if(app.Sub_Stage__c =='Terms and Conditions Accepted' && oApp.Sub_Stage__c <> 'Terms and Conditions Accepted'){
                            AstuteAPIHandler.AstuteCallHandler(app.id);
                    }
                 }
                
            }
        } 
        
        //Send SMS and task creation for SME customer for SME Customer
        ApplicationTriggerHelper.sendSMSandtaskCreation(newApp,oldApp);
        if(!recursionFlag){
            recursionFlag   = true;
            
            //CBS_API calls(KYC,OD/TOD)
            ApplicationTriggerHelper.CBS_ApiCall(newApp,oldApp);//Sanction complete,Sanction complete- Final Sanction
            
            //Delete financial data
            ApplicationTriggerHelper.deleteFinancialData(newApp,oldApp);
            
            //Clear Perfios Id on Perfios Error
            //ApplicationTriggerHelper.clearPerfiosTxId(newApp,oldApp);
            IsFirstRun      = false;
        }

    }
    
    // for Before Update
    public void BeforeUpdateCls(list<genesis__Applications__c> Applist,Map<Id,genesis__Applications__c> newApp,Map<Id,genesis__Applications__c> oldApp){
        ProcessingFee.callPropertyDetails(Applist,oldApp);
        Retry_Task_call.applicationReCal(Applist,oldApp); //added by Subas(HL)
        BREScoreCal.breCal(Applist,oldApp); 
        Calculate_Estamp_HL.calEstamp(Applist,oldApp);
        BREScoreCal.updatePerfioscheck(Applist,oldApp);

        //for SME 
        ApplicationTriggerHelper.updateAppSubStage(newApp,oldApp);
        
        CBS_API_Calling_HL.initiateDisbursment(Applist,oldApp); //Start Disbursment
        // ApplicationNMI_Calculaitons.callNMI(Applist, oldApp);
        //  GurantorDocket.callDocket(Applist, oldApp);
        for(genesis__Applications__c app : Applist){
            if(app.Succession_plan__c != null && app.Management_experience__c != null && app.Nature_of_product_manufactured_traded__c != null && app.Selling_and_distribution_arrangement__c != null && app.External_certfications__c != null && app.Government_policies__c != null && app.Industry_outlook__c != null && app.CA_with_other_banks__c != null && app.Court_cases_non_credit_related__c != null){
                app.Qualitative_input_done__c = true;
            }
        }
        //Reset sign info on change of application stage to 'Limit extended'
        ApplicationTriggerHelper.resetSignInfo(newApp,oldApp);
        
        //Capture dates for TAT calculation
        ApplicationTriggerHelper.tatCapture(newApp,oldApp);
        RejectionScenarioHandeller.RejectioMethod(newApp,oldApp); // Added for reject senario LAP
        Re_run_Bureau.run_bureau(Applist,oldApp); //Added for LAP bureau call        
        //for PL
        //RejectionScenarioHandeller.RejectioMethodPersonalLoan(newApp, oldApp);
    }
    
    // for After Insert
    List<String> listObj = new List<String>();
    public void AfterInsertCls(list<genesis__Applications__c> Applist){
        //SME Reordtype
        String recordtypeIdSME = SOQL_Util.getRecordTypeId('SME_Renewal');
        
        for(genesis__Applications__c app : Applist){
            //SendEmailHandler.SendEmail(app);
             if(app.Record_Type_Name__c==Constants.HOMELOAN){
            SendSMSService.sendSMSonCrt(JSON.serialize(app));
            }
            //// add lap create application-niladri
            if(app.Record_Type_Name__c==Constants.LAPLOAN){
            LAP_SendSMS.sendSMSonCrt1(JSON.serialize(app));
            }
            //for SME Application
            //Added by Raushan
             if(!recursionFlag){
                recursionFlag = true;
                if(String.ValueOf(app.RecordTypeId) == recordtypeIdSME){
                    listObj.add(String.valueOf(app.id));                    
                } 
            }
        }  
        if(listObj.size() > 0)SME_PRE_RENEWAL_CHECKING.createPrechecklist(listObj);
    }
    
    //For Before Insert
    public void BeofreInsertCls(list<genesis__Applications__c> Applist){
        
    }
}