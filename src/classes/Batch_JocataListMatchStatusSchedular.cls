/*
* Name          : Batch_JocataListMatchingStatus_Scheduler
* Description   : Schdule Batch_JocataListMatchingStatus
* Author        : Ajeet
*/
global class Batch_JocataListMatchStatusSchedular implements Schedulable {    
    global void execute(SchedulableContext ctx) {
        Database.executeBatch(new Batch_JocataListMatchStatus(''),1);
    }   
}