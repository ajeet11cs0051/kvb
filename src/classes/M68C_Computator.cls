/*
* Name          : M68C_Computator
* Description   : to do the calculation and store the values
* Author        : Dushyant
*/
public class M68C_Computator{
    public static void calculateM68Data(Id accId,String fiscalYear){
        System.debug(accId);
        System.debug(fiscalYear);
        String currentFY 					= 'FY'+Date.Today().Year();
        String prevFY 						= 'FY'+(Date.Today().Year()-1);
        Secured_Short_Term_Borrowings__c sstb = new Secured_Short_Term_Borrowings__c();
        UnSecuredShortTermBorrowings__c usstb = new UnSecuredShortTermBorrowings__c();
        CurrentLiabilitiesOrShortTermLiabilities__c clsl = new CurrentLiabilitiesOrShortTermLiabilities__c();
        ShortTermProvisions__c stProv = new ShortTermProvisions__c();
        ShortTermLoansAndAdvances__c secStlAdv = new ShortTermLoansAndAdvances__c();
        ShortTermLoansAndAdvances__c unsecStlAdv = new ShortTermLoansAndAdvances__c();
        ShortTermLoansAndAdvances__c dbtsecStlAdv = new ShortTermLoansAndAdvances__c();
        OtherCurrentLiabilities__c ocLib = new OtherCurrentLiabilities__c();
        OtherLongTermLiabilities__c oltLib = new OtherLongTermLiabilities__c();
        Secured_Short_Term_Borrowings__c secstBorr = new Secured_Short_Term_Borrowings__c();
        Current_Assets__c currAst = new Current_Assets__c();
        TradeReceivablesOrSundryDebtors__c trsdWithin6Month = new TradeReceivablesOrSundryDebtors__c();
        TradeReceivablesOrSundryDebtors__c exceed6Month = new TradeReceivablesOrSundryDebtors__c();
        LongTermBorrowings__c secltBorr = new LongTermBorrowings__c();
        LongTermBorrowings__c unSecltBorr = new LongTermBorrowings__c();
        UnSecuredShortTermBorrowings__c unSecStb = new UnSecuredShortTermBorrowings__c();
        LongTermProvisions__c ltpr = new LongTermProvisions__c();
        NonCurrentLiabilitiesOrLongTermLiabiliti__c nclntl = new NonCurrentLiabilitiesOrLongTermLiabiliti__c();
        TangibleAssets__c tangAst = new TangibleAssets__c();
        FixedAssets__c fAst = new FixedAssets__c();
        ShareCapital__c shcap = new ShareCapital__c();
        EquitiesAndLiabilities__c el = new EquitiesAndLiabilities__c();
        ReserveAndSurplus__c spa_rs = new ReserveAndSurplus__c();
        ReserveAndSurplus__c gen_rs = new ReserveAndSurplus__c();
        ReserveAndSurplus__c capRes_rs = new ReserveAndSurplus__c();
        ReserveAndSurplus__c crr_rs = new ReserveAndSurplus__c();
        ReserveAndSurplus__c sooa_rs = new ReserveAndSurplus__c();
        ReserveAndSurplus__c fctr_rs = new ReserveAndSurplus__c();
        ReserveAndSurplus__c h_rs = new ReserveAndSurplus__c();
        ReserveAndSurplus__c otherRes_rs = new ReserveAndSurplus__c();
        ReserveAndSurplus__c rev_rs = new ReserveAndSurplus__c();
        ReserveAndSurplus__c surpDef_rs = new ReserveAndSurplus__c();
        ReserveAndSurplus__c surpPartner_rs = new ReserveAndSurplus__c();
        Shareholders_Funds__c shFunds = new Shareholders_Funds__c();
        NonCurrentInvestments__c tradeNCI = new NonCurrentInvestments__c();
        NonCurrentInvestments__c otherNCI = new NonCurrentInvestments__c();
        CurrentInvestments__c curInv = new CurrentInvestments__c();
        NonCurrentAssets__c ncAst = new NonCurrentAssets__c();
        LongTermLoansAndAdvances__c ltlaa = new LongTermLoansAndAdvances__c();
        Assets__c ast = new Assets__c();
        BalanceSheet__c bSt = new BalanceSheet__c();
        RevenueFromOperations__c rfo = new RevenueFromOperations__c();
        InTangibleAssets__c inTangAstNet = new InTangibleAssets__c();
        Expenses__c exp = new Expenses__c();
        ProfitAndLoss__c pl = new ProfitAndLoss__c();
        NonOperatingIncome__c noInc = new NonOperatingIncome__c();
        
        //query perfios data and do the calculation
        ast = [SELECT Id FROM Assets__c WHERE Balance_Sheet__r.Perfios__r.Fiscal_Year__c = :fiscalYear AND Balance_Sheet__r.Perfios__r.Applicant_Name__c =: accId LIMIT 1];
		bSt = [SELECT Id FROM BalanceSheet__c WHERE Perfios__r.Fiscal_Year__c = :fiscalYear AND Perfios__r.Applicant_Name__c =: accId LIMIT 1];
        
        pl = [SELECT Id,ProfitIeProfitAfterTax__c FROM ProfitAndLoss__c WHERE Perfios__r.Applicant_Name__c =: accId AND Perfios__r.Fiscal_Year__c = :fiscalYear];
        exp = [SELECT Id,DepreciationAndAmortizationTotal__c,PurchasesOfStockInTrade__c,CostOfMaterialsConsumed__c,ChangesInInventoriesTotal__c FROM Expenses__c WHERE Profit_And_Loss__r.Perfios__r.Applicant_Name__c =: accId AND Profit_And_Loss__r.Perfios__r.Fiscal_Year__c = :fiscalYear];
        inTangAstNet = [SELECT Id,Others__c FROM InTangibleAssets__c WHERE Fixed_Assets__r.Non_Current_Assets__r.Assets__c =:ast.Id AND Type__c = 'In Tangible Assets Net'];
        el = [SELECT ShareApplicationMoneyPendingAllotment__c FROM EquitiesAndLiabilities__c WHERE Balance_Sheet__c =:bSt.Id];
        currAst = [SELECT OtherCurrentAssetsTotal__c,BWB_InDepositAccounts__c,BWB_InFlexiDepositAccounts__c,GrossTotalOfInventories__c,LessProvisionForObsoleteStock__c,CashOnHand__c,ChequesDraftsOnHand__c,BWB_InCurrentAccounts__c,BWB_InEEFCAccounts__c,BWB_IEA_OtherEarmarkedAccounts__c,BWB_IEA_UnpaidDividendAccounts__c,BWB_IEA_UnpaidMaturedDebentures__c,BWB_IEA_UnpaidMaturedDeposits__c,CashAndCashEquivalentsOthers__c FROM Current_Assets__c WHERE Assets__c =:ast.Id];
        sstb = [SELECT OtherLoansAndAdvances__c,Deposits__c,InterCorporateBorrowings__c,LoansAndAdvancesFromRelatedParties__c,ForeignCurrencyLoans__c,LoansRepayableOnDemandFromBanks__c,CashCredits__c,Overdrafts__c,LoansRepayableOnDemandFromOtherParties__c FROM Secured_Short_Term_Borrowings__c WHERE Short_Term_Borrowings__r.CurrentLiabilitiesOrShortTermLiabilities__r.Equities_And_Liabilities__r.Balance_Sheet__c =:bSt.Id];
        usstb = [SELECT OtherLoansAndAdvances__c,LoansAndAdvancesFromRelatedParties__c,Deposits__c,InterCorporateBorrowings__c,ForeignCurrencyLoans__c,LoansRepayableOnDemandFromBanks__c,CashCredits__c,Overdrafts__c FROM UnSecuredShortTermBorrowings__c WHERE Short_Term_Borrowings__r.CurrentLiabilitiesOrShortTermLiabilities__r.Equities_And_Liabilities__r.Balance_Sheet__c =:bSt.Id];
        clsl = [SELECT ForGoods_Acceptances__c,ForGoods_Others__c,ForExpenses_Acceptances__c,ForExpenses_Others__c FROM CurrentLiabilitiesOrShortTermLiabilities__c WHERE Equities_And_Liabilities__r.Balance_Sheet__c =:bSt.Id];
        stProv = [SELECT Total__c FROM ShortTermProvisions__c WHERE CurrentLiabilitiesOrShortTermLiabilities__r.Equities_And_Liabilities__r.Balance_Sheet__c =:bSt.Id];
        secStlAdv = [SELECT AdvanceTax__c,SecurityDeposits__c,OtherLoansAndAdvances__c,InterCorporateDeposits__c,TotalBalancesWithGovernmentAuthorities__c,TotalPrepaidExpenses__c,LoansAndAdvancesToEmployees__c,LoansAndAdvancesToRelatedParties__c,FromOthers__c,FromRelatedParties__c FROM ShortTermLoansAndAdvances__c WHERE Type__c = 'Secured Short Term Loans And Advances' AND Current_Assets__r.Assets__c =:ast.Id];
        unsecStlAdv = [SELECT AdvanceTax__c,SecurityDeposits__c,OtherLoansAndAdvances__c,InterCorporateDeposits__c,TotalBalancesWithGovernmentAuthorities__c,TotalPrepaidExpenses__c,LoansAndAdvancesToEmployees__c,LoansAndAdvancesToRelatedParties__c,FromOthers__c,FromRelatedParties__c FROM ShortTermLoansAndAdvances__c WHERE Type__c = 'Un Secured Short Term Loans And Advances' AND Current_Assets__r.Assets__c =:ast.Id];
        dbtsecStlAdv = [SELECT AdvanceTax__c,SecurityDeposits__c,OtherLoansAndAdvances__c,InterCorporateDeposits__c,TotalBalancesWithGovernmentAuthorities__c,LoansAndAdvancesToEmployees__c,LoansAndAdvancesToRelatedParties__c,FromOthers__c,FromRelatedParties__c,TotalPrepaidExpenses__c FROM ShortTermLoansAndAdvances__c WHERE Type__c = 'Doubtful Short Term Loans And Advances' AND Current_Assets__r.Assets__c =:ast.Id];
        ocLib = [SELECT IncomeReceivedInAdvance__c,UnpaidDividends__c,UnpaidMaturedDebenturesAndInterestAccrue__c,UnpaidMaturedDepositsAndInterestAccruedT__c,ContractuallyReimburseableExpenses__c,SR_Total__c,InterestAccruedOnTradePayables__c,InterestAccruedOnOthers__c,TradeSecurityDeposits__c,Others__c,CurrentMaturitiesOfLongtermDebt__c,CurrentMaturitiesOfFinanceLeaseObligatio__c,InterestAccruedButNotDueOnBorrowings__c,InterestAccruedAndDueOnBorrowings__c FROM OtherCurrentLiabilities__c WHERE CurrentLiabilitiesOrShortTermLiabilities__r.Equities_And_Liabilities__r.Balance_Sheet__c =:bSt.Id];
        oltLib = [SELECT Acceptances__c,OtherThanAcceptances__c,TradeSecurityDeposits__c,PayablesOnPurchaseOfFixedAssets__c,Others__c,InterestAccruedOnTradePayables__c,InterestAccruedOnOthers__c,InterestAccruedButNotDueOnBorrowings__c,IncomeReceivedInAdvance__c,ContractuallyReimburseableExpenses__c,AdvancesFromCustomers__c FROM OtherLongTermLiabilities__c WHERE NonCurrentLiabilities_LongTermLiability__r.Equities_And_Liabilities__r.Balance_Sheet__c =:bSt.Id];
        secstBorr = [SELECT LoansAndAdvancesFromRelatedParties__c,Deposits__c,InterCorporateBorrowings__c,OtherLoansAndAdvances__c FROM Secured_Short_Term_Borrowings__c WHERE Short_Term_Borrowings__r.CurrentLiabilitiesOrShortTermLiabilities__r.Equities_And_Liabilities__r.Balance_Sheet__c =:bSt.Id];
        trsdWithin6Month = [SELECT TotalTradeReceivables__c FROM TradeReceivablesOrSundryDebtors__c WHERE Type__c = 'Within Six Months' AND Current_Assets__r.Assets__c =: ast.id];
        exceed6Month = [SELECT TotalTradeReceivables__c FROM TradeReceivablesOrSundryDebtors__c WHERE Type__c = 'Exceeding Six Months' AND Current_Assets__r.Assets__c =: ast.id];
        secltBorr = [SELECT ForeignCurrencyLoans__c,QuasiEquity__c,BondsDebentures__c,TermLoans_FromOtherParties__c,DeferredPaymentLiabilities__c,Deposits__c,LoansAndAdvancesFromRelatedParties__c,LongTermMaturitiesOfFinancialLeaseObliga__c,OtherLoansAdvances__c,TermLoans_FromBanks__c FROM LongTermBorrowings__c WHERE Type__c = 'Secured Long Term Borrowings' AND NonCurrentLiabilities_LongTermLiability__r.Equities_And_Liabilities__r.Balance_Sheet__c =:bSt.Id];
        unSecltBorr = [SELECT ForeignCurrencyLoans__c,BondsDebentures__c,TermLoans_FromBanks__c,DeferredPaymentLiabilities__c,Deposits__c,LongTermMaturitiesOfFinancialLeaseObliga__c,OtherLoansAdvances__c,QuasiEquity__c,TermLoans_FromOtherParties__c,LoansAndAdvancesFromRelatedParties__c FROM LongTermBorrowings__c WHERE Type__c = 'UnSecured Long Term Borrowings' AND NonCurrentLiabilities_LongTermLiability__r.Equities_And_Liabilities__r.Balance_Sheet__c =:bSt.Id];
        unSecStb = [SELECT LoansRepayableOnDemandFromOtherParties__c FROM UnSecuredShortTermBorrowings__c WHERE Short_Term_Borrowings__r.CurrentLiabilitiesOrShortTermLiabilities__r.Equities_And_Liabilities__r.Balance_Sheet__c =:bSt.Id];
        ltpr = [SELECT Total__c FROM LongTermProvisions__c WHERE NonCurrentLiabilities_LongTermLiability__r.Equities_And_Liabilities__r.Balance_Sheet__c =:bSt.Id];
        nclntl = [SELECT DeferredTaxLiabilities__c	FROM NonCurrentLiabilitiesOrLongTermLiabiliti__c WHERE Equities_And_Liabilities__r.Balance_Sheet__c =:bSt.Id];
        tangAst = [SELECT FurnitureAndFixtures__c,Vehicles__c,OfficeEquipment__c,LeaseholdImprovements__c,Computers__c,Others__c,Land__c,Buildings__c,PlantAndEquipment__c FROM TangibleAssets__c WHERE Fixed_Assets__r.Non_Current_Assets__r.Assets__c =:ast.Id AND Type__c = 'Tangible Assets Net'];
        fAst = [SELECT InTangibleAssetsUnderDevelopment__c,CapitalWorkInProgress__c FROM FixedAssets__c WHERE Non_Current_Assets__r.Assets__c =:ast.Id];
        shcap = [SELECT Id,Total__c FROM ShareCapital__c WHERE Shareholders_Funds__r.Equities_And_Liabilities__r.Balance_Sheet__c =:bSt.Id];
        shFunds = [SELECT MoneyReceivedAgainstShareWarrants__c FROM Shareholders_Funds__c WHERE Equities_And_Liabilities__r.Balance_Sheet__c =:bSt.Id];
        spa_rs = [SELECT ClosingBalance__c FROM ReserveAndSurplus__c WHERE Type__c ='Securities Premium Account' AND Shareholders_Funds__r.Equities_And_Liabilities__r.Balance_Sheet__c =:bSt.Id];
        gen_rs = [SELECT ClosingBalance__c FROM ReserveAndSurplus__c WHERE Type__c ='General Reserve' AND Shareholders_Funds__r.Equities_And_Liabilities__r.Balance_Sheet__c =:bSt.Id];
        capRes_rs = [SELECT ClosingBalance__c FROM ReserveAndSurplus__c WHERE Type__c ='Capital Reserve' AND Shareholders_Funds__r.Equities_And_Liabilities__r.Balance_Sheet__c =:bSt.Id];
        crr_rs = [SELECT ClosingBalance__c FROM ReserveAndSurplus__c WHERE Type__c ='Capital Redemption Reserve' AND Shareholders_Funds__r.Equities_And_Liabilities__r.Balance_Sheet__c =:bSt.Id];
        sooa_rs = [SELECT ClosingBalance__c FROM ReserveAndSurplus__c WHERE Type__c ='Share Options Outstanding Account' AND Shareholders_Funds__r.Equities_And_Liabilities__r.Balance_Sheet__c =:bSt.Id];
        fctr_rs = [SELECT ClosingBalance__c FROM ReserveAndSurplus__c WHERE Type__c ='Foreign Currency Translation Reserve' AND Shareholders_Funds__r.Equities_And_Liabilities__r.Balance_Sheet__c =:bSt.Id];
        h_rs = [SELECT ClosingBalance__c FROM ReserveAndSurplus__c WHERE Type__c ='Hedging Reserve' AND Shareholders_Funds__r.Equities_And_Liabilities__r.Balance_Sheet__c =:bSt.Id];
        otherRes_rs = [SELECT ClosingBalance__c FROM ReserveAndSurplus__c WHERE Type__c ='Other Reserves' AND Shareholders_Funds__r.Equities_And_Liabilities__r.Balance_Sheet__c =:bSt.Id];
		rev_rs = [SELECT ClosingBalance__c FROM ReserveAndSurplus__c WHERE Type__c ='Revaluation Reserve' AND Shareholders_Funds__r.Equities_And_Liabilities__r.Balance_Sheet__c =:bSt.Id];
    	surpDef_rs = [SELECT ClosingBalance__c FROM ReserveAndSurplus__c WHERE Type__c ='Surplus Or Deficit In The Statement Of Profit And Loss' AND Shareholders_Funds__r.Equities_And_Liabilities__r.Balance_Sheet__c =:bSt.Id];
        surpPartner_rs = [SELECT ClosingBalance__c FROM ReserveAndSurplus__c WHERE Type__c ='Surplus To Be Distributed To The Partners' AND Shareholders_Funds__r.Equities_And_Liabilities__r.Balance_Sheet__c =:bSt.Id];
        tradeNCI = [SELECT NetInvestmentProperty__c,GovernmentAndTrustSecurities_Total__c,OtherLongTermInvestments__c,LessProvisionForDiminutionInValueOfInves__c,EI_Total__c,PS_Total__c,DB_Total__c,InvestmentInMutualFunds__c,InvestmentInPartnershipFirms__c FROM NonCurrentInvestments__c WHERE Type__c = 'Trade Investments' AND Non_Current_Assets__r.Assets__c =:ast.Id];
        otherNCI = [SELECT NetInvestmentProperty__c,GovernmentAndTrustSecurities_Total__c,OtherLongTermInvestments__c,LessProvisionForDiminutionInValueOfInves__c,EI_Total__c,PS_Total__c,DB_Total__c,InvestmentInMutualFunds__c,InvestmentInPartnershipFirms__c FROM NonCurrentInvestments__c WHERE Type__c = 'Other Investments' AND Non_Current_Assets__r.Assets__c =:ast.Id];
        curInv = [SELECT NetInvestmentProperty__c,IIGATS_Total__c,OtherCurrentInvestments__c,NetOfCurrentPortionOfLongTermInvestments__c,PreferenceShares_Total__c,InvestmentInMutualFunds__c,InvestmentInPartnershipFirms__c,DebenturesOrBonds_Total__c,EquityInstruments_Total__c FROM CurrentInvestments__c WHERE Current_Assets__r.Assets__c =:ast.Id];
        ncAst = [SELECT DeferredTaxAssets__c,NonCurrentAssets_Others__c FROM NonCurrentAssets__c WHERE Assets__c =: ast.Id];
        ltlaa = [SELECT Total__c FROM LongTermLoansAndAdvances__c WHERE Non_Current_Assets__r.Assets__c =:ast.Id LIMIT 1];
        rfo = [SELECT NetDomesticTurnover__c,ExpSaleOfGoodsManufactured__c,DomSaleOfGoodsManufactured__c,ExpSaleOfGoodsTraded__c,DomSaleOfGoodsTraded__c,NetExportTurnover__c,ExpSaleOrSupplyOfServices__c,DomesticSaleOrSupplyOfServices__c,Total__c,DomLessExciseAndOtherDuties__c,ExpLessExciseAndOtherDuties__c FROM RevenueFromOperations__c WHERE Profit_And_Loss__r.Perfios__r.Fiscal_Year__c =: fiscalYear  AND Profit_And_Loss__r.Perfios__r.Applicant_Name__c =: accId  LIMIT 1];
        noInc = [SELECT GrandTotal__c FROM NonOperatingIncome__c WHERE Profit_And_Loss__c =: pl.Id];
        
        M68_Balance_Sheet_Analysis__c m68Data = new M68_Balance_Sheet_Analysis__c();
        m68Data.Fiscal_Year__c 	= fiscalYear;
        m68Data.Financial_type__c = 'Actual';
        m68Data.Account__c = accId;
        m68Data.Bank_Borrowings__c = Utility.isNullOrZeroDecimal(sstb.ForeignCurrencyLoans__c)+sstb.LoansRepayableOnDemandFromBanks__c+sstb.CashCredits__c+sstb.Overdrafts__c+usstb.ForeignCurrencyLoans__c+usstb.LoansRepayableOnDemandFromBanks__c+usstb.CashCredits__c+ usstb.Overdrafts__c;
        m68Data.Sundry_Creditors__c = clsl.ForGoods_Acceptances__c+clsl.ForGoods_Others__c;
        m68Data.Sundry_Creditors_Finance__c = sstb.LoansRepayableOnDemandFromOtherParties__c;
        m68Data.Sundry_Creditors_Exp__c = clsl.ForExpenses_Acceptances__c+clsl.ForExpenses_Others__c;
        m68Data.Provisions__c = stProv.Total__c+secStlAdv.FromOthers__c+secStlAdv.FromRelatedParties__c+unsecStlAdv.FromOthers__c+unsecStlAdv.FromRelatedParties__c+dbtsecStlAdv.FromOthers__c+dbtsecStlAdv.FromRelatedParties__c;
        m68Data.Term_Loan_1_Yr__c = ocLib.CurrentMaturitiesOfLongtermDebt__c+ocLib.CurrentMaturitiesOfFinanceLeaseObligatio__c+ocLib.InterestAccruedButNotDueOnBorrowings__c+ocLib.InterestAccruedAndDueOnBorrowings__c;
		m68Data.Other_Current_Liabilities__c = oltLib.AdvancesFromCustomers__c+sstb.Deposits__c+sstb.InterCorporateBorrowings__c+sstb.LoansAndAdvancesFromRelatedParties__c+usstb.LoansAndAdvancesFromRelatedParties__c+usstb.Deposits__c+usstb.InterCorporateBorrowings__c+usstb.OtherLoansAndAdvances__c+sstb.OtherLoansAndAdvances__c;
        m68Data.CL_Others_1__c = ocLib.IncomeReceivedInAdvance__c+ocLib.UnpaidDividends__c+ocLib.UnpaidMaturedDebenturesAndInterestAccrue__c+ocLib.UnpaidMaturedDepositsAndInterestAccruedT__c+ocLib.ContractuallyReimburseableExpenses__c+ocLib.SR_Total__c+ocLib.InterestAccruedOnTradePayables__c+ocLib.InterestAccruedOnOthers__c+ocLib.TradeSecurityDeposits__c+ocLib.Others__c;
        //m68Data.CL_Others_2__c = 0.0;//Formula not given
        m68Data.Cash_and_Bank_Balances__c = currAst.CashOnHand__c+currAst.ChequesDraftsOnHand__c+currAst.BWB_InCurrentAccounts__c+currAst.BWB_InEEFCAccounts__c+currAst.BWB_IEA_OtherEarmarkedAccounts__c+currAst.BWB_IEA_UnpaidDividendAccounts__c+currAst.BWB_IEA_UnpaidMaturedDebentures__c+currAst.BWB_IEA_UnpaidMaturedDeposits__c+currAst.CashAndCashEquivalentsOthers__c;
        m68Data.Inventories__c = currAst.GrossTotalOfInventories__c+currAst.LessProvisionForObsoleteStock__c;
        m68Data.Sundry_Debtors__c = trsdWithin6Month.TotalTradeReceivables__c;
        m68Data.Advances_and_deposits__c = secStlAdv.LoansAndAdvancesToEmployees__c+secStlAdv.LoansAndAdvancesToRelatedParties__c;
        //m68Data.Advance_to_suppliers__c = 0.0;//Formula not given
        m68Data.Bank_Deposits__c = currAst.BWB_InDepositAccounts__c+currAst.BWB_InFlexiDepositAccounts__c;
        m68Data.Other_Current_Assests__c = currAst.OtherCurrentAssetsTotal__c+secStlAdv.TotalPrepaidExpenses__c+secStlAdv.TotalBalancesWithGovernmentAuthorities__c+secStlAdv.InterCorporateDeposits__c+secStlAdv.OtherLoansAndAdvances__c+secStlAdv.AdvanceTax__c+unsecStlAdv.LoansAndAdvancesToEmployees__c+unsecStlAdv.LoansAndAdvancesToRelatedParties__c+unsecStlAdv.TotalPrepaidExpenses__c+unsecStlAdv.OtherLoansAndAdvances__c+unsecStlAdv.InterCorporateDeposits__c+unsecStlAdv.TotalBalancesWithGovernmentAuthorities__c+unsecStlAdv.AdvanceTax__c+dbtsecStlAdv.LoansAndAdvancesToEmployees__c+dbtsecStlAdv.LoansAndAdvancesToRelatedParties__c+dbtsecStlAdv.TotalPrepaidExpenses__c+dbtsecStlAdv.OtherLoansAndAdvances__c+dbtsecStlAdv.InterCorporateDeposits__c+dbtsecStlAdv.TotalBalancesWithGovernmentAuthorities__c+dbtsecStlAdv.AdvanceTax__c;
        //m68Data.OCA1__c = 0.0;//Formula not given
        //m68Data.OCA2__c = 0.0;//Formula not given
        m68Data.Term_loan_Banks_FIs__c = secltBorr.TermLoans_FromBanks__c;
        m68Data.Unsecured_Loans_F_R__c = unSecltBorr.QuasiEquity__c+unSecltBorr.TermLoans_FromOtherParties__c+unSecltBorr.LoansAndAdvancesFromRelatedParties__c+unSecStb.LoansRepayableOnDemandFromOtherParties__c;
        //m68Data.Term_LoanNBFCs__c = 0.0;//Formula not given
        m68Data.Other_Term_Liabilites__c = secltBorr.ForeignCurrencyLoans__c+secltBorr.QuasiEquity__c+secltBorr.BondsDebentures__c+secltBorr.TermLoans_FromOtherParties__c+secltBorr.DeferredPaymentLiabilities__c+secltBorr.Deposits__c+secltBorr.LoansAndAdvancesFromRelatedParties__c+secltBorr.LongTermMaturitiesOfFinancialLeaseObliga__c+secltBorr.OtherLoansAdvances__c+unSecltBorr.ForeignCurrencyLoans__c+unSecltBorr.BondsDebentures__c+unSecltBorr.TermLoans_FromBanks__c+unSecltBorr.DeferredPaymentLiabilities__c+unSecltBorr.Deposits__c+unSecltBorr.LongTermMaturitiesOfFinancialLeaseObliga__c+unSecltBorr.OtherLoansAdvances__c+oltLib.Acceptances__c+oltLib.OtherThanAcceptances__c+oltLib.TradeSecurityDeposits__c+oltLib.PayablesOnPurchaseOfFixedAssets__c+oltLib.Others__c+oltLib.InterestAccruedOnTradePayables__c+oltLib.InterestAccruedOnOthers__c+oltLib.InterestAccruedButNotDueOnBorrowings__c+oltLib.IncomeReceivedInAdvance__c+oltLib.ContractuallyReimburseableExpenses__c;
        m68Data.TL_Others_1__c = ltpr.Total__c;
        m68Data.TL_Others_2__c = nclntl.DeferredTaxLiabilities__c;
        m68Data.Land_Building__c = tangAst.Land__c+tangAst.Buildings__c;
        m68Data.Plant_Machinery__c = tangAst.PlantAndEquipment__c;
        m68Data.Fixed_Assests_Others__c = tangAst.FurnitureAndFixtures__c+tangAst.Vehicles__c+tangAst.OfficeEquipment__c+tangAst.LeaseholdImprovements__c+tangAst.Computers__c+tangAst.Others__c;
        m68Data.Capital_WIP__c = fAst.CapitalWorkInProgress__c;
        m68Data.Paid_Up_Capital__c = shcap.Total__c;
        m68Data.Share_Application_Money__c = el.ShareApplicationMoneyPendingAllotment__c;
        m68Data.Share_Premium__c = spa_rs.ClosingBalance__c;
        m68Data.General_Reserves__c = gen_rs.ClosingBalance__c;
        m68Data.Other_Reserves__c = capRes_rs.ClosingBalance__c+crr_rs.ClosingBalance__c+sooa_rs.ClosingBalance__c+fctr_rs.ClosingBalance__c+h_rs.ClosingBalance__c+otherRes_rs.ClosingBalance__c;
        m68Data.Revaluation_Reserves__c = rev_rs.ClosingBalance__c;
        m68Data.Surplus_in_P_L__c = surpDef_rs.ClosingBalance__c+surpPartner_rs.ClosingBalance__c;
        m68Data.NW_Others1__c = shFunds.MoneyReceivedAgainstShareWarrants__c;
        //m68Data.NW_Others2__c = 0.0;//Formula not given
        m68Data.Inv_in_Sister_Associates__c = tradeNCI.InvestmentInPartnershipFirms__c+otherNCI.InvestmentInPartnershipFirms__c+curInv.EquityInstruments_Total__c+curInv.DebenturesOrBonds_Total__c+curInv.InvestmentInPartnershipFirms__c;
        m68Data.Inv_in_shares_etc__c = tradeNCI.EI_Total__c+tradeNCI.PS_Total__c+tradeNCI.DB_Total__c+tradeNCI.InvestmentInMutualFunds__c+otherNCI.EI_Total__c+otherNCI.DB_Total__c+otherNCI.InvestmentInMutualFunds__c+curInv.NetOfCurrentPortionOfLongTermInvestments__c+curInv.PreferenceShares_Total__c+curInv.InvestmentInMutualFunds__c;//+otherNCI.PS_Total__c
        m68Data.Non_Current_others__c = ncAst.DeferredTaxAssets__c+ncAst.NonCurrentAssets_Others__c+ltlaa.Total__c+tradeNCI.NetInvestmentProperty__c+tradeNCI.GovernmentAndTrustSecurities_Total__c+tradeNCI.OtherLongTermInvestments__c-tradeNCI.LessProvisionForDiminutionInValueOfInves__c+otherNCI.NetInvestmentProperty__c+otherNCI.GovernmentAndTrustSecurities_Total__c+otherNCI.OtherLongTermInvestments__c-otherNCI.LessProvisionForDiminutionInValueOfInves__c+curInv.NetInvestmentProperty__c+curInv.IIGATS_Total__c+curInv.OtherCurrentInvestments__c+exceed6Month.TotalTradeReceivables__c+secStlAdv.SecurityDeposits__c+unsecStlAdv.SecurityDeposits__c+dbtsecStlAdv.SecurityDeposits__c;
        //m68Data.Profit_Loss_Account__c = 0.0;//Formula not given
        //m68Data.Misc_Expenditure__c = 0.0;//Formula not given
        m68Data.Intangible_Others__c = inTangAstNet.Others__c + fAst.InTangibleAssetsUnderDevelopment__c;
        m68Data.Gross_sales__c = rfo.DomesticSaleOrSupplyOfServices__c+rfo.ExpSaleOrSupplyOfServices__c+rfo.NetDomesticTurnover__c+rfo.ExpSaleOfGoodsManufactured__c+rfo.DomSaleOfGoodsManufactured__c+rfo.ExpSaleOfGoodsTraded__c+rfo.DomSaleOfGoodsTraded__c+rfo.NetExportTurnover__c;
        m68Data.Net_sales__c = rfo.DomesticSaleOrSupplyOfServices__c+rfo.ExpSaleOrSupplyOfServices__c+rfo.NetDomesticTurnover__c+rfo.ExpSaleOfGoodsManufactured__c+rfo.DomSaleOfGoodsManufactured__c+rfo.ExpSaleOfGoodsTraded__c+rfo.DomSaleOfGoodsTraded__c+rfo.NetExportTurnover__c-rfo.DomLessExciseAndOtherDuties__c-rfo.ExpLessExciseAndOtherDuties__c;
        m68Data.Purchases__c = exp.PurchasesOfStockInTrade__c+exp.CostOfMaterialsConsumed__c+exp.ChangesInInventoriesTotal__c;
        m68Data.Depreciation__c	 = exp.DepreciationAndAmortizationTotal__c;
        m68Data.Net_profit__c	 = pl.ProfitIeProfitAfterTax__c;
        m68Data.Other_income__c	 = noInc.GrandTotal__c;
        INSERT m68Data;
        System.debug(m68Data.Id);
    }
}