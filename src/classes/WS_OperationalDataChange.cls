/*
 * @name        : WS_OperationalDataChange
 * @description : MiddleWare call to get monthly changed Operational Data.
 * @author      : Amritesh
 */ 
public class WS_OperationalDataChange {

        /*public static void operationalDeltaChange(string custID){
        try{
            Map<String,String> headerMap            = new Map<String,String>();
            headerMap.put('Content-Type','application/json');
            HTTPResponse response                   = new HTTPResponse();
            
            WS_OperationalDataChange.operation_req reqst    = new WS_OperationalDataChange.operation_req();
            reqst.exe_tenantId                              = 'cuecent_tenant';
            reqst.owner_tenantId                            = 'cuecent_tenant';
            reqst.inputVariables                            = new WS_OperationalDataChange.cls_inputVariables();
            reqst.inputVariables.in_msg                     = new WS_OperationalDataChange.cls_in_msg();
            reqst.inputVariables.in_msg.custCode            = custID;
            integer currentMonth                            = system.today().month();
            integer year                                    = system.today().year();
            
            if(currentMonth >= 1 && currentMonth <= 3) year = year - 1;
                
            reqst.inputVariables.in_msg.fromDate            = DateTime.newInstance(year,4,1).format('yyyy-MM-dd');
            reqst.inputVariables.in_msg.toDate              = System.now().format('yyyy-MM-dd');
            
            String endPoint                                 = 'https://www.kvbbank.net/ext_bpms/rest/CueRest/invokeESBService/OprData';
            response = HttpUtility.sendHTTPRequest(endPoint, 'POST', null,JSON.Serialize(reqst),headerMap,label.CA_CERTIFICATE);
            If(response.getStatusCode() == 200 || response.getStatusCode() == 201){
                String jsonData = response.getBody();  
                OperationalData_response opsData = OperationalData_response.parse(jsonData);
                If(opsData !=null){
                    OperationalDataChange_Helper.handleOpsData(opsData,custID,DateTime.newInstance(year,4,1).format('yyyy-MM-dd'));
                }
            }else{
                
                throw new CustomException('Status Code-'+response.getStatusCode()+' Status-'+response.getStatus());
            }
            
        }catch(Exception ex){
            String error = ex.getMessage();
           
        }
    }
    
    public class operation_req{
        public String exe_tenantId; //cuecent_tenant
        public String owner_tenantId;   //cuecent_tenant
        public cls_inputVariables inputVariables;
    }
    class cls_inputVariables {
        public cls_in_msg in_msg;
    }
    class cls_in_msg {
        public String custCode; //6583774
        public String fromDate; //2015-04-01
        public String toDate;   //2016-03-31
    }
    public static WS_OperationalDataChange parse(String json){
        return (WS_OperationalDataChange) System.JSON.deserialize(json, WS_OperationalDataChange.class);
    }*/
}