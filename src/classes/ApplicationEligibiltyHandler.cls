/*

* Name          : ApplicationEligibiltyHandler
* Description   : Handle HL FOR Calculating the EMI,LTV,ROI
* Author        : Venu gopal
*/

public class ApplicationEligibiltyHandler {
    public static boolean Recursing=true;
    public static void CheckEMI(List<genesis__Applications__c> APlist,Map<Id,genesis__Applications__c> oldApp){
        List<genesis__Applications__c> ApplicationList=new List<genesis__Applications__c>();
        for(genesis__Applications__c Ap:APlist){
            genesis__Applications__c oldPlan = oldApp.get(Ap.Id);
            if(((oldPlan.Sub_Stage__c!=Ap.Sub_Stage__c && Ap.Sub_Stage__c==Constants.Sub_Stage_PropInfoCaptured) && (ap.Record_Type_Name__c ==Constants.LAPLOAN || ap.Record_Type_Name__c ==Constants.HOMELOAN )) || 
            ((oldPlan.Cost_Of_Construction__c!=Ap.Cost_Of_Construction__c || oldPlan.Cost_Of_Flat_House__c!=Ap.Cost_Of_Flat_House__c  || oldPlan.Cost_Of_Land__c!=Ap.Cost_Of_Land__c  || oldPlan.Cost_Of_Repair__c!=Ap.Cost_Of_Repair__c || 
            oldPlan.Age_Of_The_Building__c!=Ap.Age_Of_The_Building__c  || oldPlan.Whether_The_Construction_Completed__c!=Ap.Whether_The_Construction_Completed__c) && ap.Record_Type_Name__c ==Constants.HOMELOAN) || 
            (oldPlan.Sub_Stage__c!=Ap.Sub_Stage__c && Ap.Sub_Stage__c==Constants.Sub_Stage_WorkInfoCaptured && ap.Record_Type_Name__c ==Constants.PERSONALLOAN) ){     
                system.debug('APlist'+APlist);
                
                system.debug('oldApp'+oldApp);
                
                system.debug('Ap'+Ap);
                
                ApplicationList.add(Ap);
            }
        }
        
        if(ApplicationList.size()>0 && Recursing==True){
            getEmi_LTV_ROIValues(ApplicationList);
        }
    }
    
    public static void getEmi_LTV_ROIValues(List<genesis__Applications__c> APPlist){
        Recursing=false;
        Decimal IntProp = 0.00;   //Addtion for Property 
        Decimal IntCIBIL = 0.00;  //Addition for CIBIL    
        List<ID> AppID=new List<ID>();
        map<id,id> AccAppID=new Map<id,id>();
        Map<id,Decimal> APPEmi=new Map<id,Decimal>();
        Map<id,List<ID>> ApplistAcc = new  Map<id,List<ID>>();
        Map<ID,Decimal>     AccEmi= new Map<id,Decimal>();
        Map<Id,Integer> AccNmax=new Map<Id,Integer>();
        List<Decimal> CibilScore=new List<Decimal>();
        Map<String,string> BranchCodemap=new map<String,string>();
        set<string> Bcodes=new set<string>();
        Decimal MaxCiblScore = 0;
        try{
            list<genesis__Applications__c> UpdateAppList = new list<genesis__Applications__c>();
            
            List<genesis__Applications__c> ApList=[select id,Branch_Code__c,genesis__Account__r.Annual_Other_Income_Recurring_Credits_IT__c,Record_Type_Name__c,genesis__Account__r.CIBIL_Score__c,
                                                   genesis__Account__r.Existing_Emis__c,genesis__Account__r.Employment_Type__c,genesis__Account__r.NMI_Approved__c,
                                                   genesis__Account__r.NMI_Claimed_By_Customer__c,genesis__Account__r.Age__c,genesis__Account__r.Age_In_Months__c,genesis__Account__r.Financial_Applicant__c,
                                                   genesis__Account__r.Net_Monthly_Income__c,genesis__Account__r.Approved_NMI_From_ITR__c,NMI_Claimed_By_Customer__c,
                                                   (select id,genesis__Party_Type__c,genesis__Party_Account_Name__r.Existing_Emis__c,
                                                    genesis__Party_Account_Name__r.CIBIL_Score__c,genesis__Party_Account_Name__r.Approved_NMI_From_ITR__c,
                                                    genesis__Party_Account_Name__r.NMI_Approved__c,genesis__Party_Account_Name__r.NMI_Claimed_By_Customer__c,
                                                    genesis__Party_Account_Name__r.Employment_Type__c,genesis__Party_Account_Name__r.Net_Monthly_Income__c,genesis__Party_Account_Name__r.Age_In_Months__c,
                                                    genesis__Party_Account_Name__r.Financial_Applicant__c,genesis__Party_Account_Name__r.age__c from genesis__Application_Parties__r where Active__c=true) from genesis__Applications__c where ID IN:APPlist];
            system.debug('ApList'+ApList);
            Decimal EMI=0;
            decimal EMI1=0;
            Integer Nmax=0;
            Integer Nmax1=0;
            integer AppNmax;
            Decimal mclrrate_oneYear= getMCLR(365,null,null);
            Decimal mclrrate_ThreeMonths= getMCLR(90,null,null);
            Boolean CoCheck = False;
            
            for(genesis__Applications__c Appl:ApList){
                
                if(Appl.genesis__Application_Parties__r.size() >0){
                    CoCheck = true;
                }
                //  Primary Applicant EMI
                
                
                
                Bcodes.add(Appl.Branch_Code__c);
                system.debug('Bcodes'+Bcodes);
                AccAppID.put(Appl.genesis__Account__r.id,Appl.id);
                AppID.add(Appl.genesis__Account__r.id);
                Decimal NMI = 0;
                Decimal NMIITR = 0;
                
                if(Appl.Record_Type_Name__c == Constants.HOMELOAN){
                    NMI=Appl.genesis__Account__r.NMI_Claimed_By_Customer__c;
                }else{  
                    NMI=Appl.genesis__Account__r.NMI_Claimed_By_Customer__c;
                }                
                NMIITR=Appl.genesis__Account__r.Net_Monthly_Income__c;
                
               /*if(appl.Record_Type_Name__c == Constants.PERSONALLOAN){
                    if(appl.genesis__Account__r.Annual_Other_Income_Recurring_Credits_IT__c != null){
                        NMIITR = NMIITR - appl.genesis__Account__r.Annual_Other_Income_Recurring_Credits_IT__c;
                    }
                }
                */
                system.debug('NMI '+NMI +' NMIRecuringIncome ----> ' + NMIITR);
                if(Appl.genesis__Account__r.Financial_Applicant__c){
                    
                    // Nmax
                    CibilScore.add(Appl.genesis__Account__r.CIBIL_Score__c);
                    if(Appl.genesis__Account__r.Employment_Type__c==Constants.EmpType_Salaried){
                        if(Appl.Record_Type_Name__c==Constants.PERSONALLOAN) {
                            Nmax=Math.min(60,(720-integer.valueOf(Appl.genesis__Account__r.Age_In_Months__c)));
                            if(NMIITR<25000 && NMIITR>=15000){
                                EMI=NMI*0.55;
                            }
                            else if(NMIITR<50000 && NMIITR>=25000){
                                EMI=NMI*0.60;
                            }
                            else if(NMIITR>=50000){
                                EMI=NMI*0.70;
                            }
                        }
                        else{
                            Nmax=NmaxCalculation(integer.valueOf(Appl.genesis__Account__r.Age_In_Months__c),true,Appl.Record_Type_Name__c,CoCheck);
                            if(NMIITR<=100000){
                                EMI=NMI*0.70;
                                
                            }
                            else if(NMIITR>100000){
                                EMI=NMI*0.75;
                            }
                        }
                    }
                    else{
                        if(Appl.Record_Type_Name__c==Constants.PERSONALLOAN ) {
                            Nmax=Math.min(60,(756-integer.valueOf(Appl.genesis__Account__r.Age_In_Months__c)));
                            EMI=NMI*0.70;
                        }
                        else{
                             Nmax=NmaxCalculation(integer.valueOf(Appl.genesis__Account__r.Age_In_Months__c),true,Appl.Record_Type_Name__c,CoCheck);
                            if(NMIITR<=66666.67){
                                EMI=NMI*0.70;
                            }
                            else if(NMIITR>66666.67){
                                EMI=NMI*0.75;
                            }
                        }
                    }
                    if(Appl.genesis__Account__r.Existing_Emis__c!=null){
                        EMI= EMI-Appl.genesis__Account__r.Existing_Emis__c;
                        if(EMI<0){
                            EMI=0;
                        }
                    }
                }
                else if(!Appl.genesis__Account__r.Financial_Applicant__c){
                    Nmax=NmaxCalculation(integer.valueOf(Appl.genesis__Account__r.Age_In_Months__c),false,Appl.Record_Type_Name__c,CoCheck);
                }
                
                AccNmax.put(Appl.genesis__Account__r.id,Nmax);
                AccEmi.put(Appl.genesis__Account__r.id, EMI);
                
                
                
                
                for(genesis__Application_Parties__c parties:Appl.genesis__Application_Parties__r){
                    
                    //  For Co-applicant and Gurantor EMI
                    AccAppID.put(parties.genesis__Party_Account_Name__r.id,Appl.id);
                    AppID.add(parties.genesis__Party_Account_Name__r.id);
                    
                    Decimal NMI1 = 0;
                    Decimal NMIITR1 = 0;
                    /*
if(parties.genesis__Party_Account_Name__r.NMI_Approved__c!=null){
NMI1=parties.genesis__Party_Account_Name__r.NMI_Approved__c;
}
else{ */
                    if(Appl.Record_Type_Name__c == Constants.HOMELOAN){
                        NMI1= parties.genesis__Party_Account_Name__r.NMI_Claimed_By_Customer__c ;
                    }else{
                        NMI1=parties.genesis__Party_Account_Name__r.NMI_Claimed_By_Customer__c;
                    }
                    
                    
                    NMIITR1=parties.genesis__Party_Account_Name__r.Net_Monthly_Income__c;
                    
                    if(parties.genesis__Party_Type__c==Constants.Co_Borrower  && parties.genesis__Party_Account_Name__r.Financial_Applicant__c){
                        
                        // NMAX
                        Nmax1=NmaxCalculation(integer.valueOf(parties.genesis__Party_Account_Name__r.Age_In_Months__c),true,Appl.Record_Type_Name__c,CoCheck);
                        CibilScore.add(parties.genesis__Party_Account_Name__r.CIBIL_Score__c);
                        
                        
                        system.debug('NMI'+NMI1);
                        if(parties.genesis__Party_Account_Name__r.Employment_Type__c==Constants.EmpType_Salaried){
                            if(NMIITR1<=100000){
                                EMI1 =NMI1*0.70;
                            }
                            else if(NMIITR1>100000){
                                EMI1=NMI1*0.75;
                            }
                            
                        }
                        else{
                            if(NMIITR1<=66666.67){
                                EMI1=NMI1*0.70;
                            }
                            else if(NMIITR1>66666.67){
                                EMI1=NMI1*0.75;
                            }
                        }
                        if(parties.genesis__Party_Account_Name__r.Existing_Emis__c!=null){
                            EMI1= EMI1-parties.genesis__Party_Account_Name__r.Existing_Emis__c;
                            if(EMI1<0){
                                EMI1=0;
                            }
                        }
                        
                        AccEmi.put(parties.genesis__Party_Account_Name__r.id, EMI1);
                    }
                    else if(parties.genesis__Party_Type__c==Constants.Gurantor  || !parties.genesis__Party_Account_Name__r.Financial_Applicant__c){
                        
                        Nmax1=NmaxCalculation(integer.valueOf(parties.genesis__Party_Account_Name__r.Age_In_Months__c),false,Appl.Record_Type_Name__c,CoCheck);
                    }
                    
                    AccNmax.put(parties.genesis__Party_Account_Name__r.id,Nmax1);
                    
                }
                system.debug('AccNmax'+AccNmax);
                system.debug('AccEmi'+AccEmi);
                ApplistAcc.put(Appl.Id, AppID);
                
            }
            system.debug('ApplistAcc'+ApplistAcc);
            
            
            List<Account> AcclISt= new List<Account>();
            Decimal x=0;
            for(string s:AccEmi.keySet()){
                account a=new account();
                a.id=s;
                a.Eligible_EMI1__c=AccEmi.get(S);
                a.NMax__c=AccNmax.get(s);
                AcclISt.add(a);
                
                
                
                if(ApplistAcc.containsKey(AccAppID.get(s))){
                    x=x+AccEmi.get(s);
                    system.debug(x);
                    if(APPEmi.containsKey(AccAppID.get(s))){
                        APPEmi.put(AccAppID.get(s), x);
                    }
                    else{
                        APPEmi.put(AccAppID.get(s), AccEmi.get(s));
                    }
                }
            }
            List<integer>  Num=new List<integer>();
            for(Id nm:AccNmax.keySet()){
                
                Num.add( AccNmax.get(nm));
            }
            
            Num.sort();
            AppNmax =num[0];  
            system.debug('CibilScore'+CibilScore);
            if(CibilScore.size()>0){
                CibilScore.sort();
                MaxCiblScore=CibilScore[(CibilScore.size()-1)];    
            }
            system.debug('Bcodes'+Bcodes);
            List<Branch_Master__c> BranchList=[select id,CODCCBRN__c,Retail_Category__c from Branch_Master__c where CODCCBRN__c IN:Bcodes];
            for(Branch_Master__c Bm:BranchList){
                if(Bm.CODCCBRN__c!=null){
                    BranchCodemap.put(Bm.CODCCBRN__c,Bm.Retail_Category__c);
                }
                
            }
            system.debug('Bcodes'+BranchList);
            system.debug('BranchCodemap'+BranchCodemap);
            system.debug('MaxCiblScore'+MaxCiblScore);
            system.debug('AppNMax'+AppNmax);
            system.debug('APPEmi'+APPEmi);
            list<genesis__Applications__c> newApplist=[select id,Branch_Code__c,Record_Type_Name__c,PrimaryApplicant_CIBIL_Score__c,Age_Of_The_Building__c,genesis__Loan_Amount__c,Total_Market_value__c,
                                                       Holiday_Max__c,genesis__Account__r.Existing_Emis__c,genesis__Account__r.Net_Monthly_Income__c,genesis__Account__r.NMI_Claimed_By_Customer__c,genesis__Account__r.Employment_Type__c,Is_Take_Over__c, Whether_The_Construction_Completed__c,Loan_Purpose__c,Cost_Of_Flat_House__c,Cost_Of_Construction__c,Cost_Of_Land__c,PMax1__c,Cost_Of_Repair__c,
                                                       (select id,Collateral_Market_Value__c from genesis__Collaterals__r) From genesis__Applications__c where id IN:APPEmi.keySet()];
            system.debug('%%%%%'+newApplist);
            for(genesis__Applications__c App:newApplist){
                
                // For ROI
                if(App.PrimaryApplicant_CIBIL_Score__c!=null){
                    if(MaxCiblScore<App.PrimaryApplicant_CIBIL_Score__c){
                        MaxCiblScore=App.PrimaryApplicant_CIBIL_Score__c;
                    }
                    
                    // For Nmax
                    if(AppNmax>0){
                        if(App.Is_Take_Over__c == true){
                            App.Nmax__c= Math.min(AppNmax,App.Outstanding_Loan_Term_Existing_Loan__c);
                        }else{
                            App.Nmax__c=AppNmax;                        
                        }
                    }
                    else{
                        App.Nmax__c=0;
                    }
                    
                    
                }
                if(App.Record_Type_Name__c==Constants.HOMELOAN){
                    // Holiday Max
                    
                    if(App.Whether_The_Construction_Completed__c){
                        App.Holiday_Max__c=3;
                    }
                    else{
                        App.Holiday_Max__c=24; 
                    } 
                    
                    // Pmax1  
                    
                    Decimal CostProperty = 0;
                    Decimal PropertyValue = 0;
                    if(App.Loan_Purpose__c==Constants.IdentifiedProperty || App.Loan_Purpose__c==Constants.Loanpurpose){
                        CostProperty=App.Cost_Of_Flat_House__c;
                        PropertyValue=App.Cost_Of_Flat_House__c;
                    }
                    else if(App.Loan_Purpose__c==Constants.RepairHouse){
                        CostProperty=App.Cost_Of_Repair__c;
                        PropertyValue=App.Cost_Of_Repair__c;
                    }
                    else if(App.Loan_Purpose__c==Constants.Construction){
                        CostProperty=App.Cost_Of_Construction__c;
                        PropertyValue=App.Cost_Of_Construction__c ;
                    }
                    else if(App.Loan_Purpose__c==Constants.ConstructionOn){
                        CostProperty=App.Cost_Of_Land__c + App.Cost_Of_Construction__c;
                        PropertyValue=App.Cost_Of_Land__c + App.Cost_Of_Construction__c;
                    }
                    system.debug('CostProperty'+CostProperty);
                    system.debug('Age of building'+ App.Age_Of_The_Building__c);
                    if(CostProperty!=null)
                        App.Cost_of_Property__c=CostProperty;
                    if(App.Age_Of_The_Building__c!=null){
                        if(App.Age_Of_The_Building__c > 30 && App.Age_Of_The_Building__c <= 50){
                            IntProp = 0.50;
                        }
                        Decimal Minvalue =0;
                        if(PropertyValue==0){
                            Minvalue=CostProperty;
                        }
                        else{
                            Minvalue=Math.min(CostProperty,PropertyValue);  
                        }
                        system.debug('Minvalue'+Minvalue);
                        //if(App.Age_Of_The_Building__c<=Constants.one){
                        
                        
                        
                        if(Minvalue<3750000){
                            App.PMax1__c = Math.min(0.90*Minvalue,3000000);
                        }
                        else if(Minvalue>=3750000 && Minvalue<=10000000){
                            App.PMax1__c = Math.min(0.80*Minvalue,7500000);
                        }
                        else if(Minvalue>10000000){
                            App.PMax1__c = 0.75*Minvalue;
                        }
                        
                        
                    }
                    // ROI HL
                    
                    if(MaxCiblScore==10 || MaxCiblScore==9 || MaxCiblScore==8 || MaxCiblScore==7|| MaxCiblScore==6 || MaxCiblScore==5 || MaxCiblScore==1 || MaxCiblScore==2 ||  MaxCiblScore==3 || MaxCiblScore==4 || MaxCiblScore== -1 ) {
                        IntCIBIL = 0.50;
                        //App.Reject_Scenario__c='No financial applicant / co-applicant with valid CIBIL score';
                        // App.Sub_Stage__c=Constants.Sub_Stage_WorkInfoCaptured; 
                        
                        App.genesis__Interest_Rate__c=   mclrrate_oneYear + IntCIBIL +IntProp;
                        App.MCLR_Rate__c=mclrrate_oneYear;
                        App.Excess_MCLR__c=0;
                        App.MCLR_Type__c=Constants.MCLR_OneYear;
                    }
                    else{
                        App.Reject_Scenario__c='';
                        if( MaxCiblScore>650  &&  MaxCiblScore<700){
                            App.genesis__Interest_Rate__c =   mclrrate_oneYear+2 + IntProp;
                            
                            App.MCLR_Rate__c=mclrrate_oneYear;
                            App.Excess_MCLR__c=2;
                            App.MCLR_Type__c=Constants.MCLR_OneYear;
                        }
                        else if( MaxCiblScore>=700 &&  MaxCiblScore<750){
                            App.genesis__Interest_Rate__c=   mclrrate_oneYear + IntProp;
                            App.MCLR_Rate__c=mclrrate_oneYear;
                            App.Excess_MCLR__c=0;
                            App.MCLR_Type__c=Constants.MCLR_OneYear;
                        }
                        else if( MaxCiblScore>=750){
                            
                            App.genesis__Interest_Rate__c=   mclrrate_ThreeMonths + IntProp;
                            App.MCLR_Rate__c=mclrrate_ThreeMonths;
                            App.Excess_MCLR__c=0;
                            App.MCLR_Type__c=Constants.MCLR_ThreeMonths;
                        }
                    }
                    
                }
                else if(App.Record_Type_Name__c==Constants.LAPLOAN){
                    Decimal Marketvalue =0;
                    system.debug('####'+App.genesis__Collaterals__r.size());
                    for(clcommon__Collateral__c cc:App.genesis__Collaterals__r){
                        if(!Utility.ISStringBlankorNull(string.valueof(cc.Collateral_Market_Value__c))){
                            Marketvalue +=cc.Collateral_Market_Value__c;
                        }
                    }
                    if(Marketvalue!=null){
                        App.PMax1__c=  Marketvalue*0.6;
                    }
                    
                    // ROI LAP
                    if(MaxCiblScore >=-1  && MaxCiblScore <= 10) {                                                
                        App.genesis__Interest_Rate__c=  getMCLR(365,BranchCodemap.get(App.Branch_Code__c),-1);                        
                    }
                    
                    else if( MaxCiblScore>=650  &&  MaxCiblScore<700){
                        App.genesis__Interest_Rate__c =   getMCLR(365,BranchCodemap.get(App.Branch_Code__c),650);                                                
                    }
                    else if( MaxCiblScore>=700 &&  MaxCiblScore<750){
                        App.genesis__Interest_Rate__c=   getMCLR(365,BranchCodemap.get(App.Branch_Code__c),700);                        
                    }
                    else if( MaxCiblScore>=750){                        
                        App.genesis__Interest_Rate__c=   getMCLR(365,BranchCodemap.get(App.Branch_Code__c),750);                        
                    }
                }
                else if(App.Record_Type_Name__c==Constants.PERSONALLOAN){
                    //  Nmax reject scenario
                    /*  if(App.Nmax__c<12){
App.Sanction_Message__c='Customer not eligible for loan based on age criterion';
App.Reject_Scenario__c='Minimum Tenure ineligible';
App.Sub_Stage__c=Constants.Sub_Stage_WorkInfoCaptured; 
}
*/
                    if(App.genesis__Account__r.Employment_Type__c==Constants.EmpType_Salaried){
                        if(MaxCiblScore>=800){
                            App.genesis__Interest_Rate__c=12;
                        }
                        else if(MaxCiblScore>=750 && MaxCiblScore<799){
                            App.genesis__Interest_Rate__c=14;
                        }
                        App.PMax1__c  =12* (App.genesis__Account__r.NMI_Claimed_By_Customer__c-App.genesis__Account__r.Existing_Emis__c);
                    }
                    else {
                        if(MaxCiblScore>=800){
                            App.genesis__Interest_Rate__c=14; // updated after discussion on 23/05/2018 - agin reupdated on 13-6-2018
                        }
                        else if(MaxCiblScore>=750 && MaxCiblScore<799){
                            App.genesis__Interest_Rate__c=16;
                        } 
                        App.PMax1__c       =3*App.genesis__Account__r.Net_Monthly_Income__c;
                    }
                }
                
                system.debug(' App.PMax1__c'+ App.PMax1__c);
                
                system.debug(MaxCiblScore);
                
                /* else{
system.debug('Cibil 123456');
App.Sub_Stage__c=Constants.Sub_Stage_WorkInfoCaptured; 
App.Reject_Scenario__c=Constants.CIBILRejectMessage;

} */
                
                // EMI
                
                App.EMI_Max__c=APPEmi.get(App.id);
                
                UpdateAppList.add(app);
            }
            system.debug('******'+UpdateAppList+'#####'+AcclISt);
            update UpdateAppList;
            update AcclISt;
        }
        catch(exception e){
            system.debug('error'+e.getMessage()+'line Number'+e.getLineNumber());
        } 
    }
    
    public static Decimal getMCLR(Integer Month,string Cat,Integer score){
        if(Cat==null && score==null){
            return[select COD_Term_From__c,MCLR_Of_Interest__c from MCLR_Master__c where COD_Term_From__c=:Month limit 1].MCLR_Of_Interest__c;
        }
        else{
            //return[select     FInal_Interest__c,  Cibil_Score__c from MCLR_Master__c where Retail_Category__c=:Cat  and   Cibil_Score__c=:score].FInal_Interest__c;
            Decimal MCLRInt = [select COD_Term_From__c,MCLR_Of_Interest__c from MCLR_Master__c where COD_Term_From__c=:Month limit 1].MCLR_Of_Interest__c;
            Decimal MCLR_Excess_Code = [Select ID,Excess__c from MCLR_Excess_Code__c where Retail_Category__c =:Cat AND From_CIBIL__c=:score Limit 1].Excess__c;
            Decimal TotalMCLR = MCLRInt + MCLR_Excess_Code;
            return TotalMCLR;
        }
    }
    
    public static integer NmaxCalculation(Integer age,Boolean Fa,string Recordtype, Boolean CoCheck){
        Integer Nm;
        Integer ageVal;
        if(CoCheck == true){
            ageVal = 900;
        }else if(CoCheck == false){
            ageVal = 840;
        }
        if(Recordtype==Constants.HOMELOAN){
            if(Fa==true){
                Nm=Math.min(300,(ageVal-age)); //change added
            }
            else if(Fa==false){
                Nm=Math.min(300,(960-age));
            }
        }
        else if(Recordtype==Constants.LAPLOAN){
            if(Fa==true){
                Nm=Math.min(100,(840-age)); //change added
            }
            else if(Fa==false){
                Nm=Math.min(100,(960-age));
            }
        }
        
        
        
        return Nm;
    }
    
    
}