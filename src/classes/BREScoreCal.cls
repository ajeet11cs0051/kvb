/*
* Name    : BREScoreCal
* Company : ET Marlabs
* Purpose : This class is used to calculate BRE computation varibles value
* Author  : Subas
*/
public class BREScoreCal {
    public static void breCal(List<genesis__Applications__c> appList, Map<Id,genesis__Applications__c> oldList){
        try{
            for(genesis__Applications__c app : appList){ 
                system.debug('####Stage####'+app.Sub_Stage__c);
                if(oldList.get(app.Id).sub_stage__c <> app.Sub_Stage__c && app.Sub_Stage__c =='Loan Requirement Captured' && (app.Record_Type_Name__c == 'Home Loan' || app.Record_Type_Name__c == 'LAP')){                    
                    Calculate_Bre(app,null,null,null);
                }
                
            }
        }catch(Exception e){}
    }
    public static void Calculate_Bre(genesis__Applications__c app, Decimal MaxLoanamt, Decimal EMIMAX, String Appstage){
        try{
            List <ID> AppIDList = new List<ID>();            
            List<genesis__Application_Parties__c> partyList = [Select Id,genesis__Party_Account_Name__c From genesis__Application_Parties__c Where genesis__Application__c =: app.Id AND Active__c =: True AND genesis__Party_Type__c=:Constants.Co_Borrower];
            for(genesis__Application_Parties__c pt : partyList){
                AppIDList.add(pt.genesis__Party_Account_Name__c);                        
            }
            AppIDList.add(app.genesis__Account__c);
            List<Account> accList = [Select ID,Perfios_Captured__c,age__c,NMI_Approved__c,NMI_Claimed_By_Customer__c,Net_Monthly_Income__c,Total_Annual_other_Income_Claimed__c,CIBIL_Score__c,Average_Balances6_months__c,Total_ChequeECS_bounces__c,Employment_Type__c,Additional_Income_Amount1__c From Account Where ID IN: AppIDList AND Financial_Applicant__c =: True];                
            
            List <Integer> CibilList = new List<Integer>();
            Integer MaxCibil = 0;
            Integer cnt = 0;
            Decimal NetSalary = 0.00;
            Decimal EMINMI = 0;
            Decimal WeightedAge = 0.00;
            Decimal weightedAvg = 0.00;
            List<Integer> ageList = new List<Integer>();
            Integer Maxage = 0;
            Decimal AdditionalIncome =0.00;
            Decimal CASAValue = 0.00;
            Decimal CheckBounce = 0.00;
            Decimal EMICASA = 0.00;
            Decimal EMI = 0;
            Decimal TotalNMIClaimed = 0;
            Decimal TotalNMIApproved = 0;
            Decimal totalAdditionalClaim = 0;
			Decimal totalAdditionalApprov = 0;
            
            /**Code added for LAP**/
            //Property Type calculation
            List<clcommon__Collateral__c> collateralList = [select Id,Collateral_Uses__c,Collateral_Market_Value__c From clcommon__Collateral__c where genesis__Application__c =: app.Id AND Active__c =: true];
            integer r = 0;
            integer c = 0;
            Decimal Col_Value = 0;
            for(clcommon__Collateral__c col : collateralList){
                if(col.Collateral_Uses__c =='Residential'){
                    r++;
                }
                if(col.Collateral_Uses__c =='Commercial'){
                    c++;
                }
                Col_Value = Col_Value + (col.Collateral_Market_Value__c != null ? col.Collateral_Market_Value__c : 0);
            }
            if(collateralList.size() >0 && r > 0){
                app.Property_Type_BRE__c = 'Residential';
            }else if(collateralList.size() == c){
                app.Property_Type_BRE__c = 'Commercial';
            }
            
            //LTV calculation
            if(Col_Value != null && Col_Value != 0){
                app.LTV__c = ((app.genesis__Loan_Amount__c != null ? app.genesis__Loan_Amount__c : 0)/Col_Value) * 100;
            }
            
            String Category = RejectionScenarioHandeller.getBranchCity(app.Branch_Code__c,true);
            if(Category == 'A'){
                app.Location_Type__c = 'Metro or Urban';
            }else if(Category == 'B' || Category == 'C'){
                app.Location_Type__c = 'Others';
            }
            
            
            /**Code end for LAP**/
            List <Perfios__c> perfiosList = [Select Id,Cheque_Bounces__c,AvgBalance_Of_6_Months__c From Perfios__c Where Active__c =: true AND Applicant_Name__c IN: AppIDList AND Application__c =: App.Id];
            for(Account acc : accList){
                CASAValue = CASAValue + (acc.Average_Balances6_months__c != null ? acc.Average_Balances6_months__c : 0);
                CheckBounce = CheckBounce + (acc.Total_ChequeECS_bounces__c != null ? acc.Total_ChequeECS_bounces__c : 0);
            }
            for(Perfios__c per : perfiosList){
                //add CASA & Cheque bounse 
                if(CASAValue == 0){
                    CASAValue = CASAValue + (per.AvgBalance_Of_6_Months__c != null ? per.AvgBalance_Of_6_Months__c : 0);
                }  
                if(CheckBounce == 0){
                    CheckBounce = CheckBounce + (per.Cheque_Bounces__c != null ? per.Cheque_Bounces__c : 0);
                }                
            }            
            Map<Decimal,String> maxNmi = new Map<Decimal,String>();
            List<Decimal> NMICust = new List<Decimal>();
            Map<String,String> EmpType = new Map<String,String>();
            for(Account acc : accList){
                if(acc.CIBIL_Score__c<>null){
                    CibilList.add(Integer.ValueOf(acc.CIBIL_Score__c));
                }
                system.debug('%%%%%%'+acc.Net_Monthly_Income__c+'$$$$$$$'+acc.NMI_Approved__c);
                if(MaxLoanamt == null && EMIMAX == null){
                    WeightedAge = WeightedAge + (acc.age__c * acc.NMI_Claimed_By_Customer__c);
                }else{
                    WeightedAge = WeightedAge + (acc.age__c * acc.NMI_Approved__c);
                }
                if(acc.age__c<>null){
                    Decimal age = acc.age__c;// + (app.Maturity_Time__c != null ? app.Maturity_Time__c : 0);
                    ageList.add(Integer.ValueOf(age));
                }
                if(acc.Total_Annual_other_Income_Claimed__c<>null){
                    AdditionalIncome = AdditionalIncome + acc.Total_Annual_other_Income_Claimed__c; 
                }                                 
                
                if(acc.Employment_Type__c == 'Agriculturist'){
                    TotalNMIClaimed =TotalNMIClaimed + (acc.Net_Monthly_Income__c != null ? acc.Net_Monthly_Income__c : 0) + (acc.Additional_Income_Amount1__c != null ? (acc.Additional_Income_Amount1__c != 0 ? acc.Additional_Income_Amount1__c/12 : 0 ) : 0);
                    Decimal NMIC = (acc.Net_Monthly_Income__c != null ? acc.Net_Monthly_Income__c : 0) + (acc.Additional_Income_Amount1__c != null ? (acc.Additional_Income_Amount1__c != 0 ? acc.Additional_Income_Amount1__c/12 : 0 ) : 0);
                    maxNmi.put(NMIC,acc.Id);
                    NMICust.add(NMIC);
                }
                else if(acc.Employment_Type__c != 'Agriculturist'){
                    Decimal minVal = Math.min((acc.Additional_Income_Amount1__c !=null ? acc.Additional_Income_Amount1__c != 0 ? acc.Additional_Income_Amount1__c/12 : 0 :0),(((acc.Net_Monthly_Income__c != null ? acc.Net_Monthly_Income__c : 0)/0.7)*0.3));
                    TotalNMIClaimed = TotalNMIClaimed + (acc.Net_Monthly_Income__c != null ? acc.Net_Monthly_Income__c : 0) + Math.min(minVal,200000/12);
                    Decimal NMIC = (acc.Net_Monthly_Income__c != null ? acc.Net_Monthly_Income__c : 0) + Math.min(minVal,200000/12);
                    maxNmi.put(NMIC,acc.Id);
                    NMICust.add(NMIC);
                }
                 
                totalAdditionalClaim = totalAdditionalClaim + (acc.Additional_Income_Amount1__c != null ? acc.Additional_Income_Amount1__c : 0);
                EmpType.put(acc.Id,acc.Employment_Type__c);
            }
            NMICust.sort();
            Integer Ncount = NMICust.size()-1;
            Decimal MaxNMIValue = NMICust[Ncount];
            String MaxNMIID = maxNmi.get(MaxNMIValue);
            String BreEmpType = '';
            if(MaxNMIID != null){
                BreEmpType = EmpType.get(MaxNMIID);
            }
            if(BreEmpType == 'Self-Employed Professional'){
                app.Employment_Type__c = 'Others';
            }else{
                app.Employment_Type__c = BreEmpType;
            }
            
            if(TotalNMIClaimed != 0 && TotalNMIClaimed != null){
                app.Additional_Income_Total_Income__c = totalAdditionalClaim/TotalNMIClaimed;
            }
            
            //NetSalary = NetSalary + app.genesis__Account__r.Net_Monthly_Income__c; // Net Salary
            if(MaxLoanamt != null || EMIMAX != null){
                if(app.NMI_Approved__c <>null ){
                    NetSalary = app.NMI_Approved__c;
                    EMI = app.Sanctioned_EMI__c != null ? app.Sanctioned_EMI__c : 0;
                } 
            }else{
                if(app.NMI_Claimed_By_Customer__c <>null && app.NMI_Claimed_By_Customer__c >0){
                    NetSalary = app.NMI_Claimed_By_Customer__c;
                    EMI = app.genesis__Payment_Amount__c != null ? app.genesis__Payment_Amount__c : 0;
                }            
            }
            if(MaxLoanamt != null || EMIMAX != null){
                if(NetSalary <>null && NetSalary<>0){
                    if(EMIMAX != null){
                        EMINMI = (EMIMAX/NetSalary)*100; //EMI/NMI ratio
                    }else{
                        EMINMI = (app.genesis__Payment_Amount__c/NetSalary)*100; //EMI/NMI ratio
                    }                
                }            
            }else{
                if(NetSalary <>null && NetSalary<>0){
                    if(app.Sanctioned_EMI__c != null){
                        EMINMI = (app.Sanctioned_EMI__c/NetSalary)*100; //EMI/NMI ratio
                    }else{
                        EMINMI = (app.genesis__Payment_Amount__c/NetSalary)*100; //EMI/NMI ratio
                    }                
                }  
            }
            
            //WeightedAge = WeightedAge + (app.genesis__Account__r.age__c * app.genesis__Account__r.Net_Monthly_Income__c);
            if(NetSalary<>null && NetSalary<>0){
                weightedAvg = (WeightedAge / NetSalary); //Weighted Average income
                if(CASAValue != 0 && CASAValue != null){
                    EMICASA = (EMI/CASAValue) * 100;                    
                }
            }
            if(CibilList.size() >0){
                CibilList.sort();
                cnt = CibilList.size()-1;
                MaxCibil = CibilList[cnt];
                system.debug('@@'+MaxCibil);//External credit agency score(CIBIL)    
            }                                               
            
            ageList.sort();
            integer i = ageList.size()-1;
            Maxage = ageList[i];  //Max age of financial applicant        
            
            //Integer TotalRepayment = Integer.ValueOf(application.genesis__Term__c + application.Holiday_Period__c);  //Total Repayment period
            
            if(AdditionalIncome==0.00 && NetSalary <>0.00){
                app.Composition_of_Income__c = 1; //ITR
            }
            else if(AdditionalIncome<>0.00 && NetSalary <>0.00){
                app.Composition_of_Income__c = 2; //Additional and ITR
            }
            else if(AdditionalIncome<>0.00 && NetSalary == 0.00){
                app.Composition_of_Income__c = 3; //Additional income
            }
            system.debug('MaxCibil:-'+MaxCibil+'EMINMI:-'+EMINMI+'weightedAvg:-'+weightedAvg+'EMI/CASA:-'+EMICASA+'Net Salary'+NetSalary+'Maxage:-'+Maxage);
            if((EMINMI<>0 && EMINMI<>null)){                       
                app.EMINMI__c = EMINMI.round();
            }
            app.Max_CIBIL_Score__c = MaxCibil;
            app.Weighted_Average_Income__c = weightedAvg.round();
            if(MaxLoanamt == null || EMIMAX == null){
                app.Max_Age__c = Maxage + (app.Maturity_Time__c != null ? app.Maturity_Time__c : 0);
            }            
            app.Reject_Scenario__c = '';
            /* if((app.AVG_Balance__c<>null && app.AVG_Balance__c>0) && (NetSalary<>null && NetSalary<>0)){
//app.EMICASA__c = (NetSalary/app.AVG_Balance__c).round();                    
//Total_Cheque_Bounces__c                                       
} */
            if(EMICASA != null){                
                app.EMICASA__c = (EMICASA);
            }
            if(CheckBounce != null){
                app.Total_Cheque_Bounces__c = CheckBounce;
            }
            system.debug('@@@@'+app);
            //update app;
            if(MaxLoanamt != null || EMIMAX != null){
                system.debug('***MaxLoanamt***'+MaxLoanamt);
                app.Sanction_Authority_Limit__c = MaxLoanamt + (app.GMRA_Amount__c != null ? app.GMRA_Amount__c : 0);
                app.Sanctioned_EMI__c = EMIMAX;
                if(app.Age_Of_The_Building__c >= 30 && app.Age_Of_The_Building__c <= 50 && app.Residual_Life_of_Property__c != null && app.Residual_Life_of_Property__c != 0){
                    app.Sanctioned_Tenure__c = (app.Residual_Life_of_Property__c/1.5).round();
                    app.Max_Age__c = Maxage + ((app.Residual_Life_of_Property__c/1.5)/12);
                }else{
                    app.Sanctioned_Tenure__c = app.genesis__Term__c;  
                    app.Max_Age__c = Maxage + (app.genesis__Term__c/12);
                }
                if(EMICASA != null){
                    app.EMICASA__c = EMICASA;
                }                
                app.Sub_Stage__c = Appstage;//'All NSTP Processes Completed';
                app.Perfios_Captured__c = true;
                update app;
             }
        }catch(Exception e){}
    }
    //Updating perfios check for BRE
    public static void updatePerfioscheck(List<genesis__Applications__c> appList, Map<Id,genesis__Applications__c> oldList){
        integer perInt = 0;
        try{
            for(genesis__Applications__c app : appList){ 
                if(oldList.get(app.Id).sub_stage__c <> app.Sub_Stage__c && app.Sub_Stage__c =='Work Information Captured' && (app.Record_Type_Name__c == 'Home Loan' || app.Record_Type_Name__c == 'LAP' || app.Record_Type_Name__c == 'Personal Loan')){                
                    List <Account> accList = queryService.accList(app.Id);
                    for(Account acc : accList){
                        if(acc.Perfios_Captured__c == true){
                            perInt++;
                        }
                    }
                    if(accList.size() == perInt){     
                        if(app.Perfios_Captured__c != true){
                            app.Perfios_Captured__c = true;   
                        }
                    }else{
                        app.Perfios_Captured__c = false;
                    }
                }                              
            }    
        }catch(Exception e){
            system.debug('Exception::'+e+e.getLineNumber());
        }
    }
}